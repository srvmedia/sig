 <style>

.margintop{
  margin-top: 1%;
}
.paddingleft{
  padding:0 3% 0 3%;
}
p{
  text-align: center;
  }
  .subtext{
    font-family: "Roboto Slab";
    font-weight: 600;
    font-size: 35px;
    text-align: center;
    background: linear-gradient(330deg, #fac325 0%, #080808 25%, #080808 50%, #fac325 75%, #e05252 100%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
  }

  .maintitle{
    font-weight: 600;
  }
  .leftmenu{
    border: solid 3px #7289b1;
    margin:2%;
    padding: 0 0 0 0;
   }
  .leftmenu ul li{
    padding-left: 5%;
  }
  ul{
    list-style: none;
  }
  a {
    color: #366dca;
  }
 .leftmenu h3{
    color: #422673;
    text-align: center;
    padding: 2%;
    margin-bottom: 0px;
    margin-top: 0%;
    background: #c6d6f0;
    border-bottom: solid 3px #7289b1;
  }
  .rightmenu{
    border-top: solid 3px #7289b1;
    padding-top: 3%;
  }
  #imagegrad .thumbnail{
    float: right;
  } 
  .tit{
    color: #3f2873;
    font-size: 23px;
    font-weight: 600;
    margin-top: 0;
  }
  .rightmenu p{
    font-size: 16px;
    color: #3c71cb;
    margin-left:3%; 
    text-align: justify;
  }
  .clubheight{
    margin-top: 4%;
  }
  .tit{
     margin-left: 3%;
  }
 
 </style>
<section>
    <div id="grad1" style="text-align:center;color:#888888;font-size:40px;font-weight:bold">
      <div class="row onimagemenu">
          <div class="col-lg-12 col-md-12 onimage">
            <p><h3 class="subtext"><span style="color:red">C</span>ommunity</h3></p>
          </div>          
      </div>
    </div>
      
</section>
<section>
  <div class="col-lg-2 col-md-2 leftmenu" >
    <h3>Alumni Directory</h3>
    <h3>Professional Clubs</h3>
    <h3>Entrepreneurship Club</h3> 
    <h3>Ask for info or support</h3>     
  </div>
   <div class="col-lg-10 col-md-10 rightmenu">
      <div class="col-lg-4 col-md-4">
          <div id="imagegrad">
            <div class="row">
              <img src="<?php echo base_url();?>assets/images/sbs-operations.png" class="thumbnail">
            </div>
          </div>
      </div>
      <div class="col-lg-8 col-md-8 ">
         <h2 class="tit">Alumni Directory</h2>
         <p>Find and connect with fellow Alumni form your batch, in your city or
            geography.</p>
          <p>Login for more information.</p>
      </div>
      <div class="clearfix"></div>
       <div class="col-lg-4 col-md-4 clubheight">
          <div id="imagegrad">
            <div class="row">
              <img src="<?php echo base_url();?>assets/images/sbs-finance.png" class="thumbnail">
            </div>
          </div>
      </div>
      <div class="col-lg-8 col-md-8 clubheight">
         <h2 class="tit">Professional Clubs</h2>
         <p>Subscribe to your tribe (your community). Discuss, ideate, share, actively
         engage in professional clubs and forums focusing on specific professional
         topics like CX, CRM, Strategy, Innovation, Digital, E-commerce, IOT, Social,
         Mobile, Big Data, Analytics, Advertising & Media, Sales, Marketing, Branding,
         Fin Tech, Block Chain, Human Capital, Future of Work, Not-For-Profit and
         many more.</p>
         <p>Get connected to industry focus groups – Automotive, Retail, IT, Public
         Sector, Education, Hospitality, others.</p>
         <p>Want to start a specific Club - submit your request to Start a Club and drive it forward.</p>
         <p>Login for more information.</p>
      </div>
      <div class="clearfix"></div>
       <div class="col-lg-4 col-md-4 clubheight">
          <div id="imagegrad">
            <div class="row">
              <img src="<?php echo base_url();?>assets/images/sbs-operations.png" class="thumbnail">
            </div>
          </div>
      </div>
      <div class="col-lg-8 col-md-8 clubheight">
         <h2 class="tit">Entrepreneurship Club</h2>
         <p>Share your business venture information, float business ideas, visit the start-
         up corner, seek funding, access resources and much more.</p>
         <p>Login for more information.</p>
      </div>
      <div class="clearfix"></div>
       <div class="col-lg-4 col-md-4 clubheight">
          <div id="imagegrad">
            <div class="row">
              <img src="<?php echo base_url();?>assets/images/sbs-finance.png" class="thumbnail">
            </div>
          </div>
      </div>
      <div class="col-lg-8 col-md-8 clubheight">
         <h2 class="tit">Ask for info or support</h2>
         <p>Reach out to the Alumni community based on region, geo, country, state, city
         for specific information and get going. Moving to a new location, city ask for
         info or support around as basic and general information as getting around
         and as specific questions such memberships to various professional
         associations and certifications in your domain.</p>
         <p>Login for more information.</p>
      </div>
   </div>
</section>
