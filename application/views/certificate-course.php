<style type="text/css">
.geoinformatics{
    width: 100%;
    height: 320px;
    background: linear-gradient(0deg, rgba(0, 0, 0,0.4), rgba(0, 0, 0,0.4)), url("<?php echo base_url();?>assets/img/acadamics/Remote-Sensing.jpg") no-repeat;
    background-size: cover;
}
/*  bhoechie tab */
div.bhoechie-tab-container{
  z-index: 10;
  background-color: transparent;
  padding: 0 !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  border:1px solid #ddd;
  margin-top: 20px;
  margin-left: 50px;
  -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  box-shadow: 0 6px 12px rgba(0,0,0,.175);
  -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  background-clip: padding-box;
  opacity: 0.97;
  filter: alpha(opacity=97);
}
div.bhoechie-tab-menu{
  padding-right: 0;
  padding-left: 0;
  padding-bottom: 0;
}
div.bhoechie-tab-menu div.list-group{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a .glyphicon,
div.bhoechie-tab-menu div.list-group>a .fa {
  color: #b42c2f;
}
div.bhoechie-tab-menu div.list-group>a:first-child{
  border-top-right-radius: 0;
  -moz-border-top-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a:last-child{
  border-bottom-right-radius: 0;
  -moz-border-bottom-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a.active,
div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
div.bhoechie-tab-menu div.list-group>a.active .fa{
  background-color: #b22629;
  background-image: #5A55A3;
  color: #ffffff;
}
div.bhoechie-tab-menu div.list-group>a.active:after{
  content: '';
  position: absolute;
  left: 100%;
  top: 50%;
  margin-top: -13px;
  border-left: 0;
  border-bottom: 13px solid transparent;
  border-top: 13px solid transparent;
  border-left: 10px solid #b42c2f;
}
.subdivclass {
    background-color: #d0cece;
    color: #b02d29;
    border-right: 2px solid #fff;
} 
li.subdivclass{
  padding-left: 0px;
  padding-right: 0px;
}   
div.bhoechie-tab-content{
  background-color: #ffffff;
  /* border: 1px solid #eeeeee; */
  padding-left: 20px;
 /* padding-top: 10px;*/
}

div.bhoechie-tab div.bhoechie-tab-content:not(.active){
  display: none;
}
.containermar{
  margin:10% 0 8% 0; 
}
 .rightpad{
      border-right: 2px solid #f19999;
    }
    .rightpad li.active{
      background: url("<?php echo base_url();?>assets/img/liback.png") no-repeat;
      background-position: center right;
      padding: 10px 0px 10px;
    }
    .menutop li.active a{
      display: inline;
    }
    .menutop.nav>li>a {
      padding: 3px 15px;
      margin-bottom: 12px;
    }
.sub-menu{
    text-align: center;
    font-size: 20px;
    font-weight: 600;
    color: #c42027;
}
</style>
<div class="row aboutop">
    <div class="geoinformatics"></div>
</div> 
<div class="container">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 menutop list-group bhoechie-tab-menu">
               <ul class="nav nav-pills nav-stacked rightpad">
                  <li class="active"><a class="liborder" data-toggle="tab" href="#Overview">Certificate Courses in P & R.S</a></li>
                  <li><a class="liborder" data-toggle="tab" href="#careerportal">Career Potential</a></li>
                  <li><a class="liborder" data-toggle="tab" href="#duration">Duration</a></li>
                  <li><a class="liborder" data-toggle="tab" href="#eligibility">Eligibility & Fee Structure</a></li>
                  <li><a class="liborder" data-toggle="tab" href="#admissionprocess">Admission procedure</a></li>
                   <li class="sub-menu">Programme Offered by SIG</li>
                   <li><a class="liborder" href="<?php echo base_url(); ?>MScinGeoinformatics">M.Sc.(Geoinformatics)</a></li>
                   <li><a class="liborder" href="<?php echo base_url(); ?>MTechGeoinformatics">M.Tech. Geoinformatics & ST</a></li>
                   <li><a class="liborder" href="<?php echo base_url(); ?>environment">M.Sc. Environment & Sustainability</a></li>
                   <li><a class="liborder" href="<?php echo base_url(); ?>courseinSpatial">Certificate Course in Spatial Economics</a></li>
                </ul>
              </div>           
              <div class="tab-content col-lg-9 col-md-9 col-sm-12 col-xs-12 bhoechie-tab">
                <!-- flight section -->
                <div id="Overview" class="tab-pane fade in active">
                         <h2 class="menutitle">
                            Certificate Courses in P &amp; R.S      
                         </h2>
                           <hr>
                          <table class="contentpaneopen">
                          <tbody><tr>
                          <td valign="top">
                          <p>The geoinformatics industry in India is at a niche stage. While the MSC & Mtech in Pune at SIG is able to address the demands of creating a sustainable talent pool for the geoinformatics industry, there is still a huge bunch of professionals looking to add to their geoinformatics expertise through certification courses. As an acknowledgement of this trend, SIG has introduced a series of certification courses that can help you add to your academic qualification in relatively lesser time. In fact, SIG also offers customized certification courses to address sector specific demands. Here is a primer to the various certification options available at SIG.</p>
                     
                          <h2 class="menutitle">About the Programme</h2>
                          <p>Symbiosis has introduced certificate programme in Digital Photogrammetric Mapping &amp; Remote Sensing to meet the demand of trained professional human resource. This programme emphasizes on creating plug and play manpower for Photogrammetric Mapping Industry and is designed by industry professionals to incorporate the quality and standards as desired by the mapping companies. Students spend time in Laboratory to learn the real-time Mapping methodologies and get experience of on the job training.</p>
                                               
                          <div class="cleardiv"></div>
                          </td>
                          </tr>

                          </tbody></table>
                </div>
                <!-- train section -->              
                <div id="careerportal" class="tab-pane fade bhoechie-tab-content">
                   <!--   <a href="/admissions/msc-geoinformatics.html" style="float:right;margin-top: -9px;"><img src="<?php echo base_url();?>assets/img/admissionbtn.jpg" title="M.Sc. In Geoinformatics" alt="M.Sc. In Geoinformatics"></a> -->
                      <h2 class="menutitle">Career Potential</h2>
                      <hr>
                      <p> Through public and private sector joint ventures Government of India has launched an initiative through projects to map major Indian cities with the help of Photogrammetric Mapping technology. This highly accurate method of creating 3D maps will help the engineers and scientists to design and build a highly developed and prosperous India of tomorrow. We envision the requirement of thousands of trained professionals in near future to meet this demand. In addition to domestic mapping work, there has been tremendous mapping workflow from countries like North and South America, Europe, Middle East, Africa, and so on.</p>
                </div>
                 <div id="duration" class="tab-pane fade bhoechie-tab-content">
                      <h2 class="menutitle">Duration</h2>
                      <hr>
                          <p>6 weeks, 5 days a week (Full Time)</p>
                          <div class="ccp-first">
                           <h2 class="menutitle">Course Content</h2>
                          <ul class="bullet-style">
                          <div class="col-lg-6">
                          <li>Computer Fundamentals </li>
                          <li>Mapping Science </li>
                          <li>Photogrammetry Fundamentals</li>
                          <li>Aerial Photography</li>
                          <li>Digital Image</li>
                          <li>Image Processing</li>
                          </div>
                          <div class="col-lg-6">
                          <li>Orthorectification</li>
                          <li>LIDAR Mapping</li>
                          <li>Remote Sensing</li>
                          <li>Personality Development &amp; Softskill</li>
                          <li>Interview Techniques</li>
                          </div>
                          </ul>

                          </div>
                          <div class="clearfix"></div>
                          <div class="ccp-second">
                          <h2 class="menutitle">Key Features</h2>
                          <ul class="bullet-style">
                          <li>State of the art Lab</li>
                          <li>Training by industry professionals</li>
                          <li>Project oriented training methodology</li>
                          <li>10 large scale Mapping projects simulation</li>
                          <li>Emphasis on American and European mapping methods</li>
                          </ul>
                          </div>
                </div>
                    <div id="eligibility" class="tab-pane fade bhoechie-tab-content">
                      <h2 class="menutitle">Eligibility</h2>
                      <hr>
                      <p>
                        HSC/Diploma/Graduate passed from any discipline. Final year students awaiting results may apply. Students are to certify that they are not color blind and can view 3D images using goggles.</p>
                      <h2 class="menutitle">Fee Structure</h2> 
                      <p>The programme fee is Rs. 25,000/-(Rupees Twenty Five Thousand Only). Rs 20,000 course fee and Rs 5,000 caution money. This fee should be paid in one single installment by DD drawn in favor of 'The Director, Symbiosis Institute of GeoInformatics' and payable at Pune, Maharashtra. The course fee is not refundable. </p>
                </div>
                    <div id="admissionprocess" class="tab-pane fade bhoechie-tab-content">
                      <h2 class="menutitle">Admission Procedure</h2>
                      <hr>
                      <p> Application form is available on the website. Students can download the application form and submit in the institute on payment of Rs 200/- in cash / DD in favor of 'The Director, Symbiosis Institute of Geoinformatics', payable at Pune at the address mentioned below The Director</p>
                      <p><strong>Address :</strong> Symbiosis Institute of Geoinformatics 5th Floor Atur Centre,</p>
                      <p> Gokhale cross road, Model Colony Pune – 411016</p> 
                      <p>The course commences on 10th March 2014. The last date for the payment of fees is 3rd March 2014.</p>
                </div>
                 
            </div>
        </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
</script>