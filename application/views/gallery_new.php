<style type="text/css">
 .jg_photo{
  width: 100%;
 }
 .img-responsive{
  min-height: 195px !important;
  min-width: 260px !important;
  max-height: 195px !important;
  max-width: 260px !important;
 }
 #portfolio .mix{
  display: none;
 }
 .filter.active{
  color:#BB0C1A;
  text-decoration: underline;
 }
 .img-thumbnail:hover{
  box-shadow: 0px 0px 2px 2px #BB0C1A;
 }
 .mix{
  text-align: center;
 }
</style>
<!--<link href="--><?php //echo base_url(); ?><!--assets/plugins/revo-slider/css/settings.css" rel="stylesheet" type="text/css"/>-->
<link href="<?php echo base_url();?>assets/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<div class="aboutop">
 <div class="gallary"></div>
</div>

<div class="container">
 <div class="col-md-3 menutop">
  <ul class="nav nav-pills nav-stacked rightpad">
   <li><a class="filter liborder" data-filter=".mix">All</a></li>
   <!--            <li><a class="liborder filter" data-filter=".khulamaunch">Khula Maunch July 2015</a></li>-->
   <li><a class="liborder filter" data-filter=".uav">UAV</a></li>
   <li><a class="liborder filter" data-filter=".educationleadership">Education Leadership Award</a></li>
   <!--            <li><a class="liborder filter" data-filter=".batch201315skit">Batch_2013-15_Skit</a></li>  -->
   <!--            <li><a class="liborder filter" data-filter=".omitjune2012">QMTI June 2012</a></li>  -->
   <!--            <li><a class="liborder filter" data-filter=".dahihandi">Dahi Handi Celebration</a></li>  -->
   <li><a class="liborder filter" data-filter=".punedarshan">Pune Darshan</a></li>
   <li><a class="liborder filter" data-filter=".sigworkshop">SIG Workshop / Conference</a></li>
   <li><a class="liborder filter" data-filter=".guestvisit">Guest Visit</a></li>
   <li><a class="liborder filter" data-filter=".lifeatsig">Life at SIG</a></li>
   <li><a class="liborder filter" data-filter=".aluminimeet">Alumini Meet</a></li>
  </ul>
 </div>
 <div class="col-md-9 menutop">
  <table class="contentpaneopen">
   <tbody><tr>
    <h2 class="menutitle"><strong>Gallery</strong></h2>
   </tr>
   </tbody></table>
  <hr>
  <div class="inner-information">
   <ul id="portfolio" class="row sorting-grid list-unstyled">
    <li class="col-md-4 col-sm-6 col-xs-12 mix punedarshan" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/pune_darshan_20150812_1896611827.jpg" title="Pune Darshan" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/pune_darshan_20150812_1896611827.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix punedarshan" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/pune_darshan_20150812_2081543895.jpg" title="Pune Darshan" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/pune_darshan_20150812_2081543895.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix punedarshan" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/pune_darshan_20150812_1732216745.jpg" title="Pune Darshan" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/pune_darshan_20150812_1732216745.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix punedarshan" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/pune_darshan_20150812_1257105262.jpg" title="Pune Darshan" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/pune_darshan_20150812_1257105262.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix uav" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/uav_20150810_1775638220.jpg" title="UAV" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/uav_20150810_1775638220.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix educationleadership" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/lokmat_awards_photos_13-feb-2014_20140219_1019555820.jpg" title="Education Leadership Award" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/lokmat_awards_photos_13-feb-2014_20140219_1019555820.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix educationleadership" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/lokmat_awards_photos_13-feb-2014_20140219_1473417182.jpg" title="Education Leadership Award" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/lokmat_awards_photos_13-feb-2014_20140219_1473417182.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix educationleadership" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/lokmat_awards_photos_13-feb-2014_20140219_1325079364.jpg" title="Education Leadership Award" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/lokmat_awards_photos_13-feb-2014_20140219_1325079364.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix educationleadership" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/lokmat_awards_photos_13-feb-2014_20140219_2086617249.jpg" title="Education Leadership Award" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/lokmat_awards_photos_13-feb-2014_20140219_2086617249.jpg" alt="" width="100%">
      </a>
     </div>
    </li>


    <li class="col-md-4 col-sm-6 col-xs-12 mix sigworkshop" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_workshop_20120424_1920889415.jpg" title="SIG Workshop / Conference" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_workshop_20120424_1920889415.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix sigworkshop" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_workshop_20120424_1688172030.jpg" title="SIG Workshop / Conference" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_workshop_20120424_1688172030.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix sigworkshop" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_workshop_20120424_1253859022.jpg" title="SIG Workshop / Conference" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_workshop_20120424_1253859022.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix sigworkshop" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_workshop_20120424_1360672356.jpg" title="SIG Workshop / Conference" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_workshop_20120424_1360672356.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix guestvisit" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/guest_visit_20120424_1523395174.jpg" title="Guest Visit" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/guest_visit_20120424_1523395174.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix guestvisit" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/guest_visit_20120424_1237505724.jpg" title="Guest Visit" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/guest_visit_20120424_1237505724.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix guestvisit" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/guestvisit/DSC_0070.JPG" title="Guest Visit" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/guestvisit/DSC_0070.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix guestvisit" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/guestvisit/DSCF5085.JPG" title="Guest Visit" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/guestvisit/DSCF5085.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix guestvisit" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/guestvisit/IMG_8401.JPG" title="Guest Visit" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/guestvisit/IMG_8401.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix guestvisit" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/guestvisit/IMG_8423.JPG" title="Guest Visit" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/guestvisit/IMG_8423.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix guestvisit" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/guestvisit/IMG_8439.JPG" title="Guest Visit" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/guestvisit/IMG_8439.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_corner_20120424_1342524329.jpg" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_corner_20120424_1342524329.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_corner_20120424_2021525244.jpg" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_corner_20120424_2021525244.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_corner_20120424_1376806095.jpg" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_corner_20120424_1376806095.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_corner_20120424_1156253197.jpg" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_corner_20120424_1156253197.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_corner_20120424_2055796163.jpg" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/sig_corner_20120424_2055796163.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSC_0048.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSC_0048.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSC_0071.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSC_0071.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSC_0100.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSC_0100.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSCF0955.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSCF0955.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSCF4741.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSCF4741.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSCF5186.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSCF5186.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSCF5188.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSCF5188.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSCF5219.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSCF5219.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSCF5277.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_DSCF5277.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG20160611103510.jpg" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG20160611103510.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_0103.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_0103.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_0129.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_0129.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_0190.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_0190.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_0210.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_0210.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_0267.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_0267.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_0436.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_0436.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_1809.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_1809.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_1817.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_1817.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_1818.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_1818.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_9352.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_9352.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix lifeatsig" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_9353.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/siglife/sig_corner_IMG_9353.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix aluminimeet" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/aluminimeet/DSC04088.JPG" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/aluminimeet/DSC04088.JPG" alt="" width="100%">
      </a>
     </div>
    </li>
    <li class="col-md-4 col-sm-6 col-xs-12 mix aluminimeet" style="padding: 5px;">
     <div class="img-thumbnail" style="margin-right: 5px;">
      <a href="<?php echo base_url(); ?>assets/img/gallary/gallery_new/aluminimeet/P_20160827_150446.jpg" title="Life at SIG" data-rel="fancybox-button" class="zoomer fancybox">
       <img class="img-responsive lazy" src="<?php echo base_url(); ?>assets/img/gallary/gallery_new/aluminimeet/P_20160827_150446.jpg" alt="" width="100%">
      </a>
     </div>
    </li>
   </ul>
  </div>
 </div>
</div>

<script type="text/javascript">
 jQuery(document).ready(function () {

  jQuery(".fancybox").fancybox();

  jQuery("a#inline").fancybox({
   'hideOnContentClick': true
  });

  /* Apply fancybox to multiple items */

  jQuery("a.group").fancybox({
   'transitionIn': 'elastic',
   'transitionOut': 'elastic',
   'speedIn': 600,
   'speedOut': 200,
   'overlayShow': false
  });

 });

 jQuery(function () {
  jQuery('#portfolio').mixItUp();
 });

</script>