<style type="text/css">
    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }
</style>
<div class="row aboutop">
    <div class="bannerimg"></div>
</div>	
<div class="container">  
 <ul class="nav nav-pills nav-stacked col-md-3 menutop">
    <li><a class="liborder" data-toggle="tab" href="#Overview">Overview</a></li>
    <li><a class="liborder" href="http://siu.edu.in/HTML_SIU/index.php" target="_blank">Symbiosis International University</a></li>
    <li><a class="liborder" data-toggle="tab" href="#Visionaries">Vision & Mission</a></li>
    <li><a class="liborder" data-toggle="tab" href="#Visionarie">Visionaries</a></li>
    <li><a class="liborder" data-toggle="tab" href="#Director">Director</a></li>
    <li><a class="liborder" data-toggle="tab" href="#Symbiosisa">Symbiosis Society</a></li>
    <li><a class="liborder" data-toggle="tab" href="#SIUcommitte">SIU Committees</a></li>
    <hr>
    <p>Quick Links</p>
    <hr>
    <li><a class="liborder" data-toggle="tab" href="#collaboration">Collaboration</a></li>
    <li><a class="liborder" data-toggle="tab" href="#testimonials">Testimonials</a></li>
    <li><a class="liborder" data-toggle="tab" href="#careers">Careers in GIS</a></li>
    <li><a class="liborder" data-toggle="tab" href="#resource">Resource Centre</a></li>    
    <li><a class="liborder" data-toggle="tab" href="#sigcorner">SIG Corner</a></li>
    <li><a class="liborder" data-toggle="tab" href="#information">Information  As Per UGC Norms</a></li>
  </ul>

  <div class="tab-content col-md-9 menutop">
    <div id="Overview" class="tab-pane fade in active">
      <h2 class="menutitle">Overview</h2>
      <img src="<?php echo base_url();?>assets/img/inner-banner.jpg" class="img-responsive">
      <br>
      <p>India is one of the fastest growing economies and the contribution of Geoinformatics towards growth has been immense. In spite of having gained international recognition at a very early stage, Geoinformatics is gradually gaining popularity in the Indian subcontinent as well. Symbiosis Pune, was amongst the first to recognize this brewing change of trends and accordingly took initiatives that led to the inception of SIG in 2004. Once SIG came into existence, there was no looking back. In line with the legacy of Symbiosis colleges in pune, SIG provides its students with the best in terms of education and learning environment. Our courses have been aimed at not merely educating students but to create competent and expert human resources to meet the ever-growing demand of digitization, environmental impact, GIS development, map analysis, remote sensing, watershed management, photogrammetric mapping, navigation and e-governance.</p>

      <p>At SIG, our endeavor is to impart education and training in geospatial Technologies. Our programmes aim to create highly trained professional equipped with a cutting edge. It helps students the students to create their own career roadmap. </p>

      <p>SIG has a distinction of maintaining 100% campus project cum placement record of its eligible and qualified students seems inception. </p>

      <p>SIG offers a variety of courses including M. Sc. Geoinformatics, M.Tech Geoinformatics and Surveying Technology, Post Graduate Diploma in Geoinformatics, Post Graduate Diploma in Disaster Management and Certificate Course Photogrammetry and Remote Sensing and customised certificate courses. </p>
      <p>Over the years SIG has identified the key needs of geospatial industry and has enriched the syllabus in terms of both theory and practical. At SIG, we take all the efforts to pedagogue various aspects of training from basic mapping science to GIS application development, analytical decision-making, softskills and personality development.</p>

    </div>
 <!--    <div id="Symbiosisiu" class="tab-pane fade">
      <h3>Menu 1</h3>
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div> -->
    <div id="Visionaries" class="tab-pane fade">
     <h2 class="menutitle">Vision & Mission</h2>
      <img src="<?php echo base_url();?>assets/img/inner-banner.jpg" class="img-responsive">
      <br>
      <h3>VISION</h3>
      <p>Promoting international understanding through quality education</p>
      <h3>MISSION</h3>
      <ul>
        <li>to inculcate the spirit of 'Vasudhaiva Kutumbakam' (the world is one family)</li>
        <li>to contribute towards knowledge generation and dissemination</li>
        <li>to promote ethical and value-based learning</li>
        <li>to foster the spirit of national development</li>
        <li>to inculcate cross-cultural sensitization</li>
        <li>to develop global competencies amongst students</li>
        <li>to nurture creativity and encourage entrepreneurship</li>
        <li>to enhance employability and contribute to human resource development</li>
        <li>to promote health and wellness amongst students, staff and the community</li>
        <li>to instill sensitivity amongst the youth towards the community and environment</li>
        <li>to produce thought provoking leaders for the society</li>
      </ul>
    </div>
    <div id="Visionarie" class="tab-pane fade">
     <h2 class="menutitle">Visionaries</h2>
      <img src="<?php echo base_url();?>assets/img/inner-banner.jpg" class="img-responsive">
      <br>
     <p>SIG has been incepted with a vision to cater to the ever evolving demands of the geoinformatics industry. Its program, curriculum, method of conducting the course, etc. have been a result of extensive research supported by the guidance of eminent visionaries. </p>
     <br> 
       <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#President">President and Founder Director</a></li>
        <li><a data-toggle="tab" href="#Principal">Principal Director</a></li>
        <li><a data-toggle="tab" href="#Chancellor">Vice Chancellor</a></li>
      </ul>
      <div class="tab-content col-md-12 menutop">
      <div id="President" class="tab-pane fade in active">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-xs-6"><img src="<?php echo base_url();?>assets/img/padmashree.jpg" ></div>
            <div class="col-lg-9 col-md-9 col-xs-6">
                <h3><strong>Padma Bhushan Dr. S.B. Mujumdar</strong></h3> 
                <p>Chancellor,</p>
                <p>Symbiosis International University,</p>
                <p>President and Founder Director,</p>
                <p>Symbiosis Society</p>
            </div>
        <p>Padma Bhushan Dr. S.B. Mujumdar, visionary and architect of modern education, sowed the seeds of Symbiosis 36 years ago as a humanitarian gesture to help international students. Today it has grown into a large tree with 36 branches. Symbiosis has now emerged as a global leader in providing quality education from kindergarten to post-graduate levels.</p> 

        <p>The 34 Symbiosis institutes are a reflection of his selfless efforts to provide quality learning- opportunity to students of all ages and at all levels.</p> 

        <p>In recognition of his contribution to modern education, the Govt. of India conferred Dr. S.B. Mujumdar with the prestigious honour of 'Padmashree' on Republic Day, 2005.</p>
        <h3 class="menutitle">Message</h3> 
        <p>GIS has been playing a pivotal role in the ever changing and fast developing technological environment, essential for social, economic and overall progress of our nation. SIG has opened vistas of opportunities for young minds from multifarious disciplines to excel in this new niche' field by way of its Post Graduate and other programmes.</p> 
        <p>In the ensuring years, GIS is bound to be much in demand in view of rapid progress of our nation and plans for overall development in infrastructure facilities, power generation sector, agriculture and many such initiatives related to socio-economic development. This would benefit both the industry and our students immensely. SIG has thus been making a valuable contribution to this cause of nation building.</p>
        <p>I wish SIG and its students all the success in their endeavours!</p>
        <h3 class="menutitle">Felicitations / Honours received</h3>
        <ul>
        <li><strong>The Padma Bhushan Award - 2012</strong></li>
        <li>At the hands of his excellency Smt. Pratibhatai Patil, President of Indian</li>
        <li><strong>Giants International Lifetime Achievement Award 2009</strong></li>
        <li><strong>The Punya Bhushan Award - 2009</strong></li>
        <li>At the hands of His Excellency Shri Mohammad Hamid Ansari, Vice . President of India</li>
        <li><strong>The Padmashree Award - 2005</strong></li>
        <li>At the hands of His Hon. President of India, Dr. A.P.J. Abdul Kalam.</li>   
        <li><strong>Pune Municipal Corporation -Yashwantrao Chavan Puraskar - 1998</strong></li>
        <li>At the hands of Shri. Ramakrishna Hegde, Former Union Minister for Commerce.</li>
        <li><strong>Rotary Club, Pune - Service Excellence Recognition Award (SERA) 1998</strong></li>
        <li><strong>Bandhuta Pratishthan Award - Promotion of Brotherhood (1997) Social Service to the downtrodden</strong></li>
        <li><strong>Institute of Marketing & Management - Excellence in Education & Top Management Club Award </strong></li>
        <li>At the hands of Dr. P.C Alexander, Governor of Maharashtra</li>
        <li><strong>Rotary Club of Pune Award- Promotion of International Understanding</strong></li>
        <li><strong>Pune Municipal Corporation - Outstanding College Teacher Award</strong></li> 
        <li>At the hands of Jayantrao Tilak</li>
        </ul>
        </div>
      </div>
       <div id="Principal" class="tab-pane fade in">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-xs-6"><img src="<?php echo base_url();?>assets/img/vidya.jpg" ></div>
            <div class="col-lg-9 col-md-9 col-xs-6">
                <h3><strong>Dr Vidya Yeravdekar </strong></h3> 
                <p>Principle Director and Acting Vice Chancellor,</p>
            </div>
        </div>
        <br>
        <div class="row">    
        <p>Dr. Vidya Yeravdekar, the Principal Director of Symbiosis Society is a Gynecologist, who also holds a degree in law. She has been following in the footsteps of her illustrious father, Dr S. B. Mujumdar in her zeal for social and educational work. She has traveled extensively abroad, visiting countries like UK, USA, UAE, Thailand, Singapore, Malaysia, Tanzania, Kenya and Ethiopia. She has participated in many educational fairs, to interact with students and parents in an attempt to promote Indian higher education abroad. </p> 

        <p>With her extensive contacts with various Ministries of Education in different countries. She has been able to address to specific needs of the youth of those countries. Under her guidance, these needs are met and provided for, in an attempt to increase the comfort level of the foreign students at Symbiosis.</p> 

        <p>Dr. Vidya's main interest is to carry forward the vision of her father, Dr. S. B. Mujumdar - to promote international understanding through quality education. Being a member of the Board of Management of the Symbiosis Society and Symbiosis International University, she contributes to the overall planning and development of Symbiosis. She has been instrumental in collaboration of Symbiosis with some of the world-class universities. She is also member of the board for Indian Council for Cultural Relations and University Grants Commission.</p>
        <h3 class="menutitle">Message</h3> 
        <p>Symbiosis Society established Symbiosis Institute of Geoinformatics, (SIG) in the year 2004. From its modest beginning since then, it has today grown into a fully integrated Geoinformatics institution of national repute.</p> 
        <p>The guiding philosophy of the institution has emphasised on creation of knowledge and influencing management practices, in order to integrate many facets of the GIS discipline. SIG aims to achieve objectives of imparting education, combined with creation, dissemination and application of knowledge, in an integrated form, to create a synergetic impact. </p>
        <p>SIG fosters and nurtures leadership qualities amongst young generation to make them capable of making difference in the Geoinformatics implementation in corporate and non-corporate sectors. It inculcates human values and professional ethics in the students, which help them make decisions and create path that are good not only for them, but also are good for the society, for the nation, and for the world as whole, in keeping with symbiosis logo of 'Vasudeva Kutumbakam', that is , the world is one family. </p>
        <p>The Institute has made valuable contribution to national causes by undertaking social projects for environmental protection and pollution control. This has helped the faculty, staff and students in application of their knowledge to live projects. </p>
        <p>I wish SIG staff and students success in their endeavours and future growth!</p> 
        </div>
      </div>
       <div id="Chancellor" class="tab-pane fade in">
      <div class="row">
            <div class="col-lg-3 col-md-3 col-xs-6"><img src="<?php echo base_url();?>assets/img/rajani.jpg" ></div>
            <div class="col-lg-9 col-md-9 col-xs-6">
                <h3><strong>Dr. Rajani R.Gupte </strong></h3> 
                <p>Vice – Chancellor ,</p>
                <p>(M.A, M.Phil, Ph.D. (Economics))</p>
            </div>
         </div>
         <br>
        <div class="row">    
        <p>Dr. Rajani Gupte, the Pro Vice – Chancellor of Symbiosis International University [SIU] , Pune has done her Ph.D in Economics from the reputed Gokhale Institute of Economics and Politics, Pune. She has more than 30 years of rich work experience in teaching and research at various prestigious institutes like Loyala College, Madras, Gokhale Institute of Politics and Economics, Pune. She has worked as a Finance Director for about six years for a business organization at Singrauli, Madhya Pradesh. She was a Founding member of Symbiosis Institute of Foreign Trade, SIFT in 1992 (now Symbiosis Institute of International Business , SIIB ) and was Director of SIIB from 2004 – 12 . She was instrumental in establishing SIIB as one of the leading B-Schools in India, and in starting innovative niche programs at SIIB in Agribusiness and Energy and Environment. Under her leadership, SIIB was the first Institute under SIU to start a Dual degree program with a German University and several active International collaborations. She has been a visiting faculty at the Oakland University, School of Business Administration Michigan, USA. She has also addressed Business leaders at Bremen University for applied Sciences, Bremen, Germany. She was appointed on the WTO cell set up by Govt. of Maharashtra, Department of Horticulture. She is a member of the International trade panel, CII, Western Region, She was the member of the Advisory Board, Chemtech World Expo 2009 – 2011. She is a UGC nominee on the Advisory Council of Kalasalingam University, Chennai. She is also the Dean of the Faculty of Management of the Symbiosis International University.</p> 

        <p>She has published a book entitled "International Business Management". She has participated in several seminars organised by the FIEO, Indo American Chamber of Commerce, Mahratha Chamber of Commerce. CII, and other Industry Associations.</p> 

        <p>Her areas of specialization are WTO, International Trade Policy, Non- Tariff Barriers, Subsidies etc.</p>        
        </div>
      </div>
      </div>
    </div>
    <div id="Director" class="tab-pane fade">
      <h2 class="menutitle">Director</h2>
       <img src="<?php echo base_url();?>assets/img/inner-banner.jpg" class="img-responsive">
       <br>
       <div style="margin-top:3%;"></div>
       <div class="row">
            <div class="col-lg-3 col-md-3 col-xs-6"><img src="<?php echo base_url();?>assets/img/tp_singh.jpg" ></div>
            <div class="col-lg-9 col-md-9 col-xs-6">
                <h3><strong>Dr. T. P. Singh </strong></h3> 
                <p>Director</p>
            </div>
        </div>
        <br>
        <div class="row">
        <p>Dr. T.P. Singh has vast experience in the field of Geospatial Technology in India and abroad. He has started his career in Geospatial technology from the Indian Institute of Remote Sensing, where he has worked on the projects of national importance. He has earned his PhD degree in the area of satellite Image Classification techniques from HNB Garhwal Central University, Srinagar and M.Phil in Geo Engineering from College of Engineering, Vishakhapatnam. Dr. Singh has received Master Degree in remote sensing from Pierre and Marie Curie University (UPMC), Paris University VI, France and M.Sc. from University of Lucknowin Environmental Science. He has received the European Professional Higher Post Graduate Diploma in Geoinformatics from GDTA (Institute under French Space Agency) in association with Paris University VI France, University of Lisbon, Portugal and Warsaw University of Technology Poland. Dr. Singh has worked on LiDAR technology at the University of Freiburg , Germany on Automatic feature extraction. Before, taking academic position at Symbiosis, he has served at different government centres. Dr Singh is the member of many professional bodies and Vice Chairman of Indian Society of Geomatics, Pune Chapter. He has Edited many books in the field of Geospatial Technology, Climate Change and Natural Resource Management and published several papers in the peer reviewed journals.</p>
        </div>
    </div>
    <div id="SIUcommitte" class="tab-pane fade">
      <h2 class="menutitle">SIU Committees</h2>
       <img src="<?php echo base_url();?>assets/img/inner-banner.jpg" class="img-responsive">
       <br>       
        <p> 
           <table class="contentpaneopen">
            <tr>
            <td valign="top"></td>
            <div id="click-menu59-4" class="box1-open">
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
            <tr>
            <td><a class="inbox1" target="_blank" href="<?php echo base_url();?>assets/img/pdf/academic-council.pdf">Academic Council</a></td>
            </tr>
            <tr>
            <td><a class="inbox1" target="_blank" href="<?php echo base_url();?>assets/img/pdf/board-of-management.pdf">Board of Management</a></td>
            </tr>
            <tr>
            <td><a class="inbox1" target="_blank" href="<?php echo base_url();?>assets/img/pdf/board-of-university-development.pdf">Board of University Development</a></td>
            </tr>
            <tr>
            <td><a class="inbox1" target="_blank" href="<?php echo base_url();?>assets/img/pdf/finance-committee.pdf">Finance Committee</a></td>
            </tr>
            <tr>
            <td><a class="inbox1" target="_blank" href="<?php echo base_url();?>assets/img/planning-monitoring-board.pdf">Planning Monitoring Board</a></td>
            </tr>
            </table>
            </p>
    </div>
     <div id="collaboration" class="tab-pane fade">
       <h2 class="menutitle">Collaboration</h2>
       <img src="<?php echo base_url();?>assets/img/collaboration.jpg" class="img-responsive">
       <br>  
       <p>Symbiosis has always believed in giving its students a global level learning platform that aids in improving their learning and collaborations are an effort in this direction. SIG, being a part of Symbiosis Pune works extensively towards this goal by establishing successful collaborations within the country. Through its collaborations, SIG students get an opportunity to learn on a larger scale and therefore develop a stronger academic background in Geoinformatics</p>
       <ul>
          <li>SIG collaborates with the National Remote Sensing Centre, Indian Space Research Organisation to execute projects of national importance.</li>
           <li>MOU has been signed between SIG and NIIT GIS Ltd. ESRI India is willing to extend industry collaboration to SIG in areas like curriculum review collaboration, Student Internship Collaboration, Placement Assistance in ESRI India, Academic R & D Centre.</li>
           <li>Research facility/Centre – State / National / International recognition: SIG has been recognized for membership of Joint Project 2 of Sentinel Asia, a Japanese Aerospace Exploration Agency project to provide support by way of web GIS and Remote Sensing techniques in Disaster Management.</li>
           <li>SIG has been appointed for SBFP project Sikkim, assisted by Japan International Corporation Association for setup a GIS Center at Secretariat in Sikkim.</li>
           <li>SIG has been nominated for Step 2 program of Japan Aerospace exploratory Agency, at International Centre for Integrated Mountain Development (ICIMOD) Kathmandu Nepal</li>
       </ul>
       <h3 class="menutitle">International Collaboration</h3>
       <ul>
            <li>SIG is a technical partner with International Water Management Institute (IWMI), Colombo for South Asia Drought Management System (SADMS) in association with World Metrological Organisation (WMO) and Global Water Partner (GWP).</li>
            <li>SIG is a member of Sentinel Asia Project with Japan Aerospace Exploratory Agency (JAXA) to provide space based data for disaster information in India. In this collaboration, SIG is authorized to access different satellite data from the Asian countries of Japan, China, India, Thailand, Philippines, S. Korea, and Malaysia and disseminate the information during disasters.</li>
            <li>SIG has sign MOU with Digital Globe Foundation USA (https://www.digitalglobe.com/)</li>
            <li>Digital Globe Foundation and SIG intend to Explore how high resolution satellite imagery remote sensing and other related tool for analyzing geospatial information can dramatically reshape the technical and financial calculus of monitoring the Earth and develop new approaches to using these tools.</li>
        </ul>    
    </div>
     <div id="testimonials" class="tab-pane fade">
       <h2 class="menutitle">Testimonials</h2>
       <img src="<?php echo base_url();?>assets/img/testimonial.jpg" class="img-responsive">
       <br>  
       <p>In its 8 years of existence SIG has managed to establish successful reputation within the industry by delivering industry ready professionals year-on-year. We have had national and international experts come to SIG to teach the students. 
      </p>
       <div class="row">
         <table class="contentpaneopen">



<tbody><tr>
<td valign="top">
In its 8 years of existence SIG has managed to establish successful reputation within the industry by delivering industry ready professionals year-on-year. We have had national and international experts come to SIG to teach the students. 
<br>
<br>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    It was very wonderful to have discussion with 
    students of SIG during my presentation. I wish good luck at SIG.<br>
<br>

    <span style="color:#73532c;font-weight:bold;">Dr. Giriraj Amarnath<br> International
 Water Management Institute, Colombo</span>
    </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%"></td>
    <td align="left" valign="top">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    SIG is doing remarkable work in creating capacity. 
Am happy to be a part of it.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">
 Shri Rajesh Mathur <br>
Vice Chairman, NIIT  GIS Ltd, Delhi</span>  </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>
<br>

<img src="/images/sep.jpg" alt="">
<br><br>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Unique institution in GIS, excellent brand in GIS with flagship 
on a peak of mountain, the ‘BEST INSTITUTION’ to mould career in GIS, Brig. K K V Khanzode excellent job!.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Shri Suniel Patil<br> ESRI  
    INDIA, Mumbai</span>    </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%"></td>
    <td align="left" valign="top">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Excellent Geospatial infrastructure, well disciplined students and faculty. I wish them all the best and bright future.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Dr. Y L P Rao <br>
 IFS Chief Conservator of Forest, Govt. of Maharashtra, Pune</span>
    </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>


<br>

<img src="/images/sep.jpg" alt="">
<br><br>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Happy to be back at the institute campus to rejuvenate my association with the institute, students and faculty.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Shri A Ramanathan<br> Sr. Executive .V.P – Projects, 
    Reliance Industries</span>  </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%"></td>
    <td align="left" valign="top">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Good to see SIG organizing execellent courses benefiting large number of students. I wish them good luck in organizing such courses in future.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Dr. V Venkateshwar Rao <br> Head, 
    Water Resources Division</span> </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>

<br>

<img src="/images/sep.jpg" alt="">
<br><br>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    This was great visit to SIG and interaction with the students was being heartening. I could see lot of interest towards Geoinformatics in the students. The centre is progressing excellently.<br>

My best wishes to the team.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Dr. Sarnam Singh<br> IIRS, 
    Dehradun</span> </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%"></td>
    <td align="left" valign="top">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    This was my first visit to the institute. I am extremely impressed with the students and the faculty.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Dr. P Nag <br>
 NATMO,     Kolkata</span>  </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>

<br>

<img src="/images/sep.jpg" alt="">
<br><br>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Lot of interesting projects carried out by the student and institute. Good collaboration is expected to implement initiatives of Govt. org. like PMC and citizens of Pune. Thank you.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Shri Rajendra Erande<br> Director IT, PMC, 
    Pune</span> </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%"></td>
    <td align="left" valign="top">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    It has been a great experience being with such students. My best wishes.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Dr. A K Jha<br> Commissioner, TRTI, 
    Pune</span> </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>

<br>

<img src="/images/sep.jpg" alt="">
<br><br>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    First of all I was impressed by foresight and vision of Brig K K V Khanzode in bringing SIG to the great level. He has taken efforts to shape this organization so well. The output students from SIG are always much above compared to other institutes. SIG is doing very good job of bridging gap between industry and academia. Wish SIG all success.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Shri Uday D Kale</span>
    </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%"></td>
    <td align="left" valign="top">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Quality and diversity of projects done by students is impressive. Wishing the next batch an equally enriching experience in their projects.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Dr. G S Rao, <br>
    Lavasa Corporation</span>   </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>

<br>

<img src="/images/sep.jpg" alt="">
<br><br>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Very good presentation listing the work done here. A very nie setup and impressive. Would definitely like to visit again.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Mr. Apoorva Bhatt<br>School of Biosciences, Univ. of Birmingham, U K</span>
    </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%"></td>
    <td align="left" valign="top">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Thank you to all staff and Director for an excellent welcome, discussion and presentation. It would be very good to see links grow between our institutions.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Mr Rae Mackay <br>
 School of Geography, Earth and Environmental Science, Univ. of Birmingham, U K</span>
    </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>

<br>

<img src="/images/sep.jpg" alt="">
<br><br>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    One of the best institution for GIS, My good wishes.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Mr. Pramod Gupta CEO<br> Indore Development Authority, Indore</span>
    </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%"></td>
    <td align="left" valign="top">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    It is a great pleasure to visit Symbiosis Institute of Geoinformatics. We at ITC are fully aware of your new courses and appreciate them very much. We hope that Symbiosis and ITC will be able to forge a strong relationship in the future.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Mr. John Horn<br>
ITC, Enschede,  The Netherlands</span>  </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>

<br>

<img src="/images/sep.jpg" alt="">
<br><br>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Simply Superb! Excellent infrastructure. Excellent human resources. Please keep it up and reach higher heights, contribute in nation building.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Shri R K Suryawanshi<br> Chairman ISG 
    Pune</span> </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%"></td>
    <td align="left" valign="top">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    I am deeply impressed with the faculties, students and the faculty of SIG and above all the spirit and dedication with which all the entries are contributing to the making of the institution. In particular it was nice experience to listen to all including students who spoke on use and promotion of geo-spatial data and technologies. Thanks.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Shri P S Acharya<br>
 ST, Technology Bhawan,     New Delhi</span>    </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>

<br>

<img src="/images/sep.jpg" alt="">
<br><br>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="/images/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    SIG is going to be centre of excellence in Geo-spatial Technologies. I am sure SIG will play an important role in developing human resources in GIS.<br>
<br>
    <span style="color:#73532c;font-weight:bold;">Maj Gen Dr R Siva Kumar <br>
 CEO NSDI, DST,     New Delhi</span>    </td>
    <td align="left" valign="bottom" width="3%"><img src="/images/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%"></td>
    <td align="left" valign="top">&nbsp;
    
    </td>
  </tr>
</tbody></table>

</td>
</tr>

</tbody></table>
           <!-- <div class="col-lg-6 col-md-6">
               <p> It was very wonderful to have discussion with students of SIG during my presentation. I wish good luck at SIG.</p>

               <p><strong>Dr. Giriraj Amarnath<br>
                International Water Management Institute, Colombo</strong></p>   

           </div>
           <div class="col-lg-6 col-md-6">
               <p> SIG is doing remarkable work in creating capacity. Am happy to be a part of it.</p>

               <p><strong>Shri Rajesh Mathur <br>
               Vice Chairman, NIIT GIS Ltd, Delhi</strong></p>   

           </div> -->

       </div>      
    </div>
  </div>
</div>

</div>    
	<!-- END: CONTENT/MISC/WHY-CHOOSE-US-1 -->
	<!-- BEGIN: CONTENT/TABS/TAB-1 -->
	
	<!-- END: CONTENT/TABS/TAB-1 -->
	<!-- BEGIN: CONTENT/MISC/LATEST-ITEMS-1 -->
<!-- 	<div class="c-content-box c-size-md c-bg-parallax" style="background-image: url(<?php echo base_url();?>assets/img/banner-back.jpg)">
		<div class="container">
			<div class="padding-medium row animated activate fadeInUp" data-fx="fadeInUp">
            <div class="col-sm-4 text-center col-md-4">
                <div class="services">
                    <div class="services-box">
                        <div class="circleBox">
                            <div class="circle-border-outside">
                                <div class="circle-border-inside">
                                    <div class="circle">
                                        <div class="circle-content">
                                            <span><i class="fa fa-newspaper-o fontpth"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="circle-triangle"></div>
                            </div>
                        </div>
                        <div class="text-center">
                         <h5 class="circlen">Latest News</h5>
                           	<div class="demo7" style="min-height: 240px;">
						<ul id="lnews">

							<li><p>	SIG Visit to Collage of Military Engineering (Indian Army) for CSR Activity.
							</p><p><span class="pull-right read_more">Read More</span></p><div class="clearfix"></div></li>
							<li><p>18th June, 2016 , ISG, IWRS and SIG conducted the Workshop on "Smart Cities And Geo-Ict Initiative - Geovision"</p><p><span class="pull-right read_more">Read More</span></p>
<div class="clearfix"></div>
							</li>
							<li><p>Certificate Course in Spatial Economics.</p><p><span class="pull-right read_more">Read More</span></p><div class="clearfix"></div></li>
							<li><p>Indian Society Of Geomatics Student Project Award Nomination</p><p><span class="pull-right read_more">Read More</span></p><div class="clearfix"></div></li>
						</ul>
						</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 text-center col-md-4">
                <div class="services">
                    <div class="services-box">
                        <div class="circleBox">
                            <div class="circle-border-outside">
                                <div class="circle-border-inside">
                                    <div class="circle">
                                        <div class="circle-content">
                                            <span><i class="fa fa-calendar fontpth"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="circle-triangle"></div>
                            </div>
                        </div>
                        <div class="text-center">
                        	<h5 class="circlen">Latest Events</h5>
                            	<div class="demo8" style="min-height: 240px;">
						<ul id="levent" >
							
							<li>	<div class="c-post">
                                       
                                        <div class="col-md-12 c-post-content">
                                            
                                            <p class="c-text" style="text-align:left"><i class="fa fa-hand-o-right rightico" aria-hidden="true"></i>
								18th June, 2016 , ISG, IWRS and SIG conducted the Workshop on "Smart Cities And Geo-Ict Initiative - Geovision"        </div>
                                    </div>
							</li>
							<li>

								<div class="c-post">
                                        
                                        <div class="col-md-12 c-post-content">
                                            
                                            <p class="c-text" style="text-align:left"><i class="fa fa-hand-o-right rightico" aria-hidden="true"></i>
							It Is A Pleasure To Inform That Indian Bank Have Approved Of A Scheme Of Educational Loan To Students Of All Symbiosis Institutes On Special Terms.</p>
                                        </div>
                                    </div>

							</li>
						<li>


						<div class="c-post">

						<div class="col-md-12 c-post-content">

						<p class="c-text" style="text-align:left"><i class="fa fa-hand-o-right rightico" aria-hidden="true"></i>
						SIG Visit to Collage of Military Engineering (Indian Army) for CSR Activity</p>
						</div>
						</div>

						</li>

						<li>


						<div class="c-post">

						<div class="col-md-12 c-post-content">

						<p class="c-text" style="text-align:left"><i class="fa fa-hand-o-right rightico" aria-hidden="true"></i>
						Conference Smart City and Geo ICT on 18 June 2016 at SIG</p>
						</div>
						</div>

						</li>


						</ul>
						</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 text-center col-md-4">
                <div class="services">
                    <div class="services-box">
                        <div class="circleBox">
                            <div class="circle-border-outside">
                                <div class="circle-border-inside">
                                    <div class="circle">
                                        <div class="circle-content">
                                            <span><i class="fa fa-bullhorn fontpth"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="circle-triangle"></div>
                            </div>
                        </div>
                        <div class="text-center">
                        	<h5 class="circlen">Ouick Links</h5>
                         
 <ul class="quicklink">
<li>Student Code of Conduct</li>
<li>Symbiosis International University</li>
<li>Collaboration</li>
<li>Testimonials</li>
<li>SIG Corner</li>
<li>Careers in GIS</li>
<li>Resource Centre</li>


                         </ul>

							
                        </div>
                    </div>
                </div>
            </div>
        </div>
			
		</div>
	</div>
 -->


<!-- <div class="c-content-box c-size-md c-bg-white " id="padd">
                <div class="c-content-tile-grid c-bs-grid-reset-space" data-auto-height="true">
                    <div class="c-content-title-1 wow animate fadeInDown">
                        <h3 class="c-font-uppercase c-center c-font-bold">Our Alumni Are In</h3>
                        <div class="c-line-center"></div>
                    </div>
                    <div class="container">

                    <div class=" wow animate fadeInUp" >
                    	<div class="col-lg-12">

	<div id="owl-example1" class="owl-carousel">
			
<div class="item"><img src="<?php echo base_url();?>assets/img/company/autodesk.png" alt="autodesk" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/clsco.png" alt="clsco" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/cybertech.png" alt="cybertech" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/dong.png" alt="dong" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/erdas.png" alt="erdas" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/esri.png" alt="esri" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/fugro.png" alt="fugro" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/hcl.png" alt="hcl" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/hexagon.png" alt="hexagon" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/ibm.png" alt="ibm" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/infosys.png" alt="infosys" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/niit.png" alt="niit" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/oracle.png" alt="oracle" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/patni.png" alt="patni" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/reliance.png" alt="reliance" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/reliance-comm.png" alt="reliance-comm" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/tata.png" alt="tata" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/tomtom.png" alt="tomtom" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/wipro.png" alt="wipro" /></div>
<div class="item"><img src="<?php echo base_url();?>assets/img/company/zobrist.png" alt="zobrist" /></div>

	</div> 
					</div> 
						</div> 
                    </div>
                   </div>
                    </div> -->
                     



	<!-- END: CONTENT/MISC/LATEST-ITEMS-1 -->
	<!-- BEGIN: CONTENT/BARS/BAR-5 -->
	
	<!-- END: CONTENT/BARS/BAR-5 -->
	<!-- BEGIN: CONTENT/MISC/SUBSCRIBE-FORM-1 -->
	<!-- <div class="c-content-box c-size-md c-bg-white" id="padd">
                <div class="c-content-tile-grid c-bs-grid-reset-space" data-auto-height="true">
                    <div class="c-content-title-1 wow animate fadeInDown">
                        <h3 class="c-font-uppercase c-center c-font-bold">Why We are</h3>
                        <div class="c-line-center"></div>
                    </div>
                    <div class="row wow animate fadeInUp">
                        <div class="col-md-6">
                            <div class="c-content-tile-1 c-bg-green">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="c-tile-content c-content-v-center" data-height="height">
                                            <div class="c-wrapper">
                                                <div class="c-body c-center">
                                                    <h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white"> Library</h3>
                                                    <p class="c-tile-body c-font-white">Lorem ipsum consectetuer elit sit amet, sit adipiscing amet, coectetuer adipiscing elit sit ame.</p>
                                                    <a href="#" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="c-tile-content c-arrow-right c-arrow-green c-content-overlay">
                                            <div class="c-overlay-wrapper">
                                                <div class="c-overlay-content">
                                                    <a href="#">
                                                        <i class="icon-link"></i>
                                                    </a>
                                                    <a href="<?php echo base_url();?>assets/img/library.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-4">
                                                        <i class="icon-magnifier"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="c-image c-overlay-object" data-height="height" style="background-image: url(<?php echo base_url();?>assets/img/library.jpg)"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="c-content-tile-1 c-bg-brown-2">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="c-tile-content c-content-v-center" data-height="height">
                                            <div class="c-wrapper">
                                                <div class="c-body c-center">
                                                    <h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white">Placements</h3>
                                                    <p class="c-tile-body c-font-white">Lorem ipsum consectetuer elit sit amet, sit adipiscing amet, coectetuer adipiscing elit sit ame.</p>
                                                    <a href="#" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="c-tile-content c-arrow-right c-arrow-brown-2 c-content-overlay">
                                            <div class="c-overlay-wrapper">
                                                <div class="c-overlay-content">
                                                    <a href="#">
                                                        <i class="icon-link"></i>
                                                    </a>
                                                    <a href="<?php echo base_url();?>assets/img/placement.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-4">
                                                        <i class="icon-magnifier"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="c-image c-overlay-object" data-height="height" style="background-image: url(<?php echo base_url();?>assets/img/placement.jpg)"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="c-content-tile-1 c-bg-red-2">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="c-tile-content c-arrow-left c-arrow-red-2 c-content-overlay">
                                            <div class="c-overlay-wrapper">
                                                <div class="c-overlay-content">
                                                    <a href="#">
                                                        <i class="icon-link"></i>
                                                    </a>
                                                    <a href="<?php echo base_url();?>assets/img/research.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-4">
                                                        <i class="icon-magnifier"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="c-image c-overlay-object" data-height="height" style="background-image: url(<?php echo base_url();?>assets/img/research.jpg"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="c-tile-content c-content-v-center" data-height="height">
                                            <div class="c-wrapper">
                                                <div class="c-body c-center">
                                                    <h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white"> Faculties & Research</h3>
                                                    <p class="c-tile-body c-font-white">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, nonummy nibh euismod tincidunt.</p>
                                                    <a href="#" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="c-content-tile-1 c-bg-blue-3">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="c-tile-content c-arrow-left c-arrow-blue-3 c-content-overlay">
                                            <div class="c-overlay-wrapper">
                                                <div class="c-overlay-content">
                                                    <a href="#">
                                                        <i class="icon-link"></i>
                                                    </a>
                                                    <a href="<?php echo base_url();?>assets/img/img7.jpg" data-lightbox="fancybox" data-fancybox-group="gallery-4">
                                                        <i class="icon-magnifier"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="c-image c-overlay-object" data-height="height" style="background-image: url(<?php echo base_url();?>assets/img/admission.jpg)"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="c-tile-content c-content-v-center" data-height="height">
                                            <div class="c-wrapper">
                                                <div class="c-body c-center">
                                                    <h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white">Admission</h3>
                                                    <p class="c-tile-body c-font-white">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, nonummy nibh euismod tincidunt.</p>
                                                    <a href="#" class="btn btn-sm c-btn-white c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->


<!-- <div class="c-layout-go2top">
	<i class="icon-arrow-up"></i>
</div>
 -->


