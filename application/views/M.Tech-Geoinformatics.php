<style type="text/css">
  
/*  bhoechie tab */
div.bhoechie-tab-container{
  z-index: 10;
  background-color: transparent;
  padding: 0 !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  border:1px solid #ddd;
  margin-top: 20px;
  margin-left: 50px;
  -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  box-shadow: 0 6px 12px rgba(0,0,0,.175);
  -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  background-clip: padding-box;
  opacity: 0.97;
  filter: alpha(opacity=97);
}
div.bhoechie-tab-menu{
  padding-right: 0;
  padding-left: 0;
  padding-bottom: 0;
}
div.bhoechie-tab-menu div.list-group{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a .glyphicon,
div.bhoechie-tab-menu div.list-group>a .fa {
  color: #b42c2f;
}
div.bhoechie-tab-menu div.list-group>a:first-child{
  border-top-right-radius: 0;
  -moz-border-top-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a:last-child{
  border-bottom-right-radius: 0;
  -moz-border-bottom-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a.active,
div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
div.bhoechie-tab-menu div.list-group>a.active .fa{
  background-color: #b22629;
  background-image: #5A55A3;
  color: #ffffff;
}
div.bhoechie-tab-menu div.list-group>a.active:after{
  content: '';
  position: absolute;
  left: 100%;
  top: 50%;
  margin-top: -13px;
  border-left: 0;
  border-bottom: 13px solid transparent;
  border-top: 13px solid transparent;
  border-left: 10px solid #b42c2f;
}
.subdivclass {
    background-color: #d0cece;
    color: #b02d29;
    border-right: 2px solid #fff;
} 
li.subdivclass{
  padding-left: 0px;
  padding-right: 0px;
}   
div.bhoechie-tab-content{
  background-color: #ffffff;
  /* border: 1px solid #eeeeee; */
  padding-left: 20px;
 /* padding-top: 10px;*/
}

div.bhoechie-tab div.bhoechie-tab-content:not(.active){
  display: none;
}
.containermar{
  margin:10% 0 8% 0; 
}
 .rightpad{
      border-right: 2px solid #f19999;
    }
    .rightpad li.active{
      background: url("<?php echo base_url();?>assets/img/liback.png") no-repeat;
      background-position: center right;
      padding: 10px 0px 10px;
    }
    .menutop li.active a{
      display: inline;
    }
    .menutop.nav>li>a {
      padding: 3px 15px;
      margin-bottom: 12px;
    }
</style>
<div class="row aboutop">
    <div class="academics"></div>
</div> 
    <div class="container">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-3 menutop list-group bhoechie-tab-menu">
               <ul class="nav nav-pills nav-stacked rightpad">
                <li><a class="liborder" data-toggle="tab" href="#Overview">Mtech In Geoinformatics</a></li>
                  <li class="active"><a class="liborder" data-toggle="tab" href="#coursestruct">Course Structure</a></li>
                  <li><a class="liborder" data-toggle="tab" href="#Workshops">Workshops</a></li>
                   <li class="sub-menu">Programme Offered by SIG</li>
                   <li><a class="liborder" href="<?php echo base_url(); ?>MTechGeoinformatics">M.Tech. Geoinformatics & ST</a></li>
                   <li><a class="liborder" href="<?php echo base_url(); ?>environment">M.Sc. Environment & Sustainability</a></li>
                   <li><a class="liborder" href="<?php echo base_url(); ?>certificatecourse">Certificate Course in Photogrammetry and Remote Sensing</a></li>
                   <li><a class="liborder" href="<?php echo base_url(); ?>courseinSpatial">Certificate Course in Spatial Economics</a></li>
                </ul>
              </div>           
              <div class="tab-content col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <!-- flight section -->
                <div id="Overview" class="tab-pane fade in active">
                </div>
                <div id="coursestruct" class="tab-pane fade in ">
                        <h2 class="menutitle">MTECH IN GEOINFORMATICS | SYMBIOSIS INSTITUTE OF GEOINFORMATICS</h2>
                      <hr>
                        <p>M.Tech Geoinformatics and ST</p>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="subdivclass col-lg-6 active"><a href="#semisterI" aria-controls="home" role="tab" data-toggle="tab">Semester - I</a></li>
                            <li role="presentation" class="subdivclass col-lg-6"><a href="#semisterII" aria-controls="profile" role="tab" data-toggle="tab">Semester - II</a></li>
                            <li role="presentation" class="subdivclass col-lg-6"><a href="#semisterIII" aria-controls="profile" role="tab" data-toggle="tab">Semester - III</a></li>
                            <li role="presentation" class="subdivclass col-lg-6"><a href="#semisterIV" aria-controls="profile" role="tab" data-toggle="tab">Semester - IV</a></li>
                         
                        </ul>
                       <div class="tab-content sempattern">
                        <div role="tabpanel" class="tab-pane active" id="semisterI">
                        <div class="col-lg-12">
                            <ul class="bullet-style">
                            <div class="col-lg-6 acamargin">
                              <li>Fundamentals of Geographic Information Systems</li>
                              <li>Fundamentals of Remote Sensing</li>
                              <li>Surveying and Cartography</li>
                              <li>Photogrammetry</li>
                              <li>Global Navigation Satellite System</li>
                              <li>Programming Languages-I</li>
                              </div>
                            <div class="col-lg-6 acamargin">
                              <li>Project Management</li>
                              <li>Fundamentals of Geographic Information Systems Lab</li>
                              <li>Fundamental of Remote Sensing Lab</li>
                              <li>Surveying and Cartography Lab</li>
                              <li>Global Navigation Satellite System Lab</li>
                              <li>Programming Languages-I Lab</li>
                            </div>
                              </ul>
                        </div>
                       
                      <div role="tabpanel" class="tab-pane active" id="semisterII">  
                        <div class="col-lg-12 msc-second">
                            <span class="colordiv sem">Semester - II</span>
                             <ul class="bullet-style">
                             <div class="col-lg-6 acamargin">
                              <li>Spatial Modeling and Analysis</li>
                              <li>Digital Image Processing</li>
                              <li>Spatial Data Base Management</li>
                              <li>Elective – I</li>
                              <li>Research Methodology</li>
                              <li>Programming Languages –II</li>
                              </div>
                             <div class="col-lg-6 acamargin">
                              <li>Cyber Security</li>
                              <li>Spatial Modeling and Analysis Lab</li>
                              <li>Digital Image Processing Lab</li>
                              <li>Spatial Data Base Management Lab</li>
                              <li>Programming Languages –II Lab</li>
                              <li>*Integrated Disaster Management</li>
                        </div>                             
                              </ul>
                      </div>  
                        <div class="clearfix"></div>
                        <div role="tabpanel" class="tab-pane" id="semisterIII">
                        <div class="col-lg-12 msc-third">
                            <span class="colordiv sem">Semester - III</span>
                            <ul class="bullet-style">
                              <li>Project- I</li>
                              <li>Seminar- I</li>
                              <li>Spatial Data Warehousing and Mining</li>
                              <li>Spatial decision Support System</li>
                              <li>Elective –II</li>
                              <li>Elective - III</li>
                              </ul>
                        </div>
                        </div>
                       <div role="tabpanel" class="tab-pane active" id="semisterIV"> 
                        <div class="col-lg-12 msc-forth">
                          <span class="colordiv sem">Semester - IV</span>
                            <ul class="bullet-style">
                              <li>Thesis</li>
                            </ul>
                        </div>
                        </div>
                    </div> 
                </div>
                <!-- train section -->              
                <div id="Workshops" class="tab-pane fade bhoechie-tab-content">
                      <h2 class="menutitle">Workshops Conducted On</h2>
                      <hr>
                              <div class="workshop">
                              <div style="float:left; ">
                              <ul class="bullet-style">
                              <li>Communication Skills</li>
                              <li>Soft Skills</li>
                              <li>Team Building</li>
                              <li>Leadership Skills</li>
                              <li>Confidence Building</li>
                              <li>Decision Making</li>
                              <li>Personality Development</li>
                              </ul>
                              </div>

                              <div style="float:right; ">
                              <ul class="bullet-style">

                              </ul>
                              </div>
                              <div class="clearfix"></div>
                               </div>
                </div>
          <!--       <div class="bhoechie-tab-content">
                    <center>
                      <h1 class="glyphicon glyphicon-credit-card" style="font-size:12em;color:#55518a"></h1>
                      <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                      <h3 style="margin-top: 0;color:#55518a">Credit Card</h3>
                    </center>
                </div>
                <div class="bhoechie-tab-content">
                    <center>
                      <h1 class="glyphicon glyphicon-credit-card" style="font-size:12em;color:#55518a"></h1>
                      <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                      <h3 style="margin-top: 0;color:#55518a">Credit Card</h3>
                    </center>
                </div> -->                
            </div>
        </div>
  </div>
</div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
</script>