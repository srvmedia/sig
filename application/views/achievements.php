		
		
		<style type="text/css">
    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }    
    .rightpad{
      border-right: 2px solid #f19999;
    }
    .rightpad li.active{
      padding: 10px 0px 10px;
    }
    li.active a{
      display: inline;
    }
   /* .nav>li>a {
      padding: 3px 15px;
      margin-bottom: 12px;
    }*/
   </style>
<div class="row aboutop">
    <div class="bannerimg"></div>
</div>	
<div class="container">  
<div class="col-md-3 ">
 <ul class="nav nav-pills nav-stacked rightpad">
    <li><a class="liborder" href="<?php echo base_url(); ?>collaborations">Collaborations</a></li>
    <li><a class="liborder" href="<?php echo base_url(); ?>testimonials" >Testimonials</a></li>
    <li><a class="liborder" href="<?php echo base_url(); ?>careers">Careers in SIG</a></li>
	<li><a class="liborder">Resource Center
    <span class="caret"></span></a>
    <ul>
      <li><a href="<?php echo base_url(); ?>faqs">FAQ's</a></li>
      <li ><a href="<?php echo base_url(); ?>scientificpapers">Scietific Papers</a></li>
    </ul>
	
  </li>
  <li class="active"><a class="liborder">SIG Corner
    <span class="caret"></span></a>
    <ul style="margin-top:10px;">
      <li><a href="<?php echo base_url(); ?>infrastructure">Infrastructure</a></li>
      <li ><a href="<?php echo base_url(); ?>achievements">Achievements</a></li>
	  <li ><a href="<?php echo base_url(); ?>orientationandpedagogy">Orientation and Pedagogy</a></li>
	  <li ><a href="<?php echo base_url(); ?>hostelandamenities">Hostel and Amenities</a></li>
	  <li ><a href="<?php echo base_url(); ?>conferences">Conferences</a></li>
    </ul>
	
  </li>
    <li><a class="liborder" href="http://www.siu.edu.in/resources.php" target="_blank">Health Facilities</a></li>  
  </ul>
</div>
  <div class="col-md-9 menutop">
   
      <h2 class="menutitle"><strong>Achievements</strong></h2>
      <hr>
    
	<table class="contentpaneopen">



<tbody><tr>
<td valign="top">
At SIG, we have always encouraged are students and staff to pursue their academic interests. Our faculties and students have brought the institute and Symbiosis in general, a series of accolades. In fact, we at SIG, encourage our students and staff to volunteer for socially relevant causes as well. As part of this initiative, SIG like other Symbiosis institute in Pune takes up welfare activities for the underprivileged strata of the society in association with NGO's. From participating in national level seminars on Geoinformatics to winning laurels in events across other colleges in the country and even doing their bit for the society, SIG has an ever growing list of achievements to showcase as a result of the merits of its staff and students. <br>
<br>

<h2 style="text-align: justify;">By Institute</h2>
<ul style="text-align: justify;" class="bullet-style">
<li>SIG has bagged National Education Leadership award 2014 by Lokmat.</li>
<li>The report of National Task Force on Geo-Special education-MHRD, August 2013 has benchmarked the SIG programme. </li>
<li>SIG has been invited as an academic advisor during the formation of Indian National GIS Organisation (INGO) by the Minister of HRD, Govt. of India.</li>
</ul>
<br>

<h2 style="text-align: justify;">By Staff</h2>
<strong>Participation in FDPs and National and International Conferences</strong>
<ul style="text-align: justify;" class="bullet-style">
  <li>Add the details of Dr T.P.Singh Sir visit to Dhaka. ( attended a three day meeting from 20<sup>th</sup> to 23<sup>rd</sup> April was  organized by the United Nation WMO , IWMI , GWP with Bangladesh Government (BMO).for the  project to develop an online drought monitoring system for South Asia.</li>
  <li>Add the Dr T.P. Singh sir award details ( International Institute for Social And Economic Reforms has confirmed  for the "Sir M. Visweswariah Best Engineering National Award" . The award was given by the Hon'ble Speaker Govt. of Karnataka at Bangalore on 28<sup>th</sup> of June)</li>
  <li>Associate Prof Dr T.P. Singh was selected and attended a FDP of one week duration on 'RS and GIS for Earth Sciences' conducted at national level by Indian Institute of Sciences, Bangalore.&nbsp;</li>
  <li>Associate Prof Dr T.P. Singh was the only person selected from India by Japan Aerospace Exploratory Administration Centinal Asia for an international one week duration Centinal Asia Step II Programme for Disaster Management, held at ICIMOD Kathmandu, Nepal and Macao, China from 28th February to 5th March 2011 . </li>
  <li>Dr T.P. Singh attended an international seminar of International Society of Photogrammetry and Remote Sensing conducted by IIRS at Dehradun on 7th and 8th March 2011, to represent SIG and present a paper written jointly by Dr. T.P. Singh and Brig K.K.V. Khanzode. <br>
    <br>
  </li>
</ul>



<h2 style="text-align: justify;">By Students</h2>
<ul style="text-align: justify;" class="bullet-style">
<li>OrleneD'cunha and Leon Ratinam won the best technical paper award at the International Conference held at IIT-Bombay in June 2014 (Geomatrix 2014).</li>
<li>The paper entitled - An Innovative Approach using Crowd Management (Crowd Control), Crowd Sourcing and GIS Techniques towards a Comprehensive Disaster Management Preparedness for The Kumbh Mela 2015.</li>
<li>Congratulations to Ms Andrie Singh for her outstanding presentation at "Brand-e-Gram" held at Chandragupta Institute of Management,Patna.</li>
<li>Ms Andrie Singh, Batch 2011-13 student participated in the event "Brand-e-Gram" held at Chandragupta Institute of Management,Patna 10th November 2012.</li>

  <li>SIG students participated in an international seminar on 'Geospatial Technology in Biodiversity Management' at IIT Kharagpur in December 2010. Our students team won the first prize for paper presentation, competing with participants from various IITs, engineering colleges and GIS institutes of repute. </li>
  <li>SIG students also participated in Map India 2011, a renowned annual national seminar on GIS and Remote Sensing held at Hyderabad, in January 2011.</li>
  <li>SIG student Ms Preethy Chandran's paper on 'Spatial Strategies for Sustainable Management' was adjudged the best Powertech paper in the national seminar organized by Department of Environment Management, Bharathidasan University, Tiruchirapalli in January 2011. </li>
  <li>Ms Pallavi Srivastava, SIG student of 2006-08 batch and Shri Pratardhan Sakalkar student of 2010-12 batch have been selected for commission in the Indian Army through Officers Training Academy , Chennai. </li>
  <li>Ms. Santh Rubini, Alumini of Batch 2009-11 is doing Research Assistantship at Oregon State University , School of Civil and Construction Engineering at US. Her research focus is on Mapping the landslides based on probabilistic techniques. She is working with the combinations of GIS and Remote sensing for analysis and LIDAR and Photogrammetric techniques for data.</li>
</ul>
<br>
<h2 style="text-align: justify;">Social Activities</h2>
<ul style="text-align: justify;" class="bullet-style">

<li>SIG staff and students visited an Old Age Home to help the old people accommodated there.</li>
<li>SIG staff and students visited an orphanage and taught them the basics of computers and geography.</li>
<li>Students conducted a Traffic Rules Awareness Drive and Safety on Roads campaign.</li>
<li>Tree Plantation in Army area.</li>
<li>Students visited schools for awareness programme on environment.</li>
<li>Faculty provide training to the differently abled army personnel.</li>



  <li>Students, staff and faculties visited Old Age and Orphan home Ishaprema-Niketan, in Nana Peth for Social cause. </li>
  <li>SIG staff, faculty and students undertook plantation of 800 saplings at Army Camp Aundh in August 2010. </li>
  <li>SIG donated 1000 saplings for planting at SIU, Lavale on the occasion of FEST workshop conducted at SIG in August 2010.</li>
  <li> SIG had organized a blood donatiojn camp at Atur Center under the aegis of SCHC in September 2010. </li>
  <li>Students and faculty visited Queens Mary's Technical Institute for war disabled and physically challenged soldiers located at Aundh Road, Kirkee for conducting classes of IT skills. </li>
  <li>Students and faculty members also visited Kendriya Vidyalaya located at Range Hills to conduct classes in various subjects for underprivileged students. </li>
  <li>A short certificate course of eight weeks duration in Fundamentals of GIS and Photogrammetry has been introduced for the disabled soldiers of Queen Mary's Technical Institute, at their request to generate interest amongst the members of the institute and to generate rehabilitation opportunities for them. </li>
</ul>
<br>
<h2 style="text-align: justify;">SIG Student Awarded UGC Merit Scholarship!</h2>
Ms. Andrie Singh student of Batch 2011-13 M.Sc Geoinformatics has been awarded UGC Merit Scholarship of Rs 2000/- per month and lump sum of Rs 1, 00, 000/- on completion of her PG degree and for her outstanding performance at undergraduate Physics Honors Examination. Ms. Andrie Singh is a Gold medalist in B.Sc (Physics) from Patna University.

</td>
</tr>

</tbody></table>
    
  </div>
</div>

</div>    
	