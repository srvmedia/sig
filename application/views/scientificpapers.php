		

<style type="text/css">



    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    li.active a{
        display: inline;

    }

    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }    
    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        padding: 10px 0px 10px;
    }
    li.active a{
        display: inline;
    }
    /* .nav>li>a {
       padding: 3px 15px;
       margin-bottom: 12px;
     }*/
</style>
<div class="row aboutop">
    <div class="bannerimg"></div>
</div>	
<div class="container">  
    <div class="col-md-3 menutop">
        <ul class="nav nav-pills nav-stacked rightpad">
            <li><a class="liborder" href="<?php echo base_url(); ?>collaborations">Collaborations</a></li>
            <li><a class="liborder" href="<?php echo base_url(); ?>testimonials" >Testimonials</a></li>
            <li><a class="liborder" href="<?php echo base_url(); ?>faqs">FAQ's</a></li>
<li class="active"><a class="liborder" href="<?php echo base_url(); ?>scientificpapers">Scietific Papers</a></li>

            <li><a class="liborder" href="http://www.siu.edu.in/resources.php" target="_blank">Health Facilities</a></li>  
        </ul>
    </div>
    <div class="col-md-9 menutop">

        <h2 class="menutitle"><strong>Scietific Papers</strong></h2>
        <hr>
        <div class="col-md-12">
            <p style="margin-bottom:30px"> Following are the latest issue of Journal of Sensor Technology (JST, March 2012, Volume 2, Issue 1), these articles can be freely downloaded from the following link:<br>
            <a href="/www.scirp.org/Journal/Home.aspx?JournalID=478" style="text-decoration:underline;" target="_blank">www.scirp.org/journal/jst</a></p>
            <br>
            <table width="100%" border="0" cellpadding="3" cellspacing="0">
                <tbody><tr>
                    <td align="left" valign="top" width="20%">EBSCO</td>
                    <td align="left" valign="top" width="44%">
                        <a href="http://search.ebscohost.com" style="text-decoration:underline;" target="_blank">http://search.ebscohost.com</a>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" width="20%">Emerald</td>
                    <td align="left" valign="top" width="44%">
                        <a href="http://www.emeraldinsight.com/" style="text-decoration:underline;" target="_blank">www.emeraldinsight.com</a>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" width="20%">Frost and Sullivan</td>
                    <td align="left" valign="top" width="44%">
                        <a href="http://ww2.frost.com/" style="text-decoration:underline;" target="_blank">ww2.frost.com</a>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" width="20%">Scopus</td>
                    <td align="left" valign="top" width="44%">
                        <a href="http://www.scopus.com/" style="text-decoration:underline;" target="_blank">www.scopus.com</a>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" width="20%">EMIS</td>
                    <td align="left" valign="top" width="44%">
                        <a href="https://www.library.siu.edu.in/index.php" style="text-decoration:underline;" target="_blank">https://library.siu.edu.in</a>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" width="20%">Jstor</td>
                    <td align="left" valign="top" width="44%">
                        <a href="http://www.jstor.org/" style="text-decoration:underline;" target="_blank">www.jstor.org</a>
                    </td>
                </tr>
                </tbody></table>
            <br><br>
            <h2>Koha Library Management Software Link</h2>
            <a href="http://symbiosis-koha.informindia.co.in:8080/" style="text-decoration:underline;" target="_blank">Koha Library Management Software Link</a>
            <br><br>
            <h2>OPAC link of koha</h2>
            <a href="http://symbiosis-koha.informindia.co.in" style="text-decoration:underline;" target="_blank">OPAC link of koha</a>
        </div>

    </div>

</div>

</div>    
