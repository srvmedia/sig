<style type="text/css">
  .jg_photo{
    width: 100%;
  }
  .margintop{
    margin-bottom: 3%;
  }
</style>
<div class="row aboutop">
    <div class="gallary"></div>
</div>
<div class="container">
<div class="inner-information">

     <div class="contentheading">Gallery</div>
<hr>
<div class="gallery">
  <div class="row sectiontableentry1">
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="Khula Maunch July 2015" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1584432309.jpg"><img src="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1584432309.jpg"  class="jg_photo" alt=""/></a>
      
        </div>
      </div>   
    </div>  
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="Khula Maunch July 2015" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1160308914.jpg"><img src="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1160308914.jpg"  class="jg_photo" alt=""/></a>
      
        </div>
      </div>   
    </div>  
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="Khula Maunch July 2015" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1721968117.jpg"><img src="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1721968117.jpg"  class="jg_photo" alt=""/></a>
      
        </div>
      </div>   
    </div>  
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="Khula Maunch July 2015" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1605383751.jpg"><img src="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1605383751.jpg"  class="jg_photo" alt=""/></a>
      
        </div>
      </div>   
    </div>
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="Khula Maunch July 2015" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1484942302.jpg"><img src="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1484942302.jpg"  class="jg_photo" alt=""/></a>
      
        </div>
      </div>   
    </div>  
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="Khula Maunch July 2015" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1176692323.jpg"><img src="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1176692323.jpg"  class="jg_photo" alt=""/></a>
      
        </div>
      </div>   
    </div>
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="Khula Maunch July 2015" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_2090470329.jpg"><img src="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_2090470329.jpg"  class="jg_photo" alt=""/></a>
      
        </div>
      </div>   
    </div>  
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="Khula Maunch July 2015" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1594787221.jpg"><img src="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1594787221.jpg"  class="jg_photo" alt=""/></a>
      
        </div>
      </div>   
    </div>
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="Khula Maunch July 2015" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1395434352.jpg"><img src="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1395434352.jpg"  class="jg_photo" alt=""/></a>
      
        </div>
      </div>   
    </div>  

</div>
</div>     
</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.js"></script>
 <style type="text/css" href="<?php echo base_url();?>assets/base/css/jquery.fancybox.css"></style>
 <script type="text/javascript">
   $(".fancybox")
    .attr('rel', 'gallery')
    .fancybox({
        padding : 0
    });
 </script>