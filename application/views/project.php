<style type="text/css">
  td.sectiontableheader {
    background: url(../images/grad3.gif) repeat-x bottom #913A27;
    border: 1px solid #E7E8E6;
    padding: 4px;
    color: #fff;
  }
  .sectiontableheader a{
    color: #fff;
  }
  .contentpaneopen{
      width: 100%;
    }  
  .tablebor td, th {
    border: 1px solid black;
    padding: 1%;
  } 
</style>
<div class="row aboutop">
    <div class="projectimg"></div>
</div>

<div class="container information-middle">
<div class="inner-information">

		 <table class="contentpaneopen">
<tbody><tr>
		<td class="contentheading" width="100%">
					Project			</td>			
		
					</tr>
</tbody></table>
<hr>
<table class="contentpaneopen">

<tbody><tr>
<td valign="top">
<table class="tablebor" width="100%" cellspacing="0" cellpadding="0" border="0">
  <tbody>
    <tr>
      <td align="right" class="sectiontableheader"> # </td>
      <td class="sectiontableheader"><a href="javascript:tableOrdering('a.title','desc','');"> Title</a></td>
      <td class="sectiontableheader"><a href="javascript:tableOrdering('a.created','desc','');">Date</a></td>
    </tr>
    <tr class="sectiontableentry1">
      <td align="right"> 1 </td>
      <td><a href="<?php echo base_url();?>assets/img/pdf/Six-Month-Project-List-Batch-2012-14.xlsx"> Six Month Project List</a></td>
      <td> Batch 2012-14 </td>
    </tr>
     <tr class="sectiontableentry1">
      <td align="right"> 2 </td>
      <td><a href="<?php echo base_url();?>assets/img/pdf/Six-Month-Project-List-Batch-2013-15.xlsx"> Six Month Project List</a></td>
      <td> Batch 2013-15 </td>
    </tr>
  </tbody>
</table>

</td>
</tr>

</tbody></table>
<span class="article_separator">&nbsp;</span>

		 
</div>
</div>
