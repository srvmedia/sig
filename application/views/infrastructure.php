		
		
		<style type="text/css">
    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }    
    .rightpad{
      border-right: 2px solid #f19999;
    }
    .rightpad li.active{
      padding: 10px 0px 10px;
    }
    li.active a{
      display: inline;
    }
   /* .nav>li>a {
      padding: 3px 15px;
      margin-bottom: 12px;
    }*/
   </style>
<div class="row aboutop">
    <div class="bannerimg"></div>
</div>	
<div class="container">  
<div class="col-md-3 ">
 <ul class="nav nav-pills nav-stacked rightpad">
    <li><a class="liborder" href="<?php echo base_url(); ?>collaborations">Collaborations</a></li>
    <li><a class="liborder" href="<?php echo base_url(); ?>testimonials" >Testimonials</a></li>
    <li><a class="liborder" href="<?php echo base_url(); ?>careers">Careers in SIG</a></li>
	<li><a class="liborder">Resource Center
    <span class="caret"></span></a>
    <ul>
      <li><a href="<?php echo base_url(); ?>faqs">FAQ's</a></li>
      <li ><a href="<?php echo base_url(); ?>scientificpapers">Scietific Papers</a></li>
    </ul>
	
  </li>
  <li class="active"><a class="liborder">SIG Corner
    <span class="caret"></span></a>
    <ul style="margin-top:10px;">
      <li><a href="<?php echo base_url(); ?>infrastructure">Infrastructure</a></li>
      <li ><a href="<?php echo base_url(); ?>achievements">Achievements</a></li>
	  <li ><a href="<?php echo base_url(); ?>orientationandpedagogy">Orientation and Pedagogy</a></li>
	  <li ><a href="<?php echo base_url(); ?>hostelandamenities">Hostel and Amenities</a></li>
	  <li ><a href="<?php echo base_url(); ?>conferences">Conferences</a></li>
    </ul>
	
  </li>
    <li><a class="liborder" href="http://www.siu.edu.in/resources.php" target="_blank">Health Facilities</a></li>  
  </ul>
</div>
  <div class="col-md-9 menutop">
   
      <h2 class="menutitle"><strong>Infrastructure</strong></h2>
      <hr>
    
	<table class="contentpaneopen">



<tbody><tr>
<td valign="top">
Symbiosis is known to provide state of the art infrastructure to facilitate the learning of its students and SIG is no exception to this rule. From a picturesque campus that comes equipped with all the facilities needed to aid in the learning of students to innovative teaching methodologies, SIG provides its students with the best in terms of academic training for Geoinformatics. SIG has infrastructure ideally suited to conduct training, which exposes students to recent developments taking place in the corporate world of Geoinformatics.
<br>
<br>
Laboratory is equipped with state-of-the-art computing facilities such as:
<ul class="bullet-style">
<li>High end computers in the network environment with large monitors</li>
<li>Server for the centralized resources management</li>
<li>Internet facilities for the world-wide literature referencing
</li>
<li><strong>GIS software</strong>: Latest version of GIS software suits that are mostly preferred and         widely used in the industry </li>
<li><strong>Image Processing software</strong>: Leading Image Processing software suites that are         much in use in the industry</li>
<li> Photogrammetry software suits</li>
<li>Global Positioning System</li>
<li>Programming tools</li>
<!--<li>Quality management tools</li>-->
<li>Internet facility and Library with state-of-the-art collection of relevant books and         journals</li>
<!--<li>Data collection</li>-->
<!--<li>Remote sensing images of different satellites
</li>-->
<li>Maps from varied sources</li>
<!--<li>Digital data on infrastructure and natural resources at various scales</li>-->
<!--<li>Library has very good quality of literature available in various domains such as        GIS, Remote Sensing, Photogrammetry, Image Processing, Software development        languages, Computer and Networking to keep updated with latest development in        geospatial worldwide.</li>-->
<li>Medical insurance for every student</li>
<li>Gymnasium, sports and yoga facilities </li>
</ul>

</td>
</tr>

</tbody></table>
    
  </div>
</div>

</div>    
	