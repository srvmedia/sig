<link href="<?php echo base_url(); ?>assets/plugins/revo-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>assets/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<section class="c-layout-revo-slider c-layout-revo-slider-4">
    <div class="tp-banner-container c-theme">
        <div class="tp-banner">
            <ul>
                <li data-transition="fade" data-slotamount="1" data-masterspeed="700" data-delay="6000" data-thumb="">
                    <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                    <img src="<?php echo base_url(); ?>assets/base/img/layout/sliders/revo-slider/base/blank.png"
                         alt="">
                    <div class="caption fulllscreenvideo tp-videolayer" data-x="0" data-y="0" data-speed="600"
                         data-start="1000" data-easing="Power4.easeOut" data-endspeed="500"
                         data-endeasing="Power4.easeOut" data-autoplay="true" data-autoplayonlyfirsttime="false"
                         data-nextslideatend="true" data-videowidth="100%" data-videoheight="100%"
                         data-videopreload="meta"
                         data-videomp4="<?php echo base_url(); ?>assets/base/media/video/SIG_Final.mp4"
                         data-videowebm="" data-videocontrols="none" data-forcecover="1" data-forcerewind="on"
                         data-aspectratio="16:9" data-volume="mute"
                         data-videoposter="<?php echo base_url(); ?>assets/base/img/layout/sliders/revo-slider/base/blank.png">
                    </div>
                    <div class="caption customin customout tp-resizeme" data-x="center" data-y="center" data-hoffset=""
                         data-voffset="-30" data-speed="500" data-start="1000"
                         data-customin="x:0;y:0;z:0;rotationX:0.5;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-easing="Back.easeOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1"
                         data-endelementdelay="0.1" data-endspeed="600">
                        <h3 class="c-block-bordered-square c-font-55 c-font-bold c-font-center c-font-uppercase c-font-white c-block"
                            style="background">
                            Welcome to Symbiosis institute <br/>of Geoinformatics</h3>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div style="margin-bottom:20px;">
        <div class="col-lg-2 col-md-2 col-sm-2 blueheader text-center arrow2"><span
                class="header_span">Latest news</span></div>
        <div class="col-lg-10 col-md-10 col-sm-10 redheader">
            <marquee>
                <a href="<?php echo base_url(); ?>welcome/newsevents/1" target="_blank" style="color:#fff;">DST
                    Sponsored Geo-Spatial Training Program</a>
            </marquee>
        </div>
        <div class="clearfix"></div>
    </div>
</section>
<div class="c-content-box c-size-md c-bg-grey-1">
    <div class="container">
        <div class="c-content-bar-2 c-opt-1">
            <div class="row" data-auto-height="true">
                <div class="col-md-7">
                    <!-- Begin: Title 1 component -->
                    <div class="c-content-title-1" data-height="height" style="height: 167px;">
                        <h3 class="c-font-uppercase c-font-bold">Symbiosis Institute of Geoinformatics</h3>
                        <p class="c-font-sbold">
                            Since its inception in 2004, Symbiosis Institute of Geoinformatics has been imparting
                            education of international very high standard in GIS. It strives to create proficient and
                            professional manpower for the ever advancing geospatial industry. SIG has 100% campus
                            placement record for all its qualified and eligible students since its inception.
                        </p>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="c-content-v-center " data-height="height">
                        <div id="owl-example" class="owl-carousel">
                            <div class="item">
                                <img src="<?php echo base_url(); ?>assets/img/banner.jpg" class="sliderborder">
                            </div>
                            <div class="item">
                                <img src="<?php echo base_url(); ?>assets/img/banner_1.jpg" class="sliderborder">
                            </div>
                            <div class="item">
                                <img src="<?php echo base_url(); ?>assets/img/banner_2.jpg" class="sliderborder">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="c-content-box c-size-md c-bg-parallax"
     style="background-image: url(<?php echo base_url(); ?>assets/img/home/background02.jpg)">
    <div class="container">
        <div class="padding-medium row animated activate fadeInUp" data-fx="fadeInUp">
            <div class="col-sm-4 text-center col-md-4">
                <div class="services">
                    <div class="services-box">
                        <div class="circleBox">
                            <div class="circle-border-outside">
                                <div class="circle-border-inside">
                                    <div class="circle">
                                        <div class="circle-content">
                                            <span><i class="fa fa-newspaper-o fontpth"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="circle-triangle"></div>
                            </div>
                        </div>
                        <div class="text-center">
                            <h5 class="circlen">Latest News</h5>
                            <div class="demo7" style="min-height: 240px;">
                                <ul id="lnews">
                                    <?php
                                    $i = 1;
                                    if (isset($all_news)) {
                                        foreach ($all_news as $news) {
                                            $time = strtotime($news['date']);
                                            $date = date("d/m/y", $time);
                                            ?>
                                            <li>
                                                <p style="text-align:left">
                                                    <a href='<?php echo base_url(); ?>welcome/newsdetail/<?php echo $news['id']; ?>'
                                                       target='_blank'>
                                                        <?php echo $news['title']; ?>
                                                    </a>
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 text-center col-md-4">
                <div class="services">
                    <div class="services-box">
                        <div class="circleBox">
                            <div class="circle-border-outside">
                                <div class="circle-border-inside">
                                    <div class="circle">
                                        <div class="circle-content">
                                            <span><i class="fa fa-calendar fontpth"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="circle-triangle"></div>
                            </div>
                        </div>
                        <div class="text-center">
                            <h5 class="circlen">Latest Events</h5>
                            <div class="demo8" style="min-height: 240px;">
                                <ul id="lnews">
                                    <?php
                                    $i = 1;
                                    if (isset($all_events)) {
                                        foreach ($all_events as $events) {
                                            $time = strtotime($events['date']);
                                            $date = date("d/m/y", $time);
                                            ?>
                                            <li>
                                                <p style="text-align:left">
                                                    <a href='<?php echo base_url(); ?>welcome/eventsdetail/<?php echo $events['id']; ?>'
                                                       target='_blank'>
                                                        <?php echo $events['title']; ?>
                                                    </a>
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 text-center col-md-4">
                <div class="services">
                    <div class="services-box">
                        <div class="circleBox">
                            <div class="circle-border-outside">
                                <div class="circle-border-inside">
                                    <div class="circle">
                                        <div class="circle-content">
                                            <span><i class="fa fa-bullhorn fontpth"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="circle-triangle"></div>
                            </div>
                        </div>
                        <div class="text-center">
                            <h5 class="circlen">Ouick Links</h5>

                            <ul class="quicklink">
                                <li><a href="http://www.sig.ac.in/images/pdf/revised_code_of_conduct.pdf"
                                       target="_blank">Student Code of Conduct</a></li>
                                <li><a href="http://siu.edu.in/downloads/phd_addmission.php" target="_blank">Symbiosis
                                        International University</a></li>
                                <li><a href="<?php echo base_url(); ?>collaborations">Collaboration</a></li>
                                <li><a href="<?php echo base_url(); ?>testimonials">Testimonials</li>
                                <!--<li><a href="<?php echo base_url(); ?>infrastructure">SIG Corner</li>-->
                                <li><a href="<?php echo base_url(); ?>careers">Careers in SIG</a></li>
                                <li><a href="<?php echo base_url(); ?>faqs">FAQs</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="c-content-box c-size-md c-bg-white " id="padd">
    <div class="c-content-tile-grid c-bs-grid-reset-space" data-auto-height="true">
        <div class="c-content-title-1 wow animate fadeInDown">
            <h3 class="c-font-uppercase c-center c-font-bold">Our Alumni Are In</h3>
            <div class="c-line-center"></div>
        </div>
        <div class="container">
            <div class=" wow animate fadeInUp">
                <div class="col-lg-12">
                    <div id="owl-example1" class="owl-carousel">
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/autodesk.png"
                                               alt="autodesk"/></div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/clsco.png" alt="clsco"/>
                        </div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/cybertech.png"
                                               alt="cybertech"/></div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/dong.png" alt="dong"/>
                        </div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/erdas.png" alt="erdas"/>
                        </div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/esri.png" alt="esri"/>
                        </div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/fugro.png" alt="fugro"/>
                        </div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/hcl.png" alt="hcl"/>
                        </div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/hexagon.png"
                                               alt="hexagon"/></div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/ibm.png" alt="ibm"/>
                        </div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/infosys.png"
                                               alt="infosys"/></div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/niit.png" alt="niit"/>
                        </div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/oracle.png"
                                               alt="oracle"/></div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/patni.png" alt="patni"/>
                        </div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/reliance.png"
                                               alt="reliance"/></div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/reliance-comm.png"
                                               alt="reliance-comm"/></div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/tata.png" alt="tata"/>
                        </div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/tomtom.png"
                                               alt="tomtom"/></div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/wipro.png" alt="wipro"/>
                        </div>
                        <div class="item"><img src="<?php echo base_url(); ?>assets/img/company/zobrist.png"
                                               alt="zobrist"/></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="c-content-box c-size-md c-bg-white" id="padd">
    <div class="c-content-tile-grid c-bs-grid-reset-space" data-auto-height="true">
        <div class="c-content-title-1 wow animate fadeInDown">
            <h3 class="c-font-uppercase c-center c-font-bold">Why We are</h3>
            <div class="c-line-center"></div>
        </div>
        <div class="row wow animate fadeInUp">
            <div class="col-md-6 col-xs-12 col-sm-12">
                <div class="c-content-tile-1 c-bg-green">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="c-tile-content c-content-v-center" data-height="height">
                                <div class="c-wrapper">
                                    <div class="c-body c-center">
                                        <h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white">
                                            Library</h3>
                                        <p class="c-tile-body c-font-white">With a rich collection of books from the
                                            widest corners of the world, Symbiosis Institute of Geoinformatics boasts of
                                            having one of the most impeccable libraries.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="c-tile-content c-arrow-right c-arrow-green c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                    </div>
                                </div>
                                <div class="c-image c-overlay-object" data-height="height"
                                     style="background-image: url(<?php echo base_url(); ?>assets/img/library.jpg)"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 col-sm-12">
                <div class="c-content-tile-1 c-bg-brown-2">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="c-tile-content c-content-v-center" data-height="height">
                                <div class="c-wrapper">
                                    <div class="c-body c-center">
                                        <h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white">
                                            Placements</h3>
                                        <p class="c-tile-body c-font-white">Unlike most of the other colleges, Symbiosis
                                            Institute of Geoinformatics has an impeccable placement record thanks to
                                            their advanced placement assistance opportunities.</p>
                                        <a href="<?php echo base_url(); ?>placement"
                                           class="btn btn-sm c-btn-white c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="c-tile-content c-arrow-right c-arrow-brown-2 c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="<?php echo base_url(); ?>placement">
                                            <i class="icon-link"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="c-image c-overlay-object" data-height="height"
                                     style="background-image: url(<?php echo base_url(); ?>assets/img/placement.jpg)"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 col-sm-12">
                <div class="c-content-tile-1 c-bg-red-2">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="c-tile-content c-arrow-left c-arrow-red-2 c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="<?php echo base_url(); ?>faculties">
                                            <i class="icon-link"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="c-image c-overlay-object" data-height="height"
                                     style="background-image: url(<?php echo base_url(); ?>assets/img/research.jpg"></div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="c-tile-content c-content-v-center" data-height="height">
                                <div class="c-wrapper">
                                    <div class="c-body c-center">
                                        <h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white">
                                            Faculties & Research</h3>
                                        <p class="c-tile-body c-font-white">With a reputation so big, Symbiosis
                                            Institute of Geoinformatics is a home for world-class faculty members.</p>
                                        <a href="<?php echo base_url(); ?>faculties"
                                           class="btn btn-sm c-btn-white c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 col-sm-12">
                <div class="c-content-tile-1 c-bg-blue-3">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="c-tile-content c-arrow-left c-arrow-blue-3 c-content-overlay">
                                <div class="c-overlay-wrapper">
                                    <div class="c-overlay-content">
                                        <a href="<?php echo base_url(); ?>admission">
                                            <i class="icon-link"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="c-image c-overlay-object" data-height="height"
                                     style="background-image: url(<?php echo base_url(); ?>assets/img/admission.jpg)"></div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="c-tile-content c-content-v-center" data-height="height">
                                <div class="c-wrapper">
                                    <div class="c-body c-center">
                                        <h3 class="c-tile-title c-font-25 c-line-height-34 c-font-uppercase c-font-bold c-font-white">
                                            Admission</h3>
                                        <p class="c-tile-body c-font-white">In a niche field like geoinformatics,
                                            Symbiosis Institute of Geoinformatics provides assistance in every step of
                                            the admission process. </p>
                                        <a href="<?php echo base_url(); ?>admission"
                                           class="btn btn-sm c-btn-white c-btn-uppercase c-btn-bold c-btn-border-1x c-btn-square">More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="c-layout-go2top">
    <i class="icon-arrow-up"></i>
</div>
<style>
    .modal {
        text-align: center;
        padding: 0!important;
    }
    .modal-title {
        font-size: 24px;
        color: #3f444a;
        text-align: center;
        font-weight: 600;
    }
    .modal-body {
        padding: 6px 20px 0px 20px;
    }
    .modal-header {
        padding: 13px 20px 13px;
    }

    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
    }

    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
    }
    .modal-dialog {
        width: 530px !important;
        margin: 30px auto;
    }
    .loader {
        margin-left: 37%;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #c41f26;
        border-bottom: 16px solid #c41f26;
        width: 100px;
        height: 100px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
        text-align:center;
    }

    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog vertical-align-center">
        <!-- Modal content-->
        <div class="loader"></div>
        <div class="modal-content" style="box-shadow: 4px 4px 30px #130507;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enquiry Form</h4>
            </div>
            <div class="modal-body">
                <p id="returnmessage"></p>
                <form id="modalform" action="#" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
<!--                        <label for="recipient-name" class="form-control-label">Name :</label>-->
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter your Name">
                    </div>
                    <div class="form-group">
<!--                        <label for="recipient-name" class="form-control-label">Email Address :</label>-->
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter your Email Address">
                    </div>
                    <div class="form-group">
<!--                        <label for="recipient-name" class="form-control-label">Email Address :</label>-->
                        <input type="tel" class="form-control" id="mobile" name="mobile" placeholder="Enter your Phone Number">
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="program" name="program">
                            <option value="" disabled selected>Select your Program</option>
                            <option value="M.Sc. Geoinformatics">M.Sc. Geoinformatics</option>
                            <option value="M.Tech. Geoinformatics">M.Tech Geoinformatics</option>
                            <option value="M.Sc. Environment and Sustainability">M.Sc. (Environment & Sustainability)</option>
                            <option value="Certificate Course">Certificate Course</option>
                        </select>
                    </div>
                    <div class="form-group">
<!--                        <label for="message-text" class="form-control-label">Message :</label>-->
                        <textarea class="form-control" id="message" name="message" placeholder="Enter Message"></textarea>
                    </div>
                    <div class="form-group">
                        <div class="g-recaptcha" data-sitekey="6LfAbQoUAAAAAKD1dtiJtY3iROvh2-G12UR4ZgWX"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="submit" class="btn btn-success" >Submit Enquiry</button>
            </div>
        </div>

    </div>
</div>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script>
    jQuery(document).ready(function () {
        $('.loader').hide();
        $('#myModal').modal({backdrop: 'static', keyboard: false,show:true});


        $('#submit').click(function () {
            var name=$('#name').val();
            var email=$('#email').val();
            var message=$('#message').val();
            var mobile = $.trim($("#mobile").val());
            var mob = /^[0-9]{10}$/;
            var program = $('#program :selected').val();
            var captchresp=$('#g-recaptcha-response').val();
            $("#returnmessage").empty();

            if (name == '' && email == '' && message == '') {
                $("#returnmessage").css('color','red');
                $("#returnmessage").append("Please Fill Required Fields");
                return false;
            } else if(name == '') {
                $("#returnmessage").css('color','red');
                $("#returnmessage").append("Name field is required");
                return false;
            }else if(email == '') {
                $("#returnmessage").css('color','red');
                $("#returnmessage").append("Email field is required");
                return false;
            }else if(message == '') {
                $("#returnmessage").css('color', 'red');
                $("#returnmessage").append("Message field is required");
                return false;
            }else if (!mobile)
            {
                $('#error1').append("Enter your Mobile.");
                return false;
            }
            if (mob.test(mobile) == false) {
                $("#returnmessage").css('color', 'red');
                $('#returnmessage').append("Enter 10 digit Mobile Number.");
                return false;
            }

            if (!program) {
                $("#returnmessage").css('color', 'red');
                $('#returnmessage').append("Please select Program");
                return false;
            }else{

                var emailReg = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;
                if (emailReg.test(email) == false)
                {
                    $("#returnmessage").css('color','red');
                    $("#returnmessage").append("Enter Valid Email ID");
                    return false;
                }
                $('.loader').show();
                $('.modal-content').hide();
                $.ajax({
                    type:'POST',
                    url:'<?php echo base_url();?>enquiryform',
                    data:{
                        name1: name,
                        email1: email,
                        message1: message,
                        mobile1: mobile,
                        program1: program,
                        captchresp1:captchresp
                    },
                    success:function(data){

                        if(data==1) {
                            $("#returnmessage").css('color','red');
                            $("#returnmessage").append("Please click on the reCAPTCHA box.");
                        }
                        if(data==0) {
                            $("#returnmessage").css('color','red');
                            $("#returnmessage").append("Robot verification failed, please try again.");
                        }
                        if(data==2) {
                            $('.modal-content').show();
                            $('#modalform').hide();
                            $('#submit').hide();
                            $('#name').val('');
                            $('#email').val('');
                            $('#message').val('');
                            $('.loader').hide();
                            $('#returnmessage').html('<div align="center"><i class="fa fa-thumbs-up" style="font-size: 200px;color:#12576b;"></i><br> <h3 class="sh" style="color:#ed6235;">Thank you for the request! </br> We will get in touch with you shortly.</h3></div>');
                        }
                        if(data==3) {
                            $('.modal-content').show();
                            $("#returnmessage").css('color','red');
                            $("#returnmessage").append("Error to send mail.");

                            $('.loader').hide();
                        }
                    },
                    error:function(){

                    }
                });
            }
        });
    });
</script>