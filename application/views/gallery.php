<style type="text/css">
  .jg_photo{
    width: 100%;
  }
</style>
<div class="row aboutop">
    <div class="gallary"></div>
</div>
<div class="container">
<div class="inner-information">

     <div class="contentheading">Gallery</div>
<hr>
<div class="gallery">
  <div class="row sectiontableentry1">
    <div class="col-lg-4 col-md-4">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
          <a title="Khula Maunch July 2015" href="<?php echo base_url();?>welcome/khulamaunchjuly2015">
            <img src="<?php echo base_url();?>assets/img/gallary/khula_maunch_july_2015_20150812_1061582253.jpg" class="jg_photo" alt="Khula Maunch July 2015">
          </a>
        </div>
      </div>
      <div class="col-lg-12 col-md-12">
        <p>  
            <a href="/photo-gallery/khula-maunch-july-2015.html">
              <b>Khula Maunch July 2015</b>
            </a>
        </p>
      </div>
    </div>
    <div class="col-lg-4 col-md-4">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
          <a title="UAV" href="<?php echo base_url();?>welcome/UAV">
            <img src="<?php echo base_url();?>assets/img/gallary/uav_20150810_1775638220.jpg" class="jg_photo" alt="UAV">
          </a>
        </div>
      </div>
      <div class="col-lg-12 col-md-12">
        <p>
            <a href="/photo-gallery/uav.html">
              <b>UAV</b>
            </a>
        </p>
      </div>
    </div>
    <div class="col-lg-4 col-md-4">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
          <a title="Education Leadership Award" href="<?php echo base_url();?>welcome/educationleadershipaward">
            <img src="<?php echo base_url();?>assets/img/gallary/lokmat_awards_photos_13-feb-2014_20140219_2086617249.jpg" class="jg_photo" alt="Education Leadership Award">
          </a>
        </div>
      </div>
      <div class="col-lg-12 col-md-12">
        <p>
            <a href="/photo-gallery/education-leadership-award.html">
              <b>Education Leadership Award</b>
            </a>
        </p>
      </div>
    </div>
    <div class="jg_clearboth"></div>

  <div class="col-lg-4 col-md-4">
      <div class="col-lg-12 col-md-12">
      <div class="jg_imgalign_gal">
        <div class="jg_photo_container">
          <a title="Batch_2013-15_Skit" href="<?php echo base_url();?>welcome/batch201315skit">
            <img src="<?php echo base_url();?>assets/img/gallary/batch_2013-15_skit_20130620_1610090263.jpg" class="jg_photo" alt="Batch_2013-15_Skit">
          </a>
        </div>
      </div>
     <div class="col-lg-12 col-md-12">
        <p>
           <a href="/photo-gallery/batch-2013-15-skit.html">
              <b>Batch_2013-15_Skit</b>
            </a>
        </p>
      </div>
    </div>
    </div>
    <div class="col-lg-4 col-md-4">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
          <a title="QMTI June 2012" href="<?php echo base_url();?>welcome/qmtijune2012"">
            <img src="<?php echo base_url();?>assets/img/gallary/qmti_batch_june_2012_20120926_1759586743.jpg" class="jg_photo" alt="QMTI June 2012">
          </a>
        </div>
      </div>
      <div class="col-lg-12 col-md-12">
          <p>
            <a href="/photo-gallery/qmti-june-2012.html">
              <b>QMTI June 2012</b>
            </a>
          </p>
      </div>
    </div>
    <div class="col-lg-4 col-md-4">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
          <a title="Dahi Handi Celebration" href="<?php echo base_url();?>welcome/dahihandicelebration"">
            <img style="width: 56%;" src="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1721968117.jpg" class="jg_photo" alt="Dahi Handi Celebration">
          </a>
        </div>
      </div>
      <div class="col-lg-12 col-md-12">
          <p>
            <a href="/photo-gallery/dahi-handi-celebration.html">
              <b>Dahi Handi Celebration</b>
            </a>
          </p>
      </div>
    </div>
    <div class="jg_clearboth"></div>
    <div class="col-lg-4 col-md-4">
      <div class="col-lg-12 col-md-12">
      <div class="jg_imgalign_gal">
        <div class="jg_photo_container">
          <a title="Pune Darshan" href="<?php echo base_url();?>welcome/punedarshan"">
            <img src="<?php echo base_url();?>assets/img/gallary/pune_darshan_20150812_1732216745.jpg" class="jg_photo" alt="Pune Darshan">
          </a>
        </div>
      </div>
      <div class="col-lg-12 col-md-12">
        <p>
           <a href="/photo-gallery/pune-darshan.html">
              <b>Pune Darshan</b>
            </a>
        </p> 
      </div>
    </div>
    </div>
    <div class="col-lg-4 col-md-4">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
          <a title="SIG Workshop / Conference" href="<?php echo base_url();?>welcome/conference"">
            <img src="<?php echo base_url();?>assets/img/gallary/sig_workshop_20120424_1688172030.jpg" class="jg_photo" alt="SIG Workshop / Conference">
          </a>
        </div>
      </div>
      <div class="col-lg-12 col-md-12">
        <p>
            <a href="/photo-gallery/-conference.html">
              <b>SIG Workshop / Conference</b>
            </a>
        </p> 
      </div>
    </div>
    <div class="col-lg-4 col-md-4">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
          <a title="Guest Visit" href="<?php echo base_url();?>welcome/guestvisit"">
            <img src="<?php echo base_url();?>assets/img/gallary/guest_visit_20120424_1523395174.jpg" class="jg_photo" alt="Guest Visit">
          </a>
        </div>
      </div>
      <div class="col-lg-12 col-md-12">
        <p>
            <a href="/photo-gallery/guest-visit.html">
              <b>Guest Visit</b>
            </a>
         </p>
      </div>
    </div>
    <div class="jg_clearboth"></div>
    <div class="col-lg-4 col-md-4">
      <div class="col-lg-12 col-md-12">
      <div class="jg_imgalign_gal">
        <div class="jg_photo_container">
          <a title="Life at SIG" href="<?php echo base_url();?>welcome/lifeatsig"">
            <img src="<?php echo base_url();?>assets/img/gallary/sig_corner_20120424_1376806095.jpg" class="jg_photo" alt="Life at SIG">
          </a>
        </div>
      </div>
      <div class="col-lg-12 col-md-12">
        <p>
              <a href="/photo-gallery/life-at-sig.html">
              <b>Life at SIG</b>
            </a>
        </p> 
      </div>
    </div>
    <div class="jg_clearboth"></div>
  </div>
</div>
</div>     
</div>
</div>
