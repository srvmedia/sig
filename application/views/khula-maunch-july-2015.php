<style type="text/css">
  .jg_photo{
    width: 100%;
  }
  .margintop{
    margin-bottom: 3%;
  }
</style>
<div class="row aboutop">
    <div class="gallary"></div>
</div>
<div class="container">
<div class="inner-information">

     <div class="contentheading">Gallery</div>
<hr>
<div class="gallery">
  <div class="row sectiontableentry1">
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="Khula Maunch July 2015" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/khula_maunch_july_2015_20150812_2092555693.jpg"><img src="<?php echo base_url();?>assets/img/gallary/khula_maunch_july_2015_20150812_2092555693.jpg"  class="jg_photo" alt=""/></a>
      
        </div>
      </div>   
    </div>
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="UVA" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/khula_maunch_july_2015_20150812_1296267828.jpg"><img src="<?php echo base_url();?>assets/img/gallary/khula_maunch_july_2015_20150812_1296267828.jpg"  class="jg_photo" alt=""/></a>
        </div>
      </div>
     </div>
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="Education Leadership Award" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/khula_maunch_july_2015_20150812_12962678281.jpg"><img src="<?php echo base_url();?>assets/img/gallary/khula_maunch_july_2015_20150812_12962678281.jpg"  class="jg_photo" alt=""/></a>
        </div>
      </div>
    
    </div>
    <div class="jg_clearboth"></div>
  <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
      <div class="jg_imgalign_gal">
        <div class="jg_photo_container">
         <a title="Batch_2013-15_Skit" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/khula_maunch_july_2015_20150812_1675187435.jpg"><img src="<?php echo base_url();?>assets/img/gallary/khula_maunch_july_2015_20150812_1675187435.jpg"  class="jg_photo" alt=""/></a>
        </div>
      </div>
       </div>
    </div>
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
         <a title="QMTI June 2012" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/khula_maunch_july_2015_20150812_10615822531.jpg"><img src="<?php echo base_url();?>assets/img/gallary/khula_maunch_july_2015_20150812_10615822531.jpg"  class="jg_photo" alt=""/></a>
        </div>
      </div>
      </div>
   <!--  <div class="col-lg-4 col-md-4">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
         <a title="Dahi Handi Celebration" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1721968117.jpg"><img src="<?php echo base_url();?>assets/img/gallary/dahi_handi_celebration_20120926_1721968117.jpg"  class="jg_photo" alt=""/></a>
        </div>
      </div>
      </div>
    <div class="jg_clearboth"></div>
    <div class="col-lg-4 col-md-4">
      <div class="col-lg-12 col-md-12">
      <div class="jg_imgalign_gal">
        <div class="jg_photo_container">
         <a title="Pune Darshan" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/pune_darshan_20150812_1732216745.jpg"><img src="<?php echo base_url();?>assets/img/gallary/pune_darshan_20150812_1732216745.jpg"  class="jg_photo" alt=""/></a>
        </div>
      </div>
      </div> -->
    </div>
<!--     <div class="col-lg-4 col-md-4">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
         <a title="SIG Workshop / Conference" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/sig_workshop_20120424_1688172030.jpg"><img src="<?php echo base_url();?>assets/img/gallary/sig_workshop_20120424_1688172030.jpg"  class="jg_photo" alt=""/></a>
        </div>
      </div>
      </div>
    <div class="col-lg-4 col-md-4">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
         <a title="Guest Visit" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/guest_visit_20120424_1523395174.jpg"><img src="<?php echo base_url();?>assets/img/gallary/guest_visit_20120424_1523395174.jpg"  class="jg_photo"  alt=""/></a>
        </div>
      </div>
    </div>
    <div class="jg_clearboth"></div>
    <div class="col-lg-4 col-md-4">
      <div class="col-lg-12 col-md-12">
      <div class="jg_imgalign_gal">
        <div class="jg_photo_container">
         <a title="Life at SIG" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/sig_corner_20120424_1376806095.jpg"><img src="<?php echo base_url();?>assets/img/gallary/sig_corner_20120424_1376806095.jpg"  class="jg_photo" alt=""/></a>
        </div>
      </div>
      </div>
    <div class="jg_clearboth"></div>
  </div> -->
</div>     
</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.js"></script>
 <style type="text/css" href="<?php echo base_url();?>assets/base/css/jquery.fancybox.css"></style>
 <script type="text/javascript">
   $(".fancybox")
    .attr('rel', 'gallery')
    .fancybox({
        padding : 0
    });
 </script>