<style type="text/css">


   h3 {
        font-size: 26px;
        margin: 0 0 10px;
        font-weight: bold;
        text-transform: capitalize;
    }

    .courseheight{
        min-height:160px;
        padding: 4% 0 4% ;
        margin:1%; 
        cursor: pointer;
    }
    .courselast{
        padding: 3% 0 3%!important;
    }
    .courseheight p{
        text-align: center;
        color: #fff;
    }

    .marginleft{
        margin-left: 10%;
    }
    .courseheight i{
        color: #fff;
        font-size: 40px;
    } 
    .courseheight:hover{
        box-shadow: 5px 5px 20px;
        border: solid 1px #e6c9ca;
        -webkit-transform: scale(1.1);
        -moz-transform: scale(1.1);
        -ms-transform: scale(1.1);
        -o-transform: scale(1.1);
        transform: scale(1.1);

    }
   .course1:hover {
       background: linear-gradient(0deg, rgba( 244,86,97,0.7), rgba( 244,86,97,0.7)),url("<?php echo base_url();?>assets/img/acadamics/01.jpg") no-repeat;
   }
   .course2:hover {
       background: linear-gradient(0deg, rgba( 244,86,97,0.7), rgba( 244,86,97,0.7)),url("<?php echo base_url();?>assets/img/acadamics/02.jpg") no-repeat;
   }
   .course3:hover {
       background: linear-gradient(0deg, rgba( 244,86,97,0.7), rgba( 244,86,97,0.7)),url("<?php echo base_url();?>assets/img/acadamics/05.jpg") no-repeat;
   }
   .course4:hover {
       background: linear-gradient(0deg, rgba( 244,86,97,0.7), rgba( 244,86,97,0.7)),url("<?php echo base_url();?>assets/img/acadamics/03.jpg") no-repeat;
   }
   .course45:hover {
       background: linear-gradient(0deg, rgba( 244,86,97,0.7), rgba( 244,86,97,0.7)),url("<?php echo base_url();?>assets/img/acadamics/04.jpg") no-repeat;
   }
    /*.courseheight i.fa:hover{
     color: #c42027;
    }*/
    .course1{background: #d33ba0;}
    .course2{background: #199c9a;}
    .course3{background: #2c6fd1;}
    .course4{background: #e7505a;}
    .course45{background: #e7b52a;}
    .courseheight:hover  i{ color:#ffffff; }
    .courseheight:hover  p{ color:#ffffff; }
</style>
<div class="aboutop">
    <div class="academics"></div>
</div> 
<div class="container">
    <h2 class="menutitle"><strong>Programmes offered</strong></h2>
    <hr>
    <div class="acamargin">
        <p>SIG is amongst the most reputed colleges in Pune that offers different types of programs dealing exclusively with expertise in the Geoinformatics domain. Here is an introduction to the various programs that aspirants can choose from, at SIG.</p>
    </div>
    <h2 class="menutitle"><strong>Program Offered at SIG</strong></h2>
    <div class="row">
        <a href="<?php echo base_url(); ?>MScinGeoinformatics">
            <div class="col-lg-2 courseheight course1">
                <p><i class="fa fa-book" aria-hidden="true"></i>
                    <br>
                    M.Sc.<br>(Geoinformatics)
                </p>
            </div>
        </a>
        <a href="<?php echo base_url(); ?>MTechGeoinformatics">
            <div class="col-lg-2 courseheight course2">
                <p><i class="fa fa-book" aria-hidden="true"></i>
                    <br>
                    M.Tech Geoinformatics
                    <br>and ST
                </p>
            </div>
        </a>
        <a href="<?php echo base_url(); ?>environment">
            <div class="col-lg-2 courseheight course45">
                <p><i class="fa fa-book" aria-hidden="true"></i>
                    <br>
                    M.Sc. Environment & Sustainability
                </p>
            </div>  
        </a> 
        <a href="<?php echo base_url(); ?>courseinSpatial">
            <div class="col-lg-2 courseheight course3">
                <p><i class="fa fa-book" aria-hidden="true"></i>
                    <br>
                    Certificate Course in Spatial Economics
                </p></div> 
        </a>
        <a href="<?php echo base_url(); ?>certificatecourse">
            <div class="col-lg-2 courseheight courselast course4">
                <p><i class="fa fa-camera" aria-hidden="true"></i>
                    <br>
                    Certificate Courses in Photogrammetry <br>& Remote Sensing
                </p>
            </div>  
        </a>   

    </div>
</div>
<br><br>
</div>
