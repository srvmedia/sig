<style type="text/css">
.geoinformatics {
    width: 100%;
    height: 320px;
    background: linear-gradient(0deg, rgba(0, 0, 0,0.4), rgba(0, 0, 0,0.4)), url("<?php echo base_url();?>assets/img/acadamics/Spatial-Economics.jpg") no-repeat;
    background-size: cover;
}
/*  bhoechie tab */
div.bhoechie-tab-container{
  z-index: 10;
  background-color: transparent;
  padding: 0 !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  border:1px solid #ddd;
  margin-top: 20px;
  margin-left: 50px;
  -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  box-shadow: 0 6px 12px rgba(0,0,0,.175);
  -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  background-clip: padding-box;
  opacity: 0.97;
  filter: alpha(opacity=97);
}
div.bhoechie-tab-menu{
  padding-right: 0;
  padding-left: 0;
  padding-bottom: 0;
}
div.bhoechie-tab-menu div.list-group{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a .glyphicon,
div.bhoechie-tab-menu div.list-group>a .fa {
  color: #b42c2f;
}
div.bhoechie-tab-menu div.list-group>a:first-child{
  border-top-right-radius: 0;
  -moz-border-top-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a:last-child{
  border-bottom-right-radius: 0;
  -moz-border-bottom-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a.active,
div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
div.bhoechie-tab-menu div.list-group>a.active .fa{
  background-color: #b22629;
  background-image: #5A55A3;
  color: #ffffff;
}
div.bhoechie-tab-menu div.list-group>a.active:after{
  content: '';
  position: absolute;
  left: 100%;
  top: 50%;
  margin-top: -13px;
  border-left: 0;
  border-bottom: 13px solid transparent;
  border-top: 13px solid transparent;
  border-left: 10px solid #b42c2f;
}
.subdivclass {
    background-color: #d0cece;
    color: #b02d29;
    border-right: 2px solid #fff;
} 
li.subdivclass{
  padding-left: 0px;
  padding-right: 0px;
}   
div.bhoechie-tab-content{
  background-color: #ffffff;
  /* border: 1px solid #eeeeee; */
  padding-left: 20px;
 /* padding-top: 10px;*/
}

div.bhoechie-tab div.bhoechie-tab-content:not(.active){
  display: none;
}
.containermar{
  margin:10% 0 8% 0; 
}
 .rightpad{
      border-right: 2px solid #f19999;
    }
    .rightpad li.active{
      background: url("<?php echo base_url();?>assets/img/liback.png") no-repeat;
      background-position: center right;
      padding: 10px 0px 10px;
    }
    .menutop li.active a{
      display: inline;
    }
    .menutop.nav>li>a {
      padding: 3px 15px;
      margin-bottom: 12px;
    }
.sub-menu{
    text-align: center;
    font-size: 20px;
    font-weight: 600;
    color: #c42027;
}
</style>
<div class="row aboutop">
    <div class="geoinformatics"></div>
</div> 
<div class="container">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 menutop list-group bhoechie-tab-menu">
               <ul class="nav nav-pills nav-stacked rightpad">
                  <li class="active"><a class="liborder" data-toggle="tab" href="#Overview">Course in Spatial Economics</a></li>
                  <li><a class="liborder" data-toggle="tab" href="#eligibility">Eligibility & Duration</a></li>
                  <li><a class="liborder" data-toggle="tab" href="#feeandregister">Fees & Registration</a></li>
                   <li class="sub-menu">Programme Offered by SIG</li>
                   <li><a class="liborder" href="<?php echo base_url(); ?>MScinGeoinformatics">M.Sc.(Geoinformatics)</a></li>
                   <li><a class="liborder" href="<?php echo base_url(); ?>MTechGeoinformatics">M.Tech. Geoinformatics & ST</a></li>
                   <li><a class="liborder" href="<?php echo base_url(); ?>environment">M.Sc. Environment & Sustainability</a></li>
                   <li><a class="liborder" href="<?php echo base_url(); ?>certificatecourse">Certificate Course in Photogrammetry and Remote Sensing</a></li>
                </ul>
              </div>           
              <div class="tab-content col-lg-9 col-md-9 col-sm-12 col-xs-12 bhoechie-tab">
                <!-- flight section -->
                <div id="Overview" class="tab-pane fade in active">
                      <div class="inner-information">
                        <table class="contentpaneopen">
                        <tbody><tr>
                           <h2 class="menutitle">
                                Certificate Course in Spatial Economics     
                            </h2>
                           
                          <td align="right" width="100%" class="buttonheading">
                          <a href="<?php echo base_url();?>assets/img/pdf/certificate-course-in-spatial-economics.pdf" title="PDF" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="<?php echo base_url();?>assets/img/pdf_button.png" alt="PDF"></a>   </td>
                          
                              <td align="right" width="100%" class="buttonheading">
                          <a href="/certificate-course-in-spatial-economics.html?tmpl=component&amp;print=1&amp;page=" title="Print" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="<?php echo base_url();?>assets/img/printButton.png" alt="Print"></a>    </td>
                          
                              <td align="right" width="100%" class="buttonheading">
                          <a href="/component/mailto/?tmpl=component&amp;link=aHR0cDovL3d3dy5zaWcuYWMuaW4vY2VydGlmaWNhdGUtY291cnNlLWluLXNwYXRpYWwtZWNvbm9taWNzLmh0bWw%3D" title="E-mail" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="<?php echo base_url();?>assets/img/emailButton.png" alt="E-mail"></a>    </td>
                                </tr>
                            <hr>         
                      </tbody></table>

                      <table class="contentpaneopen">
                      <tbody>
                      
                      
                      <tr>
                      <td valign="top">
                      <div align="justify">
                      <p>The course has been designed to demonstrate the concepts for spatial decision making in the area of an economic context. The course covers advanced topics of spatial science and its role in economic policy, with a focus on Cost Benefit Analysis (CBA), and on applications with a strong spatial dimension.Students will developtheir understanding through series of case studies that cater for urban planning and Environment Management Interest.Students will be engaged in learning involving a range of activities</p>
                      <div class="ugc">
                          <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody><tr>
                              <td class="top-sh"><table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                                  <tbody><tr>
                                    <td valign="top" align="left" style="padding:30px 10px;" class="bottom-sh">               
                                        <table width="594" cellspacing="0" cellpadding="5" border="0" class="info-section-a" style="margin-left:25px;">
                      <tbody>
                        <tr>
                          <th valign="top" align="left" class="sem">Topics</th>
                          <th width="13%" valign="top" align="left"></th>
                          <th valign="top" align="left" class="sem">Hours</th>
                        </tr>
                         <tr>
                          <td width="80%" valign="top" align="left">Micro Economics</td>
                          <td valign="top" align="left">:</td>
                          <td valign="top" align="left">7</td>
                        </tr>
                        <tr>
                          <td width="80%" valign="top" align="left">Spatial Data Concept </td>
                          <td valign="top" align="left">:</td>
                          <td valign="top" align="left"> 7 </td>
                        </tr>
                        <tr>
                          <td valign="top" align="left">Basics of Remote Sensing and GIS</td>
                          <td valign="top" align="left">:</td>
                          <td valign="top" align="left">7</td>
                        </tr>
                        <tr>
                          <td valign="top" align="left">Applications of Remote Sensing and GIS </td>
                          <td valign="top" align="left">:</td>
                          <td valign="top" align="left">7</td>
                        </tr>
                         <tr>
                           <td valign="top" align="left">Spatial  Analysis and Economics</td>
                           <td valign="top" align="left">:</td>
                           <td valign="top" align="left">6</td>
                         </tr>
                         <tr>
                           <td valign="top" align="left">Geography Determinants</td>
                           <td valign="top" align="left">:</td>
                           <td valign="top" align="left">6</td>
                         </tr>
                         <tr>
                           <td valign="top" align="left">Spatial Urban Model and economics </td>
                           <td valign="top" align="left">:</td>
                           <td valign="top" align="left">6</td>
                         </tr>
                         <tr>
                           <td valign="top" align="left">Urban landscape </td>
                           <td valign="top" align="left">:</td>
                           <td valign="top" align="left">6</td>
                         </tr>
                         <tr>
                           <td valign="top" align="left">Transport </td>
                           <td valign="top" align="left">:</td>
                           <td valign="top" align="left">6</td>
                         </tr>
                         <tr>
                          <td valign="top" align="left">Agriculture</td>
                          <td valign="top" align="left">:</td>
                          <td valign="top" align="left">6<br> </td>
                        </tr>
                        <tr>
                          <td valign="top" align="left">GIS and Insurance </td>
                          <td valign="top" align="left">:</td>
                          <td valign="top" align="left">6<br> </td>
                        </tr>
                        <tr>
                          <td valign="top" align="left">Location Analysis (LA) and Business Intelligence (BI)</td>
                          <td valign="top" align="left">:</td>
                          <td valign="top" align="left">6<br> </td>
                        </tr>
                        <tr>
                          <td valign="top" align="left">Project</td>
                          <td valign="top" align="left">:</td>
                          <td valign="top" align="left">40<br> </td>
                        </tr>
                        <tr>
                          <th valign="top" align="left">Total</th>
                          <th valign="top" align="left">:</th>
                          <th valign="top" align="left">110<br> </th>
                        </tr>
                      </tbody></table>
                                    </td>
                                  </tr>
                                </tbody></table></td>
                            </tr>
                          </tbody></table>
                      </div>
                     <div class="clearfix"></div>
                      <p><span class="highlight">Objective : </span>The course demonstrates the economics determinants, which influence spatial decisions of household, business and firms. The course will cover the new models of landuse pattern, spatial concepts, concept of Environment Economics, Economic Geography and its kindred which highly influence the modern economics.</p>

                      <p><span class="highlight">Hands on : </span>ArcGIS Tools 12 2.1 3D Analysis Tools Analysis Tools Conversion Tools Data Management Tools 
                      5 Multidimensional Tools Spatial Analyst Tools. Geocoding Tools.Hawth's Tools Additional Scripting Utilities.</p>

                      <p><span class="highlight">Learning Outcome : </span>To understand and familiar with spatial theories, Spatial Analysis,  tools related with economical processes in the space and will be informed about possibilities and methods of the business localization in the space.</p>                     
                     
                      </div>

                      </td>
                      </tr>

                      <tr>
                        <td class="modifydate">
                          Last Updated on Thursday, 30 July 2015 12:18  </td>
                      </tr>
                      </tbody></table>
                      <span class="article_separator">&nbsp;</span>

                           
                  </div>
                </div>
                <!-- train section -->              
                <div id="eligibility" class="tab-pane fade bhoechie-tab-content">
                      <h2 class="menutitle">Eligibility</h2>
                      <hr>
                      <p>Graduation in any stream (Student of final year of graduation are also eligible)  
                      Having basic knowledge of Economics and computer</p>
                     <h2 class="menutitle">Duration</h2>
                      <hr>
                      <p><span class="highlight">Duration :</span> 2 Month </p>
                      <p><span class="highlight">Timing : </span>6 to 8:30PM (Monday to Friday)</p>
                    <h2 class="menutitle">Evaluation</h2>
                      <hr> 
                      <ul class="bullet-style">
                      <li>Written Exam (end of the course):50 Marks</li>
                      <li>Project/case study(During Mid-course):50 Marks</li> 
                      </ul><br>     
                </div>
                 <div id="feeandregister" class="tab-pane fade bhoechie-tab-content">
                      <h2 class="menutitle">Fee Structure</h2>
                      <hr>
                      <p><span class="highlight">Fee : </span>Twelve Thousand (16000) Only, Ist and IIndinstallment (10000 +6000)</p>
                      <p><span class="highlight">Seats : </span>20 Seats</p> 
                      <p><span class="highlight">Registration fee : </span>Rs200</p> 
                      <p><span class="highlight">Last date of registration : </span>2 November 2015</p> 
                      <p><span class="highlight">After 2nd November 2015, Late Registration fee : </span>Rs.1000</p> 
                      <p><span class="highlight">Course start : </span>23 November 2015, Course end: 23 January 2016</p>
                      <h2 class="menutitle">Some examples of research under the spatial economic focus area are:</h2>
                      <hr>
                      <ul class="bullet-style" style="left:0">
                      <li>Transportation effect on landuse</li>
                      <li>The impact of transportation investments on property values</li>
                      <li>Simulation of future urban sprawl and job locations</li>
                      <li>Determination of spatial distribution of noxious facilities and their impact on property values</li>
                      <li>Disease clusters and their relation to socioeconomic and neighborhood spatial characteristics</li>
                      <li>Agglomeration effects of firms and their spatial spillovers</li>
                      <li>Measurement of sprawl, compactness, and walkability</li>
                      <li>Travel and air quality connection</li>
                      <li>Development of computer-based analytical tools to examine spatial interactions among places and activities </li>
                      <li>Heat island effects and their impacts on human behavior</li>
                      </ul>
                      <h2 class="menutitle">Registration</h2>
                      <hr>
                      <p>                      
                      <a href="/images/pdf/spatial-economics-application-form.pdf" target="_blank">Click here for registration...</a>
                      <p class="modifydate"><h5>( Last Updated on Thursday, 30 July 2015 12:18 )</h5></p>
                      </p>
                </div>
                 
            </div>
        </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
</script>