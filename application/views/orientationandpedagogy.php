		
		
		<style type="text/css">
    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }    
    .rightpad{
      border-right: 2px solid #f19999;
    }
    .rightpad li.active{
      padding: 10px 0px 10px;
    }
    li.active a{
      display: inline;
    }
   /* .nav>li>a {
      padding: 3px 15px;
      margin-bottom: 12px;
    }*/
   </style>
<div class="row aboutop">
    <div class="bannerimg"></div>
</div>	
<div class="container">  
<div class="col-md-3 ">
 <ul class="nav nav-pills nav-stacked rightpad">
    <li><a class="liborder" href="<?php echo base_url(); ?>collaborations">Collaborations</a></li>
    <li><a class="liborder" href="<?php echo base_url(); ?>testimonials">Testimonials</a></li>
    <li><a class="liborder" href="<?php echo base_url(); ?>careers">Careers in SIG</a></li>
	<li><a class="liborder">Resource Center
    <span class="caret"></span></a>
    <ul>
      <li><a href="<?php echo base_url(); ?>faqs">FAQ's</a></li>
      <li ><a href="<?php echo base_url(); ?>scientificpapers">Scietific Papers</a></li>
    </ul>
	
  </li>
  <li class="active"><a class="liborder">SIG Corner
    <span class="caret"></span></a>
    <ul style="margin-top:10px;">
      <li><a href="<?php echo base_url(); ?>infrastructure">Infrastructure</a></li>
      <li ><a href="<?php echo base_url(); ?>achievements">Achievements</a></li>
	  <li ><a href="<?php echo base_url(); ?>orientationandpedagogy">Orientation and Pedagogy</a></li>
	  <li ><a href="<?php echo base_url(); ?>hostelandamenities">Hostel and Amenities</a></li>
	  <li ><a href="<?php echo base_url(); ?>conferences">Conferences</a></li>
    </ul>
	
  </li>
    <li><a class="liborder" href="http://www.siu.edu.in/resources.php" target="_blank">Health Facilities</a></li>  
  </ul>
</div>
  <div class="col-md-9 menutop">
   
      <h2 class="menutitle"><strong>Orientation and Pedagogy</strong></h2>
      <hr>
    
	
	
	
	<table class="contentpaneopen">



<tbody><tr>
<td valign="top">
<h2 style="text-align: justify;">Orientation:</h2>
<p>SIG conducted a week long orientation programme which covers Micro lab, Perceptions and Attitudes, Effective and Efficient Communication, Interpersonal Relationships, Group Dynamics, Leadership, Motivation, Work life Balance, Stress Management. This programme is conducted by a professional expert in psychological counselling with 30 years of experience. 
</p>
<h2 style="text-align: justify;">Pedagogy:</h2>
<p>Group Discussions, discussions on research papers, class seminars, presentations, student conferences, guest lectures, hands on practice project and problem based learning are a part of teaching methods in the programmes. 
 Inclusion of courses related to current practices and emerging areas in the programmes, field visits, industrial visits, participation in exhibitions and competitions which are part of curricular and co-curricular activities help in developing scientific temper in students. 
 </p>

</td>
</tr>

</tbody></table>
    
  </div>
</div>

</div>    
	