<style type="text/css">
    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }
    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    .rightpad li.active a{
        display: inline;
        line-height:2;

    }
    td, th {
        padding: 5px;
        padding-left: 3%;
    }
    li.subdivclass{
        padding-left: 0px;
        padding-right: 0px;
    }
    .service-icon i.fa{
        font-size: 35px;
        color: red;
    }
    .feestrct p{
        min-height: 94px;
    }
    .feestrct i.fa {
        font-size: 22px;
        color: #ee7f1a;
    }
    .fees{
        border:solid 1px gray;
        max-height: 102px !important;
    }
    .fees p{
        text-align: center !important;
    }
    .impdate i.fa{
        font-size: 22px;
        color: #ee7f1a;
    }
    .service-figure {
        position: relative;
        border: 1px solid #999;
        -webkit-box-shadow: 0px 0px 11px 0px rgba(50, 50, 50, 0.46);
        -moz-box-shadow: 0px 0px 11px 0px rgba(50, 50, 50, 0.46);
        box-shadow: 0px 0px 11px 0px rgba(50, 50, 50, 0.46);
    }
    .service-figure:hover .service-caption, .employee-desination, .hover-overlay, .product-name:hover, .product-figure:hover .product-caption, .gallery-hover, .flicker-img li a::before, .map-overlay, .overlay-pink::before, .scrollup:hover, .video-titel .play-btn:hover {
        background: rgba( 244,86,97,0.7);
    }
    .service-figure:hover .service-icon i {
        color: #fff!important;
        border-color: #fff;
    }

    .service-figure:hover h4 {
        color: #fff!important;
        border-color: #fff;
    }
    .service-icon {
        height: 82px;
        width: 82px;
        line-height: 105px;
        text-align: center;
        border: 2px solid #999;
        display: inline-block;
        border-radius: 100%;
        margin: 0 0 20px;
    }
    h3 {
        font-size: 26px;
        margin: 0 0 10px;
        font-weight: bold;
        text-transform: capitalize;
    }
    .service-figure:hover .service-caption h3, .service-figure:hover .service-caption p, .service-figure:hover .service-caption .read-more {
        color: #fff;
    }
    .msc11{
        margin-top: 3%;
        min-height: 257px;
    }
    .item1{
        position: absolute;
        top:0px;
        background: #fff;
    }
    .service-caption {
        text-align: center;
        padding: 23px;
        background: rgba( 255,255,255,1.0);
        min-height: 231px;
        min-width: 231px;
    }
    .subdivclass {
        background-color: #d0cece;
        color: #b02d29;
        border-right: 2px solid #fff;
    }
    .item1 a:active, .item1 > a:hover{
        color: #ffffff;
    }
    .service-figure:hover .service-icon{
        border: 2px solid #fff;
    }
    img{
        width: 100%;
    }

    .sub-menu{
        text-align: center;
        font-size: 20px;
        font-weight: 600;
        color: #c42027;
    }
    .service-figure:hover{
        background-color:#ffffff;
        box-shadow: 5px 5px 20px;
        border: solid 1px #e6c9ca;
        -webkit-transform: scale(1.1);
        -moz-transform: scale(1.1);
        -ms-transform: scale(1.1);
        -o-transform: scale(1.1);
        transform: scale(1.1);
    }
    .feestrct p{
        min-height: 94px;
    }
    .feestrct i.fa {
        font-size: 22px;
        color: #ee7f1a;
    }
    .fees{
        border:solid 1px gray;
        max-height: 102px !important;
    }
    .fees p{
        text-align: center !important;
    }
</style>
<div class="aboutop">
    <div class="admission"></div>
</div>
<div class="container">
    <div class="col-md-3 menutop">
        <ul class="nav nav-pills nav-stacked rightpad">
            <li><a class="liborder" href="<?php echo base_url()?>admission#Overview">M.Sc. Geoinformatics</a></li>
            <li><a class="liborder" href="<?php echo base_url()?>admission#mtech">M.Tech. Geoinformatics & ST</a></li>
            <li><a class="liborder" href="<?php echo base_url()?>admission#environment">M.Sc. Environment & Sustainability</a></li>
            <li><a class="liborder" href="<?php echo base_url()?>admission#symbiosisiu">Certificate Course in Photogrammetry and Remote Sensing</a></li>
            <li><a class="liborder" href="<?php echo base_url()?>admission#certificate">Certificate Course in Spatial Economics</a></li>
        </ul>
    </div>
    <div class="tab-content col-md-9 menutop">
        <div id="Overview" class="tab-pane fade in active">
            <div class="inner-information">
                <div class="row">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="subdivclass col-lg-3 active"><a href="#mscgeo" aria-controls="home" role="tab" data-toggle="tab">M.Sc. (Geoinformatics)</a></li>
                        <li role="presentation" class="subdivclass col-lg-5"><a href="#mscenv" aria-controls="profile" role="tab" data-toggle="tab">M.Sc. (Environment & Sustainability)</a></li>
                        <li role="presentation" class="subdivclass col-lg-4"><a href="#mtechgeo" aria-controls="profile" role="tab" data-toggle="tab">M.Tech. (Geoinformatics & ST)</a></li>
                        </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="mscgeo">
                            <div class="col-lg-12 msc">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <h2 class="menutitle">Eligibility</h2>
                                        <hr>
                                    <p>Graduates in Engineering, IT, Computer Science, Science, Agriculture, Management, Geography and Commerce with a Minimum of 50% marks (45% marks for SC/ST candidates).</p>
                                        <p>Final year students awaiting results may also apply.</p>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <h2 class="menutitle">Selection Procedure</h2>
                                        <hr>
                                        <p><strong>Appearing for SNAP test is NOT mandatory to apply for SIG.</strong></p>
                                        <p>Candidates registered for SIG will be eligible to appear for the Personal Interview.</p>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <h2 class="menutitle">Fee structure per annum</h2>
                                        <hr>
                                        <div class="row feestrct">
                                            <div class="col-lg-2 col-xs-12 fees">
                                                <p>Category<br> <i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Open</p></div>
                                            <div class="col-lg-2 col-xs-12 fees">
                                                <p>Tuition Fee <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Rs.1,95,000</p></div>
                                            <div class="col-lg-2 col-xs-12 fees">
                                                <p>Refundable Deposit <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Rs.20,000</p></div>
                                            <div class="col-lg-2 col-xs-12 fees">
                                                <p>Total <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Rs.2,15,000</p></div>
                                            <div class="col-lg-2 col-xs-12 fees">
                                                <p>1st Installment <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Rs.1,20,000</p></div>
                                            <div class="col-lg-2 col-xs-12 fees"><p>2nd Installment <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Rs.95,000</p></div>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="col-lg-12 col-md-12" style="margin-top: 20px;">
                                        <a style="background-color: #ffa500;border: 1px solid #ffa500;" class="btn btn-xs btn-warning" href="https://siu.ishinfo.com/SIG/Register/Index.aspx" target="_blank">Apply Online</a>
                                        <a style="background-color: #ffa500;border: 1px solid #ffa500;" class="btn btn-xs btn-warning" href="<?php echo base_url();?>assets/img/pdf/Application-form-for-M.Sc-Geoinformatics-SIG.pdf" target="_blank">Download Form</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="mscenv">
                            <div class="col-lg-12 msc">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <h2 class="menutitle">Eligibility</h2>
                                        <hr>
                                        <p>Post Graduate / Graduate in Engineering, IT, Science, Computer Science, Agriculture of any statutory university with 50% marks(45% for SC / ST) at graduation level.</p>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <h2 class="menutitle">Selection Procedure</h2>
                                        <hr>
                                        <p><strong>Appearing for SNAP test is NOT mandatory to apply for SIG.</strong></p>
                                        <p>Candidates registered for SIG will be eligible to appear for the Personal Interview.</p>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <h2 class="menutitle">Fee structure per annum</h2>
                                        <hr>
                                        <div class="row feestrct">
                                            <div class="col-lg-2 col-xs-12 fees">
                                                <p>Category<br> <i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Open</p></div>
                                            <div class="col-lg-2 col-xs-12 fees">
                                                <p>Tuition Fee <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Rs.1,75,000</p></div>
                                            <div class="col-lg-2 col-xs-12 fees">
                                                <p>Refundable Deposit <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Rs.20,000</p></div>
                                            <div class="col-lg-2 col-xs-12 fees">
                                                <p>Total <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Rs.1,95,000</p></div>
                                            <div class="col-lg-2 col-xs-12 fees">
                                                <p>1st Installment <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Rs.1,00,000</p></div>
                                            <div class="col-lg-2 col-xs-12 fees"><p>2nd Installment <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Rs.95,000</p></div>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="col-lg-12 col-md-12" style="margin-top: 20px;">
                                        <a style="background-color: #ffa500;border: 1px solid #ffa500;" class="btn btn-xs btn-warning" href="https://siu.ishinfo.com/SIG/Register/Index.aspx" target="_blank">Apply Online</a>
                                        <a style="background-color: #ffa500;border: 1px solid #ffa500;" class="btn btn-xs btn-warning" href="<?php echo base_url();?>assets/img/pdf/Application-form-M.Sc.Environment-and-Sustainability-SIG.pdf" target="_blank">Download Form</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div role="tabpanel" class="tab-pane" id="mtechgeo">
                            <div class="col-lg-12 msc">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <h2 class="menutitle">Eligibility</h2>
                                        <hr>
                                        <p>At least 50% marks in any one of following branches , B.Tech/BE , M.Sc Geoinformatics, M.Sc. Physics, M.Sc. Geology, M.Sc. Environmental Science, M.Sc. Mathematics, M.Sc. Agriculture or equivalent.</p>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <h2 class="menutitle">Selection Procedure</h2>
                                        <hr>
                                        <p><strong>Appearing for SNAP test is NOT mandatory to apply for SIG.</strong></p>
                                        <p>Candidates registered for M.Tech will be eligible to appear for the Writen Test conducted by SIG.</p>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <h2 class="menutitle">Fee structure per annum</h2>
                                        <hr>
                                        <div class="row feestrct">
                                            <div class="col-lg-2 col-xs-12 fees">
                                                <p>Category<br> <i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Open</p></div>
                                            <div class="col-lg-2 col-xs-12 fees">
                                                <p>Tuition Fee <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Rs.2,20,000</p></div>
                                            <div class="col-lg-2 col-xs-12 fees">
                                                <p>Refundable Deposit <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Rs.20,000</p></div>
                                            <div class="col-lg-2 col-xs-12 fees">
                                                <p>Total <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Rs.2,40,000</p></div>
                                            <div class="col-lg-2 col-xs-12 fees">
                                                <p>1st Installment <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Rs.1,45,000</p></div>
                                            <div class="col-lg-2 col-xs-12 fees"><p>2nd Installment <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                                    Rs.95,000</p></div>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="col-lg-12 col-md-12" style="margin-top: 20px;">
                                        <a style="background-color: #ffa500;border: 1px solid #ffa500;" class="btn btn-xs btn-warning" href="https://siu.ishinfo.com/SIG/Register/Index.aspx" target="_blank">Apply Online</a>
                                        <a style="background-color: #ffa500;border: 1px solid #ffa500;" class="btn btn-xs btn-warning" href="<?php echo base_url();?>assets/img/pdf/Application-form-for-MTech-sig.pdf" target="_blank">Download Form</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="article_separator">&nbsp;</span>
            </div>

        </div>

    </div>
</div>