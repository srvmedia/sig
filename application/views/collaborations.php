		
		
		
		<style type="text/css">

    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    li.active a{
        display: inline;

    }

    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }    
    .rightpad{
      border-right: 2px solid #f19999;
    }
    .rightpad li.active{
      padding: 10px 0px 10px;
    }
    li.active a{
      display: inline;
    }
   /* .nav>li>a {
      padding: 3px 15px;
      margin-bottom: 12px;
    }*/
   </style>
<div class="row aboutop">
    <div class="bannerimg"></div>
</div>	
<div class="container">  
<div class="col-md-3 menutop">
 <ul class="nav nav-pills nav-stacked rightpad">
    <li class="active"><a  class="liborder" href="<?php echo base_url(); ?>collaborations">Collaborations</a></li>
    <li><a class="liborder" href="<?php echo base_url(); ?>testimonials">Testimonials</a></li>
<li><a class="liborder" href="<?php echo base_url(); ?>faqs">FAQ's</a></li>
<li ><a class="liborder" href="<?php echo base_url(); ?>scientificpapers">Scietific Papers</a></li>
     
    <li><a class="liborder" href="http://www.siu.edu.in/resources.php" target="_blank">Health Facilities</a></li>  
  </ul>
</div>
  <div class="col-md-9 menutop">
   
      <h2 class="menutitle"><strong>Collaborations</strong></h2>
      <hr>
     <p>Symbiosis has always believed in giving its students a global level learning platform that aids in improving their learning and collaborations are an effort in this direction. SIG, being a part of Symbiosis Pune works extensively towards this goal by establishing successful collaborations within the country. Through its collaborations, SIG students get an opportunity to learn on a larger scale and therefore develop a stronger academic background in Geoinformatics</p>
       <ul>
          <li>SIG collaborates with the National Remote Sensing Centre, Indian Space Research Organisation to execute projects of national importance.</li>
           <li>MOU has been signed between SIG and NIIT GIS Ltd. ESRI India is willing to extend industry collaboration to SIG in areas like curriculum review collaboration, Student Internship Collaboration, Placement Assistance in ESRI India, Academic R & D Centre.</li>
           <li>Research facility/Centre - State / National / International recognition: SIG has been recognized for membership of Joint Project 2 of Sentinel Asia, a Japanese Aerospace Exploration Agency project to provide support by way of web GIS and Remote Sensing techniques in Disaster Management.</li>
           <li>SIG has been appointed for SBFP project Sikkim, assisted by Japan International Corporation Association for setup a GIS Center at Secretariat in Sikkim.</li>
           <li>SIG has been nominated for Step 2 program of Japan Aerospace exploratory Agency, at International Centre for Integrated Mountain Development (ICIMOD) Kathmandu Nepal</li>
       </ul>
       <h3 class="menutitle">International Collaboration</h3>
       <ul>
            <li>SIG is a technical partner with International Water Management Institute (IWMI), Colombo for South Asia Drought Management System (SADMS) in association with World Metrological Organisation (WMO) and Global Water Partner (GWP).</li>
            <li>SIG is a member of Sentinel Asia Project with Japan Aerospace Exploratory Agency (JAXA) to provide space based data for disaster information in India. In this collaboration, SIG is authorized to access different satellite data from the Asian countries of Japan, China, India, Thailand, Philippines, S. Korea, and Malaysia and disseminate the information during disasters.</li>
            <li>SIG has sign MOU with Digital Globe Foundation USA (<a href="https://www.digitalglobe.com/">https://www.digitalglobe.com/</a>)</li>
            <li>Digital Globe Foundation and SIG intend to Explore how high resolution satellite imagery remote sensing and other related tool for analyzing geospatial information can dramatically reshape the technical and financial calculus of monitoring the Earth and develop new approaches to using these tools.</li>
        </ul>
  </div>
</div>

</div>    
	