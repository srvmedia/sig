<style type="text/css">
    p {
        text-align: justify;
    }

    td a {
        color: #0261a5;
    }

    .rightpad {
        border-right: 2px solid #f19999;
    }

    .rightpad li.active {
        background: url("<?php echo base_url();?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }

    li.active a {
        display: inline;
    }

    .nav > li > a {
        padding: 3px 15px;
        margin-bottom: 12px;
    }
    .fa:hover{
        color: #ba0000;
    }
</style>
<div class="aboutop">
    <div class="contact"></div>
</div>
<div class="container">
    <div class="col-md-3 menutop">
<!--        <ul class="nav nav-pills nav-stacked rightpad">-->
<!--            <li class="active"><a class="liborder" data-toggle="tab" href="#address">Address</a></li>-->
<!--            <li><a class="liborder" data-toggle="tab" href="#enquiry">Enquiry</a></li>-->
<!--        </ul>-->
        <table class="contentpaneopen">
            <tbody>
            <tr>
                <td class="contentheading" width="100%">
                    Address
                </td>
            </tr>
            </tbody>
        </table>
        <p>
            <br>
            <strong>Symbiosis Institute of<br> Geoinformatics</strong>
            <br>5th Floor, Atur Centre,
            <br>Gokhale Cross Road, Model Colony,
            <br>Pune – 411016
            <table border="0" width="100%">
            <tbody>
            <tr>
                <td width="10%;" valign="top"><i class="fa fa-phone"></i></td>
                <td>020 25672841/43</td>
            </tr>
            <tr>
                <td width="10%;" valign="top"><i class="fa fa-mobile"></i></td>
                <td>7709998185</td>
            </tr>
            <tr>
                <td width="10%;" valign="top"><i class="fa fa-fax"></i></td>
                <td>020 25672842</td>
            </tr>
            <tr>
                <td width="10%;" valign="top"><i class="fa fa-envelope"></i></td>
                <td><a href="mailto:enquiry@sig.ac.in">enquiry@sig.ac.in</a>,<br> <a href="mailto:admissions@sig.ac.in">admissions@sig.ac.in</a>, <br><a href="mailto:info@sig.ac.in">info@sig.ac.in</a></td>
            </tr>
            </tbody>
            </table>
            </p>
    </div>
    <div class="col-md-6 menutop" style="margin-top: 45px;">
        <iframe width="500" height="400" frameborder="0" scrolling="no"
                marginheight="0" marginwidth="0"
                src="https://maps.google.com/maps/ms?msid=201495953025523845389.0004c4b47b7884f35fb63&amp;msa=0&amp;ie=UTF8&amp;t=m&amp;ll=18.534493,73.833661&amp;spn=0.002034,0.003873&amp;z=18&amp;iwloc=0004c4b47f1a44eaaf15c&amp;output=embed"></iframe>

    </div>

    <div class="col-md-3 menutop">
        <form id="contact_form" action="#" method="POST" enctype="multipart/form-data">

            <div class="row">
                <div class="contentheading">Contact Form</div>
                <br>
                <p id="returnmessage"></p>
                <label for="name">Your name *: </label><br />
                <input id="name" class="form-control" name="name" type="text" value="" size="40" />
            </div>
            <div class="row">
                <label for="email">Your email *: </label><br />
                <input id="email" class="form-control" name="email" type="email " value="" size="40" />
            </div>
            <div class="row">
                <label for="message">Your message *:</label><br />
                <textarea id="message" class="form-control" name="message" rows="5" cols="40"></textarea><br>
            </div>
            <div class="row">
                <div class="g-recaptcha" data-sitekey="6LfAbQoUAAAAAKD1dtiJtY3iROvh2-G12UR4ZgWX"></div>
                </div>
            <button type="button" name="form[submit]" id="submit" class="btn btn-primary"
                    style="background: #af251b;border-color: #c42027;">Submit
            </button>
        </form>
    </div>
</div>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $('#submit').click(function () {
            var name=$('#name').val();
            var email=$('#email').val();
            var message=$('#message').val();
            var captchresp=$('#g-recaptcha-response').val();
            $("#returnmessage").empty();

            if (name == '' && email == '' && message == '') {
                $("#returnmessage").css('color','red');
                $("#returnmessage").append("Please Fill Required Fields");
                return false;
            } else if(name == '') {
                $("#returnmessage").css('color','red');
                $("#returnmessage").append("Name field is required");
                return false;
            }else if(email == '') {
                $("#returnmessage").css('color','red');
                $("#returnmessage").append("Email field is required");
                return false;
            }else if(message == '') {
                $("#returnmessage").css('color','red');
                $("#returnmessage").append("Message field is required");
                return false;
            }else{

                var emailReg = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;
                if (emailReg.test(email) == false)
                {
                    $("#returnmessage").css('color','red');
                    $("#returnmessage").append("Enter Valid Email ID");
                    return false;
                }

                $.ajax({
                    type:'POST',
                    url:'<?php echo base_url();?>contactusform',
                    data:{
                        name1: name,
                        email1: email,
                        message1: message,
                        captchresp1:captchresp
                    },
                    success:function(data){

                        if(data==1) {
                            $("#returnmessage").css('color','red');
                            $("#returnmessage").append("Please click on the reCAPTCHA box.");
                        }
                        if(data==0) {
                            $("#returnmessage").css('color','red');
                            $("#returnmessage").append("Robot verification failed, please try again.");
                        }
                        if(data==2) {
                            $('#name').val('');
                            $('#email').val('');
                            $('#message').val('');
                            $('#returnmessage').html('<div align="center"><i class="fa fa-thumbs-up" style="font-size: 200px;color:#12576b;"></i><br> <h3 class="sh" style="color:#ed6235;">Thank you for the request! </br> We will get in touch with you shortly.</h3></div>');
                        }
                        if(data==3) {
                            $("#returnmessage").css('color','red');
                            $("#returnmessage").append("Error to send mail.");
                        }
                    },
                    error:function(){

                    }
                });
            }
        });
        });

        </script>