<style type="text/css">
    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }    
    .rightpad{
      border-right: 2px solid #f19999;
    }
    .rightpad li.active{
      background: url("<?php echo base_url();?>assets/img/liback.png") no-repeat;
      background-position: center right;
      padding: 10px 0px 10px;
    }
    li.active a{
      display: inline;
    }
   /* .nav>li>a {
      padding: 3px 15px;
      margin-bottom: 12px;
    }*/
   </style>
<div class="aboutop">
    <div class="aboutusbanner"></div>
</div>	
<div class="container">  
<div class="col-md-3 menutop">
 <ul class="nav nav-pills nav-stacked rightpad">
    <li class="active"><a class="liborder" data-toggle="tab" href="#Overview">Overview</a></li>
    <li><a class="liborder" href="http://siu.edu.in/HTML_SIU/index.php" target="_blank">Symbiosis International University</a></li>
    <li><a class="liborder" data-toggle="tab" href="#Visionaries">Vision & Mission</a></li>
    <li><a class="liborder" data-toggle="tab" href="#Visionarie">Visionaries</a></li>
    <li><a class="liborder" data-toggle="tab" href="#Director">Director</a></li>
    <li><a class="liborder" href="http://www.symbiosis.ac.in/" target="_blank">Symbiosis Society</a></li>
    <li><a class="liborder" href="http://siu.edu.in/symbiosis-committee.php" target="_blank">SIU Committees</a></li>  
  </ul>
</div>
  <div class="tab-content col-md-9 menutop">
    <div id="Overview" class="tab-pane fade in active">
      <h2 class="menutitle"><strong>Overview</strong></h2>
      <hr>
    <!--   <img src="<?php echo base_url();?>assets/img/inner-banner.jpg" class="img-responsive"> -->
      <p>India is one of the fastest growing economies and the contribution of Geoinformatics towards growth has been immense. In spite of having gained international recognition at a very early stage, Geoinformatics is gradually gaining popularity in the Indian subcontinent as well. Symbiosis Pune, was amongst the first to recognize this brewing change of trends and accordingly took initiatives that led to the inception of SIG in 2004. Once SIG came into existence, there was no looking back. In line with the legacy of Symbiosis colleges in pune, SIG provides its students with the best in terms of education and learning environment. Our courses have been aimed at not merely educating students but to create competent and expert human resources to meet the ever-growing demand of digitization, environmental impact, GIS development, map analysis, remote sensing, watershed management, photogrammetric mapping, navigation and e-governance.</p>

      <p>At SIG, our endeavor is to impart education and training in geospatial Technologies. Our programmes aim to create highly trained professional equipped with a cutting edge. It helps students the students to create their own career roadmap. </p>

      <p>SIG has an enviable track record of 100% campus project cum placement record of its eligible students since inception. </p>

      <p>SIG offers a variety of courses including M.Sc.(Geoinformatics) , M.Sc. (Environment and Sustainability), M.Tech (Geoinformatics and Surveying Technology), Certificate Course Photogrammetry and Remote Sensing and customised certificate courses. </p>
      <p>Over the years SIG has identified the key needs of geospatial industry and has enriched the syllabus in terms of both theory and practical. At SIG, we take all the efforts to pedagogue various aspects of training from basic mapping science to GIS application development, analytical decision-making, softskills and personality development.</p>

    </div>
 <!--    <div id="Symbiosisiu" class="tab-pane fade">
      <h3>Menu 1</h3>
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div> -->
    <div id="Visionaries" class="tab-pane fade">
     <h2 class="menutitle"><strong>Vision & Mission</strong></h2>
     <hr>
      <!-- <img src="<?php echo base_url();?>assets/img/inner-banner.jpg" class="img-responsive"> -->
      <h3>VISION</h3>
      <p>Promoting international understanding through quality education</p>
      <h3>MISSION</h3>
      <ul>
        <li>to inculcate the spirit of 'Vasudhaiva Kutumbakam' (the world is one family)</li>
        <li>to contribute towards knowledge generation and dissemination</li>
        <li>to promote ethical and value-based learning</li>
        <li>to foster the spirit of national development</li>
        <li>to inculcate cross-cultural sensitization</li>
        <li>to develop global competencies amongst students</li>
        <li>to nurture creativity and encourage entrepreneurship</li>
        <li>to enhance employability and contribute to human resource development</li>
        <li>to promote health and wellness amongst students, staff and the community</li>
        <li>to instill sensitivity amongst the youth towards the community and environment</li>
        <li>to produce thought provoking leaders for the society</li>
      </ul>
    </div>
    <div id="Visionarie" class="tab-pane fade">
     <h2 class="menutitle"><strong>Visionaries</strong></h2>
    <!--   <img src="<?php echo base_url();?>assets/img/inner-banner.jpg" class="img-responsive"> -->
     <p>SIG has been incepted with a vision to cater to the ever evolving demands of the geoinformatics industry. Its program, curriculum, method of conducting the course, etc. have been a result of extensive research supported by the guidance of eminent visionaries. </p>
     <br> 
       <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#President">President and Founder Director</a></li>
        <li><a data-toggle="tab" href="#Principal">Principal Director</a></li>
        <li><a data-toggle="tab" href="#Chancellor">Vice Chancellor</a></li>
      </ul>
      <div class="tab-content col-md-12 menutop">
      <div id="President" class="tab-pane fade in active">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"><img src="<?php echo base_url();?>assets/img/padmashree.jpg" ></div>
            <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                <h3><strong>Prof. Dr. S.B. Mujumdar</strong></h3> 
                <p>Chancellor,</p>
                <p>Symbiosis International University,</p>
                <p>President and Founder Director,</p>
                <p>Symbiosis Society</p>
            </div>
        <p>Prof. Dr. S.B. Mujumdar, visionary and architect of modern education, sowed the seeds of Symbiosis 36 years ago as a humanitarian gesture to help international students. Today it has grown into a large tree with 36 branches. Symbiosis has now emerged as a global leader in providing quality education from kindergarten to post-graduate levels.</p> 

        <p>The 31 Symbiosis institutes are a reflection of his selfless efforts to provide quality learning- opportunity to students of all ages and at all levels.</p> 

        <p>In recognition of his contribution to modern education, the Govt. of India conferred Dr. S.B. Mujumdar with the prestigious honour of 'Padmashree' on Republic Day, 2005.</p>
        <h3 class="menutitle"><strong>Message</strong></h3> 
        <p>GIS has been playing a pivotal role in the ever changing and fast developing technological environment, essential for social, economic and overall progress of our nation. SIG has opened vistas of opportunities for young minds from multifarious disciplines to excel in this new niche' field by way of its Post Graduate and other programmes.</p> 
        <p>In the ensuring years, GIS is bound to be much in demand in view of rapid progress of our nation and plans for overall development in infrastructure facilities, power generation sector, agriculture and many such initiatives related to socio-economic development. This would benefit both the industry and our students immensely. SIG has thus been making a valuable contribution to this cause of nation building.</p>
        <p>I wish SIG and its students all the success in their endeavours!</p>
        <h3 class="menutitle"><strong>Felicitations / Honours received</strong></h3>
        <ul>
        <li><strong>The Padma Bhushan Award - 2012</strong></li>
        <li>At the hands of his excellency Smt. Pratibhatai Patil, President of Indian</li>
        <li><strong>Giants International Lifetime Achievement Award 2009</strong></li>
        <li><strong>The Punya Bhushan Award - 2009</strong></li>
        <li>At the hands of His Excellency Shri Mohammad Hamid Ansari, Vice . President of India</li>
        <li><strong>The Padmashree Award - 2005</strong></li>
        <li>At the hands of His Hon. President of India, Dr. A.P.J. Abdul Kalam.</li>   
        <li><strong>Pune Municipal Corporation -Yashwantrao Chavan Puraskar - 1998</strong></li>
        <li>At the hands of Shri. Ramakrishna Hegde, Former Union Minister for Commerce.</li>
        <li><strong>Rotary Club, Pune - Service Excellence Recognition Award (SERA) 1998</strong></li>
        <li><strong>Bandhuta Pratishthan Award - Promotion of Brotherhood (1997) Social Service to the downtrodden</strong></li>
        <li><strong>Institute of Marketing & Management - Excellence in Education & Top Management Club Award </strong></li>
        <li>At the hands of Dr. P.C Alexander, Governor of Maharashtra</li>
        <li><strong>Rotary Club of Pune Award- Promotion of International Understanding</strong></li>
        <li><strong>Pune Municipal Corporation - Outstanding College Teacher Award</strong></li> 
        <li>At the hands of Jayantrao Tilak</li>
        </ul>
        </div>
      </div>
       <div id="Principal" class="tab-pane fade in">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"><img src="<?php echo base_url();?>assets/img/vidya.jpg" ></div>
            <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                <h3><strong>Dr Vidya Yeravdekar </strong></h3> 
                <p>Principle Director and Acting Vice Chancellor,</p>
            </div>
        </div>
        <br>
        <div class="row">    
        <p>Dr. Vidya Yeravdekar, the Principal Director of Symbiosis Society is a Gynecologist, who also holds a degree in law. She has been following in the footsteps of her illustrious father, Dr S. B. Mujumdar in her zeal for social and educational work. She has traveled extensively abroad, visiting countries like UK, USA, UAE, Thailand, Singapore, Malaysia, Tanzania, Kenya and Ethiopia. She has participated in many educational fairs, to interact with students and parents in an attempt to promote Indian higher education abroad. </p> 

        <p>With her extensive contacts with various Ministries of Education in different countries. She has been able to address to specific needs of the youth of those countries. Under her guidance, these needs are met and provided for, in an attempt to increase the comfort level of the foreign students at Symbiosis.</p> 

        <p>Dr. Vidya's main interest is to carry forward the vision of her father, Dr. S. B. Mujumdar - to promote international understanding through quality education. Being a member of the Board of Management of the Symbiosis Society and Symbiosis International University, she contributes to the overall planning and development of Symbiosis. She has been instrumental in collaboration of Symbiosis with some of the world-class universities. She is also member of the board for Indian Council for Cultural Relations and University Grants Commission.</p>
        <h3 class="menutitle"><strong>Message</strong></h3> 
        <p>Symbiosis Society established Symbiosis Institute of Geoinformatics, (SIG) in the year 2004. From its modest beginning since then, it has today grown into a fully integrated Geoinformatics institution of national repute.</p> 
        <p>The guiding philosophy of the institution has emphasised on creation of knowledge and influencing management practices, in order to integrate many facets of the GIS discipline. SIG aims to achieve objectives of imparting education, combined with creation, dissemination and application of knowledge, in an integrated form, to create a synergetic impact. </p>
        <p>SIG fosters and nurtures leadership qualities amongst young generation to make them capable of making difference in the Geoinformatics implementation in corporate and non-corporate sectors. It inculcates human values and professional ethics in the students, which help them make decisions and create path that are good not only for them, but also are good for the society, for the nation, and for the world as whole, in keeping with symbiosis logo of 'Vasudeva Kutumbakam', that is , the world is one family. </p>
        <p>The Institute has made valuable contribution to national causes by undertaking social projects for environmental protection and pollution control. This has helped the faculty, staff and students in application of their knowledge to live projects. </p>
        <p>I wish SIG staff and students success in their endeavours and future growth!</p> 
        </div>
      </div>
       <div id="Chancellor" class="tab-pane fade in">
      <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"><img src="<?php echo base_url();?>assets/img/rajani.jpg" ></div>
            <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                <h3><strong>Dr. Rajani R.Gupte </strong></h3> 
                <p>Vice – Chancellor ,</p>
                <p>(M.A, M.Phil, Ph.D. (Economics))</p>
            </div>
         </div>
         <br>
        <div class="row">    
        <p>Dr. Rajani Gupte, the Pro Vice – Chancellor of Symbiosis International University [SIU] , Pune has done her Ph.D in Economics from the reputed Gokhale Institute of Economics and Politics, Pune. She has more than 30 years of rich work experience in teaching and research at various prestigious institutes like Loyala College, Madras, Gokhale Institute of Politics and Economics, Pune. She has worked as a Finance Director for about six years for a business organization at Singrauli, Madhya Pradesh. She was a Founding member of Symbiosis Institute of Foreign Trade, SIFT in 1992 (now Symbiosis Institute of International Business , SIIB ) and was Director of SIIB from 2004 – 12 . She was instrumental in establishing SIIB as one of the leading B-Schools in India, and in starting innovative niche programs at SIIB in Agribusiness and Energy and Environment. Under her leadership, SIIB was the first Institute under SIU to start a Dual degree program with a German University and several active International collaborations. She has been a visiting faculty at the Oakland University, School of Business Administration Michigan, USA. She has also addressed Business leaders at Bremen University for applied Sciences, Bremen, Germany. She was appointed on the WTO cell set up by Govt. of Maharashtra, Department of Horticulture. She is a member of the International trade panel, CII, Western Region, She was the member of the Advisory Board, Chemtech World Expo 2009 – 2011. She is a UGC nominee on the Advisory Council of Kalasalingam University, Chennai. She is also the Dean of the Faculty of Management of the Symbiosis International University.</p> 

        <p>She has published a book entitled "International Business Management". She has participated in several seminars organised by the FIEO, Indo American Chamber of Commerce, Mahratha Chamber of Commerce. CII, and other Industry Associations.</p> 

        <p>Her areas of specialization are WTO, International Trade Policy, Non- Tariff Barriers, Subsidies etc.</p>        
        </div>
      </div>
      </div>
    </div>
    <div id="Director" class="tab-pane fade">
      <h2 class="menutitle"><strong>Director</strong></h2>
      <hr>
      <!--  <img src="<?php echo base_url();?>assets/img/inner-banner.jpg" class="img-responsive"> -->
       <div style="margin-top:3%;"></div>
       <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"><img src="<?php echo base_url();?>assets/img/tp_singh.jpg" ></div>
            <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                <h3><strong>Dr. T. P. Singh </strong></h3> 
                <p>Director</p>
            </div>
        </div>
        <br>
        <div class="row">
        <p>Dr. T.P. Singh has vast experience in the field of Geospatial Technology in India and abroad. He has started his career in Geospatial technology from the Indian Institute of Remote Sensing, where he has worked on the projects of national importance. He has earned his PhD degree in the area of satellite Image Classification techniques from HNB Garhwal Central University, Srinagar and M.Phil in Geo Engineering from College of Engineering, Vishakhapatnam. Dr. Singh has received Master Degree in remote sensing from Pierre and Marie Curie University (UPMC), Paris University VI, France and M.Sc. from University of Lucknowin Environmental Science. He has received the European Professional Higher Post Graduate Diploma in Geoinformatics from GDTA (Institute under French Space Agency) in association with Paris University VI France, University of Lisbon, Portugal and Warsaw University of Technology Poland. Dr. Singh has worked on LiDAR technology at the University of Freiburg , Germany on Automatic feature extraction. Before, taking academic position at Symbiosis, he has served at different government centres. Dr Singh is the member of many professional bodies and Vice Chairman of Indian Society of Geomatics, Pune Chapter. He has Edited many books in the field of Geospatial Technology, Climate Change and Natural Resource Management and published several papers in the peer reviewed journals.</p>
        </div>
    </div>
    
  </div>
    <span class="article_separator">&nbsp;</span>
</div>
	