<style>
.newsdate{
	    font-size: 20px;
    font-weight: 500;
    margin-top: 15px;
}
.mix{
    text-align: center;
}
</style>
<link href="<?php echo base_url();?>assets/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<div class="aboutop">
    <div class="placement"></div>
</div>
<div class="container">
<div class="inner-information">

     <table class="contentpaneopen">
<tbody><tr>
    <td class="contentheading" width="100%">
          <?php echo $news["title"]; ?>      </td><td><div class="newsdate">
<?php $time = strtotime($news['date']);
	$date = date("m/d/y", $time);
	echo $date; 
	?></div></td>
              </tr>
</tbody></table>
<hr>
<div class="contentpaneopen">


<div class="newsdesc">
<?php if($news["description"]==""||$news["description"]==null) echo "No Description Found"; else echo $news["description"]; ?>
</div>


</div>
<br/><br/> 
<span class="article_separator">&nbsp;</span>
  
</div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery(".fancybox").fancybox();

        jQuery("a#inline").fancybox({
            'hideOnContentClick': true
        });

        /* Apply fancybox to multiple items */

        jQuery("a.group").fancybox({
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'speedIn': 600,
            'speedOut': 200,
            'overlayShow': false
        });

    });

</script>
