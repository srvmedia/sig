<style type="text/css">
    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }
    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    li.active a{
        display: inline;
    }
    .nav>li>a {
        padding: 3px 15px;
        margin-bottom: 12px;
    }
    .infholder{
        font-size: 14px;
    }
    .boxfaculty{
        border-radius: 5px;
    border: 1px solid rgba(158, 156, 156, 0.24);
    background-color: rgba(128, 128, 128, 0.15);
    padding: 5px 12px 5px 12px;
    }
</style>
<div class="aboutop">
    <div class="bannerimg"></div>
</div>	
<div class="container">  
    <div class="tab-content col-md-12 menutop">
        <div id="faculties" class="tab-pane fade in active">
            <div class="information-middle">
                <div class="inner-information">
                    <h2 class="menutitle"><strong style="color: #BB0C1A;">Dr. Navendu Chaudhary</strong><br>
                    Associate Professor
                    <a href="<?php echo base_url();?>faculties" style="float:right;font-size: 16px;"> << Back</a>
                    </h2>
                    
                    <hr> 
                    <!--<p style="margin-bottom:30px">Symbiosis colleges in Pune have been associated with a tradition to provide the best in terms of academic training coupled with industry interface. But the promise of delivering academic excellence can't happen without the support of good faculties. In line with this tradition, SIG like other Symbiosis institutes has always sourced out the finest of faculties to aid in the learning of its students.</p>-->
                    <div class="col-md-12 faculty_section">
                        <div class="imajholder text-center" style="float: left;"> <img src="<?php echo base_url(); ?>assets/img/faculty/navendu.jpg" class="img-circle">
                            <!--<p class="text-center">Dr. Navendu Chaudhary</p>-->
                        </div>
                        <div class="infholder">
                            <div>B.Sc., 1995, Microbiology, University of Pune.</div>
                            <div>M.Sc. 1997, Environmental Science, University of Pune</div>
                            <div>M.S. 2001, Environmental Engg. & Management, University of Cincinnati, USA</div>
                            <div>Ph.D., 2007, Geography, University of Cincinnati, USA</div>
                            <div>Telephone: Office - (91) 20-25672841/3 Ext. 122</div>
                            <div>Email:<a href="mailto:navendu@sig.ac.in" style="color: #BB0C1A;"> navendu@sig.ac.in</a></div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                    <div class="col-md-12 menutop">
                        <div class="boxfaculty">
                            <h3 class="menutitle">Research Interests</h3>
                            <p>My research interests are broadly in Environmental Management with an emphasis on Remote Sensing and Geographic Information Systems (GIS). I am primarily interested in investigating environmental issues by utilizing the vast analytical tools offered by current advances in remote sensing technology as well as in GIS. In this information age the future of environmental problem solving lies in effective use of available remote sensing technology and aided by GIS. I am focusing my research on developing methodologies to explain environmental issues utilizing remotely sensed data and GIS capabilities.</p>
                            
<!--                            <ul>
                                <li></li>
                            </ul>-->
                        </div>
                    </div>
                        <div class="col-md-12 menutop">
                        <div class="boxfaculty">
                            <h3 class="menutitle">Selected Publications</h3>
                            <ul>
                                <li>Navendu Chaudhary and Yogesh Pisolkar, Spatial analysis of bank branches using micro-geographic data, Proceedings of Emerging Trends in Global Financial Landscape- Approaches, Challenges and Opportunities, 2015 ISBN No. 978-1-63415-553-3</li>
                                <li>Yogesh Pisolkar and Navendu Chaudhary, Issues in Development and Intangible Asset Finance - A Case Study of Village Devbag, Coastal Maharashtra, India., Proceedings of Emerging Trends in Global Financial Landscape- Approaches, Challenges and Opportunities, 2015 ISBN No. 978-1-63415-553-3</li>
                                <li>Navendu Chaudhary, Geospatial Technology: Opportunity in fostering social enterprenureship, Annual Research Journal of Symbiosis Centre for Management Studies, Vol. 2 Issue 1 , April 2014 ISSN: 2348-0661</li>
                                <li>Frohn, Robert C.and Chaudhary, Navendu, 2010, Multi-scale Image Segmentation and Object-Oriented Processing for Land Cover Classification, GIScience and Remote Sensing, Vol.45 no.4, Page 377-391 </li>
                                <li>Frohn, R.C., Liu, L., Beck, R.A., Chaudhary, N., & O. Arellano-Neri. (2008). An Evaluation of Classification Methods for Level II Land-Cover Categories in Ohio. SPIE Proceedings in Geoinformatics 2008 and Joint Conference on GIS and Built Environment: Classification of Remote Sensing Images, 7147, 12</li>
                                <li>Chaudhary, Navendu, An object oriented approach to land-cover classification: A case study for state of Ohio, VDM, Verlag Dr. Muller, 2008, ISBN: 978-3-639-03957-3. </li>
                                <li>Sullivan, A. P., R. C. Frohn, K. Magee, and N. Chaudhary. 2006. Testing the Regional Reliability of Spectral Signatures of Archaeological Phenomena. Final Report (Grant Number MT-2210-05-NC-12 [41 pp.]) submitted to the National Park Service, National Center for Preservation Technology and Training, Natchitoches, LA</li>
                                <li>Vivek P. Utgikar, Navendu Chaudhary, Arthur Koeniger, Henry H.Tabak, John R. Haines , Rakesh Govind Toxicity of metals and metal mixtures: analysis of concentration and time dependence for zinc and copper; Water Research 38 (2004) 3651–3658.</li>
                                <li>Utgikar, Vivek P.; Harmon, Stephen M.; Chaudhary, Navendu; Tabak, Henry H.; Govind, Rakesh, Inhibition of Sulfate –Reducing Bacteria by Metal Sulfide Formation in Bioremediation of Acid Mine drainage, Environmental Toxicology, Volume: 17, Issue: 1, 2002.</li>
                                <li>Vivek P. Utgikar, Bor-Yann Chen, Navendu Chaudhary, Henry H. Tabak, John R. Haines, and Rakesh Govind, Acute Toxicity Of Heavy Metals To Acetate-Utilizing Mixed Cultures Of Sulfate-Reducing Bacteria: Ec100 And Ec50, Environmental Toxicology and Chemistry, Vol. 20, No. 12, pp. 2662–2669, 2001. </li>
                            </ul>
                        </div>
                    </div>
                        </div>
                    <script language="javascript" type="text/javascript">
                        var last_tab = 'all';
                        function show(layerName) {
                            document.getElementById(layerName).style.display = 'block';
                        }
                        function hide(layerName) {
                            document.getElementById(layerName).style.display = 'none';
                        }
                        function show_next(tab_name) {
                            document.getElementById(last_tab).className = 'tab';
                            var curr = document.getElementById(tab_name);
                            curr.className = 'tab activeTab';
                            hide(last_tab + '_data');
                            show(tab_name + '_data');
                            last_tab = tab_name;
                        }
                    </script>
                    <style>
                        .tabArea ul li { display:inline-block; margin:0 5px; }
                        .tabMain { display:inline-block; width:100%; }
                        .tabMain .imajholder {  width:168px; height:158px; float:left;  margin: 0 10px 0 0;}
                        .tabMain .infholder a.read_from {float:right; padding:5px 10px;}
                        .title-name, .position , .specilization, .inyears {margin-left:5px;}
                        .tabArea ul {
                            margin: 15px 0;
                        }
                        .saperator {
                            clear:inherit;
                            margin: 10px 0;
                            overflow: hidden;
                        }
                        .faculty_section img
                        {
                            border: 1px solid rgba(128, 128, 128, 0.32);
                            width: 130px;
                            padding: 3px;
                            margin-bottom: 10px;
                            ;
                        }
                        .infholder
                        {
                            margin-top: 10px;
                        }
                        .infholder p{font-size: 14px;text-align:left;}
                        .imajholder{width: 160px;margin-right: 16px}
                    </style>
                </div>
                
            </div>
        </div>
    </div>
</div>
