<style type="text/css">
    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }
    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    li.active a{
        display: inline;
    }
    .nav>li>a {
        padding: 3px 15px;
        margin-bottom: 12px;
    }
    .infholder{
        font-size: 14px;
    }
    .boxfaculty{
        border-radius: 5px;
    border: 1px solid rgba(158, 156, 156, 0.24);
    background-color: rgba(128, 128, 128, 0.15);
    padding: 5px 12px 5px 12px;
    }
</style>
<div class="aboutop">
    <div class="bannerimg"></div>
</div>	
<div class="container"> 
    <div class="tab-content col-md-12 menutop">
        <div id="faculties" class="tab-pane fade in active">
            <div class="information-middle">
                <div class="inner-information">
                    <h2 class="menutitle"><strong style="color: #BB0C1A;">Dr.Sandipan Das </strong><br>
                    Assistant Professor
                    <a href="<?php echo base_url();?>faculties" style="float:right;font-size: 16px;"> << Back</a>
                    </h2>
                    
                    <hr> 
                    <!--<p style="margin-bottom:30px">Symbiosis colleges in Pune have been associated with a tradition to provide the best in terms of academic training coupled with industry interface. But the promise of delivering academic excellence can't happen without the support of good faculties. In line with this tradition, SIG like other Symbiosis institutes has always sourced out the finest of faculties to aid in the learning of its students.</p>-->
                    <div class="col-md-12 faculty_section">
                        <div class="imajholder text-center" style="float: left;"> <img src="<?php echo base_url(); ?>assets/img/faculty/sandipan_das.jpg" class="img-circle">
                            <!--<p class="text-center">Dr. Navendu Chaudhary</p>-->
                        </div>
                        <div class="infholder">
                            <div>Designation: Assistant Professor</div>
                            <div>Qualification: Ph.D ,UGC NET,M.Sc. Geoinformatics</div>
                            <div>Total Experience (In years): 8.6 years</div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-12 menutop">
                        <div class="boxfaculty">
                            <h3 class="menutitle">Selected Publications</h3>
                            <ul>
                                <li>Singh, T.P., Das, S. (2014) "Predictive analysis for vegetation biomass assessment in Western Ghats region using Geospatial techniques", Journal of Indian Society of RemoteSensing [Springer], 42(3):549-557.</li>
                                <li>Das, S., Singh, T.P. (2013) "Mapping Vegetation and Forest Types using Landsat TM in the Western Ghat Region of Maharashtra, India", International Journal of Computer Applications, 76(1):33-37.</li>
                                <li>Das, S., Bhattacharya, A., Mali, S. (2013) "Study on Urban Land Suitability Assessment Using Remote Sensing and GIS: A Case Study of Khairagarh, in Chhattisgarh", International Journal of Scientific and Research Publications, 74(10): 20-26.</li>
                                <li>Mali, S., Bhailume, S., Das, S. (2013) "Geoinformatics Application for Urban Utilities Information System: A Case Study of Pune City, Maharashtra, INDIA", International Journal of Computer Applications, 65(22): 33-39. </li>
                                <li>Das, S., Mali, S., Misra, A. (2012) "Urban Landuse/Land-Cover Change Detection Analysis Using Remote Sensing Technology: A Study of Aurangabad City", International Journal of Scientific and Research Publications, 2(9):1-5.</li>
                                <li>Das, S., Singh, T.P. (2012) "Correlation analysis between biomass and spectral vegetation indices of forest ecosystem" , International Journal of Engineering Research & Technology (IJERT) , 1(5):1-13.</li>
                                <li>Warghat, S., Das, S., Doad A., Mali S. (2012) "Flood Vulnerability Analysis of the part of Karad Region, Satara District, Maharashtra using Remote Sensing and Geographic Information System technique", International Journal of Advancements in Research & Technology, 1(2):1-7.</li>
                                <li>Mali S., Panhalkar S., Pawar C.T., Das, S. (2012) "Generation of 3-D city model for urban utility information system using digital photogrammetry and GIS technology ", Global Journal of Researches in Engineering General Engineering, 12(3):27-30.</li>
                               
                            </ul>
                        </div>
                    </div>
                        </div>
                    <script language="javascript" type="text/javascript">
                        var last_tab = 'all';
                        function show(layerName) {
                            document.getElementById(layerName).style.display = 'block';
                        }
                        function hide(layerName) {
                            document.getElementById(layerName).style.display = 'none';
                        }
                        function show_next(tab_name) {
                            document.getElementById(last_tab).className = 'tab';
                            var curr = document.getElementById(tab_name);
                            curr.className = 'tab activeTab';
                            hide(last_tab + '_data');
                            show(tab_name + '_data');
                            last_tab = tab_name;
                        }
                    </script>
                    <style>
                        .tabArea ul li { display:inline-block; margin:0 5px; }
                        .tabMain { display:inline-block; width:100%; }
                        .tabMain .imajholder {  width:168px; height:158px; float:left;  margin: 0 10px 0 0;}
                        .tabMain .infholder a.read_from {float:right; padding:5px 10px;}
                        .title-name, .position , .specilization, .inyears {margin-left:5px;}
                        .tabArea ul {
                            margin: 15px 0;
                        }
                        .saperator {
                            clear:inherit;
                            margin: 10px 0;
                            overflow: hidden;
                        }
                        .faculty_section img
                        {
                            border: 1px solid rgba(128, 128, 128, 0.32);
                            width: 130px;
                            padding: 3px;
                            margin-bottom: 10px;
                            ;
                        }
                        .infholder
                        {
                            margin-top: 10px;
                        }
                        .infholder p{font-size: 14px;text-align:left;}
                        .imajholder{width: 160px;margin-right: 16px}
                    </style>
                </div>
                
            </div>
        </div>
    </div>
</div>
