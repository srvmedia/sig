<style type="text/css">
    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }
    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    li.active a{
        display: inline;
    }
    .nav>li>a {
        padding: 3px 15px;
        margin-bottom: 12px;
    }
    .infholder{
        font-size: 14px;
    }
    .boxfaculty{
        border-radius: 5px;
    border: 1px solid rgba(158, 156, 156, 0.24);
    background-color: rgba(128, 128, 128, 0.15);
    padding: 5px 12px 5px 12px;
    }
</style>
<div class="aboutop">
    <div class="bannerimg"></div>
</div>	
<div class="container"> 
    <div class="tab-content col-md-12 menutop">
        <div id="faculties" class="tab-pane fade in active">
            <div class="information-middle">
                <div class="inner-information">
                    <h2 class="menutitle"><strong style="color: #BB0C1A;">Lt. Col. BK Pradhan</strong><br>
                    Adjunct Faculty
                    <a href="<?php echo base_url();?>faculties" style="float:right;font-size: 16px;"> << Back</a>
                    </h2>
                    <hr> 
                   <div class="col-md-12 faculty_section">
                        <div class="imajholder text-center" style="float: left;"> <img src="<?php echo base_url(); ?>assets/img/faculty/bk-pradhan.jpg" class="img-circle">
                          </div>
                        <div class="infholder">
                            <div>B.E. Electronics in 1969 </div>
                            <div>Associate Member of Institution of Engineers (India) 1972 </div>
                            <div>Diploma in Management 1993, Diploma in software Development 1993</div>
                            <div>Telephone:Office - (91) 020-25672841/3 Ext. 124 </div>
                            <div>Email:<a href="mailto:pradhan@sig.ac.in" style="color: #BB0C1A;"> pradhan@sig.ac.in</a></div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                    <div class="col-md-12 menutop">
                        <div class="boxfaculty">
                            <h3 class="menutitle">Research Interests</h3>
                            <p>Scientist in Defense Research @ Development Organization (DRDO) working in image processing, GIS, Digital Cartography and 3 D visualization and simulation softwarew development .</p>
                            
                        </div>
                    </div>
                        <div class="col-md-12 menutop">
                        <div class="boxfaculty">
                            <h3 class="menutitle">Selected Publications</h3>
                            <ul>
                                <li>B K Pradhan, Sudhir Porwal and S C Jain. Search and Render Algorithm for Three-Dimensional Terrain Visualization of Large Dataset</li>
                                <li>Defense Science Journal, vol 52, No. 3, pp 277-284</li>
                            </ul>
                        </div>
                    </div>
                        </div>
                    <script language="javascript" type="text/javascript">
                        var last_tab = 'all';
                        function show(layerName) {
                            document.getElementById(layerName).style.display = 'block';
                        }
                        function hide(layerName) {
                            document.getElementById(layerName).style.display = 'none';
                        }
                        function show_next(tab_name) {
                            document.getElementById(last_tab).className = 'tab';
                            var curr = document.getElementById(tab_name);
                            curr.className = 'tab activeTab';
                            hide(last_tab + '_data');
                            show(tab_name + '_data');
                            last_tab = tab_name;
                        }
                    </script>
                    <style>
                        .tabArea ul li { display:inline-block; margin:0 5px; }
                        .tabMain { display:inline-block; width:100%; }
                        .tabMain .imajholder {  width:168px; height:158px; float:left;  margin: 0 10px 0 0;}
                        .tabMain .infholder a.read_from {float:right; padding:5px 10px;}
                        .title-name, .position , .specilization, .inyears {margin-left:5px;}
                        .tabArea ul {
                            margin: 15px 0;
                        }
                        .saperator {
                            clear:inherit;
                            margin: 10px 0;
                            overflow: hidden;
                        }
                        .faculty_section img
                        {
                            border: 1px solid rgba(128, 128, 128, 0.32);
                            width: 130px;
                            padding: 3px;
                            margin-bottom: 10px;
                            ;
                        }
                        .infholder
                        {
                            margin-top: 10px;
                        }
                        .infholder p{font-size: 14px;text-align:left;}
                        .imajholder{width: 160px;margin-right: 16px}
                    </style>
                </div>
                
            </div>
        </div>
    </div>
</div>
