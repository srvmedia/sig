<style type="text/css">
    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }
    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    li.active a{
        display: inline;
    }
    .nav>li>a {
        padding: 3px 15px;
        margin-bottom: 12px;
    }
    .infholder{
        font-size: 14px;
    }
    .boxfaculty{
        border-radius: 5px;
    border: 1px solid rgba(158, 156, 156, 0.24);
    background-color: rgba(128, 128, 128, 0.15);
    padding: 5px 12px 5px 12px;
    }
</style>
<div class="aboutop">
    <div class="bannerimg"></div>
</div>	
<div class="container"> 
    <div class="tab-content col-md-12 menutop">
        <div id="faculties" class="tab-pane fade in active">
            <div class="information-middle">
                <div class="inner-information">
                    <h2 class="menutitle"><strong style="color: #BB0C1A;">Dr. T. P. Singh</strong><br>
                   Professor
                    <a href="<?php echo base_url();?>faculties" style="float:right;font-size: 16px;"> << Back</a>
                    </h2>
                    <hr> 
                   <div class="col-md-12 faculty_section">
                        <div class="imajholder text-center" style="float: left;"> <img src="<?php echo base_url(); ?>assets/img/faculty/tp-singh.jpg" class="img-circle">
                          </div>
                        <div class="infholder">
                            <div>M.Sc. Lucknow University  </div>
                            <div>M.Phil. College of Engineering, Andhra University </div>
                            <div>M.S. Pierre and Marie CURIE University, Paris VI, Paris, France</div>
                            <div>Ph.D. H.N.B. Garhwal Central University, Srinagar , (UK) India (Research Centre: Indian Institute of Remote Sensing)</div>
                            <div>Telephone: Office - (91) 20-25672841/3 </div>
                            <div>Email:<a href="mailto:tpsingh@sig.ac.in" style="color: #BB0C1A;">tpsingh@sig.ac.in</a></div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                    <div class="col-md-12 menutop">
                        <div class="boxfaculty">
                            <h3 class="menutitle">Research Interests</h3>
                            <p>We are working in field of Vegetation Carbon Sequestration for climate change analysis, Forest Biodiversity and water inundation, using Geospatial technique. Worked in Himalaya and western Ghat Region for Biodiversity and Carbon Sequestration. I am working to Develop framework for Monitoring and assessment of International projects using GIS . Implementation of GIS and remote sensing techniques for decision making in Joint Forest Management and Ecotourism and having, Interest in using geospatial techniques for Location Base Services and utility management. We are also working in Decision Support System for Agriculture, Spatial Data mining and Fire modeling. </p>
                            
                        </div>
                    </div>
                        <div class="col-md-12 menutop">
                        <div class="boxfaculty">
                            <h3 class="menutitle">Selected Publications</h3>
                            <ul>
                                <li>Bhatt, D., P.K. Joshi and T.P. Singh. (1999). Impact of Thermal Power Plant on Physico-chemical Characters of Soil - A case study. Him. J. Env. Zool., Vol. 13(1), pp 59-64.</li>
                                <li>Singh, T.P. and P.K. Joshi. (1999). Physico-Chemical Analysis of Soil and Plant Affected by Discharge of U.P. State Sugar Corporation. Him. J. Env. Zool., Vol. 13(1), pp 65-70.</li>
                                <li>Behera ,M.D., S.P.S. Kushwaha, P.S. Roy, S. Srivastava, T.P. Singh and R.C. Dubey (2002). Comparing structure and composition of coniferous forest in Subansiri district, Arunachal Pradesh. Current Sc. Vol.82 No.1.</li>
                                <li>Srivastva Shalini, T.P. Singh, Harnam Singh,S.P.S Kushwaha,P.S. Roy (2002), mapping of deforestation in Sonitpur district. Current Sc. Vol.82 No.12.</li>
                                <li>Singh, S., Singh, T. P., Saran, S., & Roy, P. S. (2003). Identifying Biodiversity Conservation Areas In South-Eastern Arunachal Pradesh Using Geospatial Approach. International Archives Of Photogrammetry Remote Sensing And Spatial Information Sciences, 34(7/B), 1024-1029</li>
                                <li>Singh, T.P., Singh,S., Roy,P.S. and B.S.P.,Rao.(2002),Landscape chracterization and mapping in eastern Himalayan Region - A Satellite remote sensing based approach Current Science 83(10).</li>
                                <li>Singh Sarnam, D.K. Singh, Surendra Singh, T.P. Singh(2003). Less known ferns in Lahul and Spiti ,western Himalaya,2003, Indian Journal of Forestry Vol.26(4):406-408</li>
                                <li>Singh,T.P., Sarnam Singh, P.S. Roy. (2003), Assessing Jhum Induced forest loss in Durban valley, Arunachal Himalaya - A remote sensing Prospective ISRS Vol. 31 No. 1.</li>
                                <li>Singh, T.P., Joshi P.K., Roy P.S., Wiencker Holger.,(2006) Algorithms Development for automatic canopy cover extraction using Lidar and Multispectral data. International Journal of Geoinformatics, Volume 2 No.2</li>
                                <li>Singh Sarnam, Singh.T.P., and Gaurav Srivastava (2005), Vegetation Cover Type Mapping in Mouling National Park in Arunachal Pradesh, Eastern Himalayas- An Integrated Geospatial Approach ISRS Vol. 33 No. 4</li>
                                <li>Bagachi Rajashri andT. P. Singh,(2009) Forecasting of Radiation Fog: A Fuzzy Logic Rule Based Approach gis@development ejournal http://www.gisdevelopment.net/application/environment/climate/index.htm</li>
                                <li>Singh, T.P.,Wiencker Holger., Sarnam singh (2010).,. Algorithms Development and testing for automatic extraction of manmade objects using Lidar and Multispectral data. International Journal of Geoinformatics, Volume 6 No.4.</li>
                                <li>H. Srivastava and T.P. Singh (2010), Algorithm development for identification of oil spill using MODIS data , J. Indian Soc. Remote Sens.(Springer) vol. 38:161-167.</li>
                                <li>Singh, T.P. , Singh Sarnam (2011) Plant Species Diversity and Endemism at Dihang Dibang Biosphere Reserve and its Surroundings, Eastern Himalaya Biodiversity Hotspot, Asian Journal of Biodiversity Vol.3 No.12.2011.</li>
                                <li>Sandeepan Das and T.P. Singh (2012) Correlation analysis between biomass and spectral vegetation indices of forest ecosystem, International Journal of Engineering Research and technology Vol.1, Issue5.</li>
                                <li>Vidya Kumbhar and T.P. Singh (2013) A comprehensive study of application of decision support system in agriculture in Indian context, International Journal of Computer Application(0975 – 8887) Volume 63– No.14.</li>
                                <li>Singh.T.P, Singh Sarnam,Tiwari S.C.(2013) Assessment of digital image classification algorithms for forest and land use in eastern Himalayas using IRS LISS III sensor International Journal of Remote Sensing Vol.34 No.11.</li>
                                <li>Sandipan Das and T P Singh. Article: Mapping Vegetation and Forest Types using Landsat TM in the Western Ghat Region of Maharashtra, India. International Journal of Computer Applications 76(1):33-37, August 2013</li>
                                <li>T.P. Singh and Sandeepan Das, " Techniques Predictive analysis for vegetation biomass assessment in Western Ghat Region (WG) using Geospatial ", Journal of Indian Soc. Of Remote Sensing DOI 10.1007/s12523-013-0335-7 (2014)</li>
                                <li>Vidya Kumbhar and TP Singh (2013) "A Systematic Study of Application of Spatial Decision Support System in Agriculture", International journal of scientific & Engineering Research", Vol.4 Issue 12 (2014)</li>
                                <li>Gandhi Shally and Singh T.P(2014)Atumatization of forest fire detection using Geospatial techniques. Open Journal of Forestry,4,302-309.doi:10.423/ojf.2014.44036.</li>
                                <li>T.P. Singh, Vidya Kumbhar , Rohit Kumar and Smita Das (2014). Geospatial based multi-criterion analysis to identify crop suitability area in semi-arid regions of Maharashtra, India, AIJRSTEM, Issue7 Vol.3 pp256-261.</li>
                                <li>Vidya Kumbhar, Sanjukta Chaudhari, Avinanda Sen, and TP Singh "Assessment of Irrigation and Agricultural Potential Using Geospatial Techniques A Case Study of "Bhima-Ujjani" Procedia - Social and Behavioral Sciences 157 ( 2014 ) 277 – 284</li>
                                <li>Manisha Bhosale and TP Singh Prediction of Milk Yield for Planning and Management in Indian Dairy Sector Using Artificial Neural Network,(Accepted in Current Science 2015)</li>
                                <li>Sandipan Das and T.P. Singh "Land-use, diversity and biomass estimation in tropical forest of Western Ghats of Maharashtra using remote sensing and GIS, (Accepted in Springer Small Scale Forestry 2015)</li>
                            </ul>
                        </div>
                    </div>
                        <div class="col-md-12 menutop">
                        <div class="boxfaculty">
                            <h3 class="menutitle">Paper in Institute and organisation journal</h3>
                            <ul>
                                <li>T.P.Singh With P.S.Roy, Sarnam Singh, P.G.Diwakar, C.Jaganathan, Vinod Bothale, S.P.S.Kushwaha, M.C.Porwal, Sameer Saran, Shalini Srivastava, Nupur Prasad, Suddhdheel Ghose, Deepshikha and,2002.Biodiversity Information System. NNRMS bulletin (B) 27 , ISRO, Department of Space, Bangalore.</li>
                                <li>T.P. Singh, 2007 LiDAR technology in natural resource field and current Indian scenario, Earth gear vol.3,October 2007.</li>
                                <li>Book/ Chapter in book</li>
                                <li>T.P. Singh with P.K.Joshi, P. Pani, S.N. Mohapartra: Geoinformatics for Natural Resource Management Nova Science Publishers New York, ISBN: 978-160692-211-8</li>
                                <li>Roy P.S. , Singh S., Kushwaha S.P.S.,Saran S., Behera M.D., Singh T.P., Srivastava S.,et al 2002 . Biodiversity Characterisation at landscape level Arunachal Pradesh India. IIRS (NRSA) Pub. Dehradun. 293 p.</li>
                                <li>Roy, P.S., Porwal M. C., Chauhan, N., Gupta S., Padalia H.,Singh S., Biradar C. M., Singh T.P., et al. 2003. Biodiversity Characterisation at landscape level Andaman and Nicobar Islands of India. IIRS (NRSA) Pub. Dehradun. 341 p.</li>
                                <li>Roy P.S., Singh S, Birader C.M., Singh T.P., Talukdar G. 2002. Biodiversity Characterisation at landscape level Himachal Pradesh, India. IIRS (NRSA) Pub. Dehradun. 311.</li>
                                <li>P.K. Joshi and T.P. Singh, Climate change and Geoinformation, ISBN: 9788179934098 TERI Press, New Delhi</li>
                            </ul>
                            
                        </div>
                    </div>
                        <div class="col-md-12 menutop">
                        <div class="boxfaculty">
                            <h3 class="menutitle">Book/ Chapter in book</h3>
                            <ul>
                                <li>T.P. Singh with P.K.Joshi, P. Pani, S.N. Mohapartra: Geoinformatics for Natural Resource Management Nova Science Publishers New York, ISBN: 978-160692-211-8</li>
                                <li>Roy P.S. , Singh S., Kushwaha S.P.S.,Saran S., Behera M.D., Singh T.P., Srivastava S.,et al 2002 . Biodiversity Characterisation at landscape level Arunachal Pradesh India. IIRS (NRSA) Pub. Dehradun. 293 p.</li>
                                <li>Roy, P.S., Porwal M. C., Chauhan, N., Gupta S., Padalia H.,Singh S., Biradar C. M., Singh T.P., et al. 2003. Biodiversity Characterisation at landscape level Andaman and Nicobar Islands of India. IIRS (NRSA) Pub. Dehradun. 341 p.</li>
                                <li>Roy P.S., Singh S, Birader C.M., Singh T.P., Talukdar G. 2002. Biodiversity Characterisation at landscape level Himachal Pradesh, India. IIRS (NRSA) Pub. Dehradun. 311</li>
                                <li>P.K. Joshi and T.P. Singh, Climate change and Geoinformation, ISBN: 9788179934098 TERI Press, New Delhi</li>
                                </ul>
                        </div>
                    </div>
                        <div class="col-md-12 menutop">
                        <div class="boxfaculty">
                            <h3 class="menutitle">Research Papers in Seminars/Workshops</h3>
                            <ul>
                                <li>Roy, P.S., S. Singh, P.K. Joshi, T.P. Singh and S. Gupta (2000). Status of Natural Forest Cover in North East India - An Application of Space Remote Sensing. National Seminar on Environmental Issues & Priorities: Challenges of the Millennium Abstract Volume, March 2-4, 2000. Department of Ecology, Assam University, Silchar Assam.</li>
                                <li>Singh,T.P., Sarnam Singh, P.S. Roy. 2002,Assesing Jhum Induced forest loss in Dibang vally, Arunachal Himalayas - A remote Sensing Prospective, National Seminar G.K.V. Haridwar.</li>
                                <li>Singh Sarnam, T.P. Singh, P.S. Roy, S.Saran ,2002 Identifying Biodiversity Conservation area in South-Eastern Arunachal Pradesh Using Geospatial Approach .Paper presented in international workshop of International society of photogrametry and Remote Sensing, 2-5 November 2002, Hyderabad India</li>
                                <li>P.S. Roy, P.K. Joshi, G. Talukdar, M.B. Chandrashekahar, C. Jegananthan, S. Saran, S.P.S. Kushwaha, S. Singh, R.S. Tripathi, M.C. Porwal, A.K. Tiwari, S. Sudhakar, H. Singh, T.P. Singh, U. Kumar and S. Tomar (2002). Biodiversity Characterization at Landscape Level and Defining Conservation Strategies. Paper Presented in 'International Workshop on Tropical Cover Assessment and Conservation Issues in South East Asia', 12-14 February 2002, IIRS, Dehradun, India, pp. 193-206.</li>
                                <li>Singh T.P. and P.C. Tiwari (2005). Land-use land Cover Classification of Nainital District for local level planning and management.National workshop on "NRDMS Related Spatial Technology For Development Planning,29-30 July 2005 Centre For Remote sensing ,Bharathidasan University,India.</li>
                                <li>T.P. Singh & Runumi Desai (2009), Assessment of land use change and effect on crop pattern using Geospatial techniques. National Seminar on Land use planning for agricultural development and establishment of agro-economics zones:  Strategies for India with special reference to Uttar Pradesh" December 11–12, 2009, Indian Institute of Management (IIM) Lucknow, India.</li>
                                <li>T.P. Singh, A. Mishra (2010), Assessing relationship between biomass and biodiversity in western ghat region. International seminar on biodiversity and climate change, December 21-23, 2010 Indian Institute of Technology, Kharagpur, India</li>
                                <li>T.P. Singh and KKV Khanzode (2011) Geoinformation education in private institutes in India, issues and challenge, "International workshop on Multination Geomatics capacity Building achievement and challenge" ISPRS WG 6/4 ISRS, 7-8 April 2011,Dehradun , India Vidya Kumbhar and TP Singh (2013) "A Systematic Study of Application of Spatial Decision Support System in Agriculture",International Conference on Interdisciplinary Applications of Remote Sensing and Geographic Information System", April 26th- 27th April, K.J. Somaya College of Science and Commerce, Vidyavihar, Mumbai, India.</li>
                                <li>Sanjukta Chaudhari, Avinanda Sen, VidyaKumbhar and TP Singh (2013) "Assessment of Irrigation and Agricultural Potential Using Geospatial Techniques A Case Study of "Bhima-Ujjani" Project, International Conference on International Relation India and Development Partnership in Asia and Africa:Towards a New Paradigm , 14 and 15 December 2013.</li>
                                <li>T.P. Singh and Shally Gandhi (2013) Automatization of forest Fire Detection , International Society of Photogrammetry and Remote Sensing Conference, CEPT University Ahemdabad, India 21 and 22 December 2012.</li>
                                <li>T.P. Singh and Sarnam Singh (2014) Strengthing Biodiversity conservation using Geospatial techniques , A case study of Mouling National Park, Eastern Himalaya, Biodiversity Congress , 18 to 20th December, Chennai, India.</li>
                                </ul>
                        </div>
                    </div>
                        </div>
                    <script language="javascript" type="text/javascript">
                        var last_tab = 'all';
                        function show(layerName) {
                            document.getElementById(layerName).style.display = 'block';
                        }
                        function hide(layerName) {
                            document.getElementById(layerName).style.display = 'none';
                        }
                        function show_next(tab_name) {
                            document.getElementById(last_tab).className = 'tab';
                            var curr = document.getElementById(tab_name);
                            curr.className = 'tab activeTab';
                            hide(last_tab + '_data');
                            show(tab_name + '_data');
                            last_tab = tab_name;
                        }
                    </script>
                    <style>
                        .tabArea ul li { display:inline-block; margin:0 5px; }
                        .tabMain { display:inline-block; width:100%; }
                        .tabMain .imajholder {  width:168px; height:158px; float:left;  margin: 0 10px 0 0;}
                        .tabMain .infholder a.read_from {float:right; padding:5px 10px;}
                        .title-name, .position , .specilization, .inyears {margin-left:5px;}
                        .tabArea ul {
                            margin: 15px 0;
                        }
                        .saperator {
                            clear:inherit;
                            margin: 10px 0;
                            overflow: hidden;
                        }
                        .faculty_section img
                        {
                            border: 1px solid rgba(128, 128, 128, 0.32);
                            width: 130px;
                            padding: 3px;
                            margin-bottom: 10px;
                            ;
                        }
                        .infholder
                        {
                            margin-top: 10px;
                        }
                        .infholder p{font-size: 14px;text-align:left;}
                        .imajholder{width: 160px;margin-right: 16px}
                    </style>
                </div>
                
            </div>
        </div>
    </div>
</div>
