<style type="text/css">
  .jg_photo{
    width: 100%;
  }
  .margintop{
    margin-bottom: 3%;
  }
</style>
<div class="row aboutop">
    <div class="gallary"></div>
</div>
<div class="container">
<div class="inner-information">

     <div class="contentheading">Gallery</div>
<hr>
<div class="gallery">
  <div class="row sectiontableentry1">
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="Khula Maunch July 2015" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/lokmat_awards_photos_13-feb-2014_20140219_1019555820.jpg"><img src="<?php echo base_url();?>assets/img/gallary/lokmat_awards_photos_13-feb-2014_20140219_1019555820.jpg"  class="jg_photo" alt=""/></a>
      
        </div>
      </div>   
    </div>
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="UVA" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/lokmat_awards_photos_13-feb-2014_20140219_1473417182.jpg"><img src="<?php echo base_url();?>assets/img/gallary/lokmat_awards_photos_13-feb-2014_20140219_1473417182.jpg"  class="jg_photo" alt=""/></a>
        </div>
      </div>
     </div>
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="Education Leadership Award" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/lokmat_awards_photos_13-feb-2014_20140219_1325079364.jpg"><img src="<?php echo base_url();?>assets/img/gallary/lokmat_awards_photos_13-feb-2014_20140219_1325079364.jpg"  class="jg_photo" alt=""/></a>
        </div>
      </div>
    
    </div>
    <div class="jg_clearboth"></div>
  <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
      <div class="jg_imgalign_gal">
        <div class="jg_photo_container">
         <a title="Batch_2013-15_Skit" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/lokmat_awards_photos_13-feb-2014_20140219_20866172491.jpg"><img src="<?php echo base_url();?>assets/img/gallary/lokmat_awards_photos_13-feb-2014_20140219_20866172491.jpg"  class="jg_photo" alt=""/></a>
        </div>
      </div>
       </div>
    </div>   
    </div>
</div>     
</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.js"></script>
 <style type="text/css" href="<?php echo base_url();?>assets/base/css/jquery.fancybox.css"></style>
 <script type="text/javascript">
   $(".fancybox")
    .attr('rel', 'gallery')
    .fancybox({
        padding : 0
    });
 </script>