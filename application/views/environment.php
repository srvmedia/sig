<style type="text/css">

.geoinformatics{
    width: 100%;
    height: 320px;
    background: linear-gradient(0deg, rgba(0, 0, 0,0.4), rgba(0, 0, 0,0.4)), url("<?php echo base_url();?>assets/img/acadamics/Environment-Sustainability.jpg") no-repeat;
    background-size: cover;
}
    /*  bhoechie tab */
    div.bhoechie-tab-container{
        z-index: 10;
        background-color: transparent;
        padding: 0 !important;
        border-radius: 4px;
        -moz-border-radius: 4px;
        border:1px solid #ddd;
        margin-top: 20px;
        margin-left: 50px;
        -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        box-shadow: 0 6px 12px rgba(0,0,0,.175);
        -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        background-clip: padding-box;
        opacity: 0.97;
        filter: alpha(opacity=97);
    }
    div.bhoechie-tab-menu{
        padding-right: 0;
        padding-left: 0;
        padding-bottom: 0;
    }
    div.bhoechie-tab-menu div.list-group{
        margin-bottom: 0;
    }
    div.bhoechie-tab-menu div.list-group>a{
        margin-bottom: 0;
    }
    div.bhoechie-tab-menu div.list-group>a .glyphicon,
    div.bhoechie-tab-menu div.list-group>a .fa {
        color: #b42c2f;
    }
    div.bhoechie-tab-menu div.list-group>a:first-child{
        border-top-right-radius: 0;
        -moz-border-top-right-radius: 0;
    }
    div.bhoechie-tab-menu div.list-group>a:last-child{
        border-bottom-right-radius: 0;
        -moz-border-bottom-right-radius: 0;
    }
    div.bhoechie-tab-menu div.list-group>a.active,
    div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
    div.bhoechie-tab-menu div.list-group>a.active .fa{
        background-color: #b22629;
        background-image: #5A55A3;
        color: #ffffff;
    }
    div.bhoechie-tab-menu div.list-group>a.active:after{
        content: '';
        position: absolute;
        left: 100%;
        top: 50%;
        margin-top: -13px;
        border-left: 0;
        border-bottom: 13px solid transparent;
        border-top: 13px solid transparent;
        border-left: 10px solid #b42c2f;
    }
    .subdivclass {
        background-color: #d0cece;
        color: #b02d29;
        border-right: 2px solid #fff;
    } 
    li.subdivclass{
        padding-left: 0px;
        padding-right: 0px;
    }   
    div.bhoechie-tab-content{
        background-color: #ffffff;
        /* border: 1px solid #eeeeee; */
        padding-left: 20px;
        /* padding-top: 10px;*/
    }

    div.bhoechie-tab div.bhoechie-tab-content:not(.active){
        display: none;
    }
    .containermar{
        margin:10% 0 8% 0; 
    }
    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    .menutop li.active a{
        display: inline;
    }
    .menutop.nav>li>a {
        padding: 3px 15px;
        margin-bottom: 12px;
    }
.sub-menu{
    text-align: center;
    font-size: 20px;
    font-weight: 600;
    color: #c42027;
}
</style>
<div class="aboutop">
    <div class="geoinformatics"></div>
</div> 
<div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 menutop list-group bhoechie-tab-menu">
            <ul class="nav nav-pills nav-stacked rightpad">
                <li  class="active"><a class="liborder" data-toggle="tab" href="#Overview">Environment & Sustainability</a></li>
                <li><a class="liborder" data-toggle="tab" href="#coursestruct">Course Structure</a></li>
                <li class="sub-menu">Programme Offered by SIG</li>
                <li><a class="liborder" href="<?php echo base_url(); ?>MScinGeoinformatics">M.Sc.(Geoinformatics)</a></li>
                <li><a class="liborder" href="<?php echo base_url(); ?>MTechGeoinformatics">M.Tech. Geoinformatics & ST</a></li>
                <li><a class="liborder" href="<?php echo base_url(); ?>certificatecourse">Certificate Course in Photogrammetry and Remote Sensing</a></li>
                <li><a class="liborder" href="<?php echo base_url(); ?>courseinSpatial">Certificate Course in Spatial Economics</a></li>
                <!--<li><a class="liborder" data-toggle="tab" href="#Workshops">Workshops</a></li>-->
            </ul>
        </div>           
        <div class="tab-content col-lg-9 col-md-9 col-sm-12 col-xs-12 bhoechie-tab">
            <!-- flight section -->
            <div id="Overview" class="tab-pane fade in active">
                <h2 class="menutitle">Overview</h2>
                <hr>
                <p>
                    The objective of this M.Sc. Environment &amp; Sustainability, program is to enhance and broaden the skills and knowledge necessary to meet the demands and changes of the environmental marketplace. The programme will provide the in depth knowledge of relationship between policy, practice and the ecological environment, and the development of effective strategies for addressing the threats and opportunities posed by environmental issues. It will provide competency among students in the fields of renewable resources and energy efficiency, climate change and carbon management, corporate social responsibility, environmental and urban planning with Geospatial technology.</p>

                <p>
                    The students have opportunities in NGOs /Govt. Sectors and process based Industries as well.Also, there is greater scope for pursuing higher studies, which lead to job opportunities in academics and research sector. Demand of M.Sc. Environment Management professional is very high due to focus of several governments and industries in area of clean environment strategy and policy.				</p>
            <br></div>
            <div id="coursestruct" class="tab-pane fade in">
                <h2 class="menutitle">Course Structure</h2>
                <hr>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="subdivclass col-lg-3 active"><a href="#semisterI" aria-controls="home" role="tab" data-toggle="tab">Semester - I</a></li>
                    <li role="presentation" class="subdivclass col-lg-3"><a href="#semisterII" aria-controls="profile" role="tab" data-toggle="tab">Semester - II</a></li>
                    <li role="presentation" class="subdivclass col-lg-3"><a href="#semisterIII" aria-controls="profile" role="tab" data-toggle="tab">Semester - III</a></li>
                    <li role="presentation" class="subdivclass col-lg-3"><a href="#semisterIV" aria-controls="profile" role="tab" data-toggle="tab">Semester - IV</a></li>

                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="semisterI">
                        <div class="col-lg-12 msc">
                            <ul class="bullet-style">
                                <div class="col-lg-6 acamargin">
                                    <li>Business Communication Skills</li>
                                    <li>Organizational Behavior</li>
                                    <li>Global resource and Environment scenario</li>
                                    <li>Principles of GIS</li>
                                    <li>Logic Development and Programming Concepts</li>
                                </div>
                                <div class="col-lg-6 acamargin">
                                    <li>Principles of Environmental Sustainability</li>
                                    <li>Statistical and Quantitative Techniques</li>
                                    <li>Principles of Remote Sensing</li>
                                    <li>Environmental Risk Assessment</li>
                                    <li>IDMP</li>
                                </div>
                            </ul>
                        </div>
                    </div>  
                    <div role="tabpanel" class="tab-pane" id="semisterII">  
                        <div class="col-lg-12 msc msc-second">
                            <ul class="bullet-style">
                                <div class="col-lg-6 acamargin">
                                    <li>Research Methodology</li>
                                    <li>Environment Policy</li>
                                    <li>Environmental Law</li>
                                    <li>Hazardous Waste Management</li>
                                    <li>Climate Change Impacts and Adaptation</li>
                                    <li>Introduction to Python</li>
                                </div>
                                <div class="col-lg-6 acamargin">
                                    <li>Municipal Waste Management</li>
                                    <li>Environmental System Modelling</li>
                                    <li>Introduction to Database Management System</li>
                                    <li>Environmental Impact Assessment</li>
                                    <li>Environmental Economics</li>
                                </div>                             
                            </ul>
                        </div>  
                    </div>  
                    <div class="clearfix"></div>                        
                    <div role="tabpanel" class="tab-pane" id="semisterIII">
                        <div class="col-lg-12 msc msc-third">
                            <ul class="bullet-style">
                                <div class="col-lg-6 acamargin">
                                    <li>Summer Project</li>
                                    <li>Project Management</li>
                                    <li>Standard for ambient air, noise emission and effluent</li>
                                    <li>Environmental Clearance procedure in India</li>
                                    <li>Impact and Risk in Process Industry</li>

                                </div>
                                <div class="col-lg-6 acamargin">
                                    <li>Water Management and Water foot prints</li>
                                    <li>Agriculture Pollution and sustainability</li>
                                    <li>Urban Environment and Sustainability</li>
                                    <li>Sustainable Forestry</li>
                                    <li>Environmental standards and Audit</li>
                                </div> 
                            </ul>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="semisterIV"> 
                        <div class="col-lg-12 msc msc-forth">
                            <ul class="bullet-style acamargin">
                                <li>Project</li>
                            </ul>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- train section -->              
            <div id="Workshops" class="tab-pane fade bhoechie-tab-content">
                <h2 class="menutitle">Admissions Page</h2>
                <hr>
                <div class="workshop">
                    <div style="float:left; ">
                        <ul class="bullet-style">
                            <li>Communication Skills</li>
                            <li>Soft Skills</li>
                            <li>Team Building</li>
                            <li>Leadership Skills</li>
                            <li>Confidence Building</li>
                            <li>Decision Making</li>
                            <li>Personality Development</li>
                        </ul>
                    </div>

                    <div style="float:right; ">
                        <ul class="bullet-style">

                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!--       <div class="bhoechie-tab-content">
                      <center>
                        <h1 class="glyphicon glyphicon-credit-card" style="font-size:12em;color:#55518a"></h1>
                        <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                        <h3 style="margin-top: 0;color:#55518a">Credit Card</h3>
                      </center>
                  </div>
                  <div class="bhoechie-tab-content">
                      <center>
                        <h1 class="glyphicon glyphicon-credit-card" style="font-size:12em;color:#55518a"></h1>
                        <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                        <h3 style="margin-top: 0;color:#55518a">Credit Card</h3>
                      </center>
                  </div> -->                
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
    });
</script>