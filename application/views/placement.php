<div class="row aboutop">
    <div class="placement"></div>
</div>
<div class="container">
    <div class="col-md-3 menutop">
        <ul class="nav nav-pills nav-stacked rightpad">
            <li class="active"><a class="liborder" data-toggle="tab" href="#Placement">Placement</a></li>
            <li ><a class="liborder" data-toggle="tab" href="#industryinterface">Industry Interface</a></li>
            <li ><a class="liborder" data-toggle="tab" href="#placementannouncement">Placement Announcement</a></li>
            <li ><a class="liborder" data-toggle="tab" href="#placementdetails">Placement Details</a></li>
        </ul>
    </div>
    <div class="tab-content col-md-9 menutop">
        <div id="Placement" class="tab-pane fade in active">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Placement</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <table class="contentpaneopen">
                    <tbody><tr>
                        <td valign="top">
                            Placements are one of the benchmarks that students look to when it comes to choosing institutions for their post graduate degree. Unlike other MSC colleges in Pune, SIG believes in supporting its students throughout including providing ample placement assistance as well. The faculties strengthen the academic knowledge about the geoinformatics domain and also provide required guidance to the students to prepare them for the placement season. Being a part of the esteemed Symbiosis group, SIG has established a firm reputation amongst the big names of the geoinformatics industry. A dedicated placement team works round the clock to bring forth the opportunities for students in the Geoinformatics domain
                            <br>
                            <h2 style="text-align: justify;">SIG has had an enviable track record of 100% placements.</h2>
                            <hr>
                            <p>So far more than simply quantity, the quality of jobs garnered by SIG students is also worth a mention. What SIG puts into each student is reflected in the quality of students who leave its portals to enter the corporate world.</p>
                            <p>SIG offers placement support facility to those students who secure minimum acceptable grade ('C+' grade) in every subject of the first year, that is, first two module end examinations and two months project.</p>
                           <p>The institute provides guidance and assistance for selection of two months short duration summer project and for six months project-cum-placement to students, in terms of publication and distribution of placement brochure, promotional visits to potential employers, training for interviews and group discussions and facilities for campus interviews. Placement assistance, however, is a privilege and not a right.</p>
                            <p>Please note that SIG is not a placement agency and does not guarantee placements.</p>
                        </td>
                    </tr>

                    </tbody></table>
                <span class="article_separator">&nbsp;</span>
            </div>
        </div>

        <div id="industryinterface" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Industry Interface</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <table class="contentpaneopen">
                    <tbody><tr>
                        <td valign="top">
                            <p>In the past 12 years we had association with the following well known GIS industries</p>
                        </td>
                    </tr>
                    </tbody></table>
                <span class="article_separator">&nbsp;</span>
            </div>
        </div>

        <div id="placementannouncement" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Placement Announcement</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <table class="contentpaneopen">
                    <tbody><tr>
                        <td valign="top">
                            <p><strong>Congratulation </strong> Batch 2014-2016 Received <strong>highest salary increment 33.33% and average salary increment 100% over the last year placement</strong>.</p>
                        </td>
                    </tr>
                    </tbody></table>
                <span class="article_separator">&nbsp;</span>
            </div>
        </div>

        <div id="placementdetails" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Placement Details</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <table class="contentpaneopen">
                    <tbody><tr>
                        <td valign="top">
                            <p><a href="<?php echo base_url();?>assets/img/pdf/placement-brochure-2015.pdf" target="_blank">Placement Brochure 2016</a></p>
                            <p><a href="<?php echo base_url();?>assets/img/pdf/Placement_2013_15.pdf" target="_blank">Placement 2013-15</a></p>
                            <p><a href="<?php echo base_url();?>assets/img/pdf/Placement_2012_14.pdf" target="_blank">Placement 2012-14</a></p>
                        </td>
                    </tr>
                    </tbody></table>
                <span class="article_separator">&nbsp;</span>
            </div>
        </div>
        </div>
    <div class="clearfix"></div>
    <hr>

    <br/><br/>
    <div class=" wow animate fadeInUp" >
        <div class="col-lg-12">

            <div id="owl-example1" class="owl-carousel">

                <div class="item"><img src="<?php echo base_url();?>assets/img/company/autodesk.png" alt="autodesk" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/clsco.png" alt="clsco" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/cybertech.png" alt="cybertech" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/dong.png" alt="dong" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/erdas.png" alt="erdas" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/esri.png" alt="esri" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/fugro.png" alt="fugro" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/hcl.png" alt="hcl" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/hexagon.png" alt="hexagon" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/ibm.png" alt="ibm" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/infosys.png" alt="infosys" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/niit.png" alt="niit" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/oracle.png" alt="oracle" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/patni.png" alt="patni" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/reliance.png" alt="reliance" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/reliance-comm.png" alt="reliance-comm" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/tata.png" alt="tata" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/tomtom.png" alt="tomtom" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/wipro.png" alt="wipro" /></div>
                <div class="item"><img src="<?php echo base_url();?>assets/img/company/zobrist.png" alt="zobrist" /></div>

            </div>
        </div>
    </div>
    <span class="article_separator">&nbsp;</span>
    </div>
</div>
