		
		
		<style type="text/css">

    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    li.active a{
        display: inline;

    }

		.testimonialstable td {
			background-color:whitesmoke;
			padding:5px;
		}
    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }    
    .rightpad{
      border-right: 2px solid #f19999;
    }
    .rightpad li.active{
      padding: 10px 0px 10px;
    }
    li.active a{
      display: inline;
    }
   /* .nav>li>a {
      padding: 3px 15px;
      margin-bottom: 12px;
    }*/
   </style>
<div class="row aboutop">
    <div class="bannerimg"></div>
</div>	
<div class="container">  
<div class="col-md-3 menutop">
 <ul class="nav nav-pills nav-stacked rightpad">
    <li><a class="liborder" href="<?php echo base_url(); ?>collaborations">Collaborations</a></li>
    <li class="active"><a class="liborder" href="<?php echo base_url(); ?>testimonials">Testimonials</a></li>
	<li><a class="liborder" href="<?php echo base_url(); ?>faqs">FAQ's</a></li>
<li ><a class="liborder" href="<?php echo base_url(); ?>scientificpapers">Scietific Papers</a></li>

    <li><a class="liborder" href="http://www.siu.edu.in/resources.php" target="_blank">Health Facilities</a></li>  
  </ul>
</div>
  <div class="col-md-9 menutop">
   
      <h2 class="menutitle"><strong>Testimonials</strong></h2>
      <hr>
    
	
	<table class="contentpaneopen">



<tbody><tr>
<td valign="top">
In its 8 years of existence SIG has managed to establish successful reputation within the industry by delivering industry ready professionals year-on-year. We have had national and international experts come to SIG to teach the students. 
<br>
<br>

<table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    It was very wonderful to have discussion with 
    students of SIG during my presentation. I wish good luck at SIG.<br>
<br>

    <span style="color:#af251b;font-weight:bold;">Dr. Giriraj Amarnath<br> International
 Water Management Institute, Colombo</span>
    </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%" style="background-color: white;"></td>
    <td align="left" valign="top">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    SIG is doing remarkable work in creating capacity. 
Am happy to be a part of it.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">
 Shri Rajesh Mathur <br>
Vice Chairman, NIIT  GIS Ltd, Delhi</span>  </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>
<br>


<br><br>

<table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Unique institution in GIS, excellent brand in GIS with flagship 
on a peak of mountain, the ‘BEST INSTITUTION’ to mould career in GIS, Brig. K K V Khanzode excellent job!.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Shri Suniel Patil<br> ESRI  
    INDIA, Mumbai</span>    </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%" style="background-color: white;"></td>
    <td align="left" valign="top">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Excellent Geospatial infrastructure, well disciplined students and faculty. I wish them all the best and bright future.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Dr. Y L P Rao <br>
 IFS Chief Conservator of Forest, Govt. of Maharashtra, Pune</span>
    </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>


<br>


<br><br>

<table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Happy to be back at the institute campus to rejuvenate my association with the institute, students and faculty.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Shri A Ramanathan<br> Sr. Executive .V.P – Projects, 
    Reliance Industries</span>  </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%" style="background-color: white;"></td>
    <td align="left" valign="top">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Good to see SIG organizing execellent courses benefiting large number of students. I wish them good luck in organizing such courses in future.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Dr. V Venkateshwar Rao <br> Head, 
    Water Resources Division</span> </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>

<br>


<br><br>

<table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    This was great visit to SIG and interaction with the students was being heartening. I could see lot of interest towards Geoinformatics in the students. The centre is progressing excellently.<br>

My best wishes to the team.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Dr. Sarnam Singh<br> IIRS, 
    Dehradun</span> </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%" style="background-color: white;"></td>
    <td align="left" valign="top">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    This was my first visit to the institute. I am extremely impressed with the students and the faculty.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Dr. P Nag <br>
 NATMO,     Kolkata</span>  </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>

<br>


<br><br>

<table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Lot of interesting projects carried out by the student and institute. Good collaboration is expected to implement initiatives of Govt. org. like PMC and citizens of Pune. Thank you.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Shri Rajendra Erande<br> Director IT, PMC, 
    Pune</span> </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%" style="background-color: white;"></td>
    <td align="left" valign="top">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    It has been a great experience being with such students. My best wishes.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Dr. A K Jha<br> Commissioner, TRTI, 
    Pune</span> </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>

<br>


<br><br>

<table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    First of all I was impressed by foresight and vision of Brig K K V Khanzode in bringing SIG to the great level. He has taken efforts to shape this organization so well. The output students from SIG are always much above compared to other institutes. SIG is doing very good job of bridging gap between industry and academia. Wish SIG all success.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Shri Uday D Kale</span>
    </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%" style="background-color: white;"></td>
    <td align="left" valign="top">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Quality and diversity of projects done by students is impressive. Wishing the next batch an equally enriching experience in their projects.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Dr. G S Rao, <br>
    Lavasa Corporation</span>   </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>

<br>


<br><br>

<table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Very good presentation listing the work done here. A very nie setup and impressive. Would definitely like to visit again.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Mr. Apoorva Bhatt<br>School of Biosciences, Univ. of Birmingham, U K</span>
    </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%" style="background-color: white;"></td>
    <td align="left" valign="top">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Thank you to all staff and Director for an excellent welcome, discussion and presentation. It would be very good to see links grow between our institutions.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Mr Rae Mackay <br>
 School of Geography, Earth and Environmental Science, Univ. of Birmingham, U K</span>
    </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>

<br>


<br><br>

<table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    One of the best institution for GIS, My good wishes.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Mr. Pramod Gupta CEO<br> Indore Development Authority, Indore</span>
    </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%" style="background-color: white;"></td>
    <td align="left" valign="top">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    It is a great pleasure to visit Symbiosis Institute of Geoinformatics. We at ITC are fully aware of your new courses and appreciate them very much. We hope that Symbiosis and ITC will be able to forge a strong relationship in the future.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Mr. John Horn<br>
ITC, Enschede,  The Netherlands</span>  </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>

<br>


<br><br>

<table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    Simply Superb! Excellent infrastructure. Excellent human resources. Please keep it up and reach higher heights, contribute in nation building.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Shri R K Suryawanshi<br> Chairman ISG 
    Pune</span> </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%" style="background-color: white;"></td>
    <td align="left" valign="top">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    I am deeply impressed with the faculties, students and the faculty of SIG and above all the spirit and dedication with which all the entries are contributing to the making of the institution. In particular it was nice experience to listen to all including students who spoke on use and promotion of geo-spatial data and technologies. Thanks.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Shri P S Acharya<br>
 ST, Technology Bhawan,     New Delhi</span>    </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>
    </td>
  </tr>
</tbody></table>

<br>


<br><br>

<table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="44%">
    <table width="100%" class="testimonialstable">
  <tbody><tr>
    <td align="left" valign="top" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-left.jpg" alt=""></td>
    <td align="center" valign="top">
    SIG is going to be centre of excellence in Geo-spatial Technologies. I am sure SIG will play an important role in developing human resources in GIS.<br>
<br>
    <span style="color:#af251b;font-weight:bold;">Maj Gen Dr R Siva Kumar <br>
 CEO NSDI, DST,     New Delhi</span>    </td>
    <td align="left" valign="bottom" width="3%"><img src="<?php echo base_url(); ?>assets/img/inner-co-right.jpg" alt=""></td>
  </tr>
</tbody></table>

    </td>
    <td valign="top" align="left" width="6%" style="background-color: white;"></td>
    <td align="left" valign="top" style="background-color: white;" >&nbsp;
    
    </td>
  </tr>
</tbody></table>

</td>
</tr>

</tbody></table>
    
  </div>
</div>

</div>    
	