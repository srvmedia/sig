		

<style type="text/css">


    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    li.active a{
        display: inline;

    }

    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }    
    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        padding: 10px 0px 10px;
    }
    li.active a{
        display: inline;
    }
    /* .nav>li>a {
       padding: 3px 15px;
       margin-bottom: 12px;
     }*/
</style>
<div class="row aboutop">
    <div class="bannerimg"></div>
</div>	
<div class="container">  
    <div class="col-md-3 menutop">
        <ul class="nav nav-pills nav-stacked rightpad">
            <li class="active"><a class="liborder" data-toggle="tab" href="#technicalproblem">Technical Problems</a></li>
            <li><a class="liborder" data-toggle="tab" href="#applicationformdetails" >Application Form Details</a></li>
            <li><a class="liborder" data-toggle="tab" href="#postregistration">Post Registrations</a></li>
        </ul>
    </div>
    <div class="tab-content col-md-9 menutop">

        <div id="technicalproblem" class="tab-pane fade in active">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Technical Problems</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <div class="faq-inner">
                    <strong>I could not register online for SIG, since the following error message appeared - "Java Script Error".</strong> <br>

                    You should use Internet Explorer Ver 6.0 or above. Please note that the browsers like Mozilla and firefox support the application. Therefore you are advised to use Internet Explorer Ver 6 and above.
                    <br>
                    <br>
                    <strong>I could not register online for SIG, as I am not able to proceed after "I agree" in the terms and conditions page.</strong><br>

                    This application uses JavaScript. Ensure that JavaScript is enabled in your browser.<br>

                    To enable JavaScript go to: Tools&gt;&gt;InternetOptions&gt;&gt;Security&gt;&gt;Custom Level. Scroll down until you see section labeled 'Scripting' Under 'Active Scripting', select 'Enable' and click 'OK'.<br>

                    Click refresh and re-register.
                </div>
            </div>
        </div>

        <div id="applicationformdetails" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Application Form Details</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <div class="faq-inner">
                    <strong>My current address is valid only till October end. Is it possible for me to update and make a change in the address in early November? </strong> <br>

                    Yes, you can modify any set of personal details. You can do so, after you have received your ID /email and password.
                    <br>
                    <br>
                    <strong>Can I as a ward of a civilian apply for admission to SIG?    </strong><br>

                    Yes. You can apply for SIG in Open Category.
                </div>
            </div>
        </div>

        <div id="postregistration" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Post Registrations</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <div class="faq-inner">
                    <strong>I have registered online and sent the DD/Bank counterfoil. What is the next step? </strong> <br>

                    Congratulations on completing the first step to qualify for SIG ! You may received ID and Password. You would receive an email to attend PI/WAT before second week of February 2012.
                    <br>
                    <br>
                    <strong>I registered online, but sent my DD without mentioning the ID. What do I do now?     </strong><br>

                    You should send an email to admissions@sig.ac.in providing the DD number, bank on which it was drawn, your name, date of birth, phone number and email id. Mention 'Sent Payment without ID' in the subject line.
                    <br>
                    <strong><br>
                        I filled up the form online and have taken the DD also. I do not have access to a printer to print the payment advice form. What do I do? </strong><br>
                    In such a case, you should fill in your details with your ID on a plain paper and send the DD along with it to SIG.
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
