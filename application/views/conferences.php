		

<style type="text/css">
    .conferencetable{
        font-size: 16px;
    }
    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }
    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    .rightpad li.active a{
        display: inline;
        line-height:2;
    }
    li.active a{
        display: inline;
    }
    /* .nav>li>a {
       padding: 3px 15px;
       margin-bottom: 12px;
     }*/

    time.icon
    {
        font-size: 1em; /* change icon size */
        display: block;
        position: relative;
        width: 5em;
        height: 6em;
        background-color: #fff;
        border-radius: 0.6em;
        box-shadow: 0 1px 0 #bdbdbd, 0 2px 0 #fff, 0 3px 0 #bdbdbd, 0 4px 0 #fff, 0 5px 0 #bdbdbd, 0 0 0 1px #bdbdbd;
        overflow: hidden;
        margin-top: 5px;
    }
    time.icon *
    {
        display: block;
        width: 100%;
        font-size: 1em;
        font-weight: bold;
        font-style: normal;
        text-align: center;
    }
    time.icon strong
    {
        position: absolute;
        top: 0;
        padding: 0.3em 0;
        color: #fff;
        background-color: #BB0C1A;
        border-bottom: 1px dashed #BB0C1A;
        box-shadow: 0 2px 0 #BB0C1A;
    }
    time.icon em
    {
        position: absolute;
        bottom: 0.3em;
        color: #BB0C1A;
    }
    time.icon span
    {
        font-size: 1.8em;
        letter-spacing: -0.05em;
        padding-top: 1.0em;
        color: #2f2f2f;
    }
    .well{
        min-height: 140px;
        padding: 10px;
        overflow: auto;
        margin-bottom: 0px;;
    }
    .cornerhr{
        padding-top: 10px !important;
        border-top: 1px solid #BB0C1A;
    }
    .conference-img{
        height: 170px;
        width: 190px;
    }
</style>
<div class="aboutop">
    <div class="bannerimg"></div>
</div>	
<div class="container">  
    <div class="col-md-3 menutop">
        <ul class="nav nav-pills nav-stacked rightpad">
            <li class="active"><a class="liborder" data-toggle="tab" href="#conferences">Conferences</a></li>
            <li><a class="liborder" data-toggle="tab" href="#institute">Institute Conference</a></li>
            <li><a class="liborder" data-toggle="tab" href="#attendedstudent">Conference Attend by Students</a></li>
        </ul>
    </div>
    <div class="tab-content col-md-9 menutop">
        <div id="conferences" class="tab-pane fade in active">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Conferences</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <div class="faq-inner">
                    <p>SIG believes in giving its students a friendly yet motivating learning atmosphere. After all, it is through this holistic exposure across different learning platforms that SIG is able to deliver geoinformatics professionals as per industry standard. As part of its learning environment, SIG encourages its students to step out of the confines of the campus and participate in conferences and seminars on Geoinformatics across the country. Here is a brief overview of the conferences that the students from SIG have attended till date:</p>
                </div>
            </div>
        </div>

        <div id="institute" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Institute Conference</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <table cellspacing="0" cellpadding="0" border="0" width="100%" class="conferencetable">
                    <tbody>
                    <tr>
                        <td align="left" valign="top" style="padding-right:20px;"></td>
                        <td align="left" valign="top">
                            <div class="well">
                                <div class="col-sm-12">
                                    <div class="col-sm-2">
                                        <time datetime="2014-09-20" class="icon">
                                            <em>2010</em>
                                            <strong>August</strong>
                                            <span>21</span>
                                        </time>
                                    </div>
                                    <div class="col-sm-7">
                                        <strong>Geo-Vision 2010</strong>
                                        <hr class="cornerhr">
                                        <p>Forest, Environment &amp; Sustainability</p>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="imajholder text-center" style="float: left;">
                                            <img src="<?php echo base_url(); ?>assets/img/conference/research8.jpg" alt="Geo-Vision 2010" class="conference-img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding-right:20px;"></td>
                        <td align="left" valign="top">
                            <div class="well">
                                <div class="col-sm-12">
                                    <div class="col-sm-2">
                                        <time datetime="2014-09-20" class="icon">
                                            <em>2012</em>
                                            <strong>January</strong>
                                            <span>28</span>
                                        </time>
                                    </div>
                                    <div class="col-sm-7">
                                        <strong>Geo-Vision 2012</strong>
                                        <hr class="cornerhr">
                                        <p> New Trends in GIS With Special Reference To Mobile GIS</p>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="imajholder text-center" style="float: left;">
                                            <img src="<?php echo base_url(); ?>assets/img/conference/research9.jpg" alt="Geo-Vision 2010" class="conference-img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding-right:20px;"></td>
                        <td align="left" valign="top">
                            <div class="well">
                                <div class="col-sm-12">
                                    <div class="col-sm-2">
                                        <time datetime="2014-09-20" class="icon">
                                            <em>2013</em>
                                            <strong>August</strong>
                                            <span>31</span>
                                        </time>
                                    </div>
                                    <div class="col-sm-7">
                                        <strong>Geo-Vision 2013</strong>
                                        <hr class="cornerhr">
                                        <p>Water Sustainability 2020</p>
                                        <p> SIG organises 'Geovision' seminar each year in collaboration with
                                            Indian Society of Geomatics (ISG) and Centre of Advance
                                            Computing(CDAC).</p>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="imajholder text-center" style="float: left;">
                                            <img src="<?php echo base_url(); ?>assets/img/conference/research10.jpg" alt="Geo-Vision 2013" class="conference-img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding-right:20px;"></td>
                        <td align="left" valign="top">
                            <div class="well">
                                <div class="col-sm-12">
                                    <div class="col-sm-2">
                                        <time datetime="2014-09-20" class="icon">
                                            <em>2016</em>
                                            <strong>June</strong>
                                            <span>18</span>
                                        </time>
                                    </div>
                                    <div class="col-sm-7">
                                        <strong>Geo-Vision 2016</strong>
                                        <hr class="cornerhr">
                                        <p>18th June, 2016 , ISG, IWRS and SIG conducted a Workshop on "Smart Cities And Geo-Ict Initiative - Geovision"</p>
                                        <p>Dr Anand Deshpande, CEO Persistent , Dr. Rajat Moona , Director General (C-DAC) , &amp; Dr . B Mukhopadhyay Add Director General IMD Govt of India Have Inaugurated the Seminar </p>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="imajholder text-center" style="float: left;">
                                            <img src="<?php echo base_url(); ?>assets/img/conference/conference-page-geo-vision-2016.jpg" alt="Geo-Vision 2016" class="conference-img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding-right:20px;"></td>
                        <td align="left" valign="top">
                            <div class="well">
                                <div class="col-sm-12">
                                    <div class="col-sm-2">
                                        <time datetime="2014-09-20" class="icon">
                                            <em>2011</em>
                                            <strong>Feb</strong>
                                            <span>21-26</span>
                                        </time>
                                    </div>
                                    <div class="col-sm-7">
                                        <strong>Symbiosis Institute of Geoinformations</strong>
                                        <hr class="cornerhr">
                                        <p>SIG organised five day workshop on 'Remote Sensing and GIS
                                            applications' for university teachers across India. The workshop
                                            was funded by the Department of Science and Technology, Govt.
                                            of India.</p>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="imajholder text-center" style="float: left;">
                                            <img src="<?php echo base_url(); ?>assets/img/conference/research11.jpg" alt="Symbiosis Institute of Geoinformations" class="conference-img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                    </table>
            </div>
        </div>

        <div id="attendedstudent" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Conference Attend by Students</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <table cellspacing="0" cellpadding="0" border="0" width="100%" class="conferencetable">
                    <tbody>
                    <tr>
                        <td align="left" valign="top" style="padding-right:20px;"></td>
                        <td align="left" valign="top">
                            <div class="well">
                                <div class="col-sm-12">
                                    <div class="col-sm-2">
                                        <time datetime="2014-09-20" class="icon">
                                            <em>2012</em>
                                            <strong>February</strong>
                                            <span>7-9</span>
                                        </time>
                                    </div>
                                    <div class="col-sm-7">
                                        <strong>Privacy Issues in LBS Applications</strong>
                                        <hr class="cornerhr">
                                        <table border="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td align="left" width="35%" valign="top"><strong>Conference</strong></td>
                                                <td align="left" width="3%" valign="top">:</td>
                                                <td align="left" valign="top"> India Geospatial Forum'12, Gurgaon</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top"><strong>Name of the participant/s</strong></td>
                                                <td align="left" valign="top">:</td>
                                                <td align="left" valign="top">Akriti Kumar, Amrit Kumar, Andrie Singh </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="imajholder text-center" style="float: left;">
                                            <img src="<?php echo base_url(); ?>assets/img/conference/research6.jpg" alt="Privacy Issues in LBS Applications" class="conference-img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding-right:20px;"></td>
                        <td align="left" valign="top">
                            <div class="well">
                                <div class="col-sm-12">
                                    <div class="col-sm-2">
                                        <time datetime="2014-09-20" class="icon">
                                            <em>2011</em>
                                            <strong>February</strong>
                                            <span>25</span>
                                        </time>
                                    </div>
                                    <div class="col-sm-7">
                                        <strong>Caddie: A golf course application using GPS in Android Devices</strong>
                                        <hr class="cornerhr">
                                        <table border="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td align="left" width="35%" valign="top"><strong>Conference</strong></td>
                                                <td align="left" width="3%" valign="top">:</td>
                                                <td align="left" valign="top"> Geomatrix '11, IIT Bombay</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top"><strong>Name of the participant/s</strong></td>
                                                <td align="left" valign="top">:</td>
                                                <td align="left" valign="top">Dipti Sarkar and Varun Anand </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="imajholder text-center" style="float: left;">
                                            <img src="<?php echo base_url(); ?>assets/img/conference/research5.jpg" alt="Caddie" class="conference-img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding-right:20px;"></td>
                        <td align="left" valign="top">
                            <div class="well">
                                <div class="col-sm-12">
                                    <div class="col-sm-2">
                                        <time datetime="2014-09-20" class="icon">
                                            <em>2011</em>
                                            <strong>February</strong>
                                            <span>25</span>
                                        </time>
                                    </div>
                                    <div class="col-sm-7">
                                        <strong>Crime Information System (GIS based crime information system)</strong>
                                        <hr class="cornerhr">
                                        <table border="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td align="left" width="35%" valign="top"><strong>Conference</strong></td>
                                                <td align="left" width="3%" valign="top">:</td>
                                                <td align="left" valign="top"> Geomatrix 2011, I.I.T Bombay</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top"><strong>Name of the participant/s</strong></td>
                                                <td align="left" valign="top">:</td>
                                                <td align="left" valign="top">Nilesh Irwadkar,Shaily Gandhi, Vijay Pawar, Manlee Chowdhury </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="imajholder text-center" style="float: left;">
                                            <img src="<?php echo base_url(); ?>assets/img/conference/research4.jpg" alt="Crime Information System" class="conference-img">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding-right:20px;"></td>
                        <td align="left" valign="top">
                            <div class="well">
                                <div class="col-sm-12">
                                    <div class="col-sm-2">
                                        <time datetime="2014-09-20" class="icon">
                                            <em>2010</em>
                                            <strong>February</strong>
                                            <span>26-27</span>
                                        </time>
                                    </div>
                                    <div class="col-sm-7">
                                        <strong>Urban Change detection with the help of Cellular Automata</strong>
                                        <hr class="cornerhr">
                                        <p>This project examines the use of GIS and Remote Sensing in mapping Land Use Land Cover in Pune between 1999 and 2005 so as to detect the changes that has taken place in this status between these periods.It  is also use  to produce a land use land cover map of Pune at different epochs in order to detect the changes that have taken place particularly in the built-up land and subsequently predict likely changes that might take place in the same over a given period.</p>
                                        <table border="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td align="left" width="35%" valign="top"><strong>Conference</strong></td>
                                                <td align="left" width="3%" valign="top">:</td>
                                                <td align="left" valign="top"> Geomatrix' at IIT- Mumbai</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top"><strong>Name of the participant/s</strong></td>
                                                <td align="left" valign="top">:</td>
                                                <td align="left" valign="top">Anupama Joshi,Nisha Devar</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="imajholder text-center" style="float: left;">
                                            <img src="<?php echo base_url(); ?>assets/img/conference/research3.jpg" alt="Urban Change detection" class="conference-img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding-right:20px;"></td>
                        <td align="left" valign="top">
                            <div class="well">
                                <div class="col-sm-12">
                                    <div class="col-sm-2">
                                        <time datetime="2014-09-20" class="icon">
                                            <em>2010</em>
                                            <strong>February</strong>
                                            <span>26-27</span>
                                        </time>
                                    </div>
                                    <div class="col-sm-7">
                                        <strong>Ecotourism with the help of Remote Sensing and GIS</strong>
                                        <hr class="cornerhr">
                                        <p>This study is an attempt to identify potential ecotourism sites in eastern part of pune district using Remote Sensing and GIS techniques in Maharastra. After identifying the potential sites, a demonstrative plan has been made for Ecotourism development based on locally available natural resources.</p>
                                        <table border="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td align="left" width="35%" valign="top"><strong>Conference</strong></td>
                                                <td align="left" width="3%" valign="top">:</td>
                                                <td align="left" valign="top"> Geomatrix' at IIT- Mumbai</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top"><strong>Name of the participant/s</strong></td>
                                                <td align="left" valign="top">:</td>
                                                <td align="left" valign="top">Anupama Joshi</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="imajholder text-center" style="float: left;">
                                            <img src="<?php echo base_url(); ?>assets/img/conference/research2.jpg" alt="Ecotourism" class="conference-img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding-right:20px;"></td>
                        <td align="left" valign="top">
                            <div class="well">
                                <div class="col-sm-12">
                                    <div class="col-sm-2">
                                        <time datetime="2014-09-20" class="icon">
                                            <em>2010</em>
                                            <strong>December</strong>
                                            <span>20-22</span>
                                        </time>
                                    </div>
                                    <div class="col-sm-7">
                                        <strong>Site Suitability for cultivation of Biofuel in Pune</strong>
                                        <hr class="cornerhr">
                                        <p>We  used Remote sensing and GIS to locate and  find suitable sites for cultivation of Jatropha and Pangamia trees and suitable weather conditions for their growth in the Pune District .This study helped us in collection of the Jatropha &amp; Pongamia seeds that can be processed into biofuels.</p>
                                        <table border="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td align="left" width="35%" valign="top"><strong>Conference</strong></td>
                                                <td align="left" width="3%" valign="top">:</td>
                                                <td align="left" valign="top"> Geomatrix' at IIT- Mumbai</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top"><strong>Name of the participant/s</strong></td>
                                                <td align="left" valign="top">:</td>
                                                <td align="left" valign="top">Anupama Joshi,Sarat Bokka</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="imajholder text-center" style="float: left;">
                                            <img src="<?php echo base_url(); ?>assets/img/conference/research1.jpg" alt="Site Suitability" class="conference-img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

</div>    
