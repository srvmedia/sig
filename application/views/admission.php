<style type="text/css">
    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }
    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    .rightpad li.active a{
        display: inline;
        line-height:2;
    }
    td, th {
        padding: 0;
        padding-left: 3%;
    }
    li.subdivclass{
        padding-left: 0px;
        padding-right: 0px;
    }
    .service-icon i.fa{
        font-size: 35px;
        color: red;
    }
    .feestrct p{
        min-height: 94px;
    }
    .feestrct i.fa {
        font-size: 22px;
        color: #ee7f1a;
    }
    .fees{
        border:solid 1px gray;
        max-height: 102px !important;
    }
    .fees p{
        text-align: center !important;
    }
    .impdate i.fa{
        font-size: 22px;
        color: #ee7f1a;
    }
    .service-figure {
        position: relative;
        border: 1px solid #999;
        -webkit-box-shadow: 0px 0px 11px 0px rgba(50, 50, 50, 0.46);
        -moz-box-shadow: 0px 0px 11px 0px rgba(50, 50, 50, 0.46);
        box-shadow: 0px 0px 11px 0px rgba(50, 50, 50, 0.46);
    }
    .service-figure:hover .service-caption, .employee-desination, .hover-overlay, .product-name:hover, .product-figure:hover .product-caption, .gallery-hover, .flicker-img li a::before, .map-overlay, .overlay-pink::before, .scrollup:hover, .video-titel .play-btn:hover {
        background: rgba( 244,86,97,0.7);
    }
    .service-figure:hover .service-icon i {
        color: #fff!important;
        border-color: #fff;
    }

    .service-figure:hover h4 {
        color: #fff!important;
        border-color: #fff;
    }
    .service-icon {
        height: 82px;
        width: 82px;
        line-height: 105px;
        text-align: center;
        border: 2px solid #999;
        display: inline-block;
        border-radius: 100%;
        margin: 0 0 20px;
    }
    h3 {
        font-size: 26px;
        margin: 0 0 10px;
        font-weight: bold;
        text-transform: capitalize;
    }
    .service-figure:hover .service-caption h3, .service-figure:hover .service-caption p, .service-figure:hover .service-caption .read-more {
        color: #fff;
    }
    .msc11{
        margin-top: 3%;
        min-height: 257px;
    }
    .item1{
        position: absolute;
        top:0px;
        background: #fff;
    }
    .service-caption {
        text-align: center;
        padding: 23px;
        background: rgba( 255,255,255,1.0);
        min-height: 231px;
        min-width: 231px;
    }
    .subdivclass {
        background-color: #d0cece;
        color: #b02d29;
        border-right: 2px solid #fff;
    }
    .item1 a:active, .item1 > a:hover{
        color: #ffffff;
    }
    .service-figure:hover .service-icon{
        border: 2px solid #fff;
    }
    img{
        width: 100%;
    }

.sub-menu{
    text-align: center;
    font-size: 20px;
    font-weight: 600;
    color: #c42027;
}
    .service-figure:hover{
        background-color:#ffffff;
        box-shadow: 5px 5px 20px;
        border: solid 1px #e6c9ca;
        -webkit-transform: scale(1.1);
        -moz-transform: scale(1.1);
        -ms-transform: scale(1.1);
        -o-transform: scale(1.1);
        transform: scale(1.1);
    }
    .menutitle a{
        float: right;
    }
    @media (max-width: 991px) {
        .menutitle a{
            float: left !important;
        }
    }
</style>
<div class="aboutop">
    <div class="admission"></div>
</div>	 
<div class="container">
    <div class="col-md-3 menutop">  
        <ul class="nav nav-pills nav-stacked rightpad">
            <li class="active"><a class="liborder" data-toggle="tab" href="#admissions">Admissions</a></li>
                <li class="sub-menu">Post Graduate Programs</li>
            <li><a class="liborder" data-toggle="tab" href="#Overview">M.Sc. Geoinformatics</a></li>
            <li><a class="liborder" data-toggle="tab" href="#mtech">M.Tech. Geoinformatics & ST</a></li>
            <li><a class="liborder" data-toggle="tab" href="#environment">M.Sc. Environment & Sustainability</a></li>
            <li class="sub-menu">Certificate Program</li>
            <li><a class="liborder" data-toggle="tab" href="#symbiosisiu">Certificate Course in Photogrammetry and Remote Sensing</a></li>
            <li><a class="liborder" data-toggle="tab" href="#certificate">Certificate Course in Spatial Economics</a></li>
            <li class="sub-menu">Diploma Program</li>
            <li><a class="liborder" data-toggle="tab" href="#diplomaprogramme">Diploma Program</a></li>
        </ul>
    </div>
    <div class="tab-content col-md-9 menutop">
        <div id="admissions" class="tab-pane fade in active">
            <table class="contentpaneopen">
                <tbody><tr>
                <h2 class="menutitle">Admissions</h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">		 
                <table class="contentpaneopen">
                    <tbody><tr>
                            <td valign="top">
                                SIG is amongst the only Symbiosis Institute in Pune to deal with the niche domain of geoinformatics. Symbiosis Pune admissions specifically for SIG happen at different levels. From admission entrance exams to scrutinizing by experts in the personal interview sessions, students are examined from multiple aspects before making it to the coveted programs at SIG. <br>
                                <br>
<div class="sub-menu" style="font-size: 24px;margin-top: 35px;">Postgraduate Courses</div>
<br/>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-xs-12" style="margin-top:2%;">
                                        <a href="<?php echo base_url(); ?>MScinGeoinformatics">
                                        <div class="owl-item active" style="width: 233px; margin-right: 20px;">
                                            <div class="item">
                                                <figure class="service-figure">
                                                    <img src="<?php echo base_url(); ?>assets/img/acadamics/01.jpg" alt="img-01">
                                                    <figcaption class="service-caption item1">
                                                        <span class="service-icon"><i class="fa fa-book" aria-hidden="true"></i></span><h4>M.Sc. Geoinformatics</h4>
                                                        </figcaption>
                                                </figure>
                                            </div></div>
                                            </a>
                                    </div>
                                    
                                    <div class="col-lg-4 col-md-4 col-xs-12" style="margin-top:2%;">
                                        <a href="<?php echo base_url(); ?>MTechGeoinformatics">
                                        <div class="owl-item active" style="width: 233px; margin-right: 20px;">
                                            <div class="item">
                                                <figure class="service-figure">
                                                    <img src="<?php echo base_url(); ?>assets/img/acadamics/02.jpg" alt="img-01">
                                                    <figcaption class="service-caption item1">
                                                        <span class="service-icon"><i class="fa fa-book" aria-hidden="true"></i></span><h4>M.Tech(Geoinformatics and Surveying Technology)</h4>
                                                        
                                                    </figcaption>
                                                </figure>
                                            </div></div>
                                            </a>
                                    </div>
                                   
                                    
                                    <div class="col-lg-4 col-md-4 col-xs-12" style="margin-top:2%;">
                                        <a href="<?php echo base_url(); ?>environment">
                                        <div class="owl-item active" style="width: 233px; margin-right: 20px;">
                                            <div class="item">
                                                <figure class="service-figure">
                                                    <img src="<?php echo base_url(); ?>assets/img/acadamics/04.jpg" alt="img-01">
                                                    <figcaption class="service-caption item1">
                                                        <span class="service-icon"><i class="fa fa-book" aria-hidden="true"></i></span><h4>M.Sc.  Environment & Sustainability</h4>
                                                        
                                                    </figcaption>
                                                </figure>
                                            </div></div>
                                            </a>
                                    </div>
                                     <div class="clearfix"></div>
<div class="sub-menu" style="font-size: 24px;margin-top: 35px;">Certificate Courses</div>
<br/>
                                    <div class="col-lg-4 col-md-4 col-xs-12" style="margin-top:2%;">
                                        <a href="<?php echo base_url(); ?>certificatecourse">
                                        <div class="owl-item active" style="width: 233px; margin-right: 20px;">
                                            <div class="item">
                                                <figure class="service-figure">
                                                    <img src="<?php echo base_url(); ?>assets/img/acadamics/05.jpg" alt="img-01">
                                                    <figcaption class="service-caption item1">
                                                        <span class="service-icon"><i class="fa fa-book" aria-hidden="true"></i></span><h4>Certificate Course in Photogrammetry and Remote Sensing</h4>
                                                        
                                                    </figcaption>
                                                </figure>
                                            </div></div>
                                            </a>
                                    </div>
                                     <div class="col-lg-4 col-md-4 col-xs-12" style="margin-top:2%;">
                                         <a href="<?php echo base_url(); ?>courseinSpatial">
                                        <div class="owl-item active" style="width: 233px; margin-right: 20px;">
                                            <div class="item">
                                                <figure class="service-figure">
                                                    <img src="<?php echo base_url(); ?>assets/img/acadamics/03.jpg" alt="img-01">
                                                    <figcaption class="service-caption item1">
                                                        <span class="service-icon"><i class="fa fa-book" aria-hidden="true"></i></span><h4>Certificate Course in Spatial Economics</h4>
                                                        
                                                    </figcaption>
                                                </figure>
                                            </div></div>
                                        </a>
                                    </div>
                                </div>   

                          </td>
                        </tr>

                    </tbody>
                </table>
                <span class="article_separator">&nbsp;</span>		 
            </div>

        </div>
        <div id="Overview" class="tab-pane fade in">
            <div class="inner-information">

                <table class="contentpaneopen">
                    <tbody><tr>
                    <h2 class="menutitle">M.Sc. Geoinformatics		
                        <a target="_blank" href="<?php echo base_url(); ?>assets/pdf/Application-form-for-M.Sc.pdf"><img style="width:78%;" src="<?php echo base_url(); ?>assets/img/download-application.jpg" border="0"></a>
                    </h2>
                    </tr>
                    </tbody></table>
                <hr>
                <div class="row">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="subdivclass col-lg-3 active"><a href="#YsemisterI" aria-controls="home" role="tab" data-toggle="tab">Overview</a></li>
                        <li role="presentation" class="subdivclass col-lg-3"><a href="#YsemisterII" aria-controls="profile" role="tab" data-toggle="tab">Selection Procedure</a></li>
                        <li role="presentation" class="subdivclass col-lg-3"><a href="#YsemisterIII" aria-controls="profile" role="tab" data-toggle="tab">Fee structure & Dates</a></li>
                        <li role="presentation" class="subdivclass col-lg-3"><a href="#YsemisterIV" aria-controls="profile" role="tab" data-toggle="tab">International Students</a></li>                        
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="YsemisterI">
                            <div class="col-lg-12 msc">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <br>        
                                        <h2 class="menutitle">Reservations</h2>
                                        <hr>
                                        <p>Within the sanctioned intake</p>
                                        <ul>
                                            <li>SC: 15%</li>
                                            <li>ST: 7.5%</li>
                                            <li>Physically Handicapped: 3%</li>
                                            <li>Over and above the sanctioned intake:Jammu and Kashmir Migrants: 2 seats per programme.</li>
                                            <li>International Students including NRIs : 15%.</li>
                                        </ul> 
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <br>
                                        <h2 class="menutitle">Eligibility</h2>
                                        <hr>
                                        <strong>Post Graduation / Graduation</strong> with a Minimum of 50 % mark (45% marks for SC/ST candidates)
<ul>
<li>Engineering </li>
<li>IT </li>
<li>Computer Science </li>
<li>Science </li>
<li>Agriculture </li>
<li>Management </li>
<li>Geography </li>
<li>Commerce </li>
<li>Final year students awaiting results may also apply.</li>
<li>Final year students awaiting results may also apply.</li>
</ul>
                                        
                                            </p>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <h2 class="menutitle">Indian Students</h2>
                                        <hr>Intake:60 students </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="YsemisterII">
                            <div class="col-lg-12 msc msc-second">
                                <h2 class="menutitle">Selection Procedure </h2>
                                <hr>
                                <p><strong>Appearing for SNAP test is NOT mandatory to apply for SIG.</strong></p>
                                <p>Candidates registered for SIG will be eligible to appear for the Personal Interview and written ability test.</p>
                                
                                <p><strong>Candidates will be called Personal Interview (PI) and Written Ability Test (WAT)</strong></p>
                                <p> Application Forms are also available at the institute office on payment of Rs. 1000/- for Indian students in cash. By courier, send a demand draft for Rs. 1000/- in favor of , <strong>'The Director, Symbiosis Institute of Geoinformatics'</strong>,</p> 
                                <h2 class="menutitle">Payable at Pune at the address mentioned below:-<strong>The Director Address</strong></h2>
                                <hr>
                                <p>Symbiosis Institute of Geoinformatics,</p>
                                <p>5<sup>th</sup> Floor, Atur Center,</p>
                                <p>Gokhale Cross Road, Model Colony,</p>
                                <p>Pune – 411 016</p>
                                <p><strong>Tel </strong>: 020  - 2567 2841 / 43</p>
                                <p><strong>Telefax </strong> 020-2567 2842</p>
                                <p>For any query contact us at: 
                                    <a href="mailto:enquiry@sig.ac.in" style="color:#5893dd;">enquiry@sig.ac.in</a> , <a href="mailto:admissions@sig.ac.in" style="color:#5893dd;">admissions@sig.ac.in,</a></p>
                                <p>This e-mail address is being protected from spambots. You need JavaScript enabled to view it</p>
                            </div>
                        </div>  
                        <div class="clearfix"></div>
                        <div role="tabpanel" class="tab-pane" id="YsemisterIII">
                            <div class="col-lg-12 msc msc-third">
                                <h2 class="menutitle">Fee structure per annum</h2>
                                <hr>
                                <div class="row feestrct">
                                    <div class="col-lg-2 fees">
                                        <p>Category<br> <i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Open</p></div>
                                    <div class="col-lg-2 fees">
                                        <p>Tuition Fee <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.1,95,000</p></div>
                                    <div class="col-lg-2 fees">
                                        <p>Refundable Deposit <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.20,000</p></div>
                                    <div class="col-lg-2 fees">
                                        <p>Total <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.2,15,000</p></div>      	
                                    <div class="col-lg-2 fees">
                                        <p>1st Installment <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.1,20,000</p></div>
                                    <div class="col-lg-2 fees"><p>2nd Installment <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.95,000</p></div>
                                </div>  	
                                <h2 class="menutitle">Essential Documents for Confirmation of Admission</h2>
                                <hr>
                                <p>All admissions are provisional and are subject to approval by the Symbiosis International University. All students must ensure submission of one copy each of the following documents duly self attested before 15<sup>th</sup> September 2017 for submission by the Institute to SIU. </p>
                                <ul class="bullet-style">
                                    <li>Statement of Marks.</li>
                                    <li>Post Graduation / Graduation Certificate.</li>
                                    <li>Migration Certificate.</li>
                                    <li>Caste Certificate issued by competent authority of respective state/ Central         Government, if applicable.</li>
                                </ul>
                                <p>Affidavit for change in name, if applicable.</p>
                                <p>Producing originals along with the attested copy for verification is mandatory.</p>
                                <p><strong>Note:</strong> Candidates are personally responsible to ascertain whether they possess the requisite qualifications for admission. Having been admitted provisionally does not mean acceptance of eligibility. Final eligibility for admission will be decided by Symbiosis International University. </p>
                                <h2 class="menutitle">Important Dates</h2>
                                <hr>
                                <div class="imp">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td style="padding-left: 20px;" width="55%" valign="top" align="left">Submission of application form last date</td>
                                                <td class="impdate"><i class="fa fa-arrow-right" aria-hidden="true"></i></td>
                                                <td valign="top" align="left">18/02/2017</td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 20px;" width="55%" valign="top" align="left">Personal Interview / Written Ability Test</td>
                                                <td class="impdate"><i class="fa fa-arrow-right" aria-hidden="true"></i></td>
                                                <td valign="top" align="left">25th & 26th Feb 2017</td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 20px;" width="55%" valign="top" align="left">Course Commences on</td>
                                                <td class="impdate"><i class="fa fa-arrow-right" aria-hidden="true"></i></td>
                                                <td valign="top" align="left"> 2nd June 2017<br></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="YsemisterIV">
                            <div class="col-lg-12 msc msc-forth">
                                <h2 class="menutitle">The Symbiosis Centre for International Education (SCIE) </h2>
                                <hr>
                                <p>The Symbiosis Centre for International Education (SCIE) is a department of Symbiosis International University, providing leadership &amp; support for the University's efforts to internationalize the campus and the curriculum. </p>
                                <p>At Symbiosis, not only are 75 countries represented in the student community, but there is also an emphasis on internationalizing the curricula. Students and faculty are repeatedly provided with opportunities to pursue their international academic interests to the fullest, be it through academic exchanges or exposure to international conferences, seminars, workshops, etc. </p>
                                <p> Internationalization at Symbiosis works on multiple tracks, hence, the two divisions in the Symbiosis Centre for International Education include: </p>
                                <ul class="bullet-style">
                                    <li>International Students Admissions, Events and Support Services</li>
                                    <li>International Initiatives &amp; Collaborations</li>
                                </ul> 
                                <h2 class="menutitle">International Students Admissions, Events &amp; Support Services</h2>
                                <hr>
                                <p> The International Students Admissions at Symbiosis Centre for International Education is designed taking into consideration the requirements and difficulties faced by International Students. The admissions are centralized for all the constituent institutes of Symbiosis International University and routed through Symbiosis Centre for International Education (SCIE). The online admission procedure has made it accessible to each and every student from different parts of the world and in the comfort of their homes.</p>
                                <p> To check if you are an international student, please refer to the categories listed.</p>
                                <p> Categories under International Students Admissions:  </p>
                                <p><strong>Foreign National (FN)</strong></p>
                                <p>A student is eligible to apply as a Foreign National (FN) if he/she holds a foreign passport.  </p>
                                <p><b>Person of Indian Origin (PIO) </b></p>
                                <p> A student is eligible to apply as a Person of Indian Origin (PIO) if he/she has a PIO card, and is a citizen of a country other than India. </p>
                                <h2>Non Resident Indian (NRI)</h2>
                                <p><strong>(At Undergraduate Level)</strong></p>
                                <p> A student is eligible to apply as a Non Resident Indian (NRI) if he/she has appeared either for the 10 or 10+2 (equivalent to the Indian 10th &amp; 12th) examination outside India. </p>
                                <p>It is mandatory for the Students who falls in the NRI Category and 'have appeared for the 12th in India' to appear for the SET TEST. </p>
                                <p><strong>(At Postgraduate Level)</strong></p>
                                <p>A student is eligible to apply as a Non Resident Indian (NRI) if he/she has appeared either for the 10+2 examination (equivalent to the Indian 12th) or Graduation outside India. It is mandatory for one of the two qualifying examinations to be given outside India. </p>
                                <p>If you fall into any of these categories and wish to apply to Symbiosis Institute, kindly contact.</p>
                                <h2 class="menutitle">Symbiosis Centre For International Education (SCIE)</h2>
                                <hr>
                                <p><strong>Address : </strong> Symbiosis Society Senapati Bapat Road, Pune - 411004 Maharashtra, India</p>
                                <p><strong>Tel : </strong> 020 - 25671905</p>
                                <p><strong>Telefax :</strong> 020-25673854</p>
                                <p><strong>Email: </strong> <a href="mailto:intadmissions@symbiosis.ac.in?Subject%20Form%20Website" style="color:#5893dd;"> intadmissions@symbiosis.ac.in</a></p>
                                <p><strong>Website :</strong><a target="_blank" style="color:#5893dd;" href="http://www.scie.ac.in/"> www.scie.ac.in</a></p>
                            </div>
                        </div>
                    </div> 
                </div>

                <span class="article_separator">&nbsp;</span>


            </div>

        </div>

        <div id="mtech" class="tab-pane fade in">
            <div class="inner-information">

                <table class="contentpaneopen">
                    <tbody><tr>

                    <h2 class="menutitle">M.Tech ( Geoinformatics and Surveying Technology )	
                        <a style="float: right;" target="_blank" href="<?php echo base_url(); ?>assets/pdf/Application-form-for-M.Sc.pdf"><img style="width:78%;" src="<?php echo base_url(); ?>assets/img/download-application.jpg" border="0"></a>
                    </h2>	</tr>
                    </tbody></table>
                <hr>
                <div class="row">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="subdivclass col-lg-3 active"><a href="#XsemisterI" aria-controls="home" role="tab" data-toggle="tab">Overview</a></li>
                        <li role="presentation" class="subdivclass col-lg-3"><a href="#XsemisterII" aria-controls="profile" role="tab" data-toggle="tab">Selection Procedure</a></li>
                        <li role="presentation" class="subdivclass col-lg-3"><a href="#XsemisterIII" aria-controls="profile" role="tab" data-toggle="tab">Fee structure & Dates</a></li>
                        <li role="presentation" class="subdivclass col-lg-3"><a href="#XsemisterIV" aria-controls="profile" role="tab" data-toggle="tab">International Students</a></li>                        
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="XsemisterI">
                            <div class="col-lg-12 msc">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <br>        
                                        <h2 class="menutitle">Reservations</h2>
                                        <hr>
                                        <ul>
                                            <b>Within the sanctioned intake</b>
                                            <li>SC: 15%</li>
                                            <li>ST: 7.5%</li>
                                            <li>Physically Handicapped: 3%</li>
                                            <br/>
                                            <b>Over and above the sanctioned intake:</b>
                                            <li>Jammu and Kashmir Migrants: 2 seats per programme.</li>
                                            <li>International Students including NRIs : 15%.</li>
                                        </ul> 
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <br>
                                        <h2 class="menutitle">Eligibility</h2>
                                        <hr>
                                        <p>
                                            At least 50% marks in any one of following branches , B.Tech/BE , M.Sc Geoinformatics, M.Sc. Physics, M.Sc. Geology, M.Sc. Environmental Science, M.Sc. Mathematics, M.Sc. Agriculture or equivalent
                                        </p> 
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <h2 class="menutitle"> Intake</h2>
                                        <hr>18 Seats </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="XsemisterII">
                            <div class="col-lg-12 msc msc-second">
                                <br/>
                                <h2 class="menutitle">Selection Procedure</a></h2>
                                <hr>

                                <p>GATE score or entrance test for non-GATE candidates<br/>
                                    Medium of Instruction :   English<br>
                                    Program Pattern  :   Semester Pattern – 4 Semesters
                                </p>

                            </div>
                        </div>  
                        <div class="clearfix"></div>
                        <div role="tabpanel" class="tab-pane" id="XsemisterIII">
                            <div class="col-lg-12 msc msc-third">
                                <h2 class="menutitle">Fee structure per annum</h2>
                                <hr>
                                <div class="row feestrct">
                                    <div class="col-lg-2 fees">
                                        <p>Category  <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Open   </p></div> 
                                    <div class="col-lg-2 fees">
                                        <p>Tuition    Fee  <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.2,20,000 </p></div>
                                    <div class="col-lg-2 fees">
                                        <p>Refundable Deposit <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs. 20,000   </p></div>    
                                    <div class="col-lg-2 fees">
                                        <p>Total <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.2,40,000</p></div>
                                    <div class="col-lg-2 fees">
                                        <p>1st Installment <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.1,45,000 </p></div>
                                    <div class="col-lg-2 fees"><p>2nd Installment <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.95,000</p></div>
                                    <br/><br/>
                                </div>  	
                                <br/>
                                <h2 class="menutitle">Important Dates</h2>
                                <hr>
                                <div class="imp">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td style="padding-left: 20px;" width="55%" valign="top" align="left">Submission of application form last date</td>
                                                <td class="impdate"><i class="fa fa-arrow-right" aria-hidden="true"></i></td>
                                                <td valign="top" align="left">22/05/2017</td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 20px;" width="55%" valign="top" align="left">Personal Interview / Written Ability Test</td>
                                                <td class="impdate"><i class="fa fa-arrow-right" aria-hidden="true"></i></td>
                                                <td valign="top" align="left">3/06/2017</td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 20px;" width="55%" valign="top" align="left">Course Commences on</td>
                                                <td class="impdate"><i class="fa fa-arrow-right" aria-hidden="true"></i></td>
                                                <td valign="top" align="left">10/07/2017<br></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="XsemisterIV">
                            <div class="col-lg-12 msc msc-forth">
                                <h2 class="menutitle">The Symbiosis Centre for International Education (SCIE) </h2>
                                <hr>
                                <p>The Symbiosis Centre for International Education (SCIE) is a department of Symbiosis International University, providing leadership &amp; support for the University's efforts to internationalize the campus and the curriculum. </p>
                                <p>At Symbiosis, not only are 75 countries represented in the student community, but there is also an emphasis on internationalizing the curricula. Students and faculty are repeatedly provided with opportunities to pursue their international academic interests to the fullest, be it through academic exchanges or exposure to international conferences, seminars, workshops, etc. </p>
                                <p> Internationalization at Symbiosis works on multiple tracks, hence, the two divisions in the Symbiosis Centre for International Education include: </p>
                                <ul class="bullet-style">
                                    <li>International Students Admissions, Events and Support Services</li>
                                    <li>International Initiatives &amp; Collaborations</li>
                                </ul> 
                                <h2 class="menutitle">International Students Admissions, Events &amp; Support Services</h2>
                                <hr>
                                <p> The International Students Admissions at Symbiosis Centre for International Education is designed taking into consideration the requirements and difficulties faced by International Students. The admissions are centralized for all the constituent institutes of Symbiosis International University and routed through Symbiosis Centre for International Education (SCIE). The online admission procedure has made it accessible to each and every student from different parts of the world and in the comfort of their homes.</p>
                                <p> To check if you are an international student, please refer to the categories listed.</p>
                                <p> Categories under International Students Admissions:  </p>
                                <p><strong>Foreign National (FN)</strong></p>
                                <p>A student is eligible to apply as a Foreign National (FN) if he/she holds a foreign passport.  </p>
                                <p><b>Person of Indian Origin (PIO) </b></p>
                                <p> A student is eligible to apply as a Person of Indian Origin (PIO) if he/she has a PIO card, and is a citizen of a country other than India. </p>
                                <h2>Non Resident Indian (NRI)</h2>
                                <p><strong>(At Undergraduate Level)</strong></p>
                                <p> A student is eligible to apply as a Non Resident Indian (NRI) if he/she has appeared either for the 10 or 10+2 (equivalent to the Indian 10th &amp; 12th) examination outside India. </p>
                                <p>It is mandatory for the Students who falls in the NRI Category and 'have appeared for the 12th in India' to appear for the SET TEST. </p>
                                <p><strong>(At Postgraduate Level)</strong></p>
                                <p>A student is eligible to apply as a Non Resident Indian (NRI) if he/she has appeared either for the 10+2 examination (equivalent to the Indian 12th) or Graduation outside India. It is mandatory for one of the two qualifying examinations to be given outside India. </p>
                                <p>If you fall into any of these categories and wish to apply to Symbiosis Institute, kindly contact.</p>
                                <h2 class="menutitle">Symbiosis Centre For International Education (SCIE)</h2>
                                <hr>
                                <p><strong>Address : </strong> Symbiosis Society Senapati Bapat Road, Pune - 411004 Maharashtra, India</p>
                                <p><strong>Tel : </strong> 020 - 25671905</p>
                                <p><strong>Telefax :</strong> 020-25673854</p>
                                <p><strong>Email: </strong><a href="mailto:intadmissions@symbiosis.ac.in?Subject%20Form%20Website" style="color:#5893dd;"> intadmissions@symbiosis.ac.in</a></p>
                                <p><strong>Website :</strong><a target="_blank" style="color:#5893dd;" href="http://www.scie.ac.in/"> www.scie.ac.in</a></p>
                            </div>
                        </div>
                    </div> 
                </div>

                <span class="article_separator">&nbsp;</span>


            </div>

        </div>


        <div id="environment" class="tab-pane fade in">
            <div class="inner-information">

                <table class="contentpaneopen">
                    <tbody><tr>

                    <h2 class="menutitle">M.Sc. Environment & Sustainability	
                        <a style="float: right;" target="_blank" href="<?php echo base_url(); ?>assets/pdf/Application-form-for-M.Sc.pdf"><img style="width:78%;" src="<?php echo base_url(); ?>assets/img/download-application.jpg" border="0"></a>
                    </h2>	</tr>
                    </tbody></table>
                <hr>
                <div class="row">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="subdivclass col-lg-3 active"><a href="#ZsemisterI" aria-controls="home" role="tab" data-toggle="tab">Overview</a></li>
                        <li role="presentation" class="subdivclass col-lg-3"><a href="#ZsemisterII" aria-controls="profile" role="tab" data-toggle="tab">Selection Procedure</a></li>
                        <li role="presentation" class="subdivclass col-lg-3"><a href="#ZsemisterIII" aria-controls="profile" role="tab" data-toggle="tab">Fee structure & Dates</a></li>
                        <li role="presentation" class="subdivclass col-lg-3"><a href="#ZsemisterIV" aria-controls="profile" role="tab" data-toggle="tab">International Students</a></li>                        
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="ZsemisterI">
                            <div class="col-lg-12 msc">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <br>        
                                        <h2 class="menutitle">Reservations</h2>
                                        <hr>
                                        <ul>
                                            <b>Within the sanctioned intake</b>
                                            <li>SC: 15%</li>
                                            <li>ST: 7.5%</li>
                                            <li>Physically Handicapped: 3%</li>
                                            <br/>
                                            <b>Over and above the sanctioned intake:</b>
                                            <li>Jammu and Kashmir Migrants: 2 seats per programme.</li>
                                            <li>International Students including NRIs : 15%.</li>
                                        </ul> 
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <br>
                                        <h2 class="menutitle">Eligibility</h2>
                                        <hr>
                                        <p>
                                            Post Graduate / Graduate in Engineering, IT, Science, Computer    Science, Agriculture, Geography of any statutory university  with 50% marks(45% for SC / ST) at graduation level.
                                        </p> 
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <h2 class="menutitle">Indian Students</h2>
                                        <hr>30 Seats </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="ZsemisterII">
                            <div class="col-lg-12 msc msc-second">
                                <br/>
                                <h2 class="menutitle">Selection Procedure </h2>
                                <hr>

                                <p>Written Ability Test & Personal Interview<br/>
                                    For International Students : As per procedure prescribed by  SCIE.</p>

                            </div>
                        </div>  
                        <div class="clearfix"></div>
                        <div role="tabpanel" class="tab-pane" id="ZsemisterIII">
                            <div class="col-lg-12 msc msc-third">
                                <h2 class="menutitle">Fee structure per annum</h2>
                                <hr>
                                <div class="row feestrct">
                                    <div class="col-lg-2 fees">
                                        <p>Category<br> <i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Open</p></div>
                                    <div class="col-lg-2 fees">
                                        <p>Tuition Fee <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.1,75,000</p></div>
                                    <div class="col-lg-2 fees">
                                        <p>Refundable Deposit <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.20,000</p></div>
                                    <div class="col-lg-2 fees">
                                        <p>Total <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.1,95,000</p></div>
                                    <div class="col-lg-2 fees">
                                        <p>1st Installment <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.1,00,000</p></div>
                                    <div class="col-lg-2 fees"><p>2nd Installment <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.95,000</p></div>
                                    <br/><br/>
                                    <b>*Excluding caution money of Rs. 20,000/-</b>
                                </div>  	
                                <br/>
                                <h2 class="menutitle">Essential Documents for Confirmation of Admission</h2>
                                <hr>
                                <p>
                                    All admissions are provisional and are subject to approval by the Symbiosis International University. All students must ensure submission of one copy each of the following documents duly self attested before 15th September 2017 for submission by the Institute to SIU. 
                                </p>
                                <ul class="bullet-style">
                                    <li>Statement of Marks.</li>
                                    <li>Post Graduation / Graduation Certificate.</li>
                                    <li>Migration Certificate.</li>
                                    <li>Caste Certificate issued by competent authority of respective state/ Central Government, if applicable.</li>
                                </ul>
                                <p>Affidavit for change in name, if applicable.</p>
                                <p>Producing originals along with the attested copy for verification is mandatory.</p>
                                <p><strong>Note:</strong> Candidates are personally responsible to ascertain whether they possess the requisite qualifications for admission. Having been admitted provisionally does not mean acceptance of eligibility. Final eligibility for admission will be decided by Symbiosis International University. </p>
                                <h2 class="menutitle">Important Dates</h2>
                                <hr>
                                <div class="imp">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td style="padding-left: 20px;" width="55%" valign="top" align="left">Submission of application form last date</td>
                                                <td class="impdate"><i class="fa fa-arrow-right" aria-hidden="true"></i></td>
                                                <td valign="top" align="left">18/02/2017</td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 20px;" width="55%" valign="top" align="left">Personal Interview / Written Ability Test</td>
                                                <td class="impdate"><i class="fa fa-arrow-right" aria-hidden="true"></i></td>
                                                <td valign="top" align="left">25th & 26th Feb 2017</td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 20px;" width="55%" valign="top" align="left">Course Commences on</td>
                                                <td class="impdate"><i class="fa fa-arrow-right" aria-hidden="true"></i></td>
                                                <td valign="top" align="left">2nd June 2017<br></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="ZsemisterIV">
                            <div class="col-lg-12 msc msc-forth">
                                <h2 class="menutitle">The Symbiosis Centre for International Education (SCIE) </h2>
                                <hr>
                                <p>The Symbiosis Centre for International Education (SCIE) is a department of Symbiosis International University, providing leadership &amp; support for the University's efforts to internationalize the campus and the curriculum. </p>
                                <p>At Symbiosis, not only are 75 countries represented in the student community, but there is also an emphasis on internationalizing the curricula. Students and faculty are repeatedly provided with opportunities to pursue their international academic interests to the fullest, be it through academic exchanges or exposure to international conferences, seminars, workshops, etc. </p>
                                <p> Internationalization at Symbiosis works on multiple tracks, hence, the two divisions in the Symbiosis Centre for International Education include: </p>
                                <ul class="bullet-style">
                                    <li>International Students Admissions, Events and Support Services</li>
                                    <li>International Initiatives &amp; Collaborations</li>
                                </ul> 
                                <h2 class="menutitle">International Students Admissions, Events &amp; Support Services</h2>
                                <hr>
                                <p> The International Students Admissions at Symbiosis Centre for International Education is designed taking into consideration the requirements and difficulties faced by International Students. The admissions are centralized for all the constituent institutes of Symbiosis International University and routed through Symbiosis Centre for International Education (SCIE). The online admission procedure has made it accessible to each and every student from different parts of the world and in the comfort of their homes.</p>
                                <p> To check if you are an international student, please refer to the categories listed.</p>
                                <p> Categories under International Students Admissions:  </p>
                                <p><strong>Foreign National (FN)</strong></p>
                                <p>A student is eligible to apply as a Foreign National (FN) if he/she holds a foreign passport.  </p>
                                <p><b>Person of Indian Origin (PIO) </b></p>
                                <p> A student is eligible to apply as a Person of Indian Origin (PIO) if he/she has a PIO card, and is a citizen of a country other than India. </p>
                                <h2>Non Resident Indian (NRI)</h2>
                                <p><strong>(At Undergraduate Level)</strong></p>
                                <p> A student is eligible to apply as a Non Resident Indian (NRI) if he/she has appeared either for the 10 or 10+2 (equivalent to the Indian 10th &amp; 12th) examination outside India. </p>
                                <p>It is mandatory for the Students who falls in the NRI Category and 'have appeared for the 12th in India' to appear for the SET TEST. </p>
                                <p><strong>(At Postgraduate Level)</strong></p>
                                <p>A student is eligible to apply as a Non Resident Indian (NRI) if he/she has appeared either for the 10+2 examination (equivalent to the Indian 12th) or Graduation outside India. It is mandatory for one of the two qualifying examinations to be given outside India. </p>
                                <p>If you fall into any of these categories and wish to apply to Symbiosis Institute, kindly contact.</p>
                                <h2 class="menutitle">Symbiosis Centre For International Education (SCIE)</h2>
                                <hr>
                                <p><strong>Address : </strong> Symbiosis Society Senapati Bapat Road, Pune - 411004 Maharashtra, India</p>
                                <p><strong>Tel : </strong> 020 - 25671905</p>
                                <p><strong>Telefax :</strong> 020-25673854</p>
                                <p><strong>Email: </strong><a href="mailto:intadmissions@symbiosis.ac.in?Subject%20Form%20Website" style="color:#5893dd;"> intadmissions@symbiosis.ac.in</a></p>
                                <p><strong>Website :</strong><a target="_blank" style="color:#5893dd;" href="http://www.scie.ac.in/"> www.scie.ac.in</a></p>
                            </div>
                        </div>
                    </div> 
                </div>

                <span class="article_separator">&nbsp;</span>


            </div>

        </div>


        <!--    <div id="Symbiosisiu" class="tab-pane fade">
             <h3>Menu 1</h3>
             <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
           </div> -->
        <div id="symbiosisiu" class="tab-pane fade">
            <h2 class="menutitle">Certificate Course in Photogrammetry and Remote Sensing :</h2>
            <hr>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="subdivclass col-lg-3 active"><a href="#semister1" aria-controls="home" role="tab" data-toggle="tab">Eligibility</a></li>
                <li role="presentation" class="subdivclass col-lg-3"><a href="#semister2" aria-controls="profile" role="tab" data-toggle="tab">Admission procedure</a></li>
                <li role="presentation" class="subdivclass col-lg-3"><a href="#semister3" aria-controls="profile" role="tab" data-toggle="tab">Certificate Evaluation</a></li>
                <li role="presentation" class="subdivclass col-lg-3"><a href="#semister4" aria-controls="profile" role="tab" data-toggle="tab">Fee Structure</a></li>                        
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="semister1">
                    <div class="col-lg-12 msc11">
                        <p>HSC/Diploma/Graduate passed from any discipline. Final year students awaiting results may apply. Students are to certify that they are not color blind and can view 3D images using goggles.
                        </p>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="semister2">
                    <div class="col-lg-12 msc11 msc-second">
                        <p>Application form is available on the website. Students can download the application form and submit in the institute on payment of Rs 200/- in cash / DD in favor of 'The Director, Symbiosis Institute of Geoinformatics.
                        </p>
                        <p>payable at Pune at the address mentioned below<strong> The Director</strong></p>
                        <p><strong>Address : </strong> Symbiosis Institute of Geoinformatics</p>
                        <p>5th Floor Atur Centre, Gokhale cross road,</p>
                        <p>Model Colony,Pune – 411016</p>
<!--                        <p>The course commences on 10th March 2014.</p>
                        <p>The last date for the payment of fees is 3rd March 2014.</p>-->
                    </div>
                </div>  
                <div role="tabpanel" class="tab-pane" id="semister3">
                    <div class="col-lg-12 msc11 msc-third">
                        <p>Evaluation for certificate will be based on the performance of an individual in theory, practical and tests conducted at various stages. This certificated programme is approved by Symbiosis International University.</p>

                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="semister4">
                    <div class="col-lg-12 msc11 msc-forth">
                        <p>The programme fee is Rs. 25,000/-(Rupees Twenty Five Thousand Only). Rs 20,000 course fee and Rs 5,000 caution money. This fee should be paid in one single installment by DD drawn in favor of 'The Director, Symbiosis Institute of GeoInformatics' and payable at Pune, Maharashtra. The course fee is not refundable. </p>
                    </div>
                </div>
            </div> 

        </div>
        <div id="certificate" class="tab-pane fade">
            <h2 class="menutitle">Certificate Course in Spatial Economics<a href="<?php echo base_url(); ?>assets/pdf/Spatial-economics-Application_Form.pdf" target="_blank" style="float:right; margin-right: -43px;">
                    <img style="width:78%;" src="<?php echo base_url(); ?>assets/img/download-application.jpg" border="0"></a></h2>
            <hr>
            <!--            <h4 class="menutitle">  Certificate Course in Spatial Economics </h4>
                        <h4 class="menutitle">  Written by Administrator   </h4>
                        <h4 class="menutitle"> Wednesday, 16 December 2015 14:03 </h4>-->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="subdivclass col-lg-3 active"><a href="#semister11" aria-controls="home" role="tab" data-toggle="tab">Eligibility</a></li>
                <li role="presentation" class="subdivclass col-lg-3"><a href="#semister22" aria-controls="profile" role="tab" data-toggle="tab">Selection Procedure</a></li>
                <li role="presentation" class="subdivclass col-lg-3"><a href="#semister33" aria-controls="profile" role="tab" data-toggle="tab">Programme Pattern</a></li>
                <li role="presentation" class="subdivclass col-lg-3"><a href="#semister44" aria-controls="profile" role="tab" data-toggle="tab">Fee Structure</a></li>                        
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="semister11">
                    <div class="col-lg-12 msc11">
                        <p><strong>Intake:</strong> 20 Seats
                        </p>
                        <p><strong>Eligibility: </strong>12th Std. Pass</p>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="semister22">
                    <div class="col-lg-12 msc11 msc-second">
                        <p><strong>Selection Procedure:</strong> Through Personal Interview</p>
                        <p><strong>Medium of Instructions:</strong> English</p>

                    </div>
                </div>  
                <div role="tabpanel" class="tab-pane" id="semister33">
                    <div class="col-lg-12 msc11 msc-third">
                        <p>Certificate course</p>
                        <p><strong>Regestration Fee :</strong> 250/-</p>
<!--                        <p><strong>Submission of application form last date :</strong> 31 March 2016</p>
                        <p><strong>Course Commences on :</strong> Course start 4 April 2016</p>
                        <p><strong>Course end: </strong> 1 July 2016</p>
                        <p><strong>Classes Timing :</strong> 6 P.M to 8.30 P.M ( Monday to Friday )</p>-->
                        <p>The course has been designed to demonstrate the concepts for spatial decision making in the area of an economic context.</p>
                        <p>The course covers advanced topics of spatial science and its role in economic policy, with a focus on Cost Benefit Analysis (CBA), and on applications with a strong spatial dimension.</p>
                        <p>Students will develop their understanding through series of case studies that cater for urban planning and Environment Management Interest.</p>
                        <p>Students will be engaged in learning involving a range of activities</p>
                        <table border="1" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td style="width: 55px;"></td>
                                    <td style="width: 370px;"></td>
                                    <td style="width: 213px;"></td>
                                </tr>
                                <tr>
                                    <td style="width: 55px;">
                                        <p>1</p>
                                    </td>
                                    <td style="width: 370px;">
                                        <p>Hands on</p>
                                    </td>
                                    <td style="width: 213px;">
                                        <p>15 (30 Hrs Practical )</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 55px;">
                                        <p>2</p>
                                    </td>
                                    <td style="width: 370px;">
                                        <p>Grand Total (Theory+Hands on+Project)</p>
                                    </td>
                                    <td style="width: 213px;">
                                        <p>120</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table border="1" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td style="width: 55px;">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td style="width: 373px;">
                                        <p><strong>Topics</strong></p>
                                    </td>
                                    <td style="width: 210px;">
                                        <p align="center"><strong>Hours</strong></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 55px;">
                                        <p>1</p>
                                    </td>
                                    <td style="width: 373px;">
                                        <p>Micro Economics</p>
                                    </td>
                                    <td style="width: 210px;">
                                        <p align="center">8</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 55px;">
                                        <p>2</p>
                                    </td>
                                    <td style="width: 373px;">
                                        <p>Spatial Data Concept</p>
                                    </td>
                                    <td style="width: 210px;">
                                        <p align="center">8</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 55px;">
                                        <p>3</p>
                                    </td>
                                    <td style="width: 373px;">
                                        <p>Basics of Remote Sensing and GIS</p>
                                    </td>
                                    <td style="width: 210px;">
                                        <p align="center">8</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 55px;">
                                        <p>4</p>
                                    </td>
                                    <td style="width: 373px;">
                                        <p>Applications of Remote Sensing and GIS</p>
                                    </td>
                                    <td style="width: 210px;">
                                        <p align="center">6</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 55px;">
                                        <p>5</p>
                                    </td>
                                    <td style="width: 373px;">
                                        <p>Spatial  Analysis and Economics</p>
                                    </td>
                                    <td style="width: 210px;">
                                        <p align="center">8</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 55px;">
                                        <p>6</p>
                                    </td>
                                    <td style="width: 373px;">
                                        <p>Geography Determinants</p>
                                    </td>
                                    <td style="width: 210px;">
                                        <p align="center">6</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 55px;">
                                        <p>7</p>
                                    </td>
                                    <td style="width: 373px;">
                                        <p>Spatial Urban Model and economics</p>
                                    </td>
                                    <td style="width: 210px;">
                                        <p align="center">6</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 55px;">
                                        <p>8</p>
                                    </td>
                                    <td style="width: 373px;">
                                        <p>Transport</p>
                                    </td>
                                    <td style="width: 210px;">
                                        <p align="center">6</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 55px;">
                                        <p>9</p>
                                    </td>
                                    <td style="width: 373px;">
                                        <p>Ecosystem Services/Agriculture</p>
                                    </td>
                                    <td style="width: 210px;">
                                        <p align="center">8</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 55px;">
                                        <p>10</p>
                                    </td>
                                    <td style="width: 373px;">
                                        <p>GIS and Insurance</p>
                                    </td>
                                    <td style="width: 210px;">
                                        <p align="center">4</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 55px;">
                                        <p>11</p>
                                    </td>
                                    <td style="width: 373px;">
                                        <p>Location Analysis (LA) and Business Intelligence (BI)</p>
                                    </td>
                                    <td style="width: 210px;">
                                        <p align="center">7</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 55px;">
                                        <p>12</p>
                                    </td>
                                    <td style="width: 373px;">
                                        <p>Project</p>
                                    </td>
                                    <td style="width: 210px;">
                                        <p align="center">30</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 55px;">
                                        <p>&nbsp;</p>
                                    </td>
                                    <td style="width: 373px;">
                                        <p><strong>Total </strong></p>
                                    </td>
                                    <td style="width: 210px;">
                                        <p align="center">105</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p><strong>Hands on:</strong>ArcGIS Tools 12 2.1 3D Analysis Tools Analysis Tools Conversion Tools Data Management Tools 5 Multidimensional Tools Spatial Analyst Tools. Geocoding Tools. Hawth's Tools Additional Scripting Utilities.</p>
                        <p><strong> Learning Outcome: To understand and familiar with spatial theories, Spatial Analysis, tools related with economical processes in the space and will be informed about possibilities and methods of the business localization in the space</strong></p>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="semister44">
                    <div class="col-lg-12 msc11 msc-forth">

 <div class="row feestrct">
                                    <div class="col-lg-2 fees">
                                        <p>Category<br> <i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Open</p></div>
                                    <div class="col-lg-2 fees">
                                        <p>Total <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.18,000</p></div>      	
                                    <div class="col-lg-2 fees">
                                        <p>1st Installment <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.10,000</p></div>
                                    <div class="col-lg-2 fees"><p>2nd Installment <br><i class="fa fa-arrow-down" aria-hidden="true"></i><br>
                                            Rs.8000</p></div>
                                    <br/><br/>
                                </div> 

                    </div>
                </div>
            </div> 	


        </div>

        <div id="diplomaprogramme" class="tab-pane fade">
            <h2 class="menutitle">Diploma Program</h2>
            <hr>
           <p> SIG also Offer advanced certificate courses to officers,JCO,s and NCO,s form the collage of Military Engineering (CME),Pune in Geospatial Technologies.
           </p>
        </div>
    </div>

</div>    