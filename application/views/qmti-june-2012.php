<style type="text/css">
  .jg_photo{
    width: 100%;
  }
  .margintop{
    margin-bottom: 3%;
  }
</style>
<div class="row aboutop">
    <div class="gallary"></div>
</div>
<div class="container">
<div class="inner-information">

     <div class="contentheading">Gallery</div>
<hr>
<div class="gallery">
  <div class="row sectiontableentry1">
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="Khula Maunch July 2015" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/qmti_batch_june_2012_20120926_1123359390.jpg"><img src="<?php echo base_url();?>assets/img/gallary/qmti_batch_june_2012_20120926_1123359390.jpg"  class="jg_photo" alt=""/></a>
      
        </div>
      </div>   
    </div>  
    <div class="col-lg-4 col-md-4 margintop">
      <div class="col-lg-12 col-md-12">
        <div class="jg_photo_container">
        <a title="Khula Maunch July 2015" class="fancybox" href="<?php echo base_url();?>assets/img/gallary/qmti_batch_june_2012_20120926_17595867431.jpg"><img src="<?php echo base_url();?>assets/img/gallary/qmti_batch_june_2012_20120926_17595867431.jpg"  class="jg_photo" alt=""/></a>
      
        </div>
      </div>   
    </div>  
</div>
</div>     
</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.fancybox.js"></script>
 <style type="text/css" href="<?php echo base_url();?>assets/base/css/jquery.fancybox.css"></style>
 <script type="text/javascript">
   $(".fancybox")
    .attr('rel', 'gallery')
    .fancybox({
        padding : 0
    });
 </script>