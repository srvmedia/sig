<script src="<?php echo base_url(); ?>assets/plugins/jquery.min.js" defer="defer" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-migrate.min.js" defer="defer" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js" defer="defer" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/revo-slider/js/jquery.themepunch.tools.min.js" defer="defer"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" defer="defer"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" defer="defer"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/counterup/jquery.waypoints.min.js" defer="defer" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/counterup/jquery.counterup.min.js" defer="defer" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/fancybox/jquery.fancybox.pack.js" defer="defer" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/base/js/jquery.easy-ticker.js" defer="defer" type="text/javascript"></script>
<script type="text/javascript" defer="defer" src="<?php echo base_url(); ?>assets/base/js/jquery.mixitup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/base/js/components.js" defer="defer" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/base/js/app.js" defer="defer" type="text/javascript"></script>
<footer class="c-layout-footer c-layout-footer-3 c-bg-dark">
    <div class="c-prefooter">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-8">
                    <div class="c-container c-first">
                        <div class="c-content-title-1">
                            <div class="c-blog">
                                <ul class="c-links">
                                    <li>
                                        <a href="<?php echo base_url(); ?>informationasperugcnorms">Information As per
                                            UGC norms</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-4">
                    <div class="c-container">
                        <div class="c-blog">
                            <ul class="c-links">
                                <li>
                                    <a href="<?php echo base_url(); ?>disclaimer">Disclaimer</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5  col-xs-12 socialfooter">
                    <div class="c-container">
                        <div class="col-md-12">
                            <ul class="c-socials">
                                <li>
                                    <a target="_blank" href="https://www.facebook.com/sigpune">
                                        <i class="icon-social-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://twitter.com/sig_pune">
                                        <i class="icon-social-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://in.linkedin.com/in/sig-pune-753867113">
                                        <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="c-postfooter">
        <div class="container">
            <p class="c-font-oswald c-font-14 pull-left">
                Copyright &copy;2016. All Right Reserved.
            </p>
            <p class="c-font-oswald c-font-14 pull-right">
                Designed & developed by <a href="http://srmedia.com/" target="_blank">SRV Media</a>
            </p>
        </div>
    </div>
</footer>
</body>
</html>
<script>
    $(document).ready(function () {
        App.init(); // init core
        // init main slider
        var slider = $('.c-layout-revo-slider .tp-banner');
        var cont = $('.c-layout-revo-slider .tp-banner-container');
        var api = slider.show().revolution({
            delay: 15000,
            startwidth: 1170,
            startheight: (App.getViewPort().width < App.getBreakpoint('md') ? 1024 : 620),
            navigationType: "hide",
            navigationArrows: "solo",
            touchenabled: "on",
            onHoverStop: "on",
            keyboardNavigation: "off",
            navigationStyle: "circle",
            navigationHAlign: "center",
            navigationVAlign: "center",
            fullScreenAlignForce: "off",
            shadow: 0,
            fullWidth: "on",
            fullScreen: "off",
            spinner: "spinner2",
            forceFullWidth: "on",
            hideTimerBar: "on",
            hideThumbsOnMobile: "on",
            hideNavDelayOnMobile: 1500,
            hideBulletsOnMobile: "on",
            hideArrowsOnMobile: "on",
            hideThumbsUnderResolution: 0,
            videoJsPath: "rs-plugin/videojs/",
        });
        $(function () {
            $('.demo7').easyTicker({
                direction: 'up',
                visible: 2,
                interval: 8000,
                controls: {
                    up: '.btnUp',
                    down: '.btnDown',
                    toggle: '.btnToggle'
                }
            });
            $('.demo8').easyTicker({
                direction: 'up',
                visible: 2,
                interval: 8000,
                controls: {
                    up: '.btnUp',
                    down: '.btnDown',
                    toggle: '.btnToggle'
                }
            });
        });
    });
</script>
<script>
    $("#owl-example").owlCarousel({
        items: 1,
        autoPlay: true
    });
    $("#owl-example1").owlCarousel({
        items: 6,
        dots: false,
        autoPlay: true
    });
</script>
<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 938529829;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" defer="defer" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt=""
             src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/938529829/?guid=ON&amp;script=0"/>
    </div>
</noscript>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-70242154-2', 'auto');
    ga('send', 'pageview');

</script>
<!-- END: THEME SCRIPTS -->
<!-- END: LAYOUT/BASE/BOTTOM -->
       