
<style type="text/css">
    .newstable th{
        padding: 15px;
        background-color: #c42027;
        color: #fff;
        font-size: 18px;
        border-right: solid 1px #fff;
    }
    .newstable td a{
        /*color: #a10007;*/
    }
    .newstable td a:hover{
        color: #dd0000;
    }

    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }
    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    li.active a{
        display: inline;

    }
    li.active a{
        display: inline;
    }
    /* .nav>li>a {
       padding: 3px 15px;
       margin-bottom: 12px;
     }*/

    time.icon
    {
        font-size: 1em; /* change icon size */
        display: block;
        position: relative;
        width: 5em;
        height: 6em;
        background-color: #fff;
        border-radius: 0.6em;
        box-shadow: 0 1px 0 #bdbdbd, 0 2px 0 #fff, 0 3px 0 #bdbdbd, 0 4px 0 #fff, 0 5px 0 #bdbdbd, 0 0 0 1px #bdbdbd;
        overflow: hidden;
    }
    time.icon *
    {
        display: block;
        width: 100%;
        font-size: 1em;
        font-weight: bold;
        font-style: normal;
        text-align: center;
    }
    time.icon strong
    {
        position: absolute;
        top: 0;
        padding: 0.3em 0;
        color: #fff;
        background-color: #BB0C1A;
        border-bottom: 1px dashed #BB0C1A;
        box-shadow: 0 2px 0 #BB0C1A;
    }
    time.icon em
    {
        position: absolute;
        bottom: 0.3em;
        color: #BB0C1A;
    }
    time.icon span
    {
        font-size: 1.8em;
        letter-spacing: -0.05em;
        padding-top: 1.0em;
        color: #2f2f2f;
    }
    .well{
        min-height: 140px;
        padding-top: 15px;
        overflow: auto;
        margin-bottom: 0px;;
    }
    .cornerhr{
        padding-top: 10px !important;
        border-top: 1px solid #BB0C1A;
    }
    .activecolor{
        color: #dd0000;
    }
</style>
<div class="aboutop">
    <div class="cornerimg"></div>
</div>
<div class="container">
    <div class="col-md-3 menutop">
        <ul class="nav nav-pills nav-stacked rightpad">
            <li><a class="liborder" data-toggle="tab" href="#infrastructure">Infrastructure</a></li>
            <li ><a class="liborder" data-toggle="tab" href="#achievements">Achievements</a></li>
            <li class="active"><a class="liborder" data-toggle="tab" href="#news">News and Events</a></li>
            <li ><a class="liborder" data-toggle="tab" href="#orientationandpedagogy">Orientation and Pedagogy</a></li>
            <li ><a class="liborder" data-toggle="tab" href="#hostelandamenities">Hostel and Amenities</a></li>
            <li ><a class="liborder" href="<?php echo base_url(); ?>conferences">Conferences</a></li>
        </ul>
    </div>
    <div class="tab-content col-md-9 menutop">

        <div id="news" class="tab-pane fade in active">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>News</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">

                <table class="table table-striped newstable">
                    <tr>
                        <th>SNO</th><th>Title</th><th>Date</th>
                    </tr>
                    <?php
                    $i=1;
                    if(isset($all_news)) {
                        foreach ($all_news as $news) {
                            if(isset($newsid) && $newsid==$news['id']){
                                $newsclass="activecolor";
                            }else{
                                $newsclass="";
                            }
                            $time = strtotime($news['date']);
                            $date = date("d/m/y", $time);
                            echo "<tr>\n";
                            echo "<td>$i</td><td><a class='".$newsclass."' href='" . base_url() . "welcome/newsdetail/" . $news['id'] . "' target='_blank'>" . $news['title'] . "</a></td><td>" . $date . "</td>\n";
                            echo "<tr>\n\n";
                            $i++;
                        }
                    }
                    ?>
                </table>
                <span class="article_separator">&nbsp;</span>
            </div>

            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Events</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">

                <table class="table table-striped newstable">
                    <tr>
                        <th>SNO</th><th>Title</th><th>Date</th>
                    </tr>
                    <?php
                    $i=1;
                    if(isset($all_events)) {
                        foreach ($all_events as $events) {
                            $time = strtotime($events['date']);
                            $date = date("d/m/y", $time);
                            echo "<tr>\n";
                            echo "<td>$i</td><td><a href='" . base_url() . "welcome/eventsdetail/" . $events['id'] . "' target='_blank'>" . $events['title'] . "</a></td><td>" . $date . "</td>\n";
                            echo "<tr>\n\n";
                            $i++;
                        }
                    }
                    ?>
                </table>
                <span class="article_separator">&nbsp;</span>
            </div>
        </div>


        <div id="infrastructure" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Infrastructure</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <table class="contentpaneopen">
                    <tbody><tr>
                        <td valign="top">
                            Symbiosis is known to provide state of the art infrastructure to facilitate the learning of its students and SIG is no exception to this rule. From a picturesque campus that comes equipped with all the facilities needed to aid in the learning of students to innovative teaching methodologies, SIG provides its students with the best in terms of academic training for Geoinformatics. SIG has infrastructure ideally suited to conduct training, which exposes students to recent developments taking place in the corporate world of Geoinformatics.
                            <br>
                            <br>
                            Laboratory is equipped with state-of-the-art computing facilities such as:
                            <ul class="bullet-style">
                                <li>High end computers in the network environment with large monitors</li>
                                <li>Server for the centralized resources management</li>
                                <li>Internet facilities for the world-wide literature referencing
                                </li>
                                <li><strong>GIS software</strong>: Latest version of GIS software suits that are mostly preferred and         widely used in the industry </li>
                                <li><strong>Image Processing software</strong>: Leading Image Processing software suites that are         much in use in the industry</li>
                                <li> Photogrammetry software suits</li>
                                <li>Global Positioning System</li>
                                <li>Programming tools</li>
                                <!--<li>Quality management tools</li>-->
                                <li>Internet facility and Library with state-of-the-art collection of relevant books and         journals</li>
                                <!--<li>Data collection</li>-->
                                <!--<li>Remote sensing images of different satellites
                                </li>-->
                                <li>Maps from varied sources</li>
                                <!--<li>Digital data on infrastructure and natural resources at various scales</li>-->
                                <!--<li>Library has very good quality of literature available in various domains such as        GIS, Remote Sensing, Photogrammetry, Image Processing, Software development        languages, Computer and Networking to keep updated with latest development in        geospatial worldwide.</li>-->
                                <li>Medical insurance for every student</li>
                                <li>Gymnasium, sports and yoga facilities </li>
                                <li>GNSS</li>
                                <li>Medical insurance & Facility for every student</li>
                            </ul>

                        </td>
                    </tr>

                    </tbody></table>
                <span class="article_separator">&nbsp;</span>
            </div>
        </div>
        <div id="achievements" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Achievements</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <table class="contentpaneopen">

                    <tbody><tr>
                        <td valign="top">
                            At SIG, we have always encouraged are students and staff to pursue their academic interests. Our faculties and students have brought the institute and Symbiosis in general, a series of accolades. In fact, we at SIG, encourage our students and staff to volunteer for socially relevant causes as well. As part of this initiative, SIG like other Symbiosis institute in Pune takes up welfare activities for the underprivileged strata of the society in association with NGO's. From participating in national level seminars on Geoinformatics to winning laurels in events across other colleges in the country and even doing their bit for the society, SIG has an ever growing list of achievements to showcase as a result of the merits of its staff and students. <br>
                            <br>

                            <h2 style="text-align: justify;">By Institute</h2>
                            <ul style="text-align: justify;" class="bullet-style">
                                <li>SIG has bagged National Education Leadership award 2014 by Lokmat.</li>
                                <li>The report of National Task Force on Geo-Special education-MHRD, August 2013 has benchmarked the SIG programme. </li>
                                <li>SIG has been invited as an academic advisor during the formation of Indian National GIS Organisation (INGO) by the Minister of HRD, Govt. of India.</li>
                            </ul>
                            <br>

                            <h2 style="text-align: justify;">By Staff</h2>
                            <strong>Participation in FDPs and National and International Conferences</strong>
                            <ul style="text-align: justify;" class="bullet-style">
                                <li>Add the details of Dr T.P.Singh Sir visit to Dhaka. ( attended a three day meeting from 20<sup>th</sup> to 23<sup>rd</sup> April was  organized by the United Nation WMO , IWMI , GWP with Bangladesh Government (BMO).for the  project to develop an online drought monitoring system for South Asia.</li>
                                <li>Add the Dr T.P. Singh sir award details ( International Institute for Social And Economic Reforms has confirmed  for the "Sir M. Visweswariah Best Engineering National Award" . The award was given by the Hon'ble Speaker Govt. of Karnataka at Bangalore on 28<sup>th</sup> of June)</li>
                                <li>Associate Prof Dr T.P. Singh was selected and attended a FDP of one week duration on 'RS and GIS for Earth Sciences' conducted at national level by Indian Institute of Sciences, Bangalore.&nbsp;</li>
                                <li>Associate Prof Dr T.P. Singh was the only person selected from India by Japan Aerospace Exploratory Administration Centinal Asia for an international one week duration Centinal Asia Step II Programme for Disaster Management, held at ICIMOD Kathmandu, Nepal and Macao, China from 28th February to 5th March 2011 . </li>
                                <li>Dr T.P. Singh attended an international seminar of International Society of Photogrammetry and Remote Sensing conducted by IIRS at Dehradun on 7th and 8th March 2011, to represent SIG and present a paper written jointly by Dr. T.P. Singh and Brig K.K.V. Khanzode. <br>
                                    <br>
                                </li>
                            </ul>



                            <h2 style="text-align: justify;">By Students</h2>
                            <ul style="text-align: justify;" class="bullet-style">
                                <li>OrleneD'cunha and Leon Ratinam won the best technical paper award at the International Conference held at IIT-Bombay in June 2014 (Geomatrix 2014).</li>
                                <li>The paper entitled - An Innovative Approach using Crowd Management (Crowd Control), Crowd Sourcing and GIS Techniques towards a Comprehensive Disaster Management Preparedness for The Kumbh Mela 2015.</li>
                                <li>Congratulations to Ms Andrie Singh for her outstanding presentation at "Brand-e-Gram" held at Chandragupta Institute of Management,Patna.</li>
                                <li>Ms Andrie Singh, Batch 2011-13 student participated in the event "Brand-e-Gram" held at Chandragupta Institute of Management,Patna 10th November 2012.</li>

                                <li>SIG students participated in an international seminar on 'Geospatial Technology in Biodiversity Management' at IIT Kharagpur in December 2010. Our students team won the first prize for paper presentation, competing with participants from various IITs, engineering colleges and GIS institutes of repute. </li>
                                <li>SIG students also participated in Map India 2011, a renowned annual national seminar on GIS and Remote Sensing held at Hyderabad, in January 2011.</li>
                                <li>SIG student Ms Preethy Chandran's paper on 'Spatial Strategies for Sustainable Management' was adjudged the best Powertech paper in the national seminar organized by Department of Environment Management, Bharathidasan University, Tiruchirapalli in January 2011. </li>
                                <li>Ms Pallavi Srivastava, SIG student of 2006-08 batch and Shri Pratardhan Sakalkar student of 2010-12 batch have been selected for commission in the Indian Army through Officers Training Academy , Chennai. </li>
                                <li>Ms. Santh Rubini, Alumini of Batch 2009-11 is doing Research Assistantship at Oregon State University , School of Civil and Construction Engineering at US. Her research focus is on Mapping the landslides based on probabilistic techniques. She is working with the combinations of GIS and Remote sensing for analysis and LIDAR and Photogrammetric techniques for data.</li>
                            </ul>
                            <br>
                            <h2 style="text-align: justify;">Social Activities</h2>
                            <ul style="text-align: justify;" class="bullet-style">

                                <li>SIG staff and students visited an Old Age Home to help the old people accommodated there.</li>
                                <li>SIG staff and students visited an orphanage and taught them the basics of computers and geography.</li>
                                <li>Students conducted a Traffic Rules Awareness Drive and Safety on Roads campaign.</li>
                                <li>Tree Plantation in Army area.</li>
                                <li>Students visited schools for awareness programme on environment.</li>
                                <li>Faculty provide training to the differently abled army personnel.</li>



                                <li>Students, staff and faculties visited Old Age and Orphan home Ishaprema-Niketan, in Nana Peth for Social cause. </li>
                                <li>SIG staff, faculty and students undertook plantation of 800 saplings at Army Camp Aundh in August 2010. </li>
                                <li>SIG donated 1000 saplings for planting at SIU, Lavale on the occasion of FEST workshop conducted at SIG in August 2010.</li>
                                <li> SIG had organized a blood donatiojn camp at Atur Center under the aegis of SCHC in September 2010. </li>
                                <li>Students and faculty visited Queens Mary's Technical Institute for war disabled and physically challenged soldiers located at Aundh Road, Kirkee for conducting classes of IT skills. </li>
                                <li>Students and faculty members also visited Kendriya Vidyalaya located at Range Hills to conduct classes in various subjects for underprivileged students. </li>
                                <li>A short certificate course of eight weeks duration in Fundamentals of GIS and Photogrammetry has been introduced for the disabled soldiers of Queen Mary's Technical Institute, at their request to generate interest amongst the members of the institute and to generate rehabilitation opportunities for them. </li>
                            </ul>
                            <br>
                            <h2 style="text-align: justify;">SIG Student Awarded UGC Merit Scholarship!</h2>
                            Ms. Andrie Singh student of Batch 2011-13 M.Sc Geoinformatics has been awarded UGC Merit Scholarship of Rs 2000/- per month and lump sum of Rs 1, 00, 000/- on completion of her PG degree and for her outstanding performance at undergraduate Physics Honors Examination. Ms. Andrie Singh is a Gold medalist in B.Sc (Physics) from Patna University.

                        </td>
                    </tr>

                    </tbody></table>
                <span class="article_separator">&nbsp;</span>
            </div>
        </div>

        <div id="orientationandpedagogy" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Orientation and Pedagogy</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <table class="contentpaneopen">
                    <tbody><tr>
                        <td valign="top">
                            <h2 style="text-align: justify;">Orientation:</h2>
                            <p>SIG conducted a week long orientation programme which covers Micro lab, Perceptions and Attitudes, Effective and Efficient Communication, Interpersonal Relationships, Group Dynamics, Leadership, Motivation, Work life Balance, Stress Management. This programme is conducted by a professional expert in psychological counselling with 30 years of experience.
                            </p>
                            <h2 style="text-align: justify;">Pedagogy:</h2>
                            <p>Group Discussions, discussions on research papers, class seminars, presentations, student conferences, guest lectures, hands on practice project and problem based learning are a part of teaching methods in the programmes.
                                Inclusion of courses related to current practices and emerging areas in the programmes, field visits, industrial visits, participation in exhibitions and competitions which are part of curricular and co-curricular activities help in developing scientific temper in students.
                            </p>

                        </td>
                    </tr>

                    </tbody></table>
                <span class="article_separator">&nbsp;</span>
            </div>
        </div>

        <div id="hostelandamenities" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Hostel and Amenities</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <table class="contentpaneopen">



                    <tbody><tr>
                        <td valign="top">
                            <p>Limited number of seats are available on first-cum-first served basis, which are allotted on confirmation of provisional admission. Students desirous of availing hostel accommodation should apply on a plain paper while sending their first installment of fees.</p>
                            <p>The hostel rooms are spacious with natural light and ventilation. Each room has an attached toilet and
                                bathroom.</p>
                            Each student is provided with a steel cot, a study table and chair and a mattress.

                            <p><strong>Security: The following arrangements have been made to ensure the security of the inmates of the hostels: </strong></p>
                            <ul class="bullet-style" style="text-align: justify;">
                                <li>Security check at the main gate of each Hostel.</li>
                                <li>Individual security guard at each hostel building.</li>
                                <li>Lady guard at girls hostels, round the clock</li>
                                <li>Surveillance video camera at entrance of all hostel buildings.</li>
                            </ul>

                            <p><strong>Wi-Fi Internet Facilities:</strong> All hostels have secure Wi-Fi internet connectivity, through high end firewall.</p>
                            <p><strong>TV Lounge:</strong> Common TV lounge with cable connection on the ground floor of each hostel building.</p>

                        </td>
                    </tr>

                    </tbody></table>
                <span class="article_separator">&nbsp;</span>
            </div>
        </div>

        <div id="conferences" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Conferences</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <table class="contentpaneopen">
                    <tbody><tr>
                        <td valign="top">
                            SIG believes in giving its students a friendly yet motivating learning atmosphere. After all, it is through this holistic exposure across different learning platforms that SIG is able to deliver geoinformatics professionals as per industry standard. As part of its learning environment, SIG encourages its students to step out of the confines of the campus and participate in conferences and seminars on Geoinformatics across the country. Here is a brief overview of the conferences that the students from SIG have attended till date: <br>
                            <br>
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td height="44" align="left" valign="top" colspan="3"><h2 style="border-bottom: 1px solid rgb(186, 0, 0); font-size: 20px; display: inline-block;">Institute Conference</h2></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" style="padding-right:20px;"></td>
                                    <td align="left" valign="top">
                                        <div class="well">
                                            <div class="col-sm-12">
                                                <div class="col-sm-2">
                                                    <time datetime="2014-09-20" class="icon">
                                                        <em>2010</em>
                                                        <strong>August</strong>
                                                        <span>21</span>
                                                    </time>
                                                </div>
                                                <div class="col-sm-10">
                                                    <strong>Geo-Vision 2010</strong>
                                                    <hr class="cornerhr">
                                                    <p>Forest, Environment &amp; Sustainability</p>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" style="padding-right:20px;"></td>
                                    <td align="left" valign="top">
                                        <div class="well">
                                            <div class="col-sm-12">
                                                <div class="col-sm-2">
                                                    <time datetime="2014-09-20" class="icon">
                                                        <em>2012</em>
                                                        <strong>January</strong>
                                                        <span>28</span>
                                                    </time>
                                                </div>
                                                <div class="col-sm-10">
                                                    <strong>Geo-Vision 2012</strong>
                                                    <hr class="cornerhr">
                                                    <p> New Trends in GIS With Special Reference To Mobile GIS</p>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" style="padding-right:20px;"></td>
                                    <td align="left" valign="top">
                                        <div class="well">
                                            <div class="col-sm-12">
                                                <div class="col-sm-2">
                                                    <time datetime="2014-09-20" class="icon">
                                                        <em>2013</em>
                                                        <strong>August</strong>
                                                        <span>31</span>
                                                    </time>
                                                </div>
                                                <div class="col-sm-10">
                                                    <strong>Geo-Vision 2013</strong>
                                                    <hr class="cornerhr">
                                                    <p>Water Sustainability 2020</p>
                                                    <p> SIG organises 'Geovision' seminar each year in collaboration with
                                                        Indian Society of Geomatics (ISG) and Centre of Advance
                                                        Computing(CDAC).</p>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" style="padding-right:20px;"></td>
                                    <td align="left" valign="top">
                                        <div class="well">
                                            <div class="col-sm-12">
                                                <div class="col-sm-2">
                                                    <time datetime="2014-09-20" class="icon">
                                                        <em>2016</em>
                                                        <strong>June</strong>
                                                        <span>18</span>
                                                    </time>
                                                </div>
                                                <div class="col-sm-10">
                                                    <strong>Geo-Vision 2016</strong>
                                                    <hr class="cornerhr">
                                                    <p>18th June, 2016 , ISG, IWRS and SIG conducted a Workshop on "Smart Cities And Geo-Ict Initiative - Geovision"</p>
                                                    <p>Dr Anand Deshpande, CEO Persistent , Dr. Rajat Moona , Director General (C-DAC) , &amp; Dr . B Mukhopadhyay Add Director General IMD Govt of India Have Inaugurated the Seminar </p>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" style="padding-right:20px;"></td>
                                    <td align="left" valign="top">
                                        <div class="well">
                                            <div class="col-sm-12">
                                                <div class="col-sm-2">
                                                    <time datetime="2014-09-20" class="icon">
                                                        <em>2011</em>
                                                        <strong>Feb</strong>
                                                        <span>21-26</span>
                                                    </time>
                                                </div>
                                                <div class="col-sm-10">
                                                    <strong>Symbiosis Institute of Geoinformations</strong>
                                                    <hr class="cornerhr">
                                                    <p>SIG organised five day workshop on 'Remote Sensing and GIS
                                                        applications' for university teachers across India. The workshop
                                                        was funded by the Department of Science and Technology, Govt.
                                                        of India.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="44" align="left" valign="top" colspan="3"></td>
                                </tr>
                                <tr>
                                    <td height="44" align="left" valign="top" colspan="3"><h2 style="border-bottom: 1px solid rgb(186, 0, 0); font-size: 20px; display: inline-block;">Conference Attend by Students </h2></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" style="padding-right:20px;"></td>
                                    <td align="left" valign="top">
                                        <div class="well">
                                            <div class="col-sm-12">
                                                <div class="col-sm-2">
                                                    <time datetime="2014-09-20" class="icon">
                                                        <em>2012</em>
                                                        <strong>February</strong>
                                                        <span>7-9</span>
                                                    </time>
                                                </div>
                                                <div class="col-sm-10">
                                                    <strong>Privacy Issues in LBS Applications</strong>
                                                    <hr class="cornerhr">
                                                    <table border="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" width="35%" valign="top"><strong>Conference</strong></td>
                                                            <td align="left" width="3%" valign="top">:</td>
                                                            <td align="left" valign="top"> India Geospatial Forum'12, Gurgaon</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top"><strong>Name of the participant/s</strong></td>
                                                            <td align="left" valign="top">:</td>
                                                            <td align="left" valign="top">Akriti Kumar, Amrit Kumar, Andrie Singh </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" style="padding-right:20px;"></td>
                                    <td align="left" valign="top">
                                        <div class="well">
                                            <div class="col-sm-12">
                                                <div class="col-sm-2">
                                                    <time datetime="2014-09-20" class="icon">
                                                        <em>2011</em>
                                                        <strong>February</strong>
                                                        <span>25</span>
                                                    </time>
                                                </div>
                                                <div class="col-sm-10">
                                                    <strong>Caddie: A golf course application using GPS in Android Devices</strong>
                                                    <hr class="cornerhr">
                                                    <table border="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" width="35%" valign="top"><strong>Conference</strong></td>
                                                            <td align="left" width="3%" valign="top">:</td>
                                                            <td align="left" valign="top"> Geomatrix '11, IIT Bombay</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top"><strong>Name of the participant/s</strong></td>
                                                            <td align="left" valign="top">:</td>
                                                            <td align="left" valign="top">Dipti Sarkar and Varun Anand </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" style="padding-right:20px;"></td>
                                    <td align="left" valign="top">
                                        <div class="well">
                                            <div class="col-sm-12">
                                                <div class="col-sm-2">
                                                    <time datetime="2014-09-20" class="icon">
                                                        <em>2011</em>
                                                        <strong>February</strong>
                                                        <span>25</span>
                                                    </time>
                                                </div>
                                                <div class="col-sm-10">
                                                    <strong>Crime Information System (GIS based crime information system)</strong>
                                                    <hr class="cornerhr">
                                                    <table border="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" width="35%" valign="top"><strong>Conference</strong></td>
                                                            <td align="left" width="3%" valign="top">:</td>
                                                            <td align="left" valign="top"> Geomatrix 2011, I.I.T Bombay</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top"><strong>Name of the participant/s</strong></td>
                                                            <td align="left" valign="top">:</td>
                                                            <td align="left" valign="top">Nilesh Irwadkar,Shaily Gandhi, Vijay Pawar, Manlee Chowdhury </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" style="padding-right:20px;"></td>
                                    <td align="left" valign="top">
                                        <div class="well">
                                            <div class="col-sm-12">
                                                <div class="col-sm-2">
                                                    <time datetime="2014-09-20" class="icon">
                                                        <em>2010</em>
                                                        <strong>February</strong>
                                                        <span>26-27</span>
                                                    </time>
                                                </div>
                                                <div class="col-sm-10">
                                                    <strong>Urban Change detection with the help of Cellular Automata</strong>
                                                    <hr class="cornerhr">
                                                    <p>This project examines the use of GIS and Remote Sensing in mapping Land Use Land Cover in Pune between 1999 and 2005 so as to detect the changes that has taken place in this status between these periods.It  is also use  to produce a land use land cover map of Pune at different epochs in order to detect the changes that have taken place particularly in the built-up land and subsequently predict likely changes that might take place in the same over a given period.</p>
                                                    <table border="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" width="35%" valign="top"><strong>Conference</strong></td>
                                                            <td align="left" width="3%" valign="top">:</td>
                                                            <td align="left" valign="top"> Geomatrix' at IIT- Mumbai</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top"><strong>Name of the participant/s</strong></td>
                                                            <td align="left" valign="top">:</td>
                                                            <td align="left" valign="top">Anupama Joshi,Nisha Devar</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" style="padding-right:20px;"></td>
                                    <td align="left" valign="top">
                                        <div class="well">
                                            <div class="col-sm-12">
                                                <div class="col-sm-2">
                                                    <time datetime="2014-09-20" class="icon">
                                                        <em>2010</em>
                                                        <strong>February</strong>
                                                        <span>26-27</span>
                                                    </time>
                                                </div>
                                                <div class="col-sm-10">
                                                    <strong>Ecotourism with the help of Remote Sensing and GIS</strong>
                                                    <hr class="cornerhr">
                                                    <p>This study is an attempt to identify potential ecotourism sites in eastern part of pune district using Remote Sensing and GIS techniques in Maharastra. After identifying the potential sites, a demonstrative plan has been made for Ecotourism development based on locally available natural resources.</p>
                                                    <table border="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" width="35%" valign="top"><strong>Conference</strong></td>
                                                            <td align="left" width="3%" valign="top">:</td>
                                                            <td align="left" valign="top"> Geomatrix' at IIT- Mumbai</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top"><strong>Name of the participant/s</strong></td>
                                                            <td align="left" valign="top">:</td>
                                                            <td align="left" valign="top">Anupama Joshi</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" style="padding-right:20px;"></td>
                                    <td align="left" valign="top">
                                        <div class="well">
                                            <div class="col-sm-12">
                                                <div class="col-sm-2">
                                                    <time datetime="2014-09-20" class="icon">
                                                        <em>2010</em>
                                                        <strong>December</strong>
                                                        <span>20-22</span>
                                                    </time>
                                                </div>
                                                <div class="col-sm-10">
                                                    <strong>Site Suitability for cultivation of Biofuel in Pune</strong>
                                                    <hr class="cornerhr">
                                                    <p>We  used Remote sensing and GIS to locate and  find suitable sites for cultivation of Jatropha and Pangamia trees and suitable weather conditions for their growth in the Pune District .This study helped us in collection of the Jatropha &amp; Pongamia seeds that can be processed into biofuels.</p>
                                                    <table border="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" width="35%" valign="top"><strong>Conference</strong></td>
                                                            <td align="left" width="3%" valign="top">:</td>
                                                            <td align="left" valign="top"> Geomatrix' at IIT- Mumbai</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top"><strong>Name of the participant/s</strong></td>
                                                            <td align="left" valign="top">:</td>
                                                            <td align="left" valign="top">Anupama Joshi,Sarat Bokka</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="37" align="left" valign="top" colspan="3">&nbsp;</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <span class="article_separator">&nbsp;</span>
            </div>
        </div>
    </div>
</div>

</div>    
