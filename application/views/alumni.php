<style>

    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    li.active a{
        display: inline;

    }


.infholder { width:240px;  float: left;margin: 10px 5px 0 20px;  border-bottom: 1px solid #cccccc; }
.infholder p {font-size:13px; text-align: left;}
.saperator { clear: both; margin: 10px 0; overflow: hidden;}

.alumnitable {
margin-left: 20px;
}

.alumnitable td, th {
    padding: 10px !important;
}
.alumnitable th {
background-color: #a00007;
    color: #fff;
}
</style>
<div class="row aboutop">
    <div class="alumni"></div>
</div>
<div class="container">
    <div class="col-md-3 menutop">  
        <ul class="nav nav-pills nav-stacked rightpad">
            <li class="active"><a class="liborder" data-toggle="tab" href="#Alumni">Alumni</a></li>
            <li><a class="liborder" data-toggle="tab" href="#Achievements">Achievements</a></li>
            <li><a class="liborder" data-toggle="tab" href="#globalfootprint">SIG alumni global footprint</a></li>
            <li><a class="liborder" data-toggle="tab" href="#convocation">13th convocation ceremony</a></li>
        </ul>
    </div>
    <div class="tab-content col-md-9 menutop">
        <div id="Alumni" class="tab-pane fade in active">
            <div class="inner-information">

                <table class="contentpaneopen">
                    <tbody>
                        <tr>
                    <h2 class="menutitle">Alumni
                    </h2>	
                    </tr>
                    </tbody></table>
                <hr>
                <div class="row">
                    <p>As one of the country's best colleges for Geoinformatics, in it's less-than-a-decade old existence, SIG has managed to establish a successful alumni base across different sectors. Majority of our alumni are working with multi-national companies and have had opportunities to showcase their professional talents abroad. We are proud that our alumni have brought us laurels by their hard work, achievements and sincerity, which has been acknowledged by their respective companies with achievement awards. SIG strives to maintain this glorious track-record even in the future.</p>
<!--<a href="<?php echo base_url();?>assets/img/pdf/roll-of-honour.pdf" target="_blank"><img src="<?php echo base_url();?>assets/img/alumni.jpg" alt=""></a>-->
                </div>
            </div>
        </div>
        
        <div id="Achievements" class="tab-pane fade in">
            <div class="inner-information">

                <table class="contentpaneopen">
                    <tbody>
                        <tr>
                    <h2 class="menutitle">Achievements
                    </h2>	
                    </tr>
                    </tbody></table>
                <hr>
                <div class="row">
                
                    <ul class="bullet-style">
<li>"Prashant Deshmukh 2008-10 Batch, RMSI Pvt Ltd, Hyderabad working as GIS Engg: "Winner of Cult 2012 Presentation Competion (RMSI Hyderabad) RMSI ways award for Best behaviour within the team.</li>
<li>SIG student Ms Santha Rubini has been selected as a Research Scholar by the Oragon State University,USA. </li>
<li>Ms. Santh Rubini, Alumini of <b>Batch 2009-11</b> is  doing Research Assistantship at Oregon State University , School of Civil and Construction Engineering at US. Her research focus is on Mapping the landslides based on probabilistic techniques. She is working with the combinations of GIS and  Remote sensing for analysis and LIDAR and Photogrammetric techniques for data.</li>
<li><b>Batch 2009-11</b> alumini lets another feature in the cap of SIG!</li>
<li>Mr Ashish Arora won the first prize in paper presentation at the ESRI Asia-Pacific User's Conference at Delhi. </li>
<li>Ms Chaitali Chandani was awarded a special commendation prize by Magnasoft, Bangalore for her outstanding contribution in the company project. </li>
<li><b>2005-2007 Batch</b> Ms. Vijayalaxmi Patil  was awarded <b>'The Employee of the Month - February 2007'</b>  award by Cybertech,Mumbai , while she was doing her six months project. </li>
<li>Mr. Ram Kumar Singh has joined National Informatics Cell of Govt. of India for employment with them. <b>Batch 2004-06</b></li>
<li>Ms. Rajarshi Chaudhari has been selected as Research Scholar by Pune University. <b>Batch 2004-06</b></li>
<li>Mr. Samee Azami has been selected as Research Scholar by IIT, Mumbai <b>Batch 2004-06</b></li>
<li>Ms. Tanvi Garg was awarded the 'Best GIS Trainee of the Year' by Reliance Infocomm, Navi Mumbai.  <b>Batch 2004-06</b></li>
<li>Mr Gaurav Jobanputra (Cybertech, Mumbai), Mr Appaiah (RMSI, Delhi), Mr Mahesh Ghare  and Mr Susheem Kampani ( Fugro, Navi Mumbai) were chosen by their respective companies to go abroad in the very first year of their service with the companies. <b>Batch 2004-06</b></li>
<li>Ms Baljit Kaur Sathi and Mr Mr Pankaj Chaudhari of <b>2004-2006 batch</b> were  honoured with the <b>'Excellence Award'</b> and <b>'Ace of the Year'</b> awards by  ESRI (India), New Delhi. </li>
</ul>
                </div>
            </div>
        </div>
        
        <div id="globalfootprint" class="tab-pane fade in">
            <div class="inner-information">

                <table class="contentpaneopen">
                    <tbody>
                        <tr>
                    <h2 class="menutitle">SIG alumni global footprint
                    </h2>	
                    </tr>
                    </tbody></table>
                <hr>
                <div class="row">
                    <div class="saperator">    
<table class="table table-striped alumnitable">
    
<tr>
<th>Name</th><th>Description</th><th>Location</th>
</tr>

<tr>
    <td><strong>Mukesh Vyas, GISP </strong></td>
    <td>Senior Geospatial Consultant at AAM </td>
    <td><strong>Sydney, Australia</strong> </td>
    </tr>
    
    <tr>
    <td><strong>Bhakti Rane </strong></td>
    <td>Cloud Solutions Consultant at Reckon</td>
    <td><strong>Sydney, Australia</strong></td>
    </tr>       

    <tr>
    <td><strong>Komal Agarwal</strong> </td>
    <td>GIS Consultant, Infosys </td>
    <td><strong>Concord, California </strong></td>
    </tr>  

    <tr>
    <td><strong>Dipto Sarkar</strong> </td>
    <td>PHD Scholar at McGill University </td>
    <td><strong>Canada</strong></td>
    </tr>
    
    <tr>
    <td><strong>Phani Attili</strong> </td>
    <td>Product Owner , Dell </td>
    <td><strong>Round Rock, Texas</strong></td>
    </tr>
    
    <tr>
    <td><strong>Chinmay Shaligram</strong></td>
    <td>Web Technologist, One.com</td>
    <td><strong>United Arab Emirates</strong></td>
    </tr>     
    
    <tr>
    <td><strong>Khyati Rajendra</strong> </td>
    <td>GIS / Data Specialist at GISTEC </td>
    <td><strong>United Arab Emirates</strong></td>
    </tr>
    
    <tr>
    <td><strong>Pavan Nallan</strong></td>
    <td>Tech. Consultant at Autodesk</td>
    <td><strong>Houston, Texas</strong></td>
    </tr>    

<tr>
<td><strong>Roger Doctor</strong></td>
<td>GIS Developer, Agreeya Solutions</td>
<td><strong>Warren, New Jersey</strong></td></tr>

<tr>
<td><strong>Vijay Pawar</strong></td>
<td>Product Engineer, Esri R&amp;D Center</td>
<td><strong>United Arab Emirates</strong></td></tr>

<tr>
<td><strong>Jeetendra Kolte</strong></td>
<td>Works at Fugro</td>
<td><strong>Lives in Muscat, Oman</strong></td></tr>

<tr>
<td><strong>Shivam Gupta</strong></td>
<td>Worked at Hexagon Geospatial</td>
<td><strong>Lives in Münster</strong></td></tr>

    <tr>
    <td><strong>Vandana Tomar</strong> </td>
    <td>Research Assistant at CUSP, Curtin <br> University </td>
    <td><strong>Perth, Australia</strong>  </td>
    </tr>

    <tr>
    <td><strong>Akshay mohgaonkar</strong></td>
    <td>Senior Application Developer at IBM India Pvt. Ltd</td>
    <td><strong>USA</strong></td>
    </tr> 

    <tr>
    <td><strong>Preethi Chandran</strong> </td>
    <td>Technology and Business professional,<br> Yardi </td>
    <td><strong>Toronto, Canada Area</strong></td>
    </tr>

<tr>
<td><strong>Ujjwal Nigam</strong></td>
<td>Technical Team Lead (GIS)</td>
<td><strong>Perth, Western Australia,<br> Australia</strong></td>
</tr>

<tr>
<td><strong>Suraj Pai</strong></td>
<td>Lead Consultant at IBM Australia </td>
<td><strong> Lives in Darwin, Northern Territory</strong></td></tr>

<tr>
<td><strong>Sahal Basharat </strong></td>
<td>GIS Developer at WSSC Water</td>
<td><strong>Lives in Silver Spring, Maryland</strong></td></tr>

<tr>
<td><strong>Apurv Danke</strong></td>
<td>Works at Wipro</td>
<td><strong>Lives in Birmingham, United Kingdom</strong></td></tr>



<tr>
<td><strong>Suhas Patil </strong></td>
<td>Works at Fugro</td>
<td><strong>Lives in Abu Dhabi, United Arab  Emirates</strong></td></tr>

<tr>
<td><strong>Dharmendra Shukla</strong></td>
<td>Data Processor at Fugro</td>
<td><strong>Lives in Abu Dhabi, United Arab <br> Emirates</strong></td></tr>

    <tr>
    <td><strong>Pushkar Inamdar</strong></td>
    <td>Doctoral student at Mississippi State University</td>
    <td><strong>Starkville, Mississippi</strong></td>
    </tr> 

    <tr>
    <td><strong>Varun Anand</strong> </td>
    <td>Programmer Analyst | Microsoft C#, SQL Server, MVC, Techlore,  </td>
    <td><strong>Sydney, Australia</strong></td>
    </tr> 

<tr>
<td><strong> Gurminder Bharani</strong></td>
<td> Internship at International Water Management Institute (IWMI)</td>
<td><strong> Lives in  Sri Lanka</strong></td></tr>

<tr>
<td><strong> Gurminder Bharani</strong></td>
<td> Internship at International Water Management Institute (IWMI)</td>
<td><strong> Lives in  Sri Lanka</strong></td></tr>

    <tr>
    <td><strong>Bhargavi Rathi </strong></td>
    <td>Digital Marketing Strategist l Marketing l Business Development, Go4Fresh</td>
    <td><strong>Singapore</strong></td>
    </tr>
    
    <tr>  
    <td><strong> Ashish Kumar Gupta</strong></td>
    <td>GIS Analyst III - DB Administrator at Saudi Electricity Company</td>
    <td><strong>Saudi Arabia</strong></td></tr>    
</tr>
    <tr>
    <td><strong>Abhijeet Kale</strong> </td>
    <td>Research Analyst and Business Development<br/>Dy Manager at Geospatial Media and Communications</td>
    <td><strong>United Arab Emirates</strong></td>
    </tr>
    
    <tr>
    <td><strong>Pranita Ranbhise</strong></td>
    <td>Graduate Student at University Of Maryland<br/>National Center for Smart Growth Research and Education</td>
    <td><strong>Hyattsville, Maryland <br> &nbsp;</strong></td>
    </tr>     


</table>
        
    
 </div>
                </div>
            </div>
        </div>
        
        <div id="convocation" class="tab-pane fade in">
            <div class="inner-information">

                <table class="contentpaneopen">
                    <tbody>
                        <tr>
                    <h2 class="menutitle">13th convocation ceremony of batch 2014 - 2016
                    </h2>	
                    </tr>
                    </tbody></table>
                <hr>
                <div class="row">
                   <ul class="bullet-style">
<li>The 13<sup>th</sup> convocation ceremony of batch 2014 - 2016 was held on 1st October 2016.Trophies were awarded at the hands of the Director Sir to the following students.</li>
<li>Mr. Devarshi Tambe won the trophy for standing first in the class.</li>
<li>Ms. Abhilasha Belkhode won the trophy for standing second in the class.</li>
<li>Mr. Devarshi Tambe and Ms. Abhilasha Belkhode won the trophy for the best project.</li>
<!--<li>The function was followed by a small cultural evening followed by high tea.</li>-->
</ul>
                </div>
            </div>
        </div>
    </div>
</div>