<style type="text/css">
.geoinformatics {
    width: 100%;
    height: 320px;
    background: linear-gradient(0deg, rgba(0, 0, 0,0.4), rgba(0, 0, 0,0.4)), url("<?php echo base_url();?>assets/img/acadamics/geo.jpg") no-repeat;
    background-size: cover;
}
/*  bhoechie tab */
div.bhoechie-tab-container{
  z-index: 10;
  background-color: transparent;
  padding: 0 !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  border:1px solid #ddd;
  margin-top: 20px;
  margin-left: 50px;
  -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  box-shadow: 0 6px 12px rgba(0,0,0,.175);
  -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  background-clip: padding-box;
  opacity: 0.97;
  filter: alpha(opacity=97);
}
div.bhoechie-tab-menu{
  padding-right: 0;
  padding-left: 0;
  padding-bottom: 0;
}
div.bhoechie-tab-menu div.list-group{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a .glyphicon,
div.bhoechie-tab-menu div.list-group>a .fa {
  color: #b42c2f;
}
div.bhoechie-tab-menu div.list-group>a:first-child{
  border-top-right-radius: 0;
  -moz-border-top-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a:last-child{
  border-bottom-right-radius: 0;
  -moz-border-bottom-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a.active,
div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
div.bhoechie-tab-menu div.list-group>a.active .fa{
  background-color: #b22629;
  background-image: #5A55A3;
  color: #ffffff;
}
div.bhoechie-tab-menu div.list-group>a.active:after{
  content: '';
  position: absolute;
  left: 100%;
  top: 50%;
  margin-top: -13px;
  border-left: 0;
  border-bottom: 13px solid transparent;
  border-top: 13px solid transparent;
  border-left: 10px solid #b42c2f;
}
.subdivclass {
    background-color: #d0cece;
    color: #b02d29;
    border-right: 2px solid #fff;
} 
li.subdivclass{
  padding-left: 0px;
  padding-right: 0px;
}   
div.bhoechie-tab-content{
  background-color: #ffffff;
  /* border: 1px solid #eeeeee; */
  padding-left: 20px;
 /* padding-top: 10px;*/
}
div.bhoechie-tab div.bhoechie-tab-content:not(.active){
  display: none;
}
.containermar{
  margin:10% 0 8% 0; 
}
 .rightpad{
      border-right: 2px solid #f19999;
    }
    .rightpad li.active{
      background: url("<?php echo base_url();?>assets/img/liback.png") no-repeat;
      background-position: center right;
      padding: 10px 0px 10px;
    }
    .menutop li.active a{
      display: inline;
    }
    .menutop.nav>li>a {
      padding: 3px 15px;
      margin-bottom: 12px;
    }
.sub-menu{
    text-align: center;
    font-size: 20px;
    font-weight: 600;
    color: #c42027;
}
</style>
<div class="aboutop">
    <div class="geoinformatics"></div>
</div> 
<div class="container">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 menutop list-group bhoechie-tab-menu">
               <ul class="nav nav-pills nav-stacked rightpad">
                  <li class="active"><a class="liborder" data-toggle="tab" href="#Overview">M.Sc. Geoinformatics</a></li>
                  <li><a class="liborder" data-toggle="tab" href="#coursestruct">Course Structure</a></li>
                  <li><a class="liborder" data-toggle="tab" href="#careerportal">Career Potential</a></li>
                  <li><a class="liborder" data-toggle="tab" href="#Workshops">Workshops</a></li>
                   <li class="sub-menu">Programme Offered by SIG</li>
                    <li><a class="liborder" href="<?php echo base_url(); ?>MTechGeoinformatics">M.Tech. Geoinformatics & ST</a></li>
                    <li><a class="liborder" href="<?php echo base_url(); ?>environment">M.Sc. Environment & Sustainability</a></li>
                    <li><a class="liborder" href="<?php echo base_url(); ?>certificatecourse">Certificate Course in Photogrammetry and Remote Sensing</a></li>
                    <li><a class="liborder" href="<?php echo base_url(); ?>courseinSpatial">Certificate Course in Spatial Economics</a></li>
                </ul>
              </div>           
              <div class="tab-content col-lg-9 col-md-9 col-sm-12 col-xs-12 bhoechie-tab">
                <!-- flight section -->
                <div id="Overview" class="tab-pane fade in active">
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                        <h2 class="menutitle">M.Sc. Geoinformatics
                        <a href="/admissions/msc-geoinformatics.html" style="float:right;margin-top:-3px;"><img src="<?php echo base_url();?>assets/img/admissionbtn.jpg" title="M.Sc. Geoinformatics" alt="M.Sc. Geoinformatics"></a></h2>
                        <hr>
                        <p>M.Sc.(Geoinformatics) is of 100 credits, divided suitably into 4 semesters which also includes two projects. The degree is conferred under the aegis of Symbiosis International University.</p> 
                        <!--<p><h4 class="colordiv">"GEOINFORMATICS "Map your Career to make Digital India!</h4></p>-->
                        <h2 class="menutitle">Introduction</h2>
                        <p>Earth science increasingly rely on digital data acquired from satellite or aerial images analyzed by Geographical Information Systems (GIS) and visualized.</p>
                        <p>
Geoinformatics is science which has Remote Sensing, Geographic Information System (GIS), Global Navigation Sattelite System with Information Technology. Many fields benefit from geoinformatics, including urban planning and land use management, navigation systems, virtual globes, public health, environmental modeling and analysis, military, transport network planning and management, agriculture, meteorology and climate change, oceanography and atmosphere modeling, business location planning, architecture and archeological reconstruction, telecommunications, criminology and crime simulation,Business management, aviation and maritime transport. Geoinformatics becomes very important technology to decision-makers across the globe with wide range of disciplines. Many government and non government agencies using spatial data for managing their day to day activities.
</p>
                        <p>
Geoinformatics is a specialised field necessitating expert knowledge.Students from Engineering , geography, geology, agriculture, environment, forestry engineering, IT or computer science fields can get admission to M.Sc. (Geoinformatics)
</p>
                       
                         <div class="clearfix"></div>
                        </div>
                      </div>
                </div>
                <!-- train section -->
                <div  id="coursestruct" class="tab-pane fade bhoechie-tab-content">
                      <h2 class="menutitle">M.Sc. Geoinformatics Course Structure :</h2>
                      <hr>
                        <p>Details of M.Sc. Geoinformatics course curriculum are given below</p>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="subdivclass col-lg-3 active"><a href="#semisterI" aria-controls="home" role="tab" data-toggle="tab">Semester - I</a></li>
                            <li role="presentation" class="subdivclass col-lg-3"><a href="#semisterII" aria-controls="profile" role="tab" data-toggle="tab">Semester - II</a></li>
                            <li role="presentation" class="subdivclass col-lg-3"><a href="#semisterIII" aria-controls="profile" role="tab" data-toggle="tab">Semester - III</a></li>
                            <li role="presentation" class="subdivclass col-lg-3"><a href="#semisterIV" aria-controls="profile" role="tab" data-toggle="tab">Semester - IV</a></li>                        
                        </ul>
                       <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="semisterI">
                        <div class="col-lg-12 msc">
                            <ul class="bullet-style">
                            <div class="col-lg-6 acamargin">
                            <li>Business Communication Skills</li>
                            <li>Organizational Behaviour</li>
                            <li>Principles of GIS</li>
                            <li>Principles of Remote Sensing</li>
                            <li>Fundamentals of Mapping</li>
                            <li>Programming Language - I</li>
                            </div>
                            <div class="col-lg-6 acamargin">
                            <li>Natural Resources</li>
                            <li>Applied Statistics</li>
                            <li>Urban Planning and Rural Development</li>
                            <li>Disaster Scenario Mapping</li>
                            <li>Computer Fundamental and Cyber security</li>
                            <li>Integrated Disaster Management</li>
                            </div>
                            </ul>
                        </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="semisterII">
                        <div class="col-lg-12 msc msc-second">
                            <ul class="bullet-style">
                             <div class="col-lg-6 acamargin">
                            <li>Advanced Remote Sensing</li>
                            <li>Digital Image processing</li>
                            <li>Digital  Image Classification</li>
                            <li>Global Navigational Support System</li>
                            <li>Photogrammetry</li>
                            <li><b>Elective :</b> Programming Language II</li>
                            </div>
                            <div class="col-lg-6 acamargin">
                            <li><b>Elective :</b> Programming Language III</li>
                            <li>Principles of database Management System</li>
                            <li><b>GIS Application Development</b></li>
                            <li>Essentials of Internet and Web Technologies</li>
                            <li>Spatial Analysis</li>
                            </div>
                            </ul>
                        </div>
                      </div>  
                        <div class="clearfix"></div>
                        <div role="tabpanel" class="tab-pane" id="semisterIII">
                        <div class="col-lg-12 msc msc-third">
                            <ul class="bullet-style">
                             <div class="col-lg-6 acamargin">
                            <li>Summer Project</li>
                            <li>GIS Application Design</li>
                            <li>Remote Sensing Applications</li>
                            <li>Digital Photogrammetry</li>
                            <li>Software Testing Methodologies</li>
                            <li>Spatial Modeling</li>
                            <li>GIS Project Management</li>
                            </div>
                            <div class="col-lg-6 acamargin">
                            <li><b>Electives: Mobile GIS</b></li>
                            <li><b>Electives:</b> Health GIS</li>
                            <li><b>Electives:</b> Natural Resource Management</li>
                            <li><b>Electives:</b> Facility and Utility Management</li>
                            <li>Elective: Lidar and Radar Technology</li>
                            <li>Enterprise GIS</li>
                            <li>Quality Concepts</li>
                            </div>
                            </ul>
                        </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="semisterIV">
                        <div class="col-lg-12 msc msc-forth">
                          <ul class="bullet-style acamargin">
                          <li>Industry Project.</li>
                          </ul>
                        </div>
                        </div>
                    </div> 
                </div>
    
                <!-- hotel search -->
                <div id="careerportal" class="tab-pane fade bhoechie-tab-content">
                       <h2 class="menutitle">Career potential</h2>
                       <hr>
                        <p>The geoinformatics field is in its nascent stage and expanding at a rapid pace as more and more industries are employing spatial data to manage their activities. The dream of the digital India , Smart cities can not be completed without the use of Geoinformatics technology. Almost no developmental project is complete without geospatial information and it will soon become imperative. The only limit is the acute shortage of skilled professionals. Therefore, there is tremendous scope with accelerated growth and development prospects. The pervasive use in diverse disciplines ensures a wide variety of employment opportunities. The Government of India is taking numerous initiatives under Department of Space, Department of Science & Technology (Natural Resources Data Management System - NRDMS) and others. Government/has also established National Centre of Geoinformatics to serve central & state govt dept across the country  . Likewise many others initiatives are in process."</p>
                        <p>The industry is rapidly adopting these technologies for efficient delivery of products or processes. Companies such as Reliance Industries, Reliance Jio, Lavasa Corp, L&T, Suzlon are using geospatial technology for infrastructure project management. Many IT companies are handling overseas projects based on Geospatial technologies, these include, Infosys, TCS, HCL,Acenture,Tech Mahindra,Google, iGate etc. In addition to these industry giants several midsize companies such as ESRI Inc., Intergraph, Nokia,Tom Tom,Webonise Lab operates on specialized geospatial products. Many companies are in Insurance sector like WILLIS RE,SWISS RE are using Geospatial technology in big way. These companies offer variety of opportunities as GIS analyst, GIS managers, Business development executives, GIS software developers etc.</p>
                     <h2 class="menutitle">Job prospects</h2>
                        <p>In Govt Sector Central and State government departments are using geoinformatics application. Central government agencies are under the Department of Space - National Remote Sensing Centre (NRSC Hyderabad), North East Space Application Centre, (NESAC Shillong), Regional Remote Sensing Centres (RRSC Kharagpur, Dehradun, Jodhpur, Nagpur and Bangalore), Indian Space Research Organisation, (ISRO Bangalore), Advanced Data Processing Research Institute (ADRIN Hyderabad) and Space Application Centre (SAC Ahmedabad), Defense Research and Development organization (DRDO), Department of Science and technology, Ministry of Environment , Ministry of Agriculture forest and Climate Change, Ministery of Earth Science,Geological Survey of India, Indian Agriculture Research Institute (IARI) and Indian Council of Agriculture Research (ICAR) Apart of these many public sector unit have Geospatial Departments</p>                       
                </div>
                <div id="Workshops" class="tab-pane fade bhoechie-tab-content">
                      <h2 class="menutitle">Workshops Conducted On</h2>
                      <hr>
                        <div class="workshop">
                            <div class="col-lg-6">
                            <ul class="bullet-style">
                            <li>Communication Skills</li>
                            <li>Soft Skills</li>
                            <li>Team Building</li>
                            <li>Leadership Skills</li>
                            </ul>
                            </div>

                            <div class="col-lg-6">
                            <ul class="bullet-style">
                            <li>Confidence Building</li>
                            <li>Decision Making</li>
                            <li>Personality Development</li>
                            </ul>
                            </div>
                            <div class="clearfix"></div>
                             <p>Duration: The<strong> M.Sc. Geoinformatics</strong> program is a 2 year full time course.<br></p>
                        </div>
                </div>
            </div>
        </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
</script>