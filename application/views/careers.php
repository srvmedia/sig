		

<style type="text/css">


    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    .rightpad li.active a{
        display: inline;
        line-height:2;
    }
    li.active a{
        display: inline;

    }

    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }    
    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        padding: 10px 0px 10px;
    }
    li.active a{
        display: inline;
    }
    /* .nav>li>a {
       padding: 3px 15px;
       margin-bottom: 12px;
     }*/
</style>
<div class="row aboutop">
    <div class="bannerimg"></div>
</div>
<div class="container">
    <div class="col-md-3 menutop">
        <ul class="nav nav-pills nav-stacked rightpad">
            <li class="active"><a class="liborder" data-toggle="tab" href="#introduction">Introduction</a></li>
            <li><a class="liborder" data-toggle="tab" href="#educationbackground">Educational Background</a></li>
            <li><a class="liborder" data-toggle="tab" href="#jobprospect">Job Prospects</a></li>
            <li><a class="liborder" data-toggle="tab" href="#careerportal">Career Potential</a></li>
            <li><a class="liborder" data-toggle="tab" href="#careeropportunities">Career Opportunities in Geoinformatics</a></li>
            <li><a class="liborder" data-toggle="tab" href="#variousposition">Various Positions Available in Geoinformatics</a></li>
        </ul>
    </div>
    <div class="tab-content col-md-9 menutop">
        <div id="introduction" class="tab-pane fade in active">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Introduction</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <p>Geography and earth science increasingly rely on digital geographical data acquired from satellite or aerial images analyzed by Geographical Information Systems (GIS) and visualized on paper or the computer screen. Before this we remember the traces we used to make at school over a blank map of India. Different traces were used to display different information like 'wheat' or 'rice' growing areas of India etc.</p>
                <p>Now with the computer age this whole process has become 'computerized' and instead of the paper copy of map of India we have satellite or aerial photographs and the 'traces' we used to make has been replaced by the computer (along with the help of a suitable software) which digitizes (traces) the objects we are interested in.</p>
                <p>Geoinformatics is 'Geography' with Information Technology. Many fields benefit from geoinformatics, including urban planning and land use management, in-car navigation systems, virtual globes, public health, environmental modeling and analysis, military, transport network planning and management, agriculture, meteorology and climate change, oceanography and coupled ocean and atmosphere modelling, business location planning, architecture and archeological reconstruction, telecommunications, criminology and crime simulation, aviation and maritime transport. Geoinformatics becomes very important technology to decision-makers across a wide range of disciplines, industries, commercial sector, environmental agencies, local and national government, research, and academia, national survey and mapping organisations, International organisations, United Nations, emergency services, public health and epidemiology, crime mapping, transportation and infrastructure, information technology industries, GIS consulting firms, environmental management agencies, tourist industry, utility companies, market analysis and e-commerce, mineral exploration, etc. Many government and non government agencies started to use spatial data for managing their day to day activities.</p>
                <p>A <strong>Geographic Information System (GIS)</strong> is a system designed to capture, store, manipulate, analyze, manage, and present all types of geographical data. The word GIS is sometimes used for <strong>geographical information science or geospatial information studies</strong> to refer to the academic discipline or career of working with geographic information systems and is a large domain within the broader academic discipline of Geoinformatics.</p>
                <span class="article_separator">&nbsp;</span>
            </div>
        </div>

        <div id="educationbackground" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Educational Background</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <p>Geoinformatics is a specialised field necessitating expert knowledge. Background in science stream is a must. Students from geography, geology, agriculture, engineering, IT or computer science fields can gain admission to M.Sc., M.Tech. courses in Geoinformatics and Remote Sensing. There is the option to follow up with Ph.D. programmes. Shorter diploma, certificate and M.A. courses are available as well.</p>
                <span class="article_separator">&nbsp;</span>
            </div>
        </div>

        <div id="jobprospect" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Job Prospects</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <p>Major users of geoinformatics applications and therefore employers are the Central and State governments. Central government agencies are under the Department of Space - National Remote Sensing Agency (NRSA Hyderabad), North East Space Application Centre, (NESAC Shillong), Regional Remote Sensing Application Centre (RRSAC Kharagpur, Dehradun, Jodhpur, Nagpur and Bangalore), Indian Space Research Organisation, (ISRO Bangalore), Advanced Data Processing Research Institute (ADRIN Hyderabad) and Space Application Centre (SAC Ahmedabad) apart from Indian Agriculture Research Institute (IARI) and Indian Council of Agriculture Research (ICAR). State governments offer jobs in their individual.</p>
                <span class="article_separator">&nbsp;</span>
            </div>
        </div>

        <div id="careerportal" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Career Potential</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <p>The geoinformatics field is in its nascent stage and expanding at a rapid pace as more and more industries are employing spatial data to manage their activities. Almost no developmental project is complete without geospatial information and it will soon become imperative. The only limit is the acute shortage of skilled professionals.Therefore, there is tremendous scope with accelerated growth and development prospects. The pervasive use in diverse disciplines ensures a wide variety of employment opportunities. The Government of India is taking numerous initiatives under Department of Space, Department of Science & Technology (Natural Resources Data Management System - NRDMS) and others. Government/Planning commission is also setting up an Indian National GIS Organisation (INGO). Likewise many others initiatives are in process."</p>
                <p>The industry is rapidly adopting these technologies for efficient deliver of products or processes. Companies such as Relience Industries, L&T, Suzlon are using geospatial technology for infrastructure project management. Many IT companies are handling overseas projects based on Geospatikal technologies, these include, Infosys, TCS, HCl, iGate etc. In addition to these industry giants several midsize companies such as ESRI Inc, Intergraph, Nokia, operates on specialized geospatial products. These companies offer variety of opportunities as GIS analyst, GIS managers , Business development executives, GIS software developers etc.</p>
                <p>Geoinformatics sees applications across different sectors. As a result of this interlinked growth, the career opportunities in Geoinformatics are always positive. In fact, as we progress towards the idea of sustainable development, MSC colleges like SIG are all set to play a pivotal role in being able to provide a talent pool for the Geoinformatics domain. The Geoinformatics industry offers umpteen opportunities in every sector and discipline. US department of labour has declared, the Geospatial Industry has been declared as a high growth, high demand industry by the US Department of Labour. The picture would be rosy in India too. The need for Geoinformatics services is on the rise by 10-15% per annum. More than 150 companies are on constant look out for GIS professionals. However, the demand-supply gap is wide, due to the lack of awareness about Geoinformatics.</p>
                <span class="article_separator">&nbsp;</span>
            </div>
        </div>

        <div id="careeropportunities" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Career Opportunities in Geoinformatics</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <table align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        <td class="top-sh"><table align="center" width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody><tr>
                                    <td align="left" valign="top" class="bottom-sh" style="padding:30px 10px;">
                                        <table width="594" cellspacing="0" cellpadding="5" border="0" style="margin-left:25px;" class="info-section-a">
                                            <tbody>
                                            <tr>
                                                <td align="left" valign="top" class="sem">Sector</td>
                                                <td align="left" valign="top" width="13%">:</td>
                                                <td align="left" valign="top" class="sem">Job Description</td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="43%" valign="top">IT</td>
                                                <td align="left" valign="top">:</td>
                                                <td align="left" valign="top">  GIS programmer, GIS developer   </td>
                                            </tr>
                                            <tr>
                                                <td align="left" width="43%" valign="top">Environment</td>
                                                <td align="left" valign="top">:</td>
                                                <td align="left" valign="top">  Environmental Planning &amp; Management and EIA  </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">Government (State and Central)</td>
                                                <td align="left" valign="top">:</td>
                                                <td align="left" valign="top">Coordinator, Scientist, Research Associate, GIS Executive etc. </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">Geology</td>
                                                <td align="left" valign="top">:</td>
                                                <td align="left" valign="top">Geological and Geomorphologic Mapping; Terrain and Cadastral Mapping, Mining </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">Disaster Management</td>
                                                <td align="left" valign="top">:</td>
                                                <td align="left" valign="top">Assessment and Relief Measures</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">Agriculture</td>
                                                <td align="left" valign="top">:</td>
                                                <td align="left" valign="top">Crop inventory, Detecting Crop Stress, Prediction</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">Military</td>
                                                <td align="left" valign="top">:</td>
                                                <td align="left" valign="top">Schedule and fight wars and Coping up with War After-Effects</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">Urban</td>
                                                <td align="left" valign="top">:</td>
                                                <td align="left" valign="top">Town Mapping, Monitoring Surface Water and Targeting Ground Water</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">Educational Institute</td>
                                                <td align="left" valign="top">:</td>
                                                <td align="left" valign="top">Lecturer, Reader, Scientific Officer, Professor</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">Organisations like ISRO and NASA USGS</td>
                                                <td align="left" valign="top">:</td>
                                                <td align="left" valign="top">Scientist, engineers in Natural Resource management, Disaster Management, Climate Change, forecasting and prediction<br> </td>
                                            </tr>
                                            </tbody></table>
                                    </td>
                                </tr>
                                </tbody></table></td>
                    </tr>
                    </tbody></table>
            </div>
        </div>

        <div id="variousposition" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Various Positions Available in Geoinformatics</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <ul class="bullet-style">
                    <li> Project Manager –M.Sc in Geoinformatics  / M.Tech Geoinformatics and Surveying Technology</li>
                    <li>  GIS Engineer - M.Sc in Geoinformatics / B.Tech in Geoinformatics </li>
                    <li>Senior GIS Engineer - M.Tech Geoinformatics and Surveying Technology</li>
                    <li>Scientist - M.Sc in Geoinformatics /  M.Tech in Geoinformatics</li>
                    <li>GIS Analysts / consultant - M.Sc / M.Tech Geoinformatics and Surveying Technology / Remote Sensing / PGD in Geoinformatics</li>
                    <li>GIS Business Consultant / Development - M.Sc in Geoinformatics</li>
                    <li>GIS Programmer - M.Sc in Geoinformatics /  M.Tech Geoinformatics and Surveying Technology</li>
                    <li>GIS Executive – M.Sc in Geoinformatics</li>
                </ul>
            </div>
        </div>

        </div>
</div>

</div>    
