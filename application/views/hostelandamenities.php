		
		
		<style type="text/css">
    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }    
    .rightpad{
      border-right: 2px solid #f19999;
    }
    .rightpad li.active{
      padding: 10px 0px 10px;
    }
    li.active a{
      display: inline;
    }
   /* .nav>li>a {
      padding: 3px 15px;
      margin-bottom: 12px;
    }*/
   </style>
<div class="row aboutop">
    <div class="bannerimg"></div>
</div>	
<div class="container">  
<div class="col-md-3 ">
 <ul class="nav nav-pills nav-stacked rightpad">
    <li><a class="liborder" href="<?php echo base_url(); ?>collaborations">Collaborations</a></li>
    <li><a class="liborder" href="<?php echo base_url(); ?>testimonials" >Testimonials</a></li>
    <li><a class="liborder" href="<?php echo base_url(); ?>careers">Careers in SIG</a></li>
	<li><a class="liborder">Resource Center
    <span class="caret"></span></a>
    <ul>
      <li><a href="<?php echo base_url(); ?>faqs">FAQ's</a></li>
      <li ><a href="<?php echo base_url(); ?>scientificpapers">Scietific Papers</a></li>
    </ul>
	
  </li>
  <li class="active"><a class="liborder">SIG Corner
    <span class="caret"></span></a>
    <ul style="margin-top:10px;">
      <li><a href="<?php echo base_url(); ?>infrastructure">Infrastructure</a></li>
      <li ><a href="<?php echo base_url(); ?>achievements">Achievements</a></li>
	  <li ><a href="<?php echo base_url(); ?>orientationandpedagogy">Orientation and Pedagogy</a></li>
	  <li ><a href="<?php echo base_url(); ?>hostelandamenities">Hostel and Amenities</a></li>
	  <li ><a href="<?php echo base_url(); ?>conferences">Conferences</a></li>
    </ul>
	
  </li>
    <li><a class="liborder" href="http://www.siu.edu.in/resources.php" target="_blank">Health Facilities</a></li>  
  </ul>
</div>
  <div class="col-md-9 menutop">
   
      <h2 class="menutitle"><strong>Hostel and Amenities</strong></h2>
      <hr>
    
	
	
	
	<table class="contentpaneopen">



<tbody><tr>
<td valign="top">
<p>Limited number of seats are available on first-cum-first served basis, which are allotted on confirmation of provisional admission. Students desirous of availing hostel accommodation should apply on a plain paper while sending their first installment of fees.</p>
<p>The hostel rooms are spacious with natural light and ventilation. Each room has an attached toilet and 
bathroom.</p>
Each student is provided with a steel cot, a study table and chair and a mattress.

<p><strong>Security: The following arrangements have been made to ensure the security of the inmates of the hostels: </strong></p>
<ul class="bullet-style" style="text-align: justify;">
 	<li>Security check at the main gate of each Hostel.</li>
 	<li>Individual security guard at each hostel building.</li>
 	<li>Lady guard at girls hostels, round the clock</li>
 	<li>Surveillance video camera at entrance of all hostel buildings.</li>
</ul>

<p><strong>Wi-Fi Internet Facilities:</strong> All hostels have secure Wi-Fi internet connectivity, through high end firewall.</p>
<p><strong>TV Lounge:</strong> Common TV lounge with cable connection on the ground floor of each hostel building.</p>

</td>
</tr>

</tbody></table>
    
  </div>
</div>

</div>    
	