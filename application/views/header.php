<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Symbiosis Institute of Geoinformatics</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->

<script src="<?php echo base_url();?>assets/plugins/jquery.min.js" type="text/javascript"></script>
<link href='https://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url();?>assets/plugins/socicon/socicon.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/plugins/bootstrap-social/bootstrap-social.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/plugins/animate/animate.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN: BASE PLUGINS  -->
<link href="<?php echo base_url();?>assets/plugins/revo-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/plugins/owl-carousel/owl.transitions.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<!-- END: BASE PLUGINS -->
<!-- BEGIN THEME STYLES -->
<link href="<?php echo base_url();?>assets/base/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/base/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/base/css/themes/default.css" rel="stylesheet" id="style_theme" type="text/css"/>
<link href="<?php echo base_url();?>assets/base/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="<?php echo base_url();?>assets/favicon.ico"/>
</head>
<body class="c-layout-header-fixed">
<!-- BEGIN: LAYOUT/HEADERS/HEADER-1 -->
<!-- BEGIN: HEADER -->
<header class="c-layout-header c-layout-header-4 c-layout-header-default-mobile" style="border-bottom: 1px solid #c42027;">
<div class="c-navbar">
	<div class="container">
		<!-- BEGIN: BRAND -->
		<div class="c-navbar-wrapper clearfix">
			<div class="c-brand c-pull-left">
				<a href="<?php echo base_url();?>" class="c-logo">
				<img src="<?php echo base_url();?>assets/img/logo.png" alt="symbi" class="c-desktop-logo">
				<img src="<?php echo base_url();?>assets/img/logo.png" alt="symbi" class="c-desktop-logo-inverse">
				<img src="<?php echo base_url();?>assets/img/logo.png" alt="symbi" class="c-mobile-logo" style="width: 100%;">
				</a>
				<button class="c-hor-nav-toggler" type="button" data-target=".c-mega-menu">
				<span class="c-line"></span>
				<span class="c-line"></span>
				<span class="c-line"></span>
				</button>
				<button class="c-search-toggler" type="button">
				<i class="fa fa-search"></i>
				</button>
			</div>
			<!-- END: BRAND -->
			<!-- BEGIN: QUICK SEARCH -->
			<form class="c-quick-search" action="#">
				<input type="text" name="query" placeholder="Type to search..." value="" class="form-control" autocomplete="off">
				<span class="c-theme-link">&times;</span>
			</form>
			<!-- END: QUICK SEARCH -->
			<!-- BEGIN: HOR NAV -->
			<!-- BEGIN: LAYOUT/HEADERS/MEGA-MENU -->
			<!-- BEGIN: MEGA MENU -->
			<a class="btn btn-xs admbtn" href="<?php base_url();?>admissionopenforbatch201719" target="_blank">Admission Open for Batch 2017-19</a>
			<nav class="c-mega-menu c-pull-right c-mega-menu-dark c-mega-menu-dark-mobile c-fonts-uppercase c-fonts-bold" >
			<!-- BEGIN: MEGA MENU -->

				<ul class="nav navbar-nav c-theme-nav">
				<li class="c-menu-type-classic">
					<a href="<?php echo base_url();?>" class="c-link">Home</a>
				</li>
				<li class="c-menu-type-classic">
					<a href="<?php echo base_url();?>aboutsig" class="c-link">About US</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>academics" class="c-link">Academics</a>
				</li>
				<li class="c-menu-type-classic">
					<a href="<?php echo base_url();?>admission" class="c-link">Admission</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>faculties" class="c-link">Faculties & Research </a>
				</li>
                                <li>
					<a href="<?php echo base_url();?>sigcorner" class="c-link">SIG Corner</a>
				</li>
				<li class="c-quick-sidebar-toggler-wrapper">

					<a href="#" class="c-quick-sidebar-toggler">
					<span class="c-line"></span>
					</a>
				</li>
			</ul>
			<!-- END MEGA MENU -->
			</nav>

		</div>
	</div>
</div>
</header>

<nav class="c-layout-quick-sidebar">
<div class="c-header">
	<button type="button" class="c-link c-close">
	<i class="icon-login"></i>
	</button>
</div>
<div class="c-content slidemenu">

	<div class="c-section">
		<h3><a href="https://siu.ishinfo.com/SIG/Register/" target="_blank"><i class="fa fa-caret-right"></i>&nbsp;&nbsp;Apply online</a></h3>
	</div>
	<div class="c-section">
		<h3><a  href="<?php echo base_url();?>placement"><i class="fa fa-caret-right"></i>&nbsp;&nbsp;Placement</a></h3>
	</div>
	<div class="c-section">
		<h3><a  href="<?php echo base_url();?>alumni"><i class="fa fa-caret-right"></i>&nbsp;&nbsp;Alumni</a></h3>
	</div>
	<div class="c-section">
		<h3><a  href="<?php echo base_url();?>gallery"><i class="fa fa-caret-right"></i>&nbsp;&nbsp;Gallery</a></h3>
	</div>
	<div class="c-section">
		<h3><a  href="<?php echo base_url();?>collaborations"><i class="fa fa-caret-right"></i>&nbsp;&nbsp;Collaboration</a></h3>
	</div>
		<div class="c-section">
		<h3><a  href="<?php echo base_url();?>testimonials"><i class="fa fa-caret-right"></i>&nbsp;&nbsp;Testimonials</a></h3>
	</div>
	<div class="c-section">
		<h3><a  href="<?php echo base_url();?>newsevents"><i class="fa fa-caret-right"></i>&nbsp;&nbsp;News And Event</a></h3>
	</div>
	<div class="c-section">
		<h3><a  href="<?php echo base_url();?>conferences"><i class="fa fa-caret-right"></i>&nbsp;&nbsp;Conferences</a></h3>
	</div>
		<div class="c-section">
		<h3><a  href="<?php echo base_url();?>careers"><i class="fa fa-caret-right"></i>&nbsp;&nbsp;Careers in GIS</a></h3>
	</div>
<div class="c-section">
		<h3><a  href="<?php echo base_url();?>project"><i class="fa fa-caret-right"></i>&nbsp;&nbsp;Project</a></h3>
	</div>
		<div class="c-section">
		<h3><a  href="<?php echo base_url();?>faqs"><i class="fa fa-caret-right"></i>&nbsp;&nbsp;FAQs</a></h3>
	</div>
	<div class="c-section">
		<h3><a  href="<?php echo base_url();?>contact"><i class="fa fa-caret-right"></i>&nbsp;&nbsp;Contact Us</a></h3>
	</div>

</div>
</nav>
<script type="text/javascript">
       $(document).ready(function(){
 		  var str=location.href.toLowerCase();
        $('nav li a').each(function() {
                if (str.indexOf(this.href.toLowerCase()) > -1) {
						$("li.c-active").removeClass("c-active");
                        $(this).parent().addClass("c-active");
                   }
              	 							});
	/*	$('nav li .c-active').parents().each(function(){

					if ($(this).is('li')){

						$(this).addClass("c-active");
						}
												  });*/
	   })

    </script>
<!-- END: LAYOUT/SIDEBARS/QUICK-SIDEBAR -->
<!-- BEGIN: PAGE CONTAINER -->
	<!-- BEGIN: PAGE CONTENT -->
	<!-- BEGIN: LAYOUT/SLIDERS/REVO-SLIDER-4 -->
