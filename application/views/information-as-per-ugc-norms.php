<style type="text/css">
    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }
    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    .rightpad li.active a{
        display: inline;
        line-height:2;

    }
    td, th {
        padding: 1px;
        padding-left: 1%;
    }
    li.subdivclass{
        padding-left: 0px;
        padding-right: 0px;
    }
    .service-icon i.fa{
        font-size: 35px;
        color: red;
    }
    .feestrct p{
        min-height: 94px;
    }
    .feestrct i.fa {
        font-size: 22px;
        color: #ee7f1a;
    }
    .fees{
        border:solid 1px gray;
        max-height: 102px !important;
    }
    .fees p{
        text-align: center !important;
    }
    .impdate i.fa{
        font-size: 22px;
        color: #ee7f1a;
    }
    .service-figure {
        position: relative;
        border: 1px solid #999;
        -webkit-box-shadow: 0px 0px 11px 0px rgba(50, 50, 50, 0.46);
        -moz-box-shadow: 0px 0px 11px 0px rgba(50, 50, 50, 0.46);
        box-shadow: 0px 0px 11px 0px rgba(50, 50, 50, 0.46);
    }
    .service-figure:hover .service-caption, .employee-desination, .hover-overlay, .product-name:hover, .product-figure:hover .product-caption, .gallery-hover, .flicker-img li a::before, .map-overlay, .overlay-pink::before, .scrollup:hover, .video-titel .play-btn:hover {
        background: rgba( 244,86,97,0.7);
    }
    .service-figure:hover .service-icon i {
        color: #fff!important;
        border-color: #fff;
    }

    .service-figure:hover h4 {
        color: #fff!important;
        border-color: #fff;
    }
    .service-icon {
        height: 82px;
        width: 82px;
        line-height: 105px;
        text-align: center;
        border: 2px solid #999;
        display: inline-block;
        border-radius: 100%;
        margin: 0 0 20px;
    }
    h3 {
        font-size: 26px;
        margin: 0 0 10px;
        font-weight: bold;
        text-transform: capitalize;
    }
    .service-figure:hover .service-caption h3, .service-figure:hover .service-caption p, .service-figure:hover .service-caption .read-more {
        color: #fff;
    }
    .msc11{
        margin-top: 3%;
        min-height: 257px;
    }
    .item1{
        position: absolute;
        top:0px;
        background: #fff;
    }
    .service-caption {
        text-align: center;
        padding: 23px;
        background: rgba( 255,255,255,1.0);
        min-height: 216px;
        min-width: 231px;
    }
    .subdivclass {
        background-color: #d0cece;
        color: #b02d29;
        border-right: 2px solid #fff;
    }
    .item1 a:active, .item1 > a:hover{
        color: #ffffff;
    }
    .service-figure:hover .service-icon{
        border: 2px solid #fff;
    }
    img{
        width: 100%;
    }

    .sub-menu{
        text-align: center;
        font-size: 20px;
        font-weight: 600;
        color: #c42027;
    }
    .service-figure:hover{
        background-color:#ffffff;
        box-shadow: 5px 5px 20px;
        border: solid 1px #e6c9ca;
        -webkit-transform: scale(1.1);
        -moz-transform: scale(1.1);
        -ms-transform: scale(1.1);
        -o-transform: scale(1.1);
        transform: scale(1.1);
    }
    .well{
        padding: 10px;
        overflow: auto;
        box-shadow: 2px 2px 10px;
        -moz-box-shadow: 2px 2px 10px;
        -webkit-box-shadow: 2px 2px 10px;
        margin-bottom: 22px;
        background-color: rgba(245, 245, 245, 0);
    }
    /*.well:hover{*/
        /*color: #b0221e;*/
    /*}*/
    table tr{
        border-bottom: 1px solid #ddd;
    }
    table{
        font-size: 16px;
    }
</style>
<div class="aboutop">
    <div class="admission"></div>
</div>
<div class="container">
    <div class="col-md-3 menutop">
        <ul class="nav nav-pills nav-stacked rightpad">
            <li class="active"><a class="liborder" data-toggle="tab" href="#sectiona">Section A</a></li>
            <li><a class="liborder" data-toggle="tab" href="#sectionb">Section B</a></li>
            <li><a class="liborder" data-toggle="tab" href="#sectionc">Section C</a></li>
            <li><a class="liborder" data-toggle="tab" href="#sectiond">Section D</a></li>
            <li><a class="liborder" data-toggle="tab" href="#sectione">Section E</a></li>
            <li><a class="liborder" data-toggle="tab" href="#sectionf">Section F</a></li>
            <li><a class="liborder" data-toggle="tab" href="#sectiong">Section G</a></li>
            <li><a class="liborder" data-toggle="tab" href="#sectionh">Section H</a></li>
            <li><a class="liborder" data-toggle="tab" href="#sectioni">Section I</a></li>
        </ul>
    </div>
    <div class="tab-content col-md-9 menutop">
        <div id="sectiona" class="tab-pane fade in active">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle">Section A - General Information</h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tbody>
                        <tr>
                            <td align="left" valign="top" width="48%"><strong>Name of the Institution with campuses with full address</strong></td>
                            <td align="left" valign="top" width="4%">:</td>
                            <td align="left" valign="top">Symbiosis Institute of Geoinformatics 5th and 6th Floor, Atur Centre, Gokhale Cross Road, Model Colony, Pune – 411 016</td>
                        </tr>
                        <tr>
                            <td align="left" valign="top"><strong>Date and number of notification declaring the institutes as 'Deemed to be University'</strong></td>
                            <td align="left" valign="top">:</td>
                            <td align="left" valign="top">Established under section 3 of the UGC Act 1956 vide notification no. F.9-12/2001-U.3 of the Government of India</td>
                        </tr>
                        <tr>
                            <td align="left" valign="top"><strong>Number of Institution within ambit / constituent units / off-campuses</strong></td>
                            <td align="left" valign="top">:</td>
                            <td align="left" valign="top"></td>
                        </tr>
                        <tr>
                            <td align="left" valign="top"><strong> Name and contact details</strong></td>
                            <td align="left" valign="top">:</td>
                            <td align="left" valign="top"></td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                </div>

                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="48%"><strong>Appointment</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Chancellor</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Name</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Padma Bhushan Dr. S.B. Mujumdar</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Address</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Symbiosis Society , Senapati Bapat Road, Pune – 411 004</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Telephone No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">020-25673531/33/42</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Fax No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">020-256592092</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Email ID</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top"><a href="mailto:chancellor@siu.edu.in?Subject=Enquiry From Website">chancellor@siu.edu.in</a></td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Duration of Tenure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Since inception of SIU on 29th December 2003</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="48%"><strong>Appointment</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Vice Chancellor</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Name</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Dr. Rajani R.Gupte</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Address</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Symbiosis International University Lavale, Pune - 411 042</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Telephone No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">020-39116221 / 24 / 25</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Fax No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">020-29116206</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Email ID</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top"><a href="mailto:vc@siu.edu.in?Subject=Enquiry From Website">vc@siu.edu.in</a></td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Duration of Tenure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">October 2012 onwards</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="48%"><strong>Appointment</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Director</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Name</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Dr T.P.Singh</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Address</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Symbiosis Institute of Geoinformatics 5th and 6th Floor, Atur Centre, Gokhale Cross Road, Model Colony, Pune – 411 016.</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Telephone No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">020-25672842/ 7709998185</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Fax No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">020-25672842</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Email ID</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top"><a href="mailto:director@sig.ac.in?Subject=Enquiry From Website">director@sig.ac.in</a></td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Duration of Tenure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">23rd Feb 2015 onwards</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="48%"><strong>Appointment</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Registrar</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Name</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Dr S.C.Nerkar</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Address</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Symbiosis International University Lavale, Pune - 411 042.</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Telephone No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">020-39116221 / 24 / 25</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Fax No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">020-29116206</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Email ID</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top"><a href="mailto:registrar@siu.edu.in?Subject=Enquiry From Website">registrar@siu.edu.in</a></td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Duration of Tenure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">24th June 2013</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="48%"><strong>Appointment</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Finance Officer</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Name</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top"></td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Address</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Symbiosis International University Lavale, Pune - 411 042</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Telephone No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">020-39116221 / 24 / 25</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Fax No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">020-29116206</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Email ID</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top"><a href="mailto:fo@siu.edu.in?Subject=Enquiry From Website">fo@siu.edu.in</a></td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Duration of Tenure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="48%"><strong>Appointment</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Controller of Examination</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Name</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Mrs Shraddha Chitale</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Address</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Symbiosis International University Lavale, Pune - 411 042</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Telephone No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">020-39116221 / 24 / 25</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Fax No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">020-29116206</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Email ID</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top"><a href="mailto:coe@siu.edu.in?Subject=Enquiry From Website">coe@siu.edu.in</a></td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Duration of Tenure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="48%"><strong>Name of the Institute</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Symbiosis Institute of Geoinformatics</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Address</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Symbiosis Institute of Geoinformatics 5th and 6th Floor, Atur Centre, Gokhale Cross Road, Model Colony, Pune – 411 016.</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Name of the Head of the Institute</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Dr T.P.Singh</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Telephone No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">020-25672841 / 43 / 7709998185</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Fax No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">020-25672842</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Email ID</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top"><a href="mailto:director@sig.ac.in?Subject=Enquiry From Website">director@sig.ac.in</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                <span class="article_separator">&nbsp;</span>
            </div>

        </div>
        </div>

            <div id="sectionb" class="tab-pane fade">
                <table class="contentpaneopen">
                    <tbody><tr>
                        <h2 class="menutitle">Section B - Details of Infrastructure</h2>
                    </tr>
                    </tbody></table>
                <hr>
                <div class="inner-information">
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Total area in acres</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">	1.07 acres</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Built up area</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">9500 sq. ft.</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Office Space</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Administration Block</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">864.91 sq.ft.</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Academic Block</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">5888.72 sq .ft</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Other blocks such as</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Library</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">616 sq ft</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Students Facilities</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">Canteen & common room, parking</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Lecturer Theaters</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">2</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">No of Class Rooms</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">4 No.</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Stock in Library</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">747</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Journals</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">2</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">E Journals</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">350</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Periodicals</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">4</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Laboratory details</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">Laboratory fitted with air conditioners
                                        <br>
                                        <p><strong>List Of Hardware </strong></p>
                                        <ul>
                                            <li>Photogrammetry Suite Hardware</li>
                                            <li>1 IBM Server</li>
                                            <li>1 HP i PAQ Mobile Media Companion</li>
                                            <li>2 i finder Lawrence H 20 GPS</li>
                                            <li>2 i finder Hunt GPS</li>
                                            <li>1 Mobile phone with GPS</li>
                                            <li>Pocket stereoscope and mirror</li>
                                            <li>stereoscope</li>
                                            <li>Digital planimeter</li>
                                            <li>Prismatic Compass</li>
                                            <li>2 Garmin GNSS GPS</li>
                                            <li>PDA</li>
                                            </ul><br>
                                        <p><strong>List Of Software</strong></p>
                                        <ul>
                                            <li>This Details of ERDAS Imagine professional, ERDAS Imagine Vector, ERDAS Imagine Virtual GIS</li>
                                            <li>This Details of Leica Photogrammetry Solution. (addition to the above said modules)</li>
                                            <li>10 licenses of Geomedia Professional</li>
                                            <li>3 licenses of Geomedia Webmap Professional</li>
                                            <li>5 licenses of Geomedia Public Works Managers</li>
                                            <li>2 licenses of Autodesk Map 3D</li>
                                            <li>1 license of Arc GIS Server</li>
                                            <li>Windows are licenses copy</li>
                                            <li>Windows Ms office are licenses copy</li>
                                            <li>Windows Ms Project</li>
                                            <li>Windows Server licenses copy</li>
                                            <li>Licenses of Arc GIS 10.3</li>
                                            <li>Desktop Basic - 10</li>
                                            <li>Desktop Standard -10</li>
                                        </ul>
                                    </td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Seminar / Conference Hall</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">Apart from two lecture theatres in the institute a number of auditorium and halls are also available in other institutes of SIU for conducting seminar and conference</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Sports facilities</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">Sports facilities for indoor games available in campus.Outdoor Sports competitions are organized under arrangements of SIU</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Recreation facilities</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">SIG is located in the heart of Pune city</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Cafeteria</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">A cafeteria within campus has been established. In addition, there are number of cafeterias around the campus</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Cafeteria</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">A cafeteria within campus has been established. In addition, there are number of cafeterias around the campus</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Details of Transport Facilities</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">Due to central location in the heart of city, public transport is easily available</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-sm-12">
                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tbody>
                                <tr>
                                    <td align="left" valign="top" width="30%"><strong>General Infrastructure</strong></td>
                                    <td align="left" valign="top" width="4%">:</td>
                                    <td align="left" valign="top">Guest House</td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Details</strong></td>
                                    <td align="left" valign="top">:</td>
                                    <td align="left" valign="top">Guest houses are available under the Symbiosis Society</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
    </div>

        <div id="sectionc" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle">Section C- Information on faculties</h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Name of the Faculty</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Dr. Tarun Pratap Singh</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Designation</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Professor</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Name of the Faculty</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Mrs. Ashwini Mohgaonkar</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Designation</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Associate Professor</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Name of the Faculty</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Dr. Navendu Chaudhary</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Designation</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Associate Professor</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Name of the Faculty</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Lt Col B K Pradhan</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Designation</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Assistant Professor</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Name of the Faculty</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Dr. Sandipan Das</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Designation</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Assistant Professor</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Name of the Faculty</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Mr. Alok Mohagaonkar</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Designation</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Adjunct Faculty</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Name of the Faculty</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Mr. Anubandh Humbarde</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Designation</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Adjunct Faculty</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Name of the Faculty</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Ms. Niyatee Deshmukh</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Designation</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Teaching Assistant</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Name of the Faculty</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Ms. Pritanka Sandbhor</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Designation</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Tutor</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Name of the Faculty</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Ms. Ketki Metha</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Designation</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Tutor</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>

        <div id="sectiond" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle">Section D- Information on infrastructure prescribed by concern statutory authority per block of intake vis-a-vis available infrastructure.</h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <p>As per laid down norms of the UGC, the following infrastructure facilities are available in SIG.</p>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Statutory Authority</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">University Grants Commission</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Prescribed Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Furniture with fixture, equipment and good laboratory</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Available Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Yes</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Statutory Authority</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">University Grants Commission</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Prescribed Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Sports facilities</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Available Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Yes</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Statutory Authority</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">University Grants Commission</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Prescribed Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Augmentation of infrastructural facilities from time to time optimal utilization of infrastructure</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Available Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Yes</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Statutory Authority</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">University Grants Commission</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Prescribed Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Additional facilities for Gym, swimming pool, auditorium etc.</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Available Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Yes</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Statutory Authority</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">University Grants Commission</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Prescribed Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Adequate facilities for Women.</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Available Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Yes</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Statutory Authority</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">University Grants Commission</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Prescribed Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Appropriate facilities for differently abled students.</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Available Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Yes</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Statutory Authority</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">University Grants Commission</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Prescribed Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Maintenance budget for the physical facilities through existing / mobilized resources.</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Available Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Yes</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Statutory Authority</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">University Grants Commission</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Prescribed Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Utilization of the funds allocated for maintenance.</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Available Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Yes</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Statutory Authority</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">University Grants Commission</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Prescribed Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Adequate systems for maintaining and utilizing library and information facilities.</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Available Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Yes</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Statutory Authority</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">University Grants Commission</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Prescribed Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Adequate systems for minting computer and network facilities</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Available Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Yes</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Statutory Authority</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">University Grants Commission</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Prescribed Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Number of exclusive technical titles of books in the library</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Available Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">747 Nos.</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Statutory Authority</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">University Grants Commission</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Prescribed Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Number of journals / periodicals / magazines</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Available Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">6 Nos</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Statutory Authority</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">University Grants Commission</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Prescribed Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Effective and user-friendly library operations</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Available Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Yes</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Statutory Authority</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">University Grants Commission</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Prescribed Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Reading room facility faculty and students</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Available Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Yes</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Statutory Authority</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">University Grants Commission</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Prescribed Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Landscape ( approach roads/ gardens and general ambience )
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Available Infrastructure</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Yes</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>

        <div id="sectione" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle">Section E- Information on Status of Approval of Courses.</h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="48%"><strong>Name of the course offered at the time of Notification	</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">PG Masters Programme in Geoinformatics</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Name of the courses offered</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">M.Sc. ( Geoinformatics )</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Whether approval of relevant statutory council /authority obtained for newly started coursed after becoming Deemed University</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Yes</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="48%"><strong>Name of the course offered at the time of Notification	</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">PG Masters Programme in Geoinformatics</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Name of the courses offered</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Certificate course in Photogrammetry and Remote Sensing</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Whether approval of relevant statutory council /authority obtained for newly started coursed after becoming Deemed University</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Yes</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>

        <div id="sectionf" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle">Section F- Information on courses, Intake capacity, Fee etc.</h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Name of the course</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">M.Sc. Geoinformatics</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Intake Capacity</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">60</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Fees</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Rs.1.65 Lakhs per annum for open merit seats</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Name of the course</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Certificate Course in Photogrammetry and Remote Sensing</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Intake Capacity</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">40</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Fees</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">	Rs. 35,000/-</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>

        <div id="sectiong" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle">Section G- Information on students examined and passed</h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <a href="<?php echo base_url();?>assets/img/section-g.jpg" target="_blank"><img src="<?php echo base_url();?>assets/img/ugc-g.jpg" border="0" style="width: 20%;"></a>
                </div>
            </div>

        <div id="sectionh" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle">Section H- Information on Meeting of Bodies of University.</h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Meeting</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Academic Council</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Meeting No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">-</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Date of Meeting</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">7/4/2011, 23/07/2011, 8/10/2011, 9/6/2012, 22/9/2012</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Meeting</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Finance Meeting</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Meeting No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">-</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Date of Meeting</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">12/2/2011, 13/8/2011</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Meeting</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">Board of Management</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Meeting No.</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">-</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Date of Meeting</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">5/1/2011, 26/2/2011, 23/4/2011, 27/8/2011, 12/11/2011</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>

        <div id="sectioni" class="tab-pane fade">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle">Section I- Information on Inspection by Statutory Authorities.</h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <div class="well">
                    <div class="col-sm-12">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="30%"><strong>Name of the Authority</strong></td>
                                <td align="left" valign="top" width="4%">:</td>
                                <td align="left" valign="top">NAAC</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Date of Inspection</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">11th Nov. 2008</td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"><strong>Copy of the Report</strong></td>
                                <td align="left" valign="top">:</td>
                                <td align="left" valign="top">Available at the institute</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>

</div>
</div>