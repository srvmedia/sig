<style type="text/css">
    .newstable th{
        padding: 15px;
        background-color: #c42027;
        color: #fff;
        font-size: 18px;
        border-right: solid 1px #fff;
    }
    .newstable td a{
        color: #a10007;
    }
    .newstable td a:hover{
        color: #dd0000;
    }

    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }
    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    li.active a{
        display: inline;

    }
    li.active a{
        display: inline;
    }
    /* .nav>li>a {
       padding: 3px 15px;
       margin-bottom: 12px;
     }*/
</style>
<div class="row aboutop">
    <div class="bannerimg"></div>
</div>
<div class="container">
    <div class="col-md-3 menutop">
        <ul class="nav nav-pills nav-stacked rightpad">
            <li class="active"><a class="liborder" data-toggle="tab" href="#disclaimer">Disclaimer</a></li>
        </ul>
    </div>
    <div class="tab-content col-md-9 menutop">
        <div id="disclaimer" class="tab-pane fade in active">
            <table class="contentpaneopen">
                <tbody><tr>
                    <h2 class="menutitle"><strong>Disclaimer</strong></h2>
                </tr>
                </tbody></table>
            <hr>
            <div class="inner-information">
                <table class="contentpaneopen">
                    <tbody><tr>
                        <td valign="top">
                            Your use of the Symbiosis Institute of Geoinformatics Website is subject to the terms and conditions on this page and the pages linked to it:
                            <ul class="bullet-style">
                                <li>The information placed on this Website by Symbiosis Institute of Geoinformatics("University") are meant for providing basic information about the University, its constituents, its officials, the heads of the Institutions, its departments and about the services offered by the University.</li>
                                <li>The University does not make any warranty that information contained on this Website, or on any site linked to this Website, is complete, accurate or up-to-date; and the University is not responsible for the results of reliance on any such information. Information provided by Symbiosis Institute of Geoinformaticsand / or its constituents and /or its departments in printed form shall be considered to final.</li>
                                <li>Unless otherwise indicated or stated, this Website and its contents are the property of Symbiosis Institute of Geoinformatics. All the Intellectual Property contained in the material available on the Website including copyrights, trademarks, service marks, image rights on this Website belongs to the University.</li>
                                <li>The University does not make any warranty that use of the Website will be uninterrupted, virus-free or error-free; or that use of the Website will not affect other software or operating systems used to access the Website. Any warranties provided under the general law are expressly excluded to the fullest extent possible</li>
                                <li>The University accepts no liability for any loss or damage which may be suffered by other parties as a direct or indirect result of using this Website (including loss of profit, loss of opportunity, loss of business, and consequential loss).</li>
                                <li>To the extent permitted in law, the University accepts no responsibility or liability for any material / content communicated / posted by third parties via the Website in as much as the University does not exercise editorial control over any content posted to the Website by third party users. The University is entitled to in its sole discretion to remove any content which in view of the University is objectionable. The University is also entitled to edit or delete any comments posted by users which it deems defamatory, unlawful, threatening or otherwise objectionable.</li>
                                <li>These terms and conditions shall be governed by and construed in accordance with the laws of India. You agree to submit to the exclusive jurisdiction of the Pune Courts [Maharashtra, India]. You are responsible for compliance with any applicable laws of the country / state from which you are accessing this website.</li>
                            </ul>
                        </td>
                    </tr>
                    </tbody></table>
                <span class="article_separator">&nbsp;</span>
            </div>
        </div>
        </div>
    </div>

