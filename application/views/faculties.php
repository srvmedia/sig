<style type="text/css">
    p {
        text-align: justify;
    }
    td a{
        color: #0261a5;
    }
    .rightpad{
        border-right: 2px solid #f19999;
    }
    .rightpad li.active{
        background: url("<?php echo base_url(); ?>assets/img/liback.png") no-repeat;
        background-position: center right;
        padding: 10px 0px 10px;
    }
    li.active a{
        display: inline;
    }
    .nav>li>a {
        padding: 3px 15px;
        margin-bottom: 12px;
    }
    .publications{
        background-color:#BB0C1A;
        border-radius: 25px;
        padding: 1px 13px 2px 13px !important;
        color:#fff;
    }
</style>
<div class="aboutop">
    <div class="facultiesbanner"></div>
</div>	
<div class="container">  
    <div class="col-md-3 menutop">
        <ul class="nav nav-pills nav-stacked rightpad">
            <li class="active"><a class="liborder" data-toggle="tab" href="#faculties">Faculties</a></li>
            <li><a class="liborder" data-toggle="tab" href="#publications">Publications</a></li>
            <li><a class="liborder" data-toggle="tab" href="#research">Research</a></li>
        </ul>
    </div>
    <div class="tab-content col-md-9 menutop">
        <div id="faculties" class="tab-pane fade in active">
            <div class="information-middle">
                <div class="inner-information">
                    <h2 class="menutitle"><strong>Faculties</strong></h2>
                    <hr> 
                    <p style="margin-bottom:30px">Symbiosis colleges in Pune have been associated with a tradition to provide the best in terms of academic training coupled with industry interface. But the promise of delivering academic excellence can't happen without the support of good faculties. In line with this tradition, SIG like other Symbiosis institutes has always sourced out the finest of faculties to aid in the learning of its students.</p>
                    <div class="col-md-6 faculty_section">
                        <div class="imajholder text-center" style="float: left;">
                            <img src="<?php echo base_url(); ?>assets/img/faculty/navendu.jpg" class="img-circle">
                            <p class="text-center">Dr. Navendu Chaudhary</p>
                        </div>
                        <div class="infholder">
                            <p><strong>Designation :</strong> Associate Professor</p>
                            <p><strong>Qualification :</strong> M.Sc., M.S., Ph.D</p>
                            <p><strong>Total Experience : </strong> <span class="inyears">15 years </span></p>
                            <p><a href="<?php echo base_url();?>faculties/navendu_chaudhary" class="publications btn btn-sm btn-default">View More >></a> </p>
                        </div>
                    </div>
                    <div class="col-md-6 faculty_section">
                        <div class="imajholder text-center" style="float: left;"> <img src="<?php echo base_url(); ?>assets/img/faculty/sandipan_das.jpg" class="img-circle">
                            <p class="text-center">Dr.Sandipan Das</p>
                        </div>
                        <div class="infholder">
                            <p><strong>Designation : </strong> Assistant Professor</p>
                            <p><strong>Qualification : </strong> Ph.D,UGC NET,M.Sc. Geoinformatics</p>
                            <p><strong>Total Experience : </strong> <span class="inyears">8.6 years </span></p>
                            <p><a href="<?php echo base_url();?>faculties/sandipan_das" class="publications btn btn-sm btn-default">View More >></a></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                    <div class="clearfix"></div>
                    <div class="col-md-6 faculty_section">
                        <div class="imajholder text-center" style="float: left;"> <img src="<?php echo base_url(); ?>assets/img/faculty/ms_niyatee_deshmukh.jpg" class="img-circle">
                            <p class="text-center">Niyatee Deshmukh</p>
                        </div>
                        <div class="infholder">
                            <p><strong>Designation :</strong> Teaching Assistant</p>
                            <p><strong>Qualification :</strong> M.Sc. Geoinformatics</p>
                            <p><strong>Total Experience : </strong> <span class="inyears">4 years </span></p>
                        </div>
                    </div>
                    <div class="col-md-6 faculty_section">
                        <div class="imajholder text-center" style="float: left;"> <img src="<?php echo base_url(); ?>assets/img/faculty/mrs_ashwini_mohgaonkar.jpg" class="img-circle">
                            <p class="text-center">Mrs. Ashwini Mohgaonkar</p>
                        </div>
                        <div class="infholder">
                            <p><strong>Designation : </strong> Associate Professor</p>
                            <p><strong>Qualification : </strong> MCM, Diploma in Counselling and Psychology</p>
                            <p><strong>Total Experience : </strong> <span class="inyears">28 years </span></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                    <div class="clearfix"></div>				  
                    <div class="col-md-6 faculty_section">
                        <div class="imajholder text-center" style="float: left;"> <img src="<?php echo base_url(); ?>assets/img/faculty/bk-pradhan.jpg" class="img-circle">
                            <p class="text-center">Lt. Col. BK Pradhan</p>
                        </div>
                        <div class="infholder">
                            <p><strong>Designation :</strong> Adjunct Faculty</p>
                            <p><strong>Qualification :</strong> B.E. Electronics</p>
                            <p><strong>Total Experience : </strong> <span class="inyears">36 years </span></p>
                            <p><a href="<?php echo base_url();?>faculties/lt_col_bk_pradhan" class="publications btn btn-sm btn-default">View More >></a></p>
                        </div>
                    </div>
                    <div class="col-md-6 faculty_section">
                        <div class="imajholder text-center" style="float: left;"> <img src="<?php echo base_url(); ?>assets/img/faculty/tp-singh.jpg" class="img-circle">
                            <p class="text-center">Dr. T. P. Singh</p>
                        </div>
                        <div class="infholder">
                            <p><strong>Designation : </strong> Professor</p>
                            <p><strong>Qualification : </strong> Ph.D. M. Phil. M.S. Toulouse, FRANCE</p>
                            <p><strong>Total Experience : </strong> <span class="inyears"> 17 years</span></p>
                             <p><a href="<?php echo base_url();?>faculties/dr_tp_singh" class="publications btn btn-sm btn-default">View More >></a></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <script language="javascript" type="text/javascript">
                        var last_tab = 'all';
                        function show(layerName) {
                            document.getElementById(layerName).style.display = 'block';
                        }
                        function hide(layerName) {
                            document.getElementById(layerName).style.display = 'none';
                        }
                        function show_next(tab_name) {
                            document.getElementById(last_tab).className = 'tab';
                            var curr = document.getElementById(tab_name);
                            curr.className = 'tab activeTab';
                            hide(last_tab + '_data');
                            show(tab_name + '_data');
                            last_tab = tab_name;
                        }
                    </script>
                    <style>
                        .tabArea ul li { display:inline-block; margin:0 5px; }
                        .tabMain { display:inline-block; width:100%; }
                        .tabMain .imajholder {  width:168px; height:158px; float:left;  margin: 0 10px 0 0;}
                        .tabMain .infholder a.read_from {float:right; padding:5px 10px;}
                        .title-name, .position , .specilization, .inyears {margin-left:5px;}
                        .tabArea ul {
                            margin: 15px 0;
                        }
                        .saperator {
                            clear:inherit;
                            margin: 10px 0;
                            overflow: hidden;
                        }
                        .faculty_section img
                        {
                            border: 1px solid rgba(128, 128, 128, 0.32);
                            width: 130px;
                            padding: 3px;
                            margin-bottom: 10px;
                            ;
                        }
                        .infholder
                        {
                            margin-top: 10px;
                        }
                        .infholder p{font-size: 14px;text-align:left;}
                        .imajholder{width: 160px;margin-right: 16px}
                    </style>
                </div>
                <div class="inner-information">
                    <h2 class="menutitle"><strong>Research Fellow</strong></h2>
                    <hr />
                    <div class="col-md-6 faculty_section">
                        <div class="imajholder text-center" style="float: left;"> <img src="<?php echo base_url(); ?>assets/img/faculty/mrs_pritanka_s_sandbhor.jpg" alt="Pritanka Sandbhor"  class="img-circle">
                            <p class="text-center"><strong>Pritanka Sandbhor</strong></p>
                        </div>
                        <div class="infholder">
                            <p><strong>Phd Topic :</strong> <br />Urban Landscape Dynamics and its effects on Socio-ecological Changes around Pune Metropolitan Region using Geospatial Techniques.</p>                     
                        </div>
                    </div>
                    <div class="col-md-6 faculty_section">
                        <div class="imajholder text-center" style="float: left;"> <a href="/faculties-a-research/faculties/lt-col-bk-pradhan.html"> <img src="<?php echo base_url(); ?>assets/img/faculty/ketki_mehta.jpg" alt="Ketki Metha" class="img-circle"></a>
                            <p class="text-center"><strong>Ketki Metha</strong></p>
                        </div>
                        <div class="infholder">
                            <p><strong>Phd Topic : </strong> <br />Spatial Heterogeneity and Its effects on Ecosystem Dynamics and Sensitivity in Western Ghats of Maharashtra using Geospatial Techniques</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                    <div class="col-md-6 faculty_section">
                        <div class="imajholder text-center" style="float: left;"> <a href="/faculties-a-research/faculties/lt-col-bk-pradhan.html"> <img src="<?php echo base_url(); ?>assets/img/faculty/yuvraj_adagale.jpg" alt="Yuvraj Adagale" class="img-circle"></a>
                            <p class="text-center"><strong>Yuvraj Adagale</strong></p>
                        </div>
                        <div class="infholder">
                            <p><strong>Project Topic : </strong> <br />Vegetation carbon dynamic in Maharashtra</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div id="publications" class="tab-pane fade in">
            <div class="inner-information">

                <table class="contentpaneopen">
                    <tbody><tr>
                            <td class="contentheading" width="100%">
                                Publications 			</td>


                        </tr>
                    </tbody></table>

                <table class="contentpaneopen">
                    <hr>
                    <tbody><tr>
                            <td valign="top">
                                At SIG, we believe that learning process is a constant endeavor. So, in spite of teaching student, the faculties are also encouraged to pursue their academic interests in terms of further research in the geoinformatics domain. In fact, the SIG faculties have to their credit several publishing’s in some of the most reputed journals of the globe.
                                <br>
                                <br>

                                <h2>Recent Publications by Faculty and Students</h2>
                                <ul class="bullet-style">
                                    <li>Dr. T.P. Singh, <strong>Climate change and Geoinformation</strong>, May 2011, TERI Press New Delhi – National</li>
                                    <li>Dr. T.P. Singh, <strong>Geoinformation education in private institutes in India, issues and challenge, “International workshop on Multination Geomatics capacity Building achievement and challenge”</strong> , 7-8 April 2011, ISPRS WG 6/4 ISRS – National, Dehradun</li>
                                    <li>Dr. Navendu Chaudhury, Varun Anand, Dipto Sarkar (Batch 2010-12), <strong>Caddie, a Android based application that provides golfers with distance and direction to the targeted hole from their current position</strong>, 26th February 2011, Geomatrix '11, IIT Bombay  - National</li>
                                    <li>Bhavneet Verya, Debashree Dutta, Satarupa Chatterjee,Tapaleena Bhattacharjee (Batch 2010-12), Dr.Navendu Chaudhary,  <strong>The Sinking Beauty "Sunderbans</strong>, Geospatial World Forum 2011, 18th to 21st January 2011, Hyderabad – National</li>
                                    <li>Dr. T.P. Singh, <strong>Assessing relationship between biomass and biodiversity in western ghat region. International seminar on biodiversity and climate change, Indian Institute of Technology</strong>, Kharagpur, India, December 21-23, 2010, IIT Kharagpur, National</li>
                                    <li>Dr. T.P. Singh authored a book on <b>‘ Geoinformatics for Natural Resource Management’</b> which was released by Nova Science Publications in NewYork, USA in August 2009  during an International Seminar on <b>‘ Climate Change’</b>.</li>
                                    <li>Dr T. P. Singh <b>‘s paper on Fuzzy</b> Logic was published in November 2009 in GIS Development Journal. </li>
                                    <li>A paper by Dr T. P. Singh on <b>Oil Spill Detection</b> has been published in March 2010 issue of the Spirnger Journal of Remote Sensing. </li>
                                    <li>Dr Navendu Chaudhary authored a chapter in the book ‘Multi-scale Image Segmentation and Object Oriented Processing for Land Cover Classification,  <b>GI Science and Remote Sensing</b>, Vol 45, Pages 377 to 391, authors Frohn, Robert C and Navendu.</li>
                                </ul>

                            </td>
                        </tr>

                    </tbody></table>
                <span class="article_separator">&nbsp;</span>


            </div>

        </div>

        <div id="research" class="tab-pane fade">
            <div class="inner-information">

                <table class="contentpaneopen">
                    <tbody><tr>
                            <td class="contentheading" width="100%">
                                Research			</td>


                        </tr>
                    </tbody></table>
                <hr>
                <table class="contentpaneopen">
                    <tbody><tr>
                            <td valign="top">
                                Research is an integral part of academic training at Symbiosis colleges in Pune and SIG is also a firm believer in this tradition. Encouraging its students to pursue research in varied domains dealing with different specializations of geoinformatics, research is a mandatory inclusion in SIG course structure.   
                                <ul class="bullet-style">
                                    <li> Vegetation Carbon Sequestration and Biomass Assessment of Western and Central Maharashtra </li>
                                    <li> National Communication of Land use Land use cover change for climate change Assessment of Maharashtra </li>
                                    <li> National Communication of Land use Land use cover change for climate change Assessment of Rajasthan</li>
                                    <li> National Communication of Land use Land use cover change for climate change Assessment of Gujarat</li>
                                    <li> Assessment of Irrigation Potential under Accelerated Irrigation Benefit Program in Krishna River Basin</li>
                                    <li> Assessment of Irrigation Potential under Accelerated Irrigation Benefit Program in Surya River Basin</li>
                                    <li> Assessment of Irrigation Potential under Accelerated Irrigation Benefit Program in Madha NandurBar River basin</li>
                                    <li> Assessment of Irrigation Potential under Accelerated Irrigation Benefit Program in Amravati river basin </li>
                                    <li> Assessment of Irrigation Potential under Accelerated Irrigation Benefit Program in Jalgaon rivers</li>
                                </ul>
                                <h2>Upcoming Project</h2>
                                <ul class="bullet-style">
                                    <li>Vegetation Carbon Sequestration and Biomass Assessment of Western and Central Maharashtra  -   II phase </li>
                                    <li>II phase AIBP consists all river basin of Maharashtra ( Surya, Amravati, Jalgaon, Kirishna and nadurbar). </li>
                                </ul>
                                <h2>Last Two Year Project</h2>
                                <ul class="bullet-style">
                                    <li>Vegetation Carbon sequestration, Biomass assessment project</li>
                                    <li>National communication for land use land use change for climate change (for all states, Maharashtra, Gujarat, Rajasthan)</li>
                                    <li>Assessment of Irrigation potential, Sarda Shayak project (Uttar Pradesh)</li>
                                    <li> Assessment of Irrigation potential, II phase ( river basin of Maharashtra)</li>
                                </ul>

                            </td>
                        </tr>

                    </tbody></table>
                <span class="article_separator">&nbsp;</span>


            </div>
        </div>

    </div>
</div>
