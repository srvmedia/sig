<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$query = $this->db->get('news');
		$queryevents = $this->db->get('events');
		$data["all_news"] = $query->result_array();
		$data["all_events"] = $queryevents->result_array();
		$this->load->view('header');
		$this->load->view('index',$data);
		$this->load->view('footer');
	}
	public function aboutsig()
	{
		$this->load->view('header');
		$this->load->view('aboutus');
		$this->load->view('footer');
	}
	public function academics()
	{
		$this->load->view('header');
		$this->load->view('academics');
		$this->load->view('footer');
	}
	public function admission()
	{
		$this->load->view('header');
		$this->load->view('admission');
		$this->load->view('footer');
	}
	public function faculties()
	{
		$this->load->view('header');
		$this->load->view('faculties');
		$this->load->view('footer');
	}
	public function contact()
	{
		$this->load->view('header');
		$this->load->view('contact');
		$this->load->view('footer');
	}
	public function project()
	{
		$this->load->view('header');
		$this->load->view('project');
		$this->load->view('footer');
	}
	public function placement()
	{
		$this->load->view('header');
		$this->load->view('placement');
		$this->load->view('footer');
	}
	public function alumni()
	{
		$this->load->view('header');
		$this->load->view('alumni');
		$this->load->view('footer');
	}
	public function gallery()
	{
		$this->load->view('header');
		$this->load->view('gallery_new');
		$this->load->view('footer');
	}
	public function khulamaunchjuly2015()
	{
		$this->load->view('header');
		$this->load->view('khula-maunch-july-2015');
		$this->load->view('footer');
	}
	public function UAV()
	{
		$this->load->view('header');
		$this->load->view('uav');
		$this->load->view('footer');
	}
	public function educationleadershipaward()
	{
		$this->load->view('header');
		$this->load->view('education-leadership-award');
		$this->load->view('footer');
	}
	public function batch201315skit()
	{
		$this->load->view('header');
		$this->load->view('batch-2013-15-skit');
		$this->load->view('footer');
	}
	public function qmtijune2012()
	{
		$this->load->view('header');
		$this->load->view('qmti-june-2012');
		$this->load->view('footer');
	}
	public function dahihandicelebration()
	{
		$this->load->view('header');
		$this->load->view('dahi-handi-celebration');
		$this->load->view('footer');
	}
	public function punedarshan()
	{
		$this->load->view('header');
		$this->load->view('pune-darshan');
		$this->load->view('footer');
	}
	public function conference()
	{
		$this->load->view('header');
		$this->load->view('conference');
		$this->load->view('footer');
	}
	public function guestvisit()
	{
		$this->load->view('header');
		$this->load->view('guest-visit');
		$this->load->view('footer');
	}
	public function lifeatsig()
	{
		$this->load->view('header');
		$this->load->view('life-at-sig');
		$this->load->view('footer');
	}
	public function MScinGeoinformatics()
	{
		$this->load->view('header');
		$this->load->view('MSc-in-Geoinformatics');
		$this->load->view('footer');
	}
	public function MTechGeoinformatics()
	{
		$this->load->view('header');
		$this->load->view('MTech-Geoinformatics');
		$this->load->view('footer');
	}
	public function courseinSpatial()
	{
		$this->load->view('header');
		$this->load->view('course-in-Spatial');
		$this->load->view('footer');
	}
	public function certificatecourse()
	{
		$this->load->view('header');
		$this->load->view('certificate-course');
		$this->load->view('footer');
	}
	public function environment()
    {
		$this->load->view('header');
		$this->load->view('environment');
		$this->load->view('footer');
	}
	public function collaborations()
    {
		$this->load->view('header');
		$this->load->view('collaborations');
		$this->load->view('footer');
	}
	public function testimonials()
    {
		$this->load->view('header');
		$this->load->view('testimonials');
		$this->load->view('footer');
	}
	public function careers()
    {
		$this->load->view('header');
		$this->load->view('careers');
		$this->load->view('footer');
	}
	public function faqs()
    {
		$this->load->view('header');
		$this->load->view('faq');
		$this->load->view('footer');
	}
	public function scientificpapers()
    {
		$this->load->view('header');
		$this->load->view('scientificpapers');
		$this->load->view('footer');
	}
	public function infrastructure()
    {
		$this->load->view('header');
		$this->load->view('infrastructure');
		$this->load->view('footer');
	}
	public function achievements()
    {
		$this->load->view('header');
		$this->load->view('achievements');
		$this->load->view('footer');
	}
	public function orientationandpedagogy()
    {
		$this->load->view('header');
		$this->load->view('orientationandpedagogy');
		$this->load->view('footer');
	}

	public function hostelandamenities()
    {
		$this->load->view('header');
		$this->load->view('hostelandamenities');
		$this->load->view('footer');
	}
	
	public function conferences()
    {
		$this->load->view('header');
		$this->load->view('conferences');
		$this->load->view('footer');
	}

    public function sigcorner()
    {
		$query = $this->db->get('news');
		$data["all_news"] = $query->result_array();
		$this->load->view('header');
		$this->load->view('sig_corner',$data);
		$this->load->view('footer');
	}

    public function newsdetail($newsid = null)
    {
		
		$this->db->where('id',$newsid);
		$this->db->order_by("date","desc");
		$query = $this->db->get('news');
		$data["news"] = $query->result_array()[0];
		$this->load->view('header');
		$this->load->view('newsdetail',$data);
		$this->load->view('footer');
	}

	public function eventsdetail($eventsid = null)
	{
		$this->db->where('id',$eventsid);
		$this->db->order_by("date","desc");
		$query = $this->db->get('events');
		$data["news"] = $query->result_array()[0];
		$this->load->view('header');
		$this->load->view('newsdetail',$data);
		$this->load->view('footer');
	}

	public function disclaimer(){
		$this->load->view('header');
		$this->load->view('disclaimer');
		$this->load->view('footer');
	}

	public function newsevents($id=null){
		$query = $this->db->get('news');
		$queryevents = $this->db->get('events');
		$data["all_news"] = $query->result_array();
		$data["all_events"] = $queryevents->result_array();
		if(isset($id) && $id!=null){
			$data["newsid"] = $id;
		}
		$this->load->view('header');
		$this->load->view('newsevents' ,$data);
		$this->load->view('footer');
	}

	public function informationasperugcnorms(){
		$this->load->view('header');
		$this->load->view('information-as-per-ugc-norms');
		$this->load->view('footer');
	}

	public function admissionopenforbatch201719(){
		$this->load->view('header');
		$this->load->view('admission-open-for-batch-2017-19');
		$this->load->view('footer');
	}

	public function contactusform(){
		$captcha=$this->input->post('captchresp1');
		if(isset($captcha) && !empty($captcha)) {
			//your site secret key
			$secret = '6LfAbQoUAAAAAALAaMwg4kXHhX9Ba2yzKqQn1miU';
			//get verify response data
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $captcha);
			$responseData = json_decode($verifyResponse);
			if ($responseData->success) {
				$data['name'] = $this->input->post('name1');
				$data['email'] = $this->input->post('email1');
				$data['message'] = $this->input->post('message1');
				$this->send_mmail($data);
			}else{
				$response=0;
				echo json_encode($response);
//				echo "Robot verification failed, please try again.";
			}
		}else{
			$response=1;
			echo json_encode($response);
//			echo "Please click on the reCAPTCHA box";
		}

	}

	public function send_mmail($data) {
		$this->load->model('Contactus');
		$this->load->library('My_phpmailer');

		$mail = new PHPMailer;
		$mail->Mailer = "mail";
		$mail->IsSMTP();                                      // Set mailer to use SMTP , Comment for server and uncomment for local server
		//$mail->SMTPDebug  = 2;
		$mail->Host = 'smtp.sendgrid.net';
		// Specify main and backup server
		$mail->Port = 587;                                    // Set the SMTP port
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'srvmedia';                // SMTP username
		$mail->Password = 'srv@1248';

		/* $host = "smtp.mandrillapp.com"; // SMTP host
         */

		$mail->From = $data['email'];
		$mail->FromName = $data['name'];

		$mail->AddAddress('support@srvmedia.com', 'SrvMedia');  // Add a recipient
		$mail->IsHTML(true);                                  // Set email format to HTML

		$mail->Subject = 'SRV Media - ContactUs';
		$mail->Body = 'name :' . $data['name'] . '<br/>Message :' . $data['message'] . '<br/>Email :' . $data['email'].'<br/>';
		//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
		// var_dump($mail);
		if ($mail->Send()) {
			$this->Contactus->form_insert($data);
			$response = 2;
			echo json_encode($response);
		} else {
			$response = array('status' => false);
			$response = 3;
			echo json_encode($response);
		}
	}

	public function enquiryform(){
		$this->load->library('My_phpmailer');
		$captcha=$this->input->post('captchresp1');
		if(isset($captcha) && !empty($captcha)) {
//			your site secret key
			$secret = '6LfAbQoUAAAAAALAaMwg4kXHhX9Ba2yzKqQn1miU';
			//get verify response data
			$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $captcha);
			$responseData = json_decode($verifyResponse);
			if ($responseData->success) {

				$data['name'] = $this->input->post('name1');
				$data['email'] = $this->input->post('email1');
				$data['message'] = $this->input->post('message1');
				$data['mobile'] = $this->input->post('mobile1');
				$data['program'] = $this->input->post('program1');

				$mail = new PHPMailer;
				$mail->Mailer = "mail";
				$mail->IsSMTP();                                      // Set mailer to use SMTP , Comment for server and uncomment for local server
				//$mail->SMTPDebug  = 2;
				$mail->Host = 'smtp.sendgrid.net';
				// Specify main and backup server
				$mail->Port = 587;                                    // Set the SMTP port
				$mail->SMTPAuth = true;                               // Enable SMTP authentication
				$mail->Username = 'srvmedia';                // SMTP username
				$mail->Password = 'srv@1248';

				/* $host = "smtp.mandrillapp.com"; // SMTP host
				 */

				$mail->From = $data['email'];
				$mail->FromName = $data['name'];

				$mail->AddAddress('support@srvmedia.com', 'SrvMedia');  // Add a recipient
				$mail->IsHTML(true);                                  // Set email format to HTML

				$mail->Subject = 'SRV Media - ContactUs';
				$mail->Body = 'name : ' . $data['name'] . '<br/>Email : ' . $data['email'].'<br/>
								Mobile Number : '.$data['mobile'].' <br/> Program : '.$data['program'].'<br/>Message : ' . $data['message'] ;
				//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
				// var_dump($mail);
				if ($mail->Send()) {
					$response = 2;
					echo json_encode($response);
				} else {
					$response = array('status' => false);
					$response = 3;
					echo json_encode($response);
				}
			}else{
				$response=0;
				echo json_encode($response);
//				echo "Robot verification failed, please try again.";
			}
		}else{
			$response=1;
			echo json_encode($response);
//			echo "Please click on the reCAPTCHA box";
		}
	}
}
