<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faculties extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function navendu_chaudhary()
	{
		$this->load->view('header');
		$this->load->view('faculties/navendu_chaudhary');
		$this->load->view('footer');
	}
        
        public function sandipan_das()
	{
		$this->load->view('header');
		$this->load->view('faculties/sandipan_das');
		$this->load->view('footer');
	}
        
         public function lt_col_bk_pradhan()
	{
		$this->load->view('header');
		$this->load->view('faculties/lt_col_bk_pradhan');
		$this->load->view('footer');
	}

        public function dr_tp_singh()
	{
		$this->load->view('header');
		$this->load->view('faculties/dr_tp_singh');
		$this->load->view('footer');
	}


}
